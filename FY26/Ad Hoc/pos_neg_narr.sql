WITH account_arr_types AS (
    SELECT 
        a.dim_crm_account_id,
        a.crm_account_name,
        a.carr_this_account,
        CASE 
            WHEN o.net_arr > 0 THEN 'Positive'
            WHEN o.net_arr < 0 THEN 'Negative'
            ELSE 'Zero'
        END AS arr_type,
        COUNT(*) AS opportunity_count,
        LISTAGG(o.dim_crm_opportunity_id, ', ') WITHIN GROUP (ORDER BY o.dim_crm_opportunity_id) AS opportunity_ids
    FROM 
        PROD.RESTRICTED_SAFE_WORKSPACE_SALES.MART_CRM_OPPORTUNITY o
    JOIN 
        PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT a
        ON o.dim_crm_account_id = a.dim_crm_account_id
    WHERE 
        o.net_arr IS NOT NULL
        AND lower(a.account_owner) like '%smb%'
        AND a.carr_this_account > 0
        AND o.close_date >= '2025-02-01'
        AND o.is_closed = TRUE
    GROUP BY 
        a.dim_crm_account_id, 
        a.crm_account_name,
        a.carr_this_account,
        arr_type
)

SELECT 
    dim_crm_account_id,
    crm_account_name,
    carr_this_account,
    SUM(CASE WHEN arr_type = 'Positive' THEN opportunity_count ELSE 0 END) AS positive_arr_opps,
    MAX(CASE WHEN arr_type = 'Positive' THEN opportunity_ids ELSE NULL END) AS positive_opportunity_ids,
    SUM(CASE WHEN arr_type = 'Negative' THEN opportunity_count ELSE 0 END) AS negative_arr_opps,
    MAX(CASE WHEN arr_type = 'Negative' THEN opportunity_ids ELSE NULL END) AS negative_opportunity_ids
FROM 
    account_arr_types
GROUP BY 
    dim_crm_account_id, 
    crm_account_name,
    carr_this_account
HAVING 
    SUM(CASE WHEN arr_type = 'Positive' THEN opportunity_count ELSE 0 END) > 0
    AND 
    SUM(CASE WHEN arr_type = 'Negative' THEN opportunity_count ELSE 0 END) > 0
ORDER BY 
    crm_account_name;