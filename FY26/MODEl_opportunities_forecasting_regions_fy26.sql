-- This query consolidates MODEL_Opportunities_Forecasting and MODEL_Opportunities_Forecasting_gds_only

WITH
dim_crm_user AS (
  SELECT *
  FROM prod.common.dim_crm_user
),

dim_date AS (
  SELECT *
  FROM prod.common.dim_date
),

dim_order AS (
  SELECT *
  FROM prod.common.dim_order
),

dim_order_action AS (
  SELECT *
  FROM prod.common.dim_order_action
),

dim_product_detail AS (
  SELECT *
  FROM prod.common.dim_product_detail
),

dim_subscription AS (
  SELECT *
  FROM prod.common.dim_subscription
),

mart_crm_task AS (
  SELECT *
  FROM prod.common_mart_sales.mart_crm_task
),

mart_arr AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_arr
),

mart_charge AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_charge
),

mart_crm_account AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_account
),

mart_crm_opportunity AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_opportunity
),

dim_crm_account_daily_snapshot AS (
  SELECT *
  FROM prod.restricted_safe_common.dim_crm_account_daily_snapshot
),

mart_delta_arr_subscription_month AS (
  SELECT *
  FROM prod.restricted_safe_legacy.mart_delta_arr_subscription_month
),

sfdc_case AS (
  SELECT *
  FROM prod.workspace_sales.sfdc_case
),

opportunity_data AS (
  SELECT
    mart_crm_opportunity.* EXCLUDE(report_segment),
    case when report_segment = 'SMB' or opportunity_owner like '%SMB%' or opportunity_owner_role like '%SMB%' then 'SMB'
    else report_segment end as report_segment,
    user.user_role_type,
    dim_date.fiscal_year                                                              AS date_range_year,
    dim_date.fiscal_quarter_name_fy                                                   AS date_range_quarter,
    DATE_TRUNC(MONTH, dim_date.date_actual)                                           AS date_range_month,
    dim_date.first_day_of_week                                                        AS date_range_week,
    dim_date.date_id                                                                  AS date_range_id,
    dim_date.fiscal_month_name_fy,
    dim_date.fiscal_quarter_name_fy,
    dim_date.fiscal_year,
    dim_date.first_day_of_fiscal_quarter,
    CASE
      WHEN product_category LIKE '%Self%' OR product_details LIKE '%Self%' OR product_category LIKE '%Starter%'
        OR product_details LIKE '%Starter%' THEN 'Self-Managed'
      WHEN product_category LIKE '%SaaS%' OR product_details LIKE '%SaaS%' OR product_category LIKE '%Bronze%'
        OR product_details LIKE '%Bronze%' OR product_category LIKE '%Silver%' OR product_details LIKE '%Silver%'
        OR product_category LIKE '%Gold%' OR product_details LIKE '%Gold%' THEN 'SaaS'
      WHEN product_details NOT LIKE '%SaaS%'
        AND (product_details LIKE '%Premium%' OR product_details LIKE '%Ultimate%') THEN 'Self-Managed'
      WHEN product_category LIKE '%Storage%' OR product_details LIKE '%Storage%' THEN 'Storage'
      ELSE 'Other'
    END                                                                               AS delivery,
    CASE
      WHEN order_type LIKE '3%' OR order_type LIKE '2%' THEN 'Growth'
      WHEN order_type LIKE '1%' THEN 'First Order'
      WHEN order_type LIKE '4%' OR order_type LIKE '5%' OR order_type LIKE '6%' THEN 'Churn / Contraction'
    END                                                                               AS order_type_clean,
    COALESCE (order_type LIKE '5%' AND net_arr = 0, FALSE)                            AS partial_churn_0_narr_flag,
    CASE
      WHEN product_category LIKE '%Premium%' OR product_details LIKE '%Premium%' THEN 'Premium'
      WHEN product_category LIKE '%Ultimate%' OR product_details LIKE '%Ultimate%' THEN 'Ultimate'
      WHEN product_category LIKE '%Bronze%' OR product_details LIKE '%Bronze%' THEN 'Bronze'
      WHEN product_category LIKE '%Starter%' OR product_details LIKE '%Starter%' THEN 'Starter'
      WHEN product_category LIKE '%Storage%' OR product_details LIKE '%Storage%' THEN 'Storage'
      WHEN product_category LIKE '%Silver%' OR product_details LIKE '%Silver%' THEN 'Silver'
      WHEN product_category LIKE '%Gold%' OR product_details LIKE '%Gold%' THEN 'Gold'
      WHEN product_category LIKE 'CI%' OR product_details LIKE 'CI%' THEN 'CI'
      WHEN product_category LIKE '%omput%' OR product_details LIKE '%omput%' THEN 'CI'
      WHEN product_category LIKE '%Duo%' OR product_details LIKE '%Duo%' THEN 'Duo Pro'
      WHEN product_category LIKE '%uggestion%' OR product_details LIKE '%uggestion%' THEN 'Duo Pro'
      WHEN product_category LIKE '%Agile%' OR product_details LIKE '%Agile%' THEN 'Enterprise Agile Planning'
      ELSE product_category
    END                                                                               AS product_tier,
    COALESCE (LOWER(product_details) LIKE ANY ('%code suggestions%', '%duo%'), FALSE) AS duo_flag,
    COALESCE (opportunity_name LIKE '%QSR%', FALSE)                                   AS qsr_flag,
    CASE
      WHEN order_type LIKE '7%' AND qsr_flag = FALSE THEN 'PS/CI/CD'
      WHEN order_type LIKE '1%' AND net_arr > 0 THEN 'First Order'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr > 0 AND subscription_type != 'Renewal'
        AND qsr_flag = FALSE THEN 'Growth - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr > 0 AND subscription_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr = 0 AND subscription_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr < 0 AND subscription_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Contraction'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr > 0 AND subscription_type = 'Renewal'
        THEN 'Renewal - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr < 0 AND subscription_type != 'Renewal'
        THEN 'Non-Renewal - Contraction'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr = 0 AND subscription_type != 'Renewal'
        THEN 'Non-Renewal - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr = 0 AND subscription_type = 'Renewal'
        THEN 'Renewal - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr < 0 AND subscription_type = 'Renewal'
        THEN 'Renewal - Contraction'
      WHEN order_type LIKE ANY ('5.%', '6.%') THEN 'Churn'
      ELSE 'Other'
    END                                                                               AS trx_type,
    COALESCE (opportunity_name LIKE '%Startups Program%', FALSE)                      AS startup_program_flag

  FROM mart_crm_opportunity
  LEFT JOIN dim_date
    ON mart_crm_opportunity.close_date = dim_date.date_actual
  LEFT JOIN dim_crm_user AS user
    ON mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

  WHERE ((mart_crm_opportunity.is_edu_oss = 1 AND net_arr > 0) OR mart_crm_opportunity.is_edu_oss = 0)
    AND mart_crm_opportunity.is_jihu_account = FALSE
    AND stage_name NOT LIKE '%Duplicate%'
    --and (opportunity_category is null or opportunity_category not like 'Decom%')
    AND partial_churn_0_narr_flag = FALSE
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and subscription_type = 'Renewal') or is_closed = False)

),

amer_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('AMER SMB Sales')
    OR owner_role = 'Advocate_SMB_AMER'
),

emea_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('EMEA SMB Sales')
    OR owner_role = 'Advocate_SMB_EMEA'
),

apj_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('APJ SMB Sales','APAC SMB Sales')
),


account_base AS (
  SELECT
    acct.*,
    CASE
      WHEN  amer_accounts.dim_crm_account_id IS NOT NULL THEN 'AMER'
      WHEN emea_accounts.dim_crm_account_id IS NOT NULL THEN 'EMEA'
            WHEN apj_accounts.dim_crm_account_id IS NOT NULL THEN 'APJ'
      ELSE 'Other'
    END                                                              AS team,
    COALESCE (
     (snapshot_date >= '2025-02-10' AND  (emea_accounts.dim_crm_account_id IS NOT NULL OR amer_accounts.dim_crm_account_id IS NOT NULL OR apj_accounts.dim_crm_account_id IS NOT NULL))
    OR
    (snapshot_date < '2025-02-10'
    and is_jihu_account = 0
    and (parent_crm_account_region <> 'LATAM' OR parent_crm_account_region IS NULL)
    and parent_crm_account_upa_country NOT IN ('JP','RU','CN','UA','BY','HK','MO')
    and (pubsec_type NOT IN ('US-PubSec','ROW-PubSec') OR pubsec_type IS NULL)
    and record_type_id = '01261000000it9oAAA'
    -- and carr_account_family > 0
    and (((parent_crm_account_region NOT IN ('APAC','APJ') AND parent_crm_account_geo NOT IN ('APAC','APJ')) OR parent_crm_account_region IS NULL) OR (parent_crm_account_max_family_employee <= 100 or parent_crm_account_max_family_employee IS NULL))
    and 
    (
    ((parent_crm_account_max_family_employee < 250 OR parent_crm_account_max_family_employee IS NULL) and carr_account_family < 30000)
    OR
    (parent_crm_account_max_family_employee >= 250 AND parent_crm_account_max_family_employee < 500
    AND carr_account_family < 10000 AND parent_crm_account_lam_dev_count < 50 AND customer_since_date < '2024-02-01')
    )

    )
    
    , FALSE)                                                        AS gds_account_flag
  FROM dim_crm_account_daily_snapshot AS acct
  ----- amer and apac accounts
  LEFT JOIN amer_accounts
    ON acct.dim_crm_account_id = amer_accounts.dim_crm_account_id
  ----- emea emea accounts
  LEFT JOIN emea_accounts
    ON acct.dim_crm_account_id = emea_accounts.dim_crm_account_id
      LEFT JOIN apj_accounts
    ON acct.dim_crm_account_id = apj_accounts.dim_crm_account_id
  -------filtering to get current account data
  WHERE acct.carr_this_account > 0
    AND acct.snapshot_date >= '2019-12-01'
-- and(((acct.snapshot_date < '2024-02-01' or acct.snapshot_date >= '2024-03-01')
-- and  acct.snapshot_date = date_trunc('month',acct.snapshot_date))
-- or (date_trunc('month',acct.snapshot_date) = '2024-02-01' and acct.snapshot_date = current_date))
--and acct.dim_crm_account_id = '0014M00001ldLdiQAE'
)

--SELECT * from account_base;
,

opp AS (
  SELECT *
  FROM mart_crm_opportunity
  WHERE ((mart_crm_opportunity.is_edu_oss = 1 AND net_arr > 0) OR mart_crm_opportunity.is_edu_oss = 0)
    AND mart_crm_opportunity.is_jihu_account = FALSE
    AND stage_name NOT LIKE '%Duplicate%'
    AND (opportunity_category IS NULL OR opportunity_category NOT LIKE 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and subscription_type = 'Renewal') or is_closed = False)
)
,

last_carr_data AS (
  SELECT
    snapshot_date,
    dim_crm_account_id,
    MAX(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END)
      OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS last_carr_date,
    MIN(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END) OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC)                                               AS first_carr_date
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date >= '2019-12-01'
    AND crm_account_type != 'Prospect'
  -- and crm_account_name like 'Natural Substances%'
  QUALIFY last_carr_date IS NOT NULL
)
--SELECT * FROM last_carr_data;
,
full_base_data AS (
  SELECT
    opportunity_data.*,
    case when opportunity_data.opportunity_owner_role LIKE '%AMER%' then 'AMER'
   when opportunity_data.opportunity_owner_role LIKE ANY ('%EMEA%') then 'EMEA'
   when opportunity_data.opportunity_owner_role LIKE ANY ('%APJ%','%APAC%') then 'APJ'
   else
    case when account_base.team LIKE '%AMER%' then 'AMER'
    when account_base.team LIKE ANY ('%EMEA%') then 'EMEA'
     when account_base.team LIKE ANY ('%APJ%','%APAC%') then 'APJ'
    else opportunity_data.report_region
    END
    END as team,
    account_base.crm_account_owner,
    account_base.owner_role,
    account_base.gds_account_flag,
    COALESCE (report_segment = 'SMB'
    OR

    (opportunity_data.close_date < '2025-02-01'
    and opportunity_data.is_jihu_account = 0
    and (opportunity_data.parent_crm_account_region <> 'LATAM' OR opportunity_data.parent_crm_account_region IS NULL)
    and opportunity_data.parent_crm_account_upa_country NOT IN ('JP','RU','CN','UA','BY','HK','MO')
    and (account_base.pubsec_type NOT IN ('US-PubSec','ROW-PubSec') OR account_base.pubsec_type IS NULL)
    -- and carr_account_family > 0
    and (((opportunity_data.parent_crm_account_region NOT IN ('APAC','APJ') AND opportunity_data.parent_crm_account_geo NOT IN ('APAC','APJ')) OR opportunity_data.parent_crm_account_region IS NULL) OR (opportunity_data.parent_crm_account_max_family_employee <= 100 or opportunity_data.parent_crm_account_max_family_employee IS NULL))
    and 
    (
    ((opportunity_data.parent_crm_account_max_family_employee < 250 OR opportunity_data.parent_crm_account_max_family_employee IS NULL) and account_base.carr_account_family < 30000)
    OR
    (opportunity_data.parent_crm_account_max_family_employee >= 250 AND opportunity_data.parent_crm_account_max_family_employee < 500
    AND account_base.carr_account_family < 10000 AND opportunity_data.parent_crm_account_lam_dev_count < 50 AND account_base.customer_since_date < '2024-02-01')
    )

    )
    
    
     , FALSE)                                                                                                        AS gds_oppty_flag,
    gs_health_user_engagement,
    gs_health_cd,
    gs_health_devsecops,
    gs_health_ci,
    gs_health_scm,
    carr_account_family,
    carr_this_account,
    pte_score,
    ptc_score,
    ptc_predicted_arr                                                                                                AS ptc_predicted_arr__c,
    ptc_predicted_renewal_risk_category                                                                              AS ptc_predicted_renewal_risk_category__c,
    -- upgrades.prior_product,
    -- upgrades.upgrade_product,
    dim_subscription.turn_on_auto_renewal,
    last_carr_date,
    first_carr_date,
    account_base.snapshot_date                                                                                       AS account_snapshot_date,
    last_carr_data.snapshot_date                                                                                     AS last_carr_snapshot_date,
    arr_basis                                                                                                        AS atr,
    CASE
      WHEN trx_type = 'First Order' OR order_type_current LIKE '%First Order%' THEN 'First Order'
      WHEN trx_type IN ('Renewal - Uplift', 'Renewal - Flat') THEN 'Renewal Growth'
      WHEN qsr_flag OR trx_type = 'Growth - Uplift' THEN 'Nonrenewal Growth'
      WHEN trx_type IN ('Churn', 'Renewal - Contraction', 'Non-Renewal - Contraction') THEN 'C&C'
      ELSE 'Other'
    END                                                                                                              AS trx_type_grouping,
    opportunity_data.close_date                                                                                      AS test_date
  FROM opportunity_data
  LEFT JOIN last_carr_data
    ON opportunity_data.dim_crm_account_id = last_carr_data.dim_crm_account_id
      AND ((
        order_type NOT LIKE '%First Order%'
        AND is_closed
        AND
        last_carr_data.snapshot_date = opportunity_data.close_date - 1
      )
      OR
      (
        order_type LIKE '%First Order%'
        AND is_closed
        AND last_carr_data.snapshot_date = last_carr_data.first_carr_date
      )
      OR
      (is_closed = FALSE AND last_carr_data.snapshot_date = CURRENT_DATE)
      )
  LEFT JOIN account_base
    ON opportunity_data.dim_crm_account_id = account_base.dim_crm_account_id
      AND
      (
        (
          is_closed
          AND order_type LIKE '%First Order%'
          AND account_base.snapshot_date = first_carr_date
          AND last_carr_data.dim_crm_account_id = account_base.dim_crm_account_id
        )
        OR
        (
          is_closed
          AND order_type NOT LIKE '%First Order%'
          AND account_base.snapshot_date = last_carr_date
          AND last_carr_data.dim_crm_account_id = account_base.dim_crm_account_id
        )
        OR
        (
          is_closed = FALSE
          AND account_base.snapshot_date = CURRENT_DATE
        )
      )
  LEFT JOIN (select
distinct
dim_crm_opportunity_id_current_open_renewal, 
first_value(turn_on_auto_renewal) over(partition by dim_crm_opportunity_id_current_open_renewal order by turn_on_auto_renewal asc) as TURN_ON_AUTO_RENEWAL
from dim_subscription
where dim_crm_opportunity_id_current_open_renewal is not null) dim_subscription
    ON opportunity_data.dim_crm_opportunity_id = dim_subscription.dim_crm_opportunity_id_current_open_renewal
)

--select * from full_base_data;
,
renewal_forecast_base_data AS (
  SELECT
    team,
    close_month,
    SUM(atr)                     AS total_atr,
    SUM(net_arr)                 AS total_net_arr,
    SUM(CASE
      WHEN trx_type_grouping = 'Renewal Growth' AND is_won THEN net_arr
      ELSE 0
    END) / NULLIFZERO(total_atr) AS renewal_growth_rate,
    SUM(CASE
      WHEN trx_type_grouping = 'C&C' THEN net_arr
      ELSE 0
    END) / NULLIFZERO(total_atr) AS c_and_c_rate,
    SUM(CASE
      WHEN ptc_predicted_arr__c IS NOT NULL AND is_closed = FALSE THEN ptc_predicted_arr__c
      ELSE 0
    END)
    - SUM(CASE
      WHEN ptc_predicted_arr__c IS NOT NULL AND is_closed = FALSE THEN atr
      ELSE 0
    END)                         AS ai_atr_net_forecast,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Churn (Actionable)'
        AND is_closed
        AND trx_type != 'Churn'
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_churn_narr_best_case,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Churn (Actionable)'
        AND is_closed
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_churn_narr_commit,

    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Contract (Actionable)'
        AND is_closed
        AND trx_type != 'Churn'
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_contract_narr_best_case,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Contract (Actionable)'
        AND is_closed
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_contract_narr_commit,

    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Renew'
        AND is_closed
        AND trx_type != 'Churn'
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_renew_narr_best_case,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Renew'
        AND is_closed
        THEN net_arr
      ELSE 0
    END)                         AS predicted_will_renew_narr_commit,


    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Churn (Actionable)'
        AND is_closed
        THEN atr
      ELSE 0
    END)                         AS predicted_will_churn_atr,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Contract (Actionable)'
        AND is_closed
        THEN atr
      ELSE 0
    END)                         AS predicted_will_contract_atr,
    SUM(CASE
      WHEN ptc_predicted_renewal_risk_category__c = 'Will Renew'
        AND is_closed
        THEN atr
      ELSE 0
    END)                         AS predicted_will_renew_atr
  FROM full_base_data
  WHERE close_date >= DATEADD('month', -12, DATE_TRUNC('month', CURRENT_DATE))
    AND ((is_closed AND close_month < DATE_TRUNC('month', CURRENT_DATE)))
    AND subscription_type = 'Renewal'
    AND gds_oppty_flag
  GROUP BY ALL
)

--select * from renewal_forecast_base_data;
,
renewal_forecast_rates AS (
  SELECT
    *,
    PERCENTILE_CONT(.8) WITHIN GROUP (ORDER BY renewal_growth_rate ASC)
      OVER (PARTITION BY team) AS renewal_growth_rate_best_case,
    PERCENTILE_CONT(.4) WITHIN GROUP (ORDER BY renewal_growth_rate ASC)
      OVER (PARTITION BY team) AS renewal_growth_rate_commit,
    PERCENTILE_CONT(.6) WITHIN GROUP (ORDER BY renewal_growth_rate ASC)
      OVER (PARTITION BY team) AS renewal_growth_rate_most_likely,
    PERCENTILE_CONT(.8) WITHIN GROUP (ORDER BY c_and_c_rate ASC)
      OVER (PARTITION BY team) AS c_and_c_rate_best_case,
    PERCENTILE_CONT(.4) WITHIN GROUP (ORDER BY c_and_c_rate ASC)
      OVER (PARTITION BY team) AS c_and_c_rate_commit,
    PERCENTILE_CONT(.6) WITHIN GROUP (ORDER BY c_and_c_rate ASC)
      OVER (PARTITION BY team) AS c_and_c_rate_most_likely
      --,
  -- sum(predicted_will_churn_narr_best_case)  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_churn_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_churn_best_case,

  --  sum(predicted_will_contract_narr_best_case)  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_contract_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_contract_best_case,

  --  sum(predicted_will_renew_narr_best_case)  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_renew_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_renew_best_case,

  --  sum(predicted_will_churn_narr_commit )  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_churn_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_churn_commit,

  --  sum(predicted_will_contract_narr_commit)  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_contract_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_contract_commit,

  --  sum(predicted_will_renew_narr_commit)  over (partition by team,calculated_tier)
  --  /
  --  sum(predicted_will_renew_atr) over (partition by team,calculated_tier) as predicted_atr_rate_will_renew_commit
  FROM renewal_forecast_base_data
)

--select * from renewal_forecast_rates;
,

limited_renewal_forecast_rates AS (
  SELECT DISTINCT
    team,
    renewal_growth_rate_best_case,
    renewal_growth_rate_most_likely,
    renewal_growth_rate_commit,
    c_and_c_rate_best_case,
    c_and_c_rate_most_likely,
    c_and_c_rate_commit--,
  -- predicted_atr_rate_will_churn_best_case,
  -- predicted_atr_rate_will_churn_commit,
  -- predicted_atr_rate_will_contract_best_case,
  -- predicted_atr_rate_will_contract_commit,
  -- predicted_atr_rate_will_renew_best_case,
  -- predicted_atr_rate_will_renew_commit
  FROM renewal_forecast_rates
),

renewal_arr_predictions AS (
  SELECT
    full_base_data.*,
    CASE
      WHEN is_closed = FALSE THEN atr
      ELSE 0
    END                                                                     AS open_atr,
    limited_renewal_forecast_rates.* EXCLUDE (team),
    --Best Case
    -- case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then
    --   atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then predicted_atr_rate_will_churn_best_case
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then predicted_atr_rate_will_contract_best_case
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then predicted_atr_rate_will_renew_best_case
    --               else 0 end)
    --   else atr * (renewal_growth_rate_best_case + c_and_c_rate_best_case) end as predicted_renewal_arr_best_case,
    open_atr * (renewal_growth_rate_best_case + c_and_c_rate_best_case)     AS predicted_renewal_arr_best_case_no_ai,
    --Commit
    -- case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then
    --   atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then (predicted_atr_rate_will_churn_best_case + predicted_atr_rate_will_churn_commit)/2
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then (predicted_atr_rate_will_contract_best_case + predicted_atr_rate_will_contract_commit)/2
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then (predicted_atr_rate_will_renew_best_case + predicted_atr_rate_will_renew_commit)/2
    --               else 0 end)
    --   else atr * (renewal_growth_rate_most_likely + c_and_c_rate_most_likely) end as predicted_renewal_arr_most_likely,
    open_atr * (renewal_growth_rate_most_likely + c_and_c_rate_most_likely) AS predicted_renewal_arr_most_likely_no_ai,
    --Most Likely
    -- case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then
    --   atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then predicted_atr_rate_will_churn_commit
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then predicted_atr_rate_will_contract_commit
    --               when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then predicted_atr_rate_will_renew_commit
    --               else 0 end)
    --   else atr * (renewal_growth_rate_commit + c_and_c_rate_commit) end as predicted_renewal_arr_commit,
    open_atr * (renewal_growth_rate_commit + c_and_c_rate_commit)           AS predicted_renewal_arr_commit_no_ai,
    open_atr * c_and_c_rate_best_case                                       AS predicted_c_and_c_arr_best_case,
    open_atr * c_and_c_rate_commit                                          AS predicted_c_and_c_arr_commit,
    open_atr * c_and_c_rate_most_likely                                     AS predicted_c_and_c_arr_most_likely
  FROM full_base_data
  LEFT JOIN limited_renewal_forecast_rates
    ON full_base_data.team = limited_renewal_forecast_rates.team
  --and month(full_base_data.close_month = renewal_forecast_rates.close_month
  WHERE subscription_type = 'Renewal'
    AND gds_oppty_flag
    AND full_base_data.subscription_type = 'Renewal'
    --and full_base_data.is_closed = false
    AND full_base_data.fiscal_year = 2026
)

--select * from renewal_arr_predictions;
,
renewal_forecast_output AS (
  SELECT
    'Renewal'                                    AS forecast_type,
    team,
    close_month,
    SUM(CASE
      WHEN is_closed THEN net_arr
      ELSE 0
    END)                                         AS total_actual_narr,
    SUM(CASE
      WHEN is_closed AND net_arr < 0 THEN net_arr
      ELSE 0
    END)                                         AS total_actual_c_and_c_narr,
    SUM(CASE
      WHEN is_closed AND net_arr > 0 THEN net_arr
      ELSE 0
    END)                                         AS total_actual_renewal_growth_narr,
    SUM(CASE
      WHEN is_closed THEN atr
      ELSE 0
    END)                                         AS total_closed_atr,
    SUM(atr)                                     AS total_atr,
    SUM(CASE
      WHEN is_closed = FALSE THEN atr
      ELSE 0
    END)                                         AS total_open_atr,
    SUM(CASE
      WHEN ptc_predicted_arr__c IS NOT NULL AND is_closed = FALSE THEN ptc_predicted_arr__c
      ELSE 0
    END)
    - SUM(CASE
      WHEN ptc_predicted_arr__c IS NOT NULL AND is_closed = FALSE THEN atr
      ELSE 0
    END)                                         AS ai_atr_net_forecast,
    -- sum(predicted_renewal_arr_best_case) as predicted_renewal_arr_best_case,
    -- sum(predicted_renewal_arr_commit) as predicted_renewal_arr_commit,
    -- sum(predicted_renewal_arr_most_likely) as predicted_renewal_arr_most_likely,
    SUM(predicted_renewal_arr_best_case_no_ai)   AS predicted_renewal_arr_best_case_no_ai,
    SUM(predicted_renewal_arr_commit_no_ai)      AS predicted_renewal_arr_commit_no_ai,
    SUM(predicted_renewal_arr_most_likely_no_ai) AS predicted_renewal_arr_most_likely_no_ai,
    SUM(predicted_c_and_c_arr_best_case)         AS predicted_c_and_c_arr_best_case,
    SUM(predicted_c_and_c_arr_commit)            AS predicted_c_and_c_arr_commit,
    SUM(predicted_c_and_c_arr_most_likely)       AS predicted_c_and_c_arr_most_likely
  FROM renewal_arr_predictions
  WHERE fiscal_year = 2026
  GROUP BY ALL
)
--SELECT * from renewal_forecast_output;
,

fo_forecast_base_data AS (
  SELECT
    team,
    DATEADD('year', 1, close_month)                  AS fy26_close_month,
    COUNT(DISTINCT dim_crm_opportunity_id)           AS fo_count,
    SUM(net_arr)                                     AS total_net_arr,
    COUNT(
      DISTINCT
      CASE
        WHEN lower(opportunity_name) NOT LIKE '%promo%'
          THEN
            dim_crm_opportunity_id
      END
    )                                                AS fo_count_without_promos,
    SUM(
      CASE
        WHEN lower(opportunity_name) NOT LIKE '%promo%'
          THEN
            net_arr
        ELSE 0
      END
    )                                                AS net_arr_without_promos,
    total_net_arr / fo_count                         AS total_actual_asp,
    net_arr_without_promos / fo_count_without_promos AS actual_asp_no_promos
  FROM full_base_data
  WHERE close_date >= DATEADD('month', -12, DATE_TRUNC('month', CURRENT_DATE))
    AND ((is_closed AND close_month < DATE_TRUNC('month', CURRENT_DATE)))
    AND trx_type = 'First Order'
    AND gds_oppty_flag
    AND is_won
  GROUP BY ALL
)

--SELECT * FROM fo_forecast_base_data;
,

date_spine AS (
  (
    SELECT
      date_actual,
      'EMEA' AS team
    FROM dim_date
    WHERE date_actual = DATE_TRUNC('month', date_actual)
      AND fiscal_year = 2026
  )
  UNION ALL
  (
    SELECT
      date_actual,
      'AMER' AS team
    FROM dim_date
    WHERE date_actual = DATE_TRUNC('month', date_actual)
      AND fiscal_year = 2026
  )
    UNION ALL
  (
    SELECT
      date_actual,
      'APJ' AS team
    FROM dim_date
    WHERE date_actual = DATE_TRUNC('month', date_actual)
      AND fiscal_year = 2026
  )
)
--SELECT * FROM date_spine;
,

fo_forecast_rates AS (
  SELECT DISTINCT
    team,
    PERCENTILE_CONT(.8) WITHIN GROUP (ORDER BY actual_asp_no_promos ASC) OVER (PARTITION BY team) AS asp_best_case,
    PERCENTILE_CONT(.4) WITHIN GROUP (ORDER BY actual_asp_no_promos ASC) OVER (PARTITION BY team) AS asp_commit,
    PERCENTILE_CONT(.6) WITHIN GROUP (ORDER BY actual_asp_no_promos ASC)
      OVER (PARTITION BY team)                                                                    AS asp_most_likely,
    PERCENTILE_CONT(.8) WITHIN GROUP (ORDER BY fo_count_without_promos ASC)
      OVER (PARTITION BY team)                                                                    AS fo_count_best_case,
    PERCENTILE_CONT(.4) WITHIN GROUP (ORDER BY fo_count_without_promos ASC)
      OVER (PARTITION BY team)                                                                    AS fo_count_commit,
    PERCENTILE_CONT(.6) WITHIN GROUP (ORDER BY fo_count_without_promos ASC)
      OVER (PARTITION BY team)                                                                    AS fo_count_most_likely
  FROM fo_forecast_base_data
)
--SELECT * FROM fo_forecast_rates;
,
fo_forecast_output AS (
  SELECT
    'First Order'                          AS forecast_type,
    date_spine.team,
    date_spine.date_actual                 AS close_month,
    fo_forecast_rates.* EXCLUDE (team),
    asp_best_case * fo_count_best_case     AS predicted_fo_arr_best_case,
    asp_commit * fo_count_commit           AS predicted_fo_arr_commit,
    asp_most_likely * fo_count_most_likely AS predicted_fo_arr_most_likely
  FROM date_spine
  LEFT JOIN fo_forecast_rates
    ON date_spine.team = fo_forecast_rates.team
)

--SELECT * FROM fo_forecast_output;
,

-- Cosider unifying with ultimate CTE
ultimate_accounts AS (
  SELECT DISTINCT dim_parent_crm_account_id
  FROM mart_arr
  WHERE arr_month = DATE_TRUNC('month', CURRENT_DATE)
    AND product_tier_name LIKE '%Ultimate%'
),

get_monthly_carr AS (
  SELECT
    DATEADD('year', 1, snapshot_date) AS fy26_target_month,
    CASE
      WHEN amer_accounts.dim_crm_account_id IS NOT NULL THEN 'AMER'
      WHEN emea_accounts.dim_crm_account_id IS NOT NULL THEN 'EMEA'
          WHEN apj_accounts.dim_crm_account_id IS NOT NULL THEN 'APJ'
      ELSE 'Other'
    END                               AS team,
    COALESCE (
     (snapshot_date >= '2025-02-10' AND  (emea_accounts.dim_crm_account_id IS NOT NULL OR amer_accounts.dim_crm_account_id IS NOT NULL OR apj_accounts.dim_crm_account_id IS NOT NULL))
    OR
    (snapshot_date < '2025-02-10'
    and is_jihu_account = 0
    and (parent_crm_account_region <> 'LATAM' OR parent_crm_account_region IS NULL)
    and parent_crm_account_upa_country NOT IN ('JP','RU','CN','UA','BY','HK','MO')
    and (pubsec_type NOT IN ('US-PubSec','ROW-PubSec') OR pubsec_type IS NULL)
    and record_type_id = '01261000000it9oAAA'
    -- and carr_account_family > 0
    and (((parent_crm_account_region NOT IN ('APAC','APJ') AND parent_crm_account_geo NOT IN ('APAC','APJ')) OR parent_crm_account_region IS NULL) OR (parent_crm_account_max_family_employee <= 100 or parent_crm_account_max_family_employee IS NULL))
    and 
    (
    ((parent_crm_account_max_family_employee < 250 OR parent_crm_account_max_family_employee IS NULL) and carr_account_family < 30000)
    OR
    (parent_crm_account_max_family_employee >= 250 AND parent_crm_account_max_family_employee < 500
    AND carr_account_family < 10000 AND parent_crm_account_lam_dev_count < 50 AND customer_since_date < '2024-02-01')
    )

    )
    
    , FALSE)                                                        AS gds_account_flag,
    SUM(carr_this_account)            AS total_monthly_carr
  FROM dim_crm_account_daily_snapshot AS acct
  LEFT JOIN amer_accounts
    ON acct.dim_crm_account_id = amer_accounts.dim_crm_account_id
  LEFT JOIN emea_accounts
    ON acct.dim_crm_account_id = emea_accounts.dim_crm_account_id
     LEFT JOIN apj_accounts
    ON acct.dim_crm_account_id = apj_accounts.dim_crm_account_id
  LEFT JOIN ultimate_accounts
    ON acct.dim_parent_crm_account_id = ultimate_accounts.dim_parent_crm_account_id
  WHERE snapshot_date >= DATEADD('month', -12, DATE_TRUNC('month', CURRENT_DATE))
    AND snapshot_date < DATE_TRUNC('month', CURRENT_DATE)
    AND snapshot_date = DATE_TRUNC('month', snapshot_date)
    AND gds_account_flag
  GROUP BY ALL
)
--select * from get_monthly_carr;
,

nonrenewal_growth_base_data AS (
  SELECT
    team,
    DATEADD('year', 1, close_month) AS fy26_close_month,
    SUM(net_arr)                    AS total_net_arr
  FROM full_base_data
  WHERE close_date >= DATEADD('month', -12, DATE_TRUNC('month', CURRENT_DATE))
    AND is_closed
    AND close_month < DATE_TRUNC('month', CURRENT_DATE)
    AND trx_type_grouping = 'Nonrenewal Growth'
    AND gds_oppty_flag
    AND is_won
  GROUP BY ALL
)
--select * from nonrenewal_growth_base_data;
,

nonrenewal_growth_rates AS (
  SELECT
    nonrenewal_growth_base_data.*,
    get_monthly_carr.total_monthly_carr,
    total_net_arr / get_monthly_carr.total_monthly_carr AS nonrenewal_growth_rate
  FROM nonrenewal_growth_base_data
  LEFT JOIN get_monthly_carr
    ON nonrenewal_growth_base_data.team = get_monthly_carr.team
      AND nonrenewal_growth_base_data.fy26_close_month = get_monthly_carr.fy26_target_month
)
,
nonrenewal_growth_calcs AS (
  SELECT
    *,
    PERCENTILE_CONT(.8) WITHIN GROUP (ORDER BY nonrenewal_growth_rate ASC)
      OVER (PARTITION BY team) AS nonrenewal_growth_best_case,
    PERCENTILE_CONT(.4) WITHIN GROUP (ORDER BY nonrenewal_growth_rate ASC)
      OVER (PARTITION BY team) AS nonrenewal_growth_commit,
    PERCENTILE_CONT(.6) WITHIN GROUP (ORDER BY nonrenewal_growth_rate ASC)
      OVER (PARTITION BY team) AS nonrenewal_growth_most_likely
  FROM nonrenewal_growth_rates
)
,
nonrenewal_growth_output AS (
  SELECT DISTINCT
    nonrenewal_growth_calcs.* EXCLUDE (fy26_close_month, total_net_arr, total_monthly_carr, nonrenewal_growth_rate),
    carr.total_monthly_carr AS carr
  FROM nonrenewal_growth_calcs
  LEFT JOIN
    (
      SELECT
        team,
        total_monthly_carr
      FROM get_monthly_carr
      WHERE fy26_target_month = DATEADD('month', 11, DATE_TRUNC('month', CURRENT_DATE))
    ) AS carr
    ON nonrenewal_growth_calcs.team = carr.team
)
,
combined_output AS (
  SELECT *

  FROM (
    SELECT
      forecast_type,
      renewal_forecast_output.team,
      close_month,
      ai_atr_net_forecast,
      -- predicted_renewal_arr_best_case,
      -- predicted_renewal_arr_commit,
      -- predicted_renewal_arr_most_likely,
      predicted_renewal_arr_best_case_no_ai,
      predicted_renewal_arr_commit_no_ai,
      predicted_renewal_arr_most_likely_no_ai,
      predicted_c_and_c_arr_best_case,
      predicted_c_and_c_arr_commit,
      predicted_c_and_c_arr_most_likely,
      0    AS asp_best_case,
      0    AS asp_commit,
      0    AS asp_most_likely,
      0    AS fo_count_best_case,
      0    AS fo_count_commit,
      0    AS fo_count_most_likely,
      0    AS predicted_fo_arr_best_case,
      0    AS predicted_fo_arr_commit,
      0    AS predicted_fo_arr_most_likely,
      nonrenewal_growth_best_case,
      nonrenewal_growth_commit,
      nonrenewal_growth_most_likely,
      carr AS current_carr
    FROM renewal_forecast_output
    LEFT JOIN nonrenewal_growth_output
      ON renewal_forecast_output.team = nonrenewal_growth_output.team
  )
  UNION ALL
  (
    SELECT
      forecast_type,
      team,
      close_month,
      0 AS ai_atr_net_forecast,
      0 AS predicted_renewal_arr_best_case,
      0 AS predicted_renewal_arr_commit,
      0 AS predicted_renewal_arr_most_likely,
      -- 0 as predicted_renewal_arr_best_case_no_ai,
      -- 0 as predicted_renewal_arr_commit_no_ai,
      -- 0 as predicted_renewal_arr_most_likely_no_ai,
      0 AS predicted_c_and_c_arr_best_case,
      0 AS predicted_c_and_c_arr_commit,
      0 AS predicted_c_and_c_arr_most_likely,
      asp_best_case,
      asp_commit,
      asp_most_likely,
      fo_count_best_case,
      fo_count_commit,
      fo_count_most_likely,
      predicted_fo_arr_best_case,
      predicted_fo_arr_commit,
      predicted_fo_arr_most_likely,
      0 AS nonrenewal_growth_best_case,
      0 AS nonrenewal_growth_commit,
      0 AS nonrenewal_growth_most_likely,
      0 AS current_carr
    FROM fo_forecast_output
  )
),

actual_data AS (
  SELECT
    close_month,
    CASE
      WHEN team IS NULL OR team = 'Other' THEN 'AMER'
      ELSE team
    END                                    AS team,
    CASE
      WHEN trx_type_grouping IN ('Renewal Growth', 'C&C', 'Nonrenewal Growth') THEN 'Renewal'
      WHEN trx_type_grouping = 'First Order' THEN 'First Order'
      ELSE 'Renewal'
    END                                    AS forecast_type,
    SUM(net_arr)                           AS total_net_arr,
    SUM(CASE
      WHEN is_won OR (is_closed AND subscription_type = 'Renewal') THEN net_arr
      ELSE 0
    END)                                   AS closed_net_arr,
    COUNT(DISTINCT dim_crm_opportunity_id) AS total_trx,
    COUNT(DISTINCT CASE
      WHEN is_won THEN dim_crm_opportunity_id
    END)                                   AS won_trx,
    SUM(CASE
      WHEN subscription_type = 'Renewal' THEN atr
      ELSE 0
    END)                                   AS total_atr,
    SUM(CASE
      WHEN is_closed AND subscription_type = 'Renewal' THEN atr
      ELSE 0
    END)                                   AS closed_total_atr,
    SUM(CASE
      WHEN is_closed AND subscription_type = 'Renewal' THEN won_arr_basis_for_clari
      ELSE 0
    END)                                   AS closed_total_won_atr,
    SUM(CASE
      WHEN is_won AND trx_type_grouping = 'Nonrenewal Growth' THEN net_arr
      ELSE 0
    END)                                   AS closed_nonrenewal_arr,
    SUM(CASE
      WHEN is_closed AND trx_type_grouping = 'C&C' AND net_arr < 0 THEN net_arr
      ELSE 0
    END)                                   AS closed_c_and_c_arr,
    SUM(CASE
      WHEN is_won AND forecast_type = 'First Order' THEN net_arr
      ELSE 0
    END)                                   AS closed_fo_arr,
    SUM(CASE
      WHEN is_closed AND trx_type_grouping = 'Renewal Growth' AND net_arr > 0 THEN net_arr
      ELSE 0
    END)                                   AS closed_renewal_arr,
    SUM(CASE
      WHEN (is_won OR (is_closed AND subscription_type = 'Renewal')) AND trx_type_grouping != 'First Order'
        AND PARENT_CRM_ACCOUNT_SALES_SEGMENT != 'SMB' THEN net_arr
      ELSE 0
    END)                                   AS non_gds_account_arr
  FROM full_base_data
  WHERE --forecast_type is not null
    --and
    gds_oppty_flag
    AND fiscal_year = 2026
  GROUP BY ALL
)
,
add_actual_data AS (
  SELECT
    combined_output.* EXCLUDE (predicted_fo_arr_best_case, predicted_fo_arr_commit, predicted_fo_arr_most_likely
    ),
    ZEROIFNULL(actual_data.total_net_arr)                                                                  AS total_net_arr_actual,
    ZEROIFNULL(actual_data.total_trx)                                                                      AS total_trx_actual,
    ZEROIFNULL(actual_data.total_atr)                                                                      AS total_atr_actual,
    ZEROIFNULL(actual_data.closed_total_atr)                                                               AS closed_total_atr_actual,
       ZEROIFNULL(actual_data.closed_total_won_atr)                                                               AS closed_total_won_atr_actual,
    ZEROIFNULL(actual_data.closed_net_arr)                                                                 AS closed_net_arr_actual,
    ZEROIFNULL(actual_data.won_trx)                                                                        AS won_trx_actual,
    ZEROIFNULL(actual_data.closed_c_and_c_arr)                                                             AS closed_c_and_c_arr_actual,
    ZEROIFNULL(actual_data.closed_nonrenewal_arr)                                                          AS closed_nonrenewal_arr_actual,
    ZEROIFNULL(actual_data.closed_fo_arr)                                                                  AS closed_fo_arr_actual,
    ZEROIFNULL(actual_data.closed_renewal_arr)                                                             AS closed_renewal_arr_actual,
    ZEROIFNULL(actual_data.non_gds_account_arr)                                                            AS closed_non_gds_account_arr_actual,
    (DATEDIFF(
      'day', GREATEST(CURRENT_DATE, combined_output.close_month),
      DATEADD('month', 1, combined_output.close_month)
    ) - 1)
    / (DATEDIFF('day', combined_output.close_month, DATEADD('month', 1, combined_output.close_month)) - 1)
      AS perc_month_remaining_mid,
    CASE
      WHEN perc_month_remaining_mid < 0 THEN 0
      ELSE perc_month_remaining_mid
    END                                                                                                    AS perc_month_remaining,
    predicted_fo_arr_best_case * perc_month_remaining                                                      AS predicted_fo_arr_best_case,
    predicted_fo_arr_most_likely * perc_month_remaining                                                    AS predicted_fo_arr_most_likely,
    predicted_fo_arr_commit * perc_month_remaining                                                         AS predicted_fo_arr_commit,
    (ZEROIFNULL(
      LAG(predicted_renewal_arr_best_case_no_ai + closed_net_arr_actual, 1)
        OVER (
          PARTITION BY combined_output.forecast_type, combined_output.team
          ORDER BY combined_output.close_month ASC
        )
    ) + current_carr + predicted_c_and_c_arr_best_case)
    * nonrenewal_growth_best_case * perc_month_remaining                                                   AS predicted_nonrenewal_arr_best_case,
    (ZEROIFNULL(
      LAG(predicted_renewal_arr_most_likely_no_ai + closed_net_arr_actual, 1)
        OVER (
          PARTITION BY combined_output.forecast_type, combined_output.team
          ORDER BY combined_output.close_month ASC
        )
    ) + current_carr
    + predicted_c_and_c_arr_most_likely) * nonrenewal_growth_most_likely
    * perc_month_remaining                                                                                 AS predicted_nonrenewal_arr_most_likely,
    (ZEROIFNULL(
      LAG(predicted_renewal_arr_commit_no_ai + closed_net_arr_actual, 1)
        OVER (
          PARTITION BY combined_output.forecast_type, combined_output.team
          ORDER BY combined_output.close_month ASC
        )
    ) + current_carr + predicted_c_and_c_arr_commit)
    * nonrenewal_growth_commit * perc_month_remaining                                                      AS predicted_nonrenewal_arr_commit
  FROM --actual_data full outer join
    combined_output
  LEFT JOIN actual_data
    ON combined_output.close_month = actual_data.close_month
      AND combined_output.team = actual_data.team
      AND combined_output.forecast_type = actual_data.forecast_type
      --where combined_output.team <> 'Other'
  --and combined_output.calculated_tier is not null
  --and combined_output.team is not null
  ORDER BY
    combined_output.forecast_type, combined_output.team,
    combined_output.close_month
)

SELECT *
FROM add_actual_data