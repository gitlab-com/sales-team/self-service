with fo_charge_data as (
select dim_crm_account_id, subscription_name, arr, quantity,
arr / nullifzero(quantity) as price from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
where rate_plan_charge_name like '%Premium%'
and charge_amendment_type = 'NewSubscription'
and type_of_arr_change = 'New'
and subscription_version = 1
and effective_start_date >= '2024-02-01'
and quantity > 0
and arr > 0)

,
add_ec_data as (
select fo_charge_data.*,
dim_crm_account.crm_account_owner_user_segment,
dim_crm_account.parent_crm_account_max_family_employee,
case when dim_crm_account.parent_crm_account_max_family_employee BETWEEN 251 AND 500 then '250-500'
when dim_crm_account.parent_crm_account_max_family_employee BETWEEN 101 AND 250 then '100-250'
else '0-100' end as ec_band
from
fo_charge_data
inner join PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT
on fo_charge_data.dim_crm_account_id = dim_crm_account.dim_crm_account_id
where dim_crm_account.parent_crm_account_max_family_employee <= 500
)

select
ec_band,
count(*) as total_fos,
avg(quantity)::INT as avg_quantity,
count(distinct case when quantity <= 20 then dim_crm_account_id else null end) as count_under_20_licenses,
(count_under_20_licenses / total_fos)::DOUBLE as perc_under_20
from
add_ec_data
group by all