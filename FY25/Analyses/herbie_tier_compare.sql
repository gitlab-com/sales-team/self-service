with herbie_accounts_current as (
select
distinct
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_crm_account_id,
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_this_account,
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family,
case when DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family >= 7000 then 'High Tier'
when DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family < 7000 and DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family >= 1000 then 'Mid Tier'
else 'No Touch' end as current_tier
from
PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
left join
(
select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR_SNAPSHOT_BOTTOM_UP
where arr_month = '2024-01-01'
and snapshot_date = '2024-01-01'
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_parent_crm_account_id =
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_parent_crm_account_id
where
(PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE <= 100 or PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE is null)
and
CARR_ACCOUNT_FAMILY > 0
and
CARR_ACCOUNT_FAMILY <= 30000
--and parent_crm_account_business_unit in ('ENTG','COMM')
and PARENT_CRM_ACCOUNT_SALES_SEGMENT in ('SMB','Mid-Market','Large','MM','LRG','MID-MARKET','LARGE')
and ultimate.dim_parent_crm_account_id is null
and snapshot_date = current_date
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
),

herbie_accounts_start as (
select
distinct
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_crm_account_id,
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_this_account,
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family,
case when DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family >= 7000 then 'High Tier'
when DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family < 7000 and DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.carr_account_family >= 1000 then 'Mid Tier'
else 'No Touch' end as starting_tier
from
PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
left join
(
select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR_SNAPSHOT_BOTTOM_UP
where arr_month = '2023-02-01'
and snapshot_date = '2023-02-01'
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_parent_crm_account_id =
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_parent_crm_account_id
where
(PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE <= 100 or PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE is null)
and
CARR_ACCOUNT_FAMILY > 0
and
CARR_ACCOUNT_FAMILY <= 30000
--and parent_crm_account_business_unit in ('ENTG','COMM')
and PARENT_CRM_ACCOUNT_SALES_SEGMENT in ('SMB','Mid-Market','Large','MM','LRG','MID-MARKET','LARGE')
and ultimate.dim_parent_crm_account_id is null
and snapshot_date = '2023-02-01'
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
)

select
starting_tier,
current_tier,
count(distinct herbie_accounts_current.dim_crm_account_id) as acct_count,
sum(herbie_accounts_current.carr_this_account) as carr
from
herbie_accounts_current
inner join herbie_accounts_start on herbie_accounts_current.dim_crm_account_id = herbie_accounts_start.dim_crm_account_id
group by 1,2

