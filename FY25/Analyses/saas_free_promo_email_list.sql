with private_free_namespaces as (
select
    dim_namespace.created_at::date as created_at, 
    dim_namespace.creator_id, 
    dim_namespace.dim_namespace_id as namespace_id,
    dim_namespace.is_setup_for_company,
    dim_namespace.GITLAB_PLAN_TITLE,
    dim_namespace.namespace_name,
    case when dim_namespace.namespace_name is null or dim_namespace.namespace_name like '%masked%' then dim_namespace.namespace_id::STRING else dim_namespace.namespace_name end as namespace_value,
    case when dim_namespace.namespace_name is null or dim_namespace.namespace_name like '%masked%' then 2 else 1 end as namespace_sort,
    dim_namespace.namespace_type,
    dim_namespace.visibility_level,
    fct_usage_storage.storage_gib,
    count(distinct gitlab_dotcom_memberships.user_id) as user_count
  from prod.common.dim_namespace
  left join PROD.LEGACY.GITLAB_DOTCOM_NAMESPACE_SETTINGS on dim_namespace.dim_namespace_id = 
  GITLAB_DOTCOM_NAMESPACE_SETTINGS.namespace_id
  left join legacy.gitlab_dotcom_memberships on dim_namespace.dim_namespace_id = gitlab_dotcom_memberships.ultimate_parent_id
  left join 
  (select ultimate_parent_namespace_id,
  sum(storage_gib) as storage_gib
  from
  common.fct_usage_storage
  where snapshot_month = '2024-09-01'
  group by 1
  ) fct_usage_storage
  
  on dim_namespace.dim_namespace_id = fct_usage_storage.ultimate_parent_namespace_id
  left join PROD.COMMON.DIM_USER
  on dim_user.user_id = gitlab_dotcom_memberships.user_id
  left join 
  (
  select
  dim_ultimate_parent_namespace_id,
  max(event_date) as most_recent_event
  from
  PROD.COMMON.FCT_EVENT_NAMESPACE_DAILY 
  where is_umau
  group by 1
  ) fct_event_namespace_daily
  on fct_event_namespace_daily.dim_ultimate_parent_namespace_id = 
  dim_namespace.ultimate_parent_namespace_id
  where dim_namespace.NAMESPACE_IS_ULTIMATE_PARENT = TRUE
    and dim_namespace.namespace_type in ( 'Group')
    and dim_namespace.GITLAB_PLAN_TITLE like any ('Free'
    --,'Trial'
    )
    and dim_namespace.namespace_creator_is_blocked = FALSE -- same as blocked.user_id IS NULLL
    and dim_namespace.namespace_is_internal = FALSE
    and dim_namespace.visibility_level = 'private'
    and GITLAB_DOTCOM_NAMESPACE_SETTINGS.repository_read_only = 0
    and gitlab_dotcom_memberships.is_billable = TRUE	
    and dim_user.last_sign_in_date >= '2024-08-01'
    and most_recent_event >= '2024-08-01'
    and gitlab_dotcom_memberships.is_active
    group by all
    having --user_count >= 4 and 
    storage_gib < 5
    ),

namespace_owners as
(
select 
distinct
--ns.*,
memberships.user_id,																						
users.email,
users.first_name,
users.last_name,
        users.is_email_opted_in,
        user_detail.country
        ,
        case when marketing.dim_marketing_contact_id is not null then true else false end as marketing_mart_flag,
        marketing.*
        ,
        person.dim_crm_person_id,
        person.country as person_country,
        person.has_opted_out_email as person_email_opt_out,
        -- ns_user_count.num_users,
        case when 
          user_detail.country like any ('%Russia%','%China%','%Belarus%')
          or marketo_country like any ('%Russia%','%China%','%Belarus%')
           or person_country like any ('%Russia%','%China%','%Belarus%')
            or email like any ('%.ru','%.by','%.cn') then true else false end as excluded_country_flag,
            case when 
          user_detail.country like any ('%Russia%','%China%','%Belarus%') then 'User table'
          when marketo_country like any ('%Russia%','%China%','%Belarus%') then 'mart_marketing'
           when person_country like any ('%Russia%','%China%','%Belarus%') then 'dim_crm_person'
            when email like any ('%.ru','%.by','%.cn') then 'tld' else 'not excluded' end as excluded_country_source
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
          inner  join legacy.gitlab_dotcom_members members --join to get timestamp -- xxxxx inner join reduces the row count from 15M to 9M, but we decided to do this for now, as the impact to group namespace is <2% 																									
            ON memberships.user_id = members.user_id																									
        and memberships.membership_source_id = members.source_id 																									
        inner join private_free_namespaces ns--limit to just the top-level namespaces we care about										
            ON memberships.ultimate_parent_id = ns.namespace_id 	

        inner join PROD.LEGACY.GITLAB_DOTCOM_USERS_XF users
        on users.user_id = memberships.user_id
        inner join prod.common.dim_user user_detail on user_detail.user_id = memberships.user_id
        left join RAW.DRIVELOAD.MARKETING_DNC_LIST on lower(MARKETING_DNC_LIST.address) = lower(users.email)
         left join 
         (select
dim_marketing_contact_id,
sfdc_record_id,
dim_crm_account_id,
country as marketo_country,
marketo_lead_id,
is_marketo_opted_in,
has_marketo_unsubscribed,
is_sfdc_lead_contact,
sfdc_lead_contact,
is_sfdc_opted_out,
gitlab_dotcom_user_id
from 
PROD.COMMON_MART_MARKETING.MART_MARKETING_CONTACT_NO_PII
where gitlab_dotcom_user_id is not null) marketing on 
       memberships.user_id = marketing.gitlab_dotcom_user_id
left join prod.common.dim_crm_person person on 
(--marketing.marketo_lead_id = person.marketo_lead_id or 
marketing.sfdc_record_id = person.sfdc_record_id)
left join private_free_namespaces on private_free_namespaces.namespace_id = memberships.ultimate_parent_id
left join restricted_safe_common_mart_sales.mart_crm_account on mart_crm_account.dim_crm_account_id = marketing.dim_crm_account_id
             where memberships.is_billable = TRUE																	
             and memberships.access_level = 50
             and MARKETING_DNC_LIST.address is null
             and mart_crm_account.crm_account_owner like '%SMB Sales'
             and mart_crm_account.crm_account_type <> 'Customer'
             and person.email_domain_type = 'Business email domain'
             and private_free_namespaces.user_count < 3
--order by cohort, email, namespace_sort asc, namespace_value asc
 --         limit 10
)    

select 
*
from namespace_owners
