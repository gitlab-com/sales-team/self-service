-- Pulls accounts with FY25 renewals that are >= 25 users, that had FY24 EoA renewals of < 25 users.


with eoa_accounts_fy24 as
(
--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
select distinct dim_crm_account_id from
(
SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
) order by dim_crm_account_id asc
)
)
, prior_sub_25_eoa as
(
select distinct dim_crm_account_id from
(
select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
dim_subscription.dim_crm_opportunity_id_current_open_renewal,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
inner join eoa_accounts_fy24 on eoa_accounts_fy24.dim_crm_account_id = mart_charge.dim_crm_account_id
-- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
where
mart_charge.rate_plan_name like '%Premium%'
-- and 
-- mart_charge.term_start_date <= '2024-02-01'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2023-02-01'
and mart_charge.term_start_date < current_date
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and (lower(rate_plan_charge_description) like '%eoa%'
or
((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
and quantity < 25
)
)

--Gets product, price, quantity information for current customers (current month ARR > 0) that have future subscription end dates
,
arr_data as
(
SELECT 
mart_arr.*
,monthly_mart.snapshot_month 
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME like '%Premium%'
)

select *
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(ARR / license_user_count)  as arr_per_user,
max_billable_user_count as current_user_count,
arr_per_user/12 as monthly_price_per_user,
case when monthly_price_per_user < (case when subscription_end_month > '2024-04-01' then 29 else 23.78 end) and product_tier_name like '%Premium%' then true else false end as fy25_premium_price_increase_flag,
case when fy25_premium_price_increase_flag then (
    (case when subscription_end_month > '2024-04-01' then 29 else 23.78 end)
     - monthly_price_per_user) * current_user_count else 0 end as fy25_estimated_price_increase_impact,
case when arr + fy25_estimated_price_increase_impact >= 7000 and arr < 7000 then true else false end as fy25_likely_price_increase_uptier_flag
FROM arr_data
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE dim_crm_account_id in
(select * from prior_sub_25_eoa)
and current_user_count >= 25
--and PRODUCT_TIER_NAME like '%Premium%'


