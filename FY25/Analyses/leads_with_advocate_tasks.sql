select
task.*,
mart_crm_person.sfdc_record_type,
sfdc_lead_xf.lean_data_matched_account
from
prod.common_mart_sales.mart_crm_task task
 inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
 left join PROD.COMMON_MART_MARKETING.MART_CRM_PERSON on task.dim_crm_person_id = mart_crm_person.dim_crm_person_id
 left join PROD.LEGACY.SFDC_LEAD_XF on sfdc_lead_xf.lead_id = mart_crm_person.sfdc_record_id
   where
      task.dim_crm_user_id is not null
and
      task.is_deleted = false
and task_date >= '2024-02-01'
and task_status = 'Completed'
and user_role_name like any ('%Advocate%')
and sfdc_record_type <> 'contact'