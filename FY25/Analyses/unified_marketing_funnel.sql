with 
site_sessions_base as (
    select
      CASE
        WHEN traffic_source_medium LIKE '%paidsocial%' THEN 'Paid Social'
        ELSE channel_grouping
      END AS channel_grouping,
      s.visit_start_time,
      total_new_visits is not null as is_new_user,
      s.visitor_id,
      s.visit_id,
      s.client_id
    from PROD.LEGACY.GA360_SESSION_HIT as h 
        left join PROD.LEGACY.GA360_SESSION as s
        on h.visit_id = s.visit_id 
            and h.visitor_id = s.visitor_id
    where 
        s.visit_start_time >= '2022-01-01'   
        and lower(h.hit_type) = 'page'
        and h.host_name in ('about.gitlab.com')
        and lower(h.page_path) not like '%/handbook%' 
        and lower(h.page_path) not like '%/job%'
        and lower(h.page_path) not like '%/unsubscribe%'
        and lower(h.is_entrance) = 'true'
), site_sessions as (
    select
    date_trunc('month', visit_start_time)        as metric_time,
    'site_sessions'                              as metric_name,
    channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct site_sessions_base.visitor_id, site_sessions_base.visit_id) as metric_value
    from
    site_sessions_base
    group by 1,2,3
), page_view_sign_in as (
    select
     visitor_id,
     visit_id
    from PROD.LEGACY.GA360_SESSION_HIT 
    where 
        visit_start_time >= '2022-01-01'  
        and lower(hit_type) = 'page'
        and lower(page_path) like '/users/sign_in%'
    group by 1, 2
), site_sessions_exclude_signin as (
    select
    date_trunc('month', visit_start_time)        as metric_time,
    'site_sessions_exclude_signin'               as metric_name,
    channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct site_sessions_base.visitor_id, site_sessions_base.visit_id)         as metric_value
    from
    site_sessions_base
        left join page_view_sign_in
            on site_sessions_base.visitor_id = page_view_sign_in.visitor_id
                and site_sessions_base.visit_id = page_view_sign_in.visit_id
        where page_view_sign_in.visit_id is null 
            and page_view_sign_in.visitor_id is null
    group by 1,2,3
), new_users as (
    select
    date_trunc('month', visit_start_time)        as metric_time,
    'new_users'                                  as metric_name,
    channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct client_id)                    as metric_value
    from
    site_sessions_base
    where is_new_user
    group by 1,2,3
), new_users_exclude_signin as (
    select
    date_trunc('month', visit_start_time)        as metric_time,
    'new_users_exclude_signin'                   as metric_name,
    channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct client_id)                    as metric_value
    from
    site_sessions_base
        left join page_view_sign_in
            on site_sessions_base.visitor_id = page_view_sign_in.visitor_id
                and site_sessions_base.visit_id = page_view_sign_in.visit_id
    where page_view_sign_in.visit_id is null 
        and page_view_sign_in.visitor_id is null
        and is_new_user
    group by 1,2,3

-- engaged_sessions, could pull from the a seperate select but this woks as well
), engaged_session as (
    select 
    date_trunc('month', s.visit_start_time)      as metric_time,
    'engaged_sessions'                           as metric_name,
    CASE
        WHEN traffic_source_medium
         LIKE '%paidsocial%' THEN 'Paid Social'
        ELSE channel_grouping
    END                                          as channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct s.visitor_id, s.visit_id)     as metric_value
    from PROD.LEGACY.GA360_SESSION_HIT as h 
    left join PROD.LEGACY.GA360_SESSION as s
        on h.visit_id = s.visit_id and h.visitor_id = s.visitor_id    
    where
        s.visit_start_time >= '2022-01-01'  
        and lower(h.hit_type) like 'event'
        and h.host_name in ('about.gitlab.com')
        and lower(h.event_category) in ('cta click')
        and lower(h.event_action) not like 'in-line'
        and (h.custom_dimensions:"Click URL" like '%about.gitlab.com/sales/%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/trials/new%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/trial_registrations/new%'
            or h.custom_dimensions:"Click URL" like '%customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53%'
            or h.custom_dimensions:"Click URL" like '%customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff%'
            )
    group by 1,2,3
), engaged_session_exclude_signin as (
    select 
    date_trunc('month', s.visit_start_time)      as metric_time,
    'engaged_sessions_exclude_signin'            as metric_name,
    CASE
        WHEN traffic_source_medium
         LIKE '%paidsocial%' THEN 'Paid Social'
        ELSE channel_grouping
    END                                          as channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct s.visitor_id, s.visit_id)     as metric_value
    from PROD.LEGACY.GA360_SESSION_HIT as h 
    left join PROD.LEGACY.GA360_SESSION as s
        on h.visit_id = s.visit_id and h.visitor_id = s.visitor_id
    left join page_view_sign_in
    on s.visitor_id = page_view_sign_in.visitor_id
        and s.visit_id = page_view_sign_in.visit_id
    where
            page_view_sign_in.visit_id is null 
        and page_view_sign_in.visitor_id is null
        and s.visit_start_time >= '2022-01-01'  
        and lower(h.hit_type) like 'event'
        and h.host_name in ('about.gitlab.com')
        and lower(h.event_category) in ('cta click')
        and lower(h.event_action) not like 'in-line'
        and (h.custom_dimensions:"Click URL" like '%about.gitlab.com/sales/%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/trials/new%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/trial_registrations/new%'
            or h.custom_dimensions:"Click URL" like '%customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53%'
            or h.custom_dimensions:"Click URL" like '%customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a%'
            or h.custom_dimensions:"Click URL" like '%gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff%'
            )
    group by 1,2,3

), ga_saas_trial_signup as (
    select 
    date_trunc('month', s.visit_start_time)      as metric_time,
    'ga_saas_trial_signup'                       as metric_name,
    channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct s.visitor_id, s.visit_id)     as metric_value
    from prod.legacy.ga360_session_hit as h 
    left join site_sessions_base as s
        on h.visit_id = s.visit_id and h.visitor_id = s.visitor_id    
    where
        lower(h.hit_type) like 'event'
        and h.host_name like 'gitlab.com'
        and h.event_category like 'SaaS Free Trial'
        and (h.event_action like 'About Your Company' or h.event_action like 'Form Submit')
        and (h.event_label like 'ultimate_trial' or h.event_label is null)        
    group by 1,2,3

--Pages Per Session
), sessions_with_page_count as(
    SELECT 
        visit_id,
        visitor_id,
        MAX(total_pageviews) AS total_pageviews
    FROM PROD.LEGACY.GA360_SESSION
    group by 1,2

), site_sessions_base_grouped as (
    -- group the site_sessions_base so it joins with the page views
    select
    visit_start_time,
    visitor_id,
    visit_id
    from
    site_sessions_base
    group by 1,2,3

), pages_per_session as (
    SELECT
        date_trunc('month', visit_start_time)    as metric_time,
        'pages_per_session'                      as metric_name,
        null                                     as channel_grouping,
        -- null                                     as is_valuable_signup,
        avg(total_pageviews)                     as metric_value 
    FROM site_sessions_base_grouped
    LEFT JOIN sessions_with_page_count 
        ON site_sessions_base_grouped.visit_id = sessions_with_page_count.visit_id 
        AND site_sessions_base_grouped.visitor_id = sessions_with_page_count.visitor_id
    group by 1,2,3
), pages_per_session_exclude_signin as (
    SELECT
        date_trunc('month', visit_start_time)    as metric_time,
        'pages_per_session_exclude_signin'       as metric_name,
        null                                     as channel_grouping,
        -- null                                     as is_valuable_signup,
        avg(total_pageviews)                     as metric_value 
    FROM site_sessions_base_grouped
    LEFT JOIN sessions_with_page_count 
        ON site_sessions_base_grouped.visit_id = sessions_with_page_count.visit_id 
        AND site_sessions_base_grouped.visitor_id = sessions_with_page_count.visitor_id
    LEFT JOIN page_view_sign_in
        ON site_sessions_base_grouped.visit_id = page_view_sign_in.visit_id 
        AND site_sessions_base_grouped.visitor_id = page_view_sign_in.visitor_id
    WHERE (page_view_sign_in.visit_id is null and page_view_sign_in.visitor_id is null) 
    group by 1,2,3

-- snowplow from here down

), trials_base as (
    select
    ultimate_parent_namespace_id,
    trial_start_date,
    creator_is_valuable_signup,
    namespace_contains_valuable_signup,
    has_team_activation,
    first_paid_subscription_start_date,
    is_namespace_created_within_2min_of_creator_invite_acceptance
    from common_mart_product.rpt_namespace_onboarding
    where 
    is_namespace_created_within_2min_of_creator_invite_acceptance = false

), snowplow_page_views_base as (
    select
    session_id,
    user_snowplow_domain_id,
    gsc_namespace_id,
    page_view_start,
    gsc_plan,
    page_url,
    PAGE_VIEW_ID,
    page_url_path,
    page_url_host
    from PROD.LEGACY.SNOWPLOW_PAGE_VIEWS_ALL
    where page_view_start >= '2022-01-01'  
        -- marketing
        and ((page_url_host = 'about.gitlab.com'
        and page_url not like '%/job%'
        and page_url not like '%/handbook%'
        and page_url not like '%/unsubscribe%'
        ) or (
        -- product
            page_url_host like 'gitlab.com'
        ))
        
), snowplow_page_views_marketing as (
    select
    session_id,
    user_snowplow_domain_id,
    min(page_view_start) as first_marketing_page_view
    from snowplow_page_views_base
    where
        page_url_host = 'about.gitlab.com'
    group by 1,2

-- Pages per Trial
), trial_namespace_ids_from_starts as (
    --namespace id only available for certain gitlab.com pages
    -- additional we need to ensure we are joining to a namespace id instead of another type of ID
    select distinct
    page_view_id,
    gsc_namespace_id,
    user_snowplow_domain_id,
    creator_is_valuable_signup,
    trial_start_date
    from snowplow_page_views_base
      join trials_base 
        on snowplow_page_views_base.gsc_namespace_id = trials_base.ultimate_parent_namespace_id 
        and page_url_host like 'gitlab.com'
         -- trial date is equal to or after the pageview date
        and trial_start_date >= date_trunc(day, page_view_start)
        -- trial and page views must have happened in the same month
        and date_trunc('month', trial_start_date) = date_trunc('month',page_view_start)

), pages_per_trial_start as (
    -- counting pageviews for trialed users who viewed the marketing site 
    select
    date_trunc('month', trial_start_date)        as metric_time,
    'pages_per_trial_start'                      as metric_name,
    null                                         as channel_grouping,
    -- creator_is_valuable_signup                   as is_valuable_signup,
    COUNT(DISTINCT snowplow_page_views_base.page_view_id) / COUNT(DISTINCT snowplow_page_views_base.user_snowplow_domain_id) AS metric_value
    -- this should be selecting from the markting page view cte, but it has a min and 
    -- it is easier to rebuild it rather than create and debug something else
    from snowplow_page_views_base 
        join trial_namespace_ids_from_starts
            on trial_namespace_ids_from_starts.user_snowplow_domain_id = snowplow_page_views_base.user_snowplow_domain_id
            -- trial and page views have to have been in the same month
            and date_trunc('month', trial_namespace_ids_from_starts.trial_start_date) 
                = date_trunc('month',snowplow_page_views_base.page_view_start)
    where page_url_host = 'about.gitlab.com'
    group by 1,2,3

--SaaS Trial Pageviews
-- start on marketing site then move to product trial page in the same session
-- linked by session so i'm not worried about old page views coming back and haulting us
), snowplow_page_views_trials as (
    select 
    date_trunc('month', page_view_start)         as metric_time,
    'saas_trial_pages_views_users'               as metric_name,
    null                                         as channel_grouping,
    -- null                                         as is_valuable_signup,
    count(distinct snowplow_page_views_base.user_snowplow_domain_id) as metric_value
    from snowplow_page_views_base
       join snowplow_page_views_marketing 
            on snowplow_page_views_base.session_id = snowplow_page_views_marketing.session_id 
    where page_view_start >= '2022-01-01'  
        and (snowplow_page_views_base.page_url like '%/trial_registrations/new%' 
            or snowplow_page_views_base.page_url like '%/trials/new%')
        -- start on the marketing site, then move to the product
        and page_view_start >= first_marketing_page_view
        and date_trunc('month', page_view_start) = date_trunc('month', first_marketing_page_view)
    group by 1,2,3

--SaaS Trial Signups
-- start on marketing site and create a trial
-- linked by session so i'm not worried about old page views coming back and haulting us

), namespaces_in_trial_touched_marketing_site as (
    select 
        distinct
        gsc_namespace_id,
        min(page_view_start) as page_view_start
    from snowplow_page_views_base
    join snowplow_page_views_marketing
            on snowplow_page_views_base.session_id = snowplow_page_views_marketing.session_id
            and snowplow_page_views_base.page_view_start >= snowplow_page_views_marketing.first_marketing_page_view
            and date_trunc('month', snowplow_page_views_base.page_view_start) 
                = date_trunc('month', snowplow_page_views_marketing.first_marketing_page_view)
    where gsc_plan like '%trial%'
    group by 1

), snowplow_trial_signup_namespaces as (
    select  
    date_trunc('month', trial_start_date)        as metric_time,
    'saas_trial_signups_namespace_marketing'     as metric_name,
    null                                         as channel_grouping,
    -- creator_is_valuable_signup                   as is_valuable_signup,
    count(distinct ultimate_parent_namespace_id) as metric_value
    from trials_base
        join namespaces_in_trial_touched_marketing_site
            on trials_base.ultimate_parent_namespace_id 
                = namespaces_in_trial_touched_marketing_site.gsc_namespace_id
    where
        trial_start_date >= date_trunc(day, page_view_start)
        and date_trunc('month', trial_start_date) = date_trunc('month', page_view_start)
    group by 1,2,3
), snowplow_trial_signup_namespaces_vs as (
    select  
    date_trunc('month', trial_start_date)        as metric_time,
    'saas_trial_signups_namespace_marketing_vs'  as metric_name,
    null                                         as channel_grouping,
    -- creator_is_valuable_signup                   as is_valuable_signup,
    count(distinct ultimate_parent_namespace_id) as metric_value
    from common_mart_product.rpt_namespace_onboarding
        join namespaces_in_trial_touched_marketing_site
            on rpt_namespace_onboarding.ultimate_parent_namespace_id 
                = namespaces_in_trial_touched_marketing_site.gsc_namespace_id
    where
        trial_start_date >= date_trunc(day, page_view_start)
        and date_trunc('month', trial_start_date) = date_trunc('month', page_view_start)
        and creator_is_valuable_signup
    group by 1,2,3

-- SaaS new User
),  user_welcome_page AS ( --welcome page can come after standard or SSO steps, so including requirement for all users in subsequent steps to view users/sign_up Page prior to, but on the same day as subsequent steps. 

   SELECT DISTINCT
    b.page_view_start,
    b.user_snowplow_domain_id
  FROM snowplow_page_views_base AS b
  JOIN snowplow_page_views_marketing
   ON b.user_snowplow_domain_id = snowplow_page_views_marketing.user_snowplow_domain_id 
   AND b.page_view_start::DATE = snowplow_page_views_marketing.first_marketing_page_view::DATE
   AND b.page_view_start > snowplow_page_views_marketing.first_marketing_page_view
  WHERE b.page_url like '%users/sign_up/welcome'
  and page_view_start >= '2022-01-01'  

-- we should be able to make is_valuable_signup a real field
), sass_new_user as (
    SELECT 
        DATE_TRUNC(month, page_view_start)           AS metric_time,
        'saas_new_user'                              as metric_name,
        null                                         as channel_grouping,
        -- null                                         as is_valuable_signup,
        COUNT(DISTINCT user_snowplow_domain_id)      as metric_value
    from user_welcome_page
    group by 1,2,3

), day_14_active_trials_rate as (
    select
    *,
    dateadd(day, 14, trial_start_date) as day_14_of_trial
    from trials_base
    where dateadd(day, 45, trial_start_date) <= CURRENT_DATE
    and creator_is_valuable_signup
), namespaces_14_day_activation_rate as (
    select
        date_trunc('month', day_14_of_trial)         as metric_time,
        '14_day_activation_rate'                     as metric_name,
        null                                         as channel_grouping,
        -- creator_is_valuable_signup                   as is_valuable_signup,
        count(distinct case when has_team_activation then ultimate_parent_namespace_id end)
            / count(distinct ultimate_parent_namespace_id) as metric_value
    from day_14_active_trials_rate
    where
    metric_time >= '2022-01-01'
    and creator_is_valuable_signup
    group by 1,2,3
), namespaces_14_day_activation_numerator as (
    select
        date_trunc('month', day_14_of_trial)         as metric_time,
        '14_day_activation_numerator'                as metric_name,
        null                                         as channel_grouping,
        -- creator_is_valuable_signup                   as is_valuable_signup,
        count(distinct case when has_team_activation then ultimate_parent_namespace_id end) as metric_value
    from day_14_active_trials_rate
    where
    metric_time >= '2022-01-01'
    and creator_is_valuable_signup
    group by 1,2,3
), namespaces_14_day_activation_denominator as (
    select
        date_trunc('month', day_14_of_trial)         as metric_time,
        '14_day_activation_denominator'              as metric_name,
        null                                         as channel_grouping,
        -- creator_is_valuable_signup                   as is_valuable_signup,
        count(distinct ultimate_parent_namespace_id) as metric_value
    from day_14_active_trials_rate
    where
    metric_time >= '2022-01-01'
    and creator_is_valuable_signup
    group by 1,2,3    
-- 45-day free to paid conversion rate
), namespaces_on_day_45_of_trial as (
    -- gets namespace ids that just started a trial based on the segment 
    select  
    *,
    dateadd(day, 45, trial_start_date) as day_45_of_trial
    from trials_base
        -- limmit to trials on thier x date
        where dateadd(day, 45, trial_start_date) <= CURRENT_DATE
        and creator_is_valuable_signup
), day_45_free_to_paid_rate as (
    select
        date_trunc('month', day_45_of_trial)               as metric_time,
        '45_day_trial_paid_rate'                           as metric_name,
        null                                               as channel_grouping,
        -- creator_is_valuable_signup                         as is_valuable_signup,
        count(distinct 
            case when 
                -- subscribed within 45 days
                first_paid_subscription_start_date <= day_45_of_trial 
                -- ensure the paid sub is after the trial
                and first_paid_subscription_start_date >= trial_start_date 
                then ultimate_parent_namespace_id end)
                / 
            count(distinct ultimate_parent_namespace_id)
            as metric_value
    from
    namespaces_on_day_45_of_trial
    where metric_time >= '2022-01-01'  
    and creator_is_valuable_signup
    group by 1,2,3
), day_45_free_to_paid_numerator as (
    select
        date_trunc('month', day_45_of_trial)               as metric_time,
        '45_day_trial_paid_numerator'                      as metric_name,
        null                                               as channel_grouping,
        -- creator_is_valuable_signup                         as is_valuable_signup,
        count(distinct 
            case when 
                -- subscribed within 45 days
                first_paid_subscription_start_date <= day_45_of_trial 
                -- ensure the paid sub is after the trial
                and first_paid_subscription_start_date >= trial_start_date 
                then ultimate_parent_namespace_id end)                
            as metric_value
    from
    namespaces_on_day_45_of_trial
    where metric_time >= '2022-01-01'  
    and creator_is_valuable_signup
    group by 1,2,3
),  day_45_free_to_paid_denominator as (
    select
        date_trunc('month', day_45_of_trial)               as metric_time,
        '45_day_trial_paid_denominator'                    as metric_name,
        null                                               as channel_grouping,
        -- creator_is_valuable_signup                         as is_valuable_signup,
        count(distinct ultimate_parent_namespace_id)       as metric_value
    from
    namespaces_on_day_45_of_trial
    where metric_time >= '2022-01-01'  
    and creator_is_valuable_signup
    group by 1,2,3
),  paid_conversions_non_cohorted as (
    -- gets namespace ids that just started a trial based on the segment 
    select
    date_trunc('month', first_paid_subscription_start_date) as metric_time,
    'paid_conversions_non_cohorted'                         as metric_name,
    null                                                    as channel_grouping,
    -- creator_is_valuable_signup                              as is_valuable_signup,
    count(distinct ultimate_parent_namespace_id)            as metric_value
    from trials_base
    where
        trial_start_date is not null
        and first_paid_subscription_start_date >= trial_start_date
        and first_paid_subscription_start_date >= '2022-01-01'  
        and creator_is_valuable_signup
    group by 1,2,3
), final_union as (
    select
    *
    from
    site_sessions
    
    union all

    select
    *
    from 
    site_sessions_exclude_signin

    union all 
    
    select
    *
    from
    new_users

    union all 
    
    select
    *
    from
    new_users_exclude_signin
    
    union all
    
    select
    *
    from
    engaged_session

    union all
    
    select
    *
    from
    engaged_session_exclude_signin
    
    union all

    select
    *
    from
    ga_saas_trial_signup

    union all 
    
    select
    *
    from
    pages_per_session

    union all
    
    select
    *
    from
    pages_per_session_exclude_signin
    
    union all
    
    select
    *
    from
    pages_per_trial_start
    
    union all
    
    select
    *
    from
    snowplow_page_views_trials
    
    union all
    
    select
    *
    from
    snowplow_trial_signup_namespaces
    
    union all
    
    select
    *
    from
    snowplow_trial_signup_namespaces_vs
    
    union all 
    
    select
    *
    from
    sass_new_user
    
    union all 
    
    select
    *
    from
    namespaces_14_day_activation_rate

    union all 
    
    select
    *
    from
    namespaces_14_day_activation_numerator

    union all 
    
    select
    *
    from
    namespaces_14_day_activation_denominator
    
    union all 
    
    select
    *
    from
    day_45_free_to_paid_rate
    
    union all 

    select 
    *
    from 
    day_45_free_to_paid_numerator

    union all

    select 
    *
    from 
    day_45_free_to_paid_denominator

    union all
    
    select
    *
    from
    paid_conversions_non_cohorted
)
select
metric_time::date as metric_time,
metric_name,
channel_grouping,
-- is_valuable_signup,
metric_value,
fiscal_quarter_name_fy,
fiscal_quarter,
fiscal_year
from
final_union
    join common.dim_date on
    dim_date.date_actual = final_union.metric_time