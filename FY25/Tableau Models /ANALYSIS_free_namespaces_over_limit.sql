with private_free_namespaces as (
  select
    dim_namespace.created_at::date as created_at, 
    dim_namespace.creator_id, 
    dim_namespace.dim_namespace_id as namespace_id,
    dim_namespace.is_setup_for_company,
    dim_namespace.GITLAB_PLAN_TITLE,
    dim_namespace.namespace_name,
    case when dim_namespace.namespace_name is null or dim_namespace.namespace_name like '%masked%' then dim_namespace.namespace_id::STRING else dim_namespace.namespace_name end as namespace_value,
    case when dim_namespace.namespace_name is null or dim_namespace.namespace_name like '%masked%' then 2 else 1 end as namespace_sort,
    dim_namespace.namespace_type,
    dim_namespace.visibility_level
  from prod.common.dim_namespace
  left join PROD.LEGACY.GITLAB_DOTCOM_NAMESPACE_SETTINGS on dim_namespace.dim_namespace_id = 
  GITLAB_DOTCOM_NAMESPACE_SETTINGS.namespace_id
  where dim_namespace.NAMESPACE_IS_ULTIMATE_PARENT = TRUE
    and dim_namespace.namespace_type in ( 'Group')
    and dim_namespace.GITLAB_PLAN_TITLE like any ('Free'
    --,'Trial'
    )
    and dim_namespace.namespace_creator_is_blocked = FALSE -- same as blocked.user_id IS NULLL
    and dim_namespace.namespace_is_internal = FALSE
    and dim_namespace.visibility_level = 'private'
    and GITLAB_DOTCOM_NAMESPACE_SETTINGS.repository_read_only = 0

),
namespace_storage_usage as (
  select
    private_free_namespaces.namespace_id as ultimate_parent_namespace_id,
    snapshot_month,
    coalesce(fct_usage_storage.storage_gib,0) as total_size_gb,
    coalesce(fct_usage_storage.total_purchased_storage_gib,0) as total_purchased_storage_gib
  from private_free_namespaces
  inner join common.fct_usage_storage
    on private_free_namespaces.namespace_id = fct_usage_storage.ULTIMATE_PARENT_NAMESPACE_ID 
    and fct_usage_storage.snapshot_month = date_trunc('month',current_date)
  where 1=1
    and total_size_gb < (5 + total_purchased_storage_gib)
),
namespaces_with_subscriptions as (
    -- treated as seperate step to combined rtp onboarding and dim_subscription models 
    select
        coalesce(charges.dim_namespace_id::varchar, dim_subscription.namespace_id::varchar) as    namespace_id,
        rate_plan_name
    from
    common.dim_subscription
        inner join (
                select dim_namespace_id ,
                rate_plan_name
                from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
                where rate_plan_name like any ('%SaaS%','%Gold%','%Silver%','%Bronze%')
            ) charges
            on dim_subscription.namespace_id = charges.dim_namespace_id
    -- where subscription_name not in
    -- (
    -- select subscription_name
    -- from
    -- PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
    -- where rate_plan_name like '%Storage%'
    -- )
),
storage_namespaces_with_subscriptions as (
        select
            namespace_id
        from
        namespaces_with_subscriptions
        join namespace_storage_usage
            on namespace_storage_usage.ultimate_parent_namespace_id = namespaces_with_subscriptions.namespace_id
    group by 1
),

big_projects as
(
select
projects.namespace_id,
max(storage_size) as highest_storage
from
PROD.LEGACY.GITLAB_DOTCOM_PROJECT_STATISTICS projects
where projects.storage_size >= 536870912000
--and projects.storage_size <= 2147483648000
group by 1
order by highest_storage desc
),

namespace_list as
(
select
    private_free_namespaces.*,
    namespace_storage_usage.total_size_gb,
    namespace_storage_usage.total_purchased_storage_gib,
    case when big_projects.namespace_id is not null then true else false end as large_project_flag
from
namespace_storage_usage 
    join private_free_namespaces 
        on private_free_namespaces.namespace_id = namespace_storage_usage.ultimate_parent_namespace_id
    left join storage_namespaces_with_subscriptions
        on namespace_storage_usage.ultimate_parent_namespace_id = storage_namespaces_with_subscriptions.namespace_id
    left join big_projects on big_projects.namespace_id = namespace_storage_usage.ultimate_parent_namespace_id
where storage_namespaces_with_subscriptions.namespace_id is null
--and big_projects.namespace_id is null
),

ns_user_count as
(
select ns.namespace_id,count(distinct memberships.user_id)  as num_users																									
																									
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
       																									
        inner join namespace_list ns--limit to just the top-level namespaces we care about																									
            ON memberships.ultimate_parent_id = ns.namespace_id 																where memberships.is_billable = TRUE																				  and memberships.is_active
            group by 1
),

namespace_owners as
(
select 
distinct
case when namespace_type = 'Group' and num_users <= 5 then 'Cohort 1'
when namespace_type = 'Group' and num_users > 5 then 'Cohort 2'
when namespace_type = 'User' then 'Cohort 3'
else 'other' end as cohort,
ns.*,memberships.user_id,																						
users.email,
users.first_name,
users.last_name,
        users.is_email_opted_in,
        user_detail.country
        ,
        case when marketing.dim_marketing_contact_id is not null then true else false end as marketing_mart_flag,
        marketing.*
        ,
        person.dim_crm_person_id,
        person.country as person_country,
        person.has_opted_out_email as person_email_opt_out,
        ns_user_count.num_users,
        case when 
          user_detail.country like any ('%Russia%','%China%','%Belarus%')
          or marketo_country like any ('%Russia%','%China%','%Belarus%')
           or person_country like any ('%Russia%','%China%','%Belarus%')
            or email like any ('%.ru','%.by','%.cn') then true else false end as excluded_country_flag,
            case when 
          user_detail.country like any ('%Russia%','%China%','%Belarus%') then 'User table'
          when marketo_country like any ('%Russia%','%China%','%Belarus%') then 'mart_marketing'
           when person_country like any ('%Russia%','%China%','%Belarus%') then 'dim_crm_person'
            when email like any ('%.ru','%.by','%.cn') then 'tld' else 'not excluded' end as excluded_country_source
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
          inner  join legacy.gitlab_dotcom_members members --join to get timestamp -- xxxxx inner join reduces the row count from 15M to 9M, but we decided to do this for now, as the impact to group namespace is <2% 																									
            ON memberships.user_id = members.user_id																									
        and memberships.membership_source_id = members.source_id 																									
        inner join namespace_list ns--limit to just the top-level namespaces we care about										
            ON memberships.ultimate_parent_id = ns.namespace_id 	

        inner join PROD.LEGACY.GITLAB_DOTCOM_USERS_XF users
        on users.user_id = memberships.user_id
        inner join prod.common.dim_user user_detail on user_detail.user_id = memberships.user_id
        left join RAW.DRIVELOAD.MARKETING_DNC_LIST on lower(MARKETING_DNC_LIST.address) = lower(users.email)
         left join 
         (select
dim_marketing_contact_id,
sfdc_record_id,
dim_crm_account_id,
country as marketo_country,
marketo_lead_id,
is_marketo_opted_in,
has_marketo_unsubscribed,
is_sfdc_lead_contact,
sfdc_lead_contact,
is_sfdc_opted_out,
gitlab_dotcom_user_id
from 
PROD.COMMON_MART_MARKETING.MART_MARKETING_CONTACT_NO_PII
where gitlab_dotcom_user_id is not null) marketing on 
       memberships.user_id = marketing.gitlab_dotcom_user_id
left join prod.common.dim_crm_person person on 
(--marketing.marketo_lead_id = person.marketo_lead_id or 
marketing.sfdc_record_id = person.sfdc_record_id)
left join ns_user_count on ns_user_count.namespace_id = memberships.ultimate_parent_id
             where memberships.is_billable = TRUE																	
             and memberships.access_level = 50
             and MARKETING_DNC_LIST.address is null
order by cohort, email, namespace_sort asc, namespace_value asc
 --         limit 10
)
--,

-- contact_output as (
-- select
-- distinct
-- cohort,
-- email,
-- namespace_list,
-- is_email_opted_in,
-- storage_total,
-- case when country is null or country = 'Not Found' then 
-- case when marketo_country is null or marketo_country = 'Not Found' then 
-- person_country else marketo_country end else country end as country,
-- is_marketo_opted_in,
-- has_marketo_unsubscribed,
-- is_sfdc_opted_out,
-- gitlab_dotcom_user_id,
-- dim_crm_account_id
-- from
-- (
select
listagg(distinct namespace_value , ', ') over(partition by cohort, email) as namespace_list,
* 
from namespace_owners
where excluded_country_flag = false
and num_users > 5
and total_size_gb < 5
-- )
-- where --cohort = 'Cohort 1'
-- --and 
-- has_marketo_unsubscribed <> true
-- and email not like '%gg.com'
-- and email not like '%163.com'
-- and email not like '%protonmail.com'
-- )

-- select 
-- contact_output.*,
-- mart_crm_account.parent_crm_account_sales_segment,
-- mart_crm_account.parent_crm_account_max_family_employee,
-- mart_crm_account.crm_account_owner
-- from
-- contact_output
-- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on contact_output.dim_crm_account_id = mart_crm_account.dim_crm_account_id
-- where
-- mart_crm_account.crm_account_owner like '%SMB Sales'
-- and (mart_crm_account.parent_crm_account_max_family_employee <= 100
-- or mart_crm_account.parent_crm_account_max_family_employee is null)
