/*
Pulls CW FO counts by Fiscal Quarter for FY24
Separates all SaaS FO into buckets:
Buy Now - purchased within 1 day of namespace creation
Trial - purchased from Trial namespace from 2-40 days after trial start
Free - purchased from Free namespace at least 2 days after namespace creation
Storage
Self-Managed is included as a bucket for totaling purposes
LT_Segment field is either “FO” or “false” to specify FO AEs
*/

with dim_namespace as
(
    select * from
    common.dim_namespace
),

FCT_TRIAL_LATEST as
(
    select * from
    PROD.COMMON.FCT_TRIAL_LATEST
),

bdg_namespace_order_subscription as
(
    select * from
prod.common.bdg_namespace_order_subscription
),

dim_crm_user AS (
  SELECT *
  FROM prod.common.dim_crm_user
),

dim_date AS (
  SELECT *
  FROM prod.common.dim_date
),

dim_order AS (
  SELECT *
  FROM prod.common.dim_order
),

dim_order_action AS (
  SELECT *
  FROM prod.common.dim_order_action
),

dim_product_detail AS (
  SELECT *
  FROM prod.common.dim_product_detail
),

dim_subscription AS (
  SELECT *
  FROM prod.common.dim_subscription
),

mart_crm_task AS (
  SELECT *
  FROM prod.common_mart_sales.mart_crm_task
),

mart_arr AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_arr
),

mart_charge AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_charge
),

mart_crm_account AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_account
),

mart_crm_opportunity AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_opportunity
),

dim_crm_account_daily_snapshot AS (
  SELECT *
  FROM prod.restricted_safe_common.dim_crm_account_daily_snapshot
),

mart_delta_arr_subscription_month AS (
  SELECT *
  FROM prod.restricted_safe_legacy.mart_delta_arr_subscription_month
),

PREP_CRM_CASE as
(
select * from
PROD.COMMON_PREP.PREP_CRM_CASE
),

case_history as
(
select * from
RAW.SALESFORCE_V2_STITCH.CASEHISTORY
),


namespaces AS 
(SELECT DISTINCT
  namespace.dim_namespace_id as namespace_id,
  namespace.creator_id,
  created_at as namespace_created_at,
  CASE is_setup_for_company
    WHEN TRUE THEN 'True'
    WHEN FALSE THEN 'False'
    ELSE 'None'
  END AS company_setup_filter,
  namespace.visibility_level AS namespace_visibility_level -- bringing in the visibility level for namespaces
FROM dim_namespace namespace 
WHERE NAMESPACE_IS_ULTIMATE_PARENT -- this ensures the subgroup = group namespace 
AND namespace_is_internal = FALSE -- this blocks internal namespaces
),
  

opportunity_data AS (
  SELECT
    mart_crm_opportunity.*,
    user.user_role_type,
    dim_date.fiscal_year                                                              AS date_range_year,
    dim_date.fiscal_quarter_name_fy                                                   AS date_range_quarter,
    DATE_TRUNC(MONTH, dim_date.date_actual)                                           AS date_range_month,
    dim_date.first_day_of_week                                                        AS date_range_week,
    dim_date.date_id                                                                  AS date_range_id,
    dim_date.fiscal_month_name_fy,
    dim_date.fiscal_quarter_name_fy,
    dim_date.fiscal_year,
    dim_date.first_day_of_fiscal_quarter,
    CASE
      WHEN product_category LIKE '%Self%' OR product_details LIKE '%Self%' OR product_category LIKE '%Starter%'
        OR product_details LIKE '%Starter%' THEN 'Self-Managed'
      WHEN product_category LIKE '%SaaS%' OR product_details LIKE '%SaaS%' OR product_category LIKE '%Bronze%'
        OR product_details LIKE '%Bronze%' OR product_category LIKE '%Silver%' OR product_details LIKE '%Silver%'
        OR product_category LIKE '%Gold%' OR product_details LIKE '%Gold%' THEN 'SaaS'
      WHEN product_details NOT LIKE '%SaaS%'
        AND (product_details LIKE '%Premium%' OR product_details LIKE '%Ultimate%') THEN 'Self-Managed'
      WHEN product_category LIKE '%Storage%' OR product_details LIKE '%Storage%' THEN 'Storage'
      ELSE 'Other'
    END                                                                               AS delivery,
    CASE
      WHEN order_type LIKE '3%' OR order_type LIKE '2%' THEN 'Growth'
      WHEN order_type LIKE '1%' THEN 'First Order'
      WHEN order_type LIKE '4%' OR order_type LIKE '5%' OR order_type LIKE '6%' THEN 'Churn / Contraction'
    END                                                                               AS order_type_clean,
    COALESCE (order_type LIKE '5%' AND net_arr = 0, FALSE)                            AS partial_churn_0_narr_flag,
    CASE
      WHEN product_category LIKE '%Premium%' OR product_details LIKE '%Premium%' THEN 'Premium'
      WHEN product_category LIKE '%Ultimate%' OR product_details LIKE '%Ultimate%' THEN 'Ultimate'
      WHEN product_category LIKE '%Bronze%' OR product_details LIKE '%Bronze%' THEN 'Bronze'
      WHEN product_category LIKE '%Starter%' OR product_details LIKE '%Starter%' THEN 'Starter'
      WHEN product_category LIKE '%Storage%' OR product_details LIKE '%Storage%' THEN 'Storage'
      WHEN product_category LIKE '%Silver%' OR product_details LIKE '%Silver%' THEN 'Silver'
      WHEN product_category LIKE '%Gold%' OR product_details LIKE '%Gold%' THEN 'Gold'
      WHEN product_category LIKE 'CI%' OR product_details LIKE 'CI%' THEN 'CI'
      WHEN product_category LIKE '%omput%' OR product_details LIKE '%omput%' THEN 'CI'
      WHEN product_category LIKE '%Duo%' OR product_details LIKE '%Duo%' THEN 'Duo Pro'
      WHEN product_category LIKE '%uggestion%' OR product_details LIKE '%uggestion%' THEN 'Duo Pro'
      WHEN product_category LIKE '%Agile%' OR product_details LIKE '%Agile%' THEN 'Enterprise Agile Planning'
      ELSE product_category
    END                                                                               AS product_tier,
    COALESCE (LOWER(product_details) LIKE ANY ('%code suggestions%', '%duo%'), FALSE) AS duo_flag,
    COALESCE (opportunity_name LIKE '%QSR%', FALSE)                                   AS qsr_flag,
    CASE
      WHEN order_type LIKE '7%' AND qsr_flag = FALSE THEN 'PS/CI/CD'
      WHEN order_type LIKE '1%' AND net_arr > 0 THEN 'First Order'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr > 0 AND sales_type != 'Renewal'
        AND qsr_flag = FALSE THEN 'Growth - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr > 0 AND sales_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr = 0 AND sales_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%', '7%') AND net_arr < 0 AND sales_type != 'Renewal'
        AND qsr_flag = TRUE THEN 'QSR - Contraction'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr > 0 AND sales_type = 'Renewal'
        THEN 'Renewal - Uplift'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr < 0 AND sales_type != 'Renewal'
        THEN 'Non-Renewal - Contraction'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr = 0 AND sales_type != 'Renewal'
        THEN 'Non-Renewal - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr = 0 AND sales_type = 'Renewal'
        THEN 'Renewal - Flat'
      WHEN order_type LIKE ANY ('2.%', '3.%', '4.%') AND net_arr < 0 AND sales_type = 'Renewal'
        THEN 'Renewal - Contraction'
      WHEN order_type LIKE ANY ('5.%', '6.%') THEN 'Churn'
      ELSE 'Other'
    END                                                                               AS trx_type,
    COALESCE (opportunity_name LIKE '%Startups Program%', FALSE)                      AS startup_program_flag

  FROM mart_crm_opportunity
  LEFT JOIN dim_date
    ON mart_crm_opportunity.close_date = dim_date.date_actual
  LEFT JOIN dim_crm_user AS user
    ON mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

  WHERE ((mart_crm_opportunity.is_edu_oss = 1 AND net_arr > 0) OR mart_crm_opportunity.is_edu_oss = 0)
    AND mart_crm_opportunity.is_jihu_account = FALSE
    AND stage_name NOT LIKE '%Duplicate%'
    --and (opportunity_category is null or opportunity_category not like 'Decom%')
    AND partial_churn_0_narr_flag = FALSE
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

)

--select * from opportunity_data;
,

/*
We pull all the Pooled cases, using the record type ID.

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/

case_data as
(
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
PREP_CRM_CASE.*
 ,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.user_role_type
from PREP_CRM_CASE
left join dim_crm_user on PREP_CRM_CASE.dim_crm_user_id = dim_crm_user.dim_crm_user_id

where
PREP_CRM_CASE.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and PREP_CRM_CASE.created_date >= '2024-02-01'
--and prep_crm_caseis_closed
-- and (prep_crm_casereason not like '%Spam%' or reason is null)
),

get_case_owner_history as
(
select 
iff(first_value(OLDVALUE) over(partition by caseid order by createddate asc) is null
or first_value(OLDVALUE) over(partition by caseid order by createddate asc) = '00G8X000006WmU3UAK',
first_value(NEWVALUE) over(partition by caseid order by createddate asc),
first_value(OLDVALUE) over(partition by caseid order by createddate asc))
as dim_crm_user_id_first_owner,
date(first_value(createddate) over(partition by caseid order by createddate asc)) as first_change_date,
first_value(NEWVALUE) over(partition by caseid,date(createddate) order by createddate desc) as dim_crm_user_id_last_owner_on_date,
date(lag(createddate,1) over(partition by caseid order by createddate asc)) as prior_owner_change_date,
date(lead(createddate,1) over(partition by caseid order by createddate asc)) as next_owner_change_date,
--first_value(NEWVALUE) over(partition by caseid order by createddate asc) as user_name_first_owner,
case_data.dim_crm_user_id as dim_crm_user_id_current_owner,
case_history.* EXCLUDE createddate,
date(createddate) as change_date
from case_history
inner join case_data on case_history.caseid = case_data.case_id
 where 
 field = 'Owner'
and datatype = 'EntityId'
--and case_id = '500PL000008CLxBYAW'
),

case_owner_logic as
(
select
distinct
dim_date.date_actual,
case_data.case_id,
case_data.created_date::date as created_date,
get_case_owner_history.change_date,
get_case_owner_history.dim_crm_user_id_first_owner,
get_case_owner_history.first_change_date,
get_case_owner_history.prior_owner_change_date,
get_case_owner_history.next_owner_change_date,
get_case_owner_history.dim_crm_user_id_last_owner_on_date,
case 
when get_case_owner_history.caseid is null then case_data.dim_crm_user_id
else
case
when date_actual < created_date then null
when date_actual >= created_date and date_actual < first_change_date then dim_crm_user_id_first_owner
when date_actual >= created_date and date_actual >= change_date and (date_actual < next_owner_change_date or next_owner_change_date is null) then dim_crm_user_id_last_owner_on_date
else null end
end
as dim_crm_user_id_owner_on_date
from
dim_date
left join case_data
left join get_case_owner_history on get_case_owner_history.caseid = case_data.case_id
--and change_date = date_actual
where
dim_date.fiscal_year = 2025
--and case_id = '500PL000005mnhTYAQ'
order by 1 asc
),

case_history_output as
(
select
distinct
date_actual,
case_id,
dim_crm_user_id_first_owner,
created_date,
dim_crm_user_id_owner_on_date
from
case_owner_logic
where
(dim_crm_user_id_owner_on_date is not null or date_actual < created_date)
order by 1 asc
),

with task_data as (
WITH base_data AS (
  SELECT *
  FROM (
    SELECT *
  FROM (
    SELECT
      task_id,
      task_status,
      task.dim_crm_account_id,
      task.dim_crm_user_id,
      task.dim_crm_person_id,
      task_type,
      task_subject,
      CASE
        WHEN task_subject LIKE '%Outreach%' AND task_subject NOT LIKE '%Advanced Outreach%' THEN 'Outreach'
        WHEN task_subject LIKE '%Clari%' THEN 'Clari'
        WHEN task_subject LIKE '%Conversica%' THEN 'Conversica'
        ELSE 'Other'
      END AS outreach_clari_flag,
      -------flagging groove tasks
      CASE
        WHEN (contains(lower(task_subject), '[inbox]') or contains(lower(task_subject), '[flow]')) AND outreach_clari_flag = 'Other' then TRUE else FALSE end as groove_flag,
      --------categorizing groove tasks
            CASE
        WHEN groove_flag = True and task_type = 'Call' then 'Groove Call'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Flow]') then 'Groove Flow Email'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Inbox]') then 'Groove Inbox Email'
        WHEN groove_flag = True and task_type = 'Call' and contains(task_subject, 'Flow') then 'Groove Flow Call'
        End as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      CASE
        WHEN groove_flag = TRUE and contains(lower(task_subject), '<<') then 'Inbound' 
        WHEN groove_flag = True and contains(lower(task_subject), '>>') then 'Outbound'
        Else 'Other' 
        END as groove_inbound_outbound_flag,
      --------flagging gong tasks
      CASE 
        WHEN contains(lower(task_subject), 'gong') and outreach_clari_flag = 'Other' then TRUE Else FALSE end as gong_flag,
      CASE
        WHEN gong_flag = True and task_type = 'Meeting' then TRUE 
        WHEN contains(lower(task_subject), '(ii)') then TRUE
        WHEN contains(lower(task_subject), '#meeting') then TRUE 
        WHEN task_type = 'Meeting' then TRUE
        ELSE False End as meeting_flag, 
      -----------Flagging for groove task types
    
      task_created_date,
      task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      CASE
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[Out]%' OR task_subject LIKE '%utbound%')
          THEN 'Outbound'
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[In]%' OR task_subject LIKE '%nbound%')
          THEN 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email received') then 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email sent') then 'Outbound'
        WHEN groove_inbound_outbound_flag = 'Inbound' then 'Inbound'
        WHEN groove_inbound_outbound_flag = 'Outbound' then 'Outbound'
        ELSE 'Other'
      END                                                                                                                 AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,

      
      task_date,
      user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_TASK AS task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id
    WHERE task.dim_crm_user_id IS NOT NULL
      AND is_deleted = FALSE
      AND task_date >= '2024-02-01'
      AND task_status = 'Completed'

      UNION 

      SELECT 
      event_id as task_id,
      'Completed' as task_status,
      dim_crm_account_id,
      task.dim_crm_user_id,
      dim_crm_person_id,
      event_type as task_type,
      event_subject as task_subject,
      'Other' AS outreach_clari_flag,
      -------flagging groove tasks
      FALSE as groove_flag,
      --------categorizing groove tasks
      'Non-Groove - Event' as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      'Other' as groove_inbound_outbound_flag,
      --------flagging gong tasks
      FALSE as gong_flag,
      TRUE as meeting_flag, 
      -----------Flagging for groove task types
    
      event_created_date as task_created_date,
      event_created_by_id as task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      'Other' AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,
     event_date as task_date,
     user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_EVENT task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id 
    WHERE 
      lower(event_subject) like '%#meeting' or lower(event_subject) like '%#meeting%' or lower(event_subject) like '#meeting%'
      OR lower(event_subject) like '%(ii)%'
      AND event_date >= '2024-02-01'
  )

  WHERE (
      user_role_name LIKE ANY ('%AE%', '%SDR%', '%BDR%', '%Advocate%')
      OR crm_user_sales_segment = 'SMB')
)
  ),
meeting_deduped AS (
  SELECT *,
    CASE 
      WHEN meeting_flag = TRUE THEN
        ROW_NUMBER() OVER (
          PARTITION BY dim_crm_account_id, task_date  -- Partitioning by account AND date
          ORDER BY 
            task_created_date ASC,
            CASE 
              WHEN task_type = 'Meeting' THEN 1
              ELSE 2 
            END
        )
      ELSE 1  -- Non-meetings get a rank of 1 so they're always kept
    END as meeting_rank
  FROM base_data
)
SELECT *
FROM meeting_deduped
WHERE meeting_rank = 1) 
,

first_order AS (
  SELECT DISTINCT
    dim_parent_crm_account_id,
    LAST_VALUE(net_arr) OVER (PARTITION BY dim_parent_crm_account_id ORDER BY close_date ASC)     AS net_arr,
    LAST_VALUE(close_date) OVER (PARTITION BY dim_parent_crm_account_id ORDER BY close_date ASC)  AS close_date,
    LAST_VALUE(fiscal_year) OVER (PARTITION BY dim_parent_crm_account_id ORDER BY close_date ASC) AS fiscal_year,
    LAST_VALUE(sales_qualified_source_name)
      OVER (PARTITION BY dim_parent_crm_account_id ORDER BY close_date ASC)                       AS sqs
  FROM mart_crm_opportunity
  LEFT JOIN dim_date
    ON mart_crm_opportunity.close_date = dim_date.date_actual
  WHERE is_won
    AND order_type = '1. New - First Order'
),

latest_churn AS (
  SELECT
    snapshot_date                                                                               AS close_date,
    dim_crm_account_id,
    carr_this_account,
    LAG(carr_this_account, 1) OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC) AS prior_carr,
    -prior_carr                                                                                 AS net_arr
    ,
    MAX(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END) OVER (PARTITION BY dim_crm_account_id)                                                 AS last_carr_date
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date >= '2019-02-01'
    AND snapshot_date = DATE_TRUNC('month', snapshot_date)
  -- and dim_crm_account_id = '0014M00001lbBt1QAE'
  QUALIFY
    prior_carr > 0
    AND carr_this_account = 0
    AND snapshot_date > last_carr_date
),

high_value_case AS  (SELECT 
    *
    FROM (SELECT
    dim_crm_account_id,
    case_id,
    prep_crm_case.dim_crm_user_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
    MAX(prep_crm_case.created_date) over(partition by dim_crm_ACCOUNT_ID) as last_high_value_date
    FROM prep_crm_case
    LEFT JOIN common.dim_crm_user 
        ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
--    and account_id = '0014M00001gTGESQA4'
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date 
),

start_values AS ( -- This is a large slow to query table
  SELECT
    dim_crm_account_id,
    carr_account_family,
    carr_this_account,
    parent_crm_account_lam_dev_count,
    pte_score,
    ptc_score
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date = '2024-02-10' -----placeholder date for start of year
),

first_high_value_case AS (
  SELECT *
  FROM (
    SELECT
dim_crm_account_id,
    case_id,
    prep_crm_case.dim_crm_user_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
      MIN(prep_crm_case.created_date) OVER (PARTITION BY dim_crm_account_id) AS first_high_value_date
    FROM prep_crm_case
    LEFT JOIN dim_crm_user
      ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id IN ('0128X000001pPRkQAM')
      AND LOWER(subject) LIKE '%high value account%'
  ) -----subject placeholder - this could change
  WHERE created_date = first_high_value_date
),

eoa_cohorts AS (
   select distinct dim_crm_account_id from
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
))),

free_promo AS (
  SELECT DISTINCT dim_crm_account_id
  FROM mart_charge
  WHERE subscription_start_date >= '2023-02-01'
    AND rate_plan_charge_description = 'fo-discount-70-percent'
),

price_increase AS (
  SELECT DISTINCT dim_crm_account_id
  FROM (
    SELECT
      charge.*,
      arr / NULLIFZERO(quantity)     AS actual_price,
      prod.annual_billing_list_price AS list_price
    FROM mart_charge AS charge
    INNER JOIN dim_product_detail AS prod
      ON charge.dim_product_detail_id = prod.dim_product_detail_id
    WHERE subscription_start_date >= '2023-04-01'
      AND subscription_start_date <= '2023-07-01'
      AND type_of_arr_change = 'New'
      AND quantity > 0
      AND actual_price > 228
      AND actual_price < 290
      AND rate_plan_charge_name LIKE '%Premium%'
  )
),

ultimate AS (
  SELECT DISTINCT
    dim_parent_crm_account_id,
    arr_month
  FROM mart_arr
  WHERE product_tier_name LIKE '%Ultimate%'
    AND arr > 0
),

amer_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('AMER SMB Sales', 'APAC SMB Sales')
    OR owner_role = 'Advocate_SMB_AMER'
),

emea_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('EMEA SMB Sales')
    OR owner_role = 'Advocate_SMB_EMEA'
),

account_base AS (
  SELECT
    acct.*,
    CASE
      WHEN first_high_value_case.case_id IS NOT NULL THEN 'Tier 1'
      WHEN first_high_value_case.case_id IS NULL
        THEN
          CASE
            WHEN acct.carr_this_account > 7000 THEN 'Tier 1'
            WHEN acct.carr_this_account < 3000 AND acct.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
            ELSE 'Tier 2'
          END
    END                                                              AS calculated_tier,
    CASE
      WHEN (acct.snapshot_date >= '2024-02-01' AND amer_accounts.dim_crm_account_id IS NOT NULL
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('AMER SMB Sales', 'APAC SMB Sales')
            OR owner_role = 'Advocate_SMB_AMER'
        )*/)
      OR (
        acct.snapshot_date < '2024-02-01' AND (
          acct.parent_crm_account_geo IN ('AMER', 'APJ', 'APAC')
          OR acct.parent_crm_account_region IN ('AMER', 'APJ', 'APAC')
        )
      ) THEN 'AMER/APJ'
      WHEN (acct.snapshot_date >= '2024-02-01' AND emea_accounts.dim_crm_account_id IS NOT NULL
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('EMEA SMB Sales')
            OR owner_role = 'Advocate_SMB_EMEA'
        )*/)
      OR (
        acct.snapshot_date < '2024-02-01' AND (
          acct.parent_crm_account_geo IN ('EMEA')
          OR acct.parent_crm_account_region IN ('EMEA')
        )
      ) THEN 'EMEA'
      ELSE 'Other'
    END                                                              AS team,
    COALESCE ((acct.snapshot_date >= '2024-02-01' AND (emea_accounts.dim_crm_account_id IS NOT NULL OR amer_accounts.dim_crm_account_id IS NOT NULL)
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('AMER SMB Sales', 'APAC SMB Sales', 'EMEA SMB Sales')
            OR owner_role LIKE 'Advocate%'
        )*/)
    OR
    (
      acct.snapshot_date < '2024-02-01'
      AND (acct.carr_account_family <= 30000 --and acct.carr_this_account > 0
      )
      AND (
        acct.parent_crm_account_max_family_employee <= 100
        OR acct.parent_crm_account_max_family_employee IS NULL
      )
      AND ultimate.dim_parent_crm_account_id IS NULL
      AND acct.parent_crm_account_sales_segment IN ('SMB', 'Mid-Market', 'Large')
      AND acct.parent_crm_account_upa_country != 'JP'
      AND acct.is_jihu_account = FALSE
    ), FALSE)                                                        AS gds_account_flag,
    fo.fiscal_year                                                   AS fo_fiscal_year,
    fo.close_date                                                    AS fo_close_date,
    fo.net_arr                                                       AS fo_net_arr,
    fo.sqs                                                           AS fo_sqs,
    churn.close_date                                                 AS churn_close_date,
    churn.net_arr                                                    AS churn_net_arr,
    NOT COALESCE (fo_fiscal_year <= 2024, FALSE)                     AS new_fy25_fo_flag,
    first_high_value_case.created_date                               AS first_high_value_case_created_date,
    high_value_case.case_owner_name                                  AS high_value_account_owner,
    --high_value_case.team as high_value_account_team,
    high_value_case.manager_name                                     AS high_value_manager_name,
    high_value_case.case_id                                          AS high_value_case_id,
    start_values.carr_account_family                                 AS starting_carr_account_family,
    start_values.carr_this_account                                   AS starting_carr_this_account,
    CASE
      WHEN start_values.carr_this_account > 7000 THEN 'Tier 1'
      WHEN start_values.carr_this_account < 3000 AND start_values.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
      ELSE 'Tier 2'
    END                                                              AS starting_calculated_tier,
    start_values.pte_score                                           AS starting_pte_score,
    start_values.ptc_score                                           AS starting_ptc_score,
    start_values.parent_crm_account_lam_dev_count                    AS starting_parent_crm_account_lam_dev_count,
    COALESCE (eoa.dim_crm_account_id IS NOT NULL, FALSE)             AS eoa_flag,
    COALESCE (free_promo.dim_crm_account_id IS NOT NULL, FALSE)      AS free_promo_flag,
    COALESCE (price_increase.dim_crm_account_id IS NOT NULL, FALSE)  AS price_increase_promo_flag,
    COALESCE (ultimate.dim_parent_crm_account_id IS NOT NULL, FALSE) AS ultimate_customer_flag
  FROM dim_crm_account_daily_snapshot AS acct
  ------subquery that gets latest FO data
  LEFT JOIN first_order AS fo
    ON acct.dim_parent_crm_account_id = fo.dim_parent_crm_account_id
  ---------subquery that gets latest churn data
  LEFT JOIN latest_churn AS churn
    ON acct.dim_crm_account_id = churn.dim_crm_account_id
  --------subquery to get high tier case owner
  LEFT JOIN high_value_case
    ON acct.dim_crm_account_id = high_value_case.dim_crm_account_id
  --------------subquery to get start of FY25 values
  LEFT JOIN start_values
    ON acct.dim_crm_account_id = start_values.dim_crm_account_id
  -----subquery to get FIRST high value case
  LEFT JOIN first_high_value_case
    ON acct.dim_crm_account_id = first_high_value_case.dim_crm_account_id
  -----EOA cohort accounts
  LEFT JOIN eoa_cohorts AS eoa
    ON acct.dim_crm_account_id = eoa.dim_crm_account_id
  ------free limit promo cohort accounts
  LEFT JOIN free_promo
    ON acct.dim_crm_account_id = free_promo.dim_crm_account_id
  ------price increase promo cohort accounts
  LEFT JOIN price_increase
    ON acct.dim_crm_account_id = price_increase.dim_crm_account_id
  LEFT JOIN ultimate
    ON acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id
      AND ultimate.arr_month = DATE_TRUNC('month', acct.snapshot_date)
  ----- amer and apac accounts
  LEFT JOIN amer_accounts
    ON acct.dim_crm_account_id = amer_accounts.dim_crm_account_id
  ----- emea emea accounts
  LEFT JOIN emea_accounts
    ON acct.dim_crm_account_id = emea_accounts.dim_crm_account_id
  -------filtering to get current account data
  WHERE acct.carr_this_account > 0
    AND acct.snapshot_date >= '2019-12-01'
-- and(((acct.snapshot_date < '2024-02-01' or acct.snapshot_date >= '2024-03-01')
-- and  acct.snapshot_date = date_trunc('month',acct.snapshot_date))
-- or (date_trunc('month',acct.snapshot_date) = '2024-02-01' and acct.snapshot_date = current_date))
--and acct.dim_crm_account_id = '0014M00001ldLdiQAE'
)

--SELECT * from account_base;
,

opp AS (
  SELECT *
  FROM mart_crm_opportunity
  WHERE ((mart_crm_opportunity.is_edu_oss = 1 AND net_arr > 0) OR mart_crm_opportunity.is_edu_oss = 0)
    AND mart_crm_opportunity.is_jihu_account = FALSE
    AND stage_name NOT LIKE '%Duplicate%'
    AND (opportunity_category IS NULL OR opportunity_category NOT LIKE 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)
),

upgrades AS ( 
  select distinct dim_crm_opportunity_id from
(
SELECT
   arr_month,
  type_of_arr_change,
  arr.product_category[0] as upgrade_product,
  previous_month_product_category[0] as prior_product,
  opp.is_web_portal_purchase,
  arr.dim_crm_account_id, 
  opp.dim_crm_opportunity_id,
  SUM(beg_arr)                      AS beg_arr,
  SUM(end_arr)                      AS end_arr,
  SUM(end_arr) - SUM(beg_arr)       AS delta_arr,
  SUM(seat_change_arr)              AS seat_change_arr,
  SUM(price_change_arr)             AS price_change_arr,
  SUM(tier_change_arr)              AS tier_change_arr,
  SUM(beg_quantity)                 AS beg_quantity,
  SUM(end_quantity)                 AS end_quantity,
  SUM(seat_change_quantity)         AS delta_seat_change,
  COUNT(*)                          AS nbr_customers_upgrading
FROM restricted_safe_legacy.mart_delta_arr_subscription_month arr
left join 
(
SELECT
*
FROM restricted_safe_common_mart_sales.mart_crm_opportunity
WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

)
 opp
  on (arr.DIM_CRM_ACCOUNT_ID = opp.DIM_CRM_ACCOUNT_ID
  and arr.arr_month = date_trunc('month',opp.subscription_start_date)
 -- and opp.order_type = '3. Growth'
  and opp.is_won)
WHERE 
 (ARRAY_CONTAINS('Self-Managed - Starter'::VARIANT, previous_month_product_category)
          OR ARRAY_CONTAINS('SaaS - Bronze'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('SaaS - Premium'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('Self-Managed - Premium'::VARIANT, previous_month_product_category)
       )
   AND tier_change_arr > 0
GROUP BY 1,2,3,4,5,6,7
))

--select * from upgrades;
,

promo_data_actual_price AS (
  SELECT
    mart_charge.*,
    dim_subscription.dim_crm_opportunity_id,
    MAX(arr) OVER (PARTITION BY mart_charge.dim_subscription_id)      AS actual_arr,
    MAX(quantity) OVER (PARTITION BY mart_charge.dim_subscription_id) AS actual_quantity,
    actual_arr / NULLIFZERO(actual_quantity)                          AS actual_price
  FROM mart_charge
  LEFT JOIN dim_subscription
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
  WHERE
    -- effective_start_date >= '2023-02-01'
    -- and
    dim_subscription.subscription_start_date >= '2023-04-01'
    AND dim_subscription.subscription_start_date <= '2023-07-01'
    AND type_of_arr_change = 'New'
    AND quantity > 0
    -- and actual_price > 228
    -- and actual_price < 290
    AND rate_plan_charge_name LIKE '%Premium%'
  QUALIFY
    actual_price > 228
    AND actual_price < 290
),

price_increase_promo_fo_data AS (--Gets all FO opportunities associated with Price Increase promo

  SELECT DISTINCT
    dim_crm_opportunity_id,
    actual_price
  FROM promo_data_actual_price
)

--select * from price_increase_promo_fo_data;
,

bronz_starter_accounts AS (
  SELECT dim_crm_account_id
  --,product_rate_plan_name
  FROM mart_arr
  WHERE arr_month >= '2020-02-01'
    AND arr_month <= '2022-02-01'
    AND product_rate_plan_name LIKE ANY ('%Bronze%', '%Starter%')
),

eoa_accounts AS (
  SELECT
    mart_arr.arr_month,
    --,ping_created_at
    mart_arr.subscription_end_month,
    mart_arr.dim_crm_account_id,
    mart_arr.crm_account_name,
    --,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
    mart_arr.dim_subscription_id,
    mart_arr.dim_subscription_id_original,
    mart_arr.subscription_name,
    mart_arr.subscription_sales_type,
    mart_arr.auto_pay,
    mart_arr.default_payment_method_type,
    mart_arr.contract_auto_renewal,
    mart_arr.turn_on_auto_renewal,
    mart_arr.turn_on_cloud_licensing,
    mart_arr.contract_seat_reconciliation,
    mart_arr.turn_on_seat_reconciliation,
    COALESCE (mart_arr.contract_seat_reconciliation = 'Yes' AND mart_arr.turn_on_seat_reconciliation = 'Yes', FALSE) AS qsr_enabled_flag,
    mart_arr.product_tier_name,
    mart_arr.product_delivery_type,
    mart_arr.product_rate_plan_name,
    mart_arr.arr,
    --,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
    (mart_arr.arr / NULLIFZERO(mart_arr.quantity))                                                                   AS arr_per_user,
    arr_per_user / 12                                                                                                AS monthly_price_per_user,
    mart_arr.mrr / NULLIFZERO(mart_arr.quantity)                                                                     AS mrr_check
  FROM mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
  LEFT JOIN bronz_starter_accounts
    ON mart_arr.dim_crm_account_id = bronz_starter_accounts.dim_crm_account_id
  WHERE arr_month = '2023-01-01'
    AND product_tier_name LIKE '%Premium%'
    AND ((
      monthly_price_per_user >= 14
      AND monthly_price_per_user <= 16
    ) OR (monthly_price_per_user >= 7.5 AND monthly_price_per_user <= 9.5
    ))
    AND bronz_starter_accounts.dim_crm_account_id IS NOT NULL
/*dim_crm_account_id IN
                    (
                      SELECT
                        dim_crm_account_id
--,product_rate_plan_name
                      FROM mart_arr
                      WHERE arr_month >= '2020-02-01'
                        AND arr_month <= '2022-02-01'
                        AND product_rate_plan_name LIKE ANY ('%Bronze%', '%Starter%')
                    )*/
--ORDER BY mart_arr.dim_crm_account_id ASC
),

eoa_accounts_fy24 AS (
--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
  SELECT DISTINCT dim_crm_account_id
  FROM eoa_accounts
),

past_eoa_uplift_opportunities AS (
  SELECT
    mart_charge.*,
    dim_subscription.dim_crm_opportunity_id,
    dim_subscription.dim_crm_opportunity_id_current_open_renewal,
    previous_mrr / NULLIFZERO(previous_quantity) AS previous_price,
    mrr / NULLIFZERO(quantity)                   AS price_after_renewal
  FROM mart_charge
  LEFT JOIN dim_subscription
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
  INNER JOIN eoa_accounts_fy24
    ON mart_charge.dim_crm_account_id = eoa_accounts_fy24.dim_crm_account_id
  -- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
  WHERE mart_charge.rate_plan_name LIKE '%Premium%'
    -- and
    -- mart_charge.term_start_date <= '2024-02-01'
    AND type_of_arr_change != 'New'
    AND mart_charge.term_start_date >= '2022-02-01'
    AND price_after_renewal > previous_price
    AND previous_quantity != 0
    AND quantity != 0
    AND (
      LOWER(rate_plan_charge_description) LIKE '%eoa%'
      OR
      (
        (previous_price >= 5 AND previous_price <= 7)
        OR (previous_price >= 8 AND previous_price <= 10)
        OR (previous_price >= 14 AND previous_price <= 16)
      )
    )
),

past_eoa_uplift_opportunity_data AS (--Gets all opportunities associated with EoA special pricing uplift
  SELECT DISTINCT dim_crm_opportunity_id
  FROM past_eoa_uplift_opportunities
)

--select * from past_eoa_uplift_opportunity_data;
,
free_limit_promo_fo_data AS (--Gets all opportunities associated with Free Limit 70% discount

  SELECT DISTINCT
    dim_crm_opportunity_id
    -- ,
    -- actual_price
  FROM (
    SELECT
      mart_charge.*,
      dim_subscription.dim_crm_opportunity_id,
      MAX(arr) OVER (PARTITION BY mart_charge.dim_subscription_id)      AS actual_arr,
      MAX(quantity) OVER (PARTITION BY mart_charge.dim_subscription_id) AS actual_quantity,
      actual_arr / NULLIFZERO(actual_quantity)                          AS actual_price
    FROM mart_charge
    LEFT JOIN dim_subscription
      ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
    WHERE
      -- effective_start_date >= '2023-02-01'
      -- and
      rate_plan_charge_description LIKE '%70%'
      AND type_of_arr_change = 'New'
  )
)

--select * from free_limit_promo_fo_data;
,

discounted_accounts AS (
  SELECT DISTINCT dim_crm_account_id
  FROM mart_charge
  WHERE subscription_start_date >= '2023-02-01'
    AND rate_plan_charge_description = 'fo-discount-70-percent'
),

open_eoa_renewals AS (
  SELECT
    mart_charge.*,
    dim_subscription.dim_crm_opportunity_id,
    close_date,
    MAX(mart_charge.arr) OVER (PARTITION BY mart_charge.dim_subscription_id)      AS actual_arr,
    MAX(mart_charge.quantity) OVER (PARTITION BY mart_charge.dim_subscription_id) AS actual_quantity,
    actual_arr / NULLIFZERO(actual_quantity)                                      AS actual_price,
    previous_mrr / NULLIFZERO(previous_quantity)                                  AS previous_price,
    mrr / NULLIFZERO(quantity)                                                    AS price_after_renewal,
    dim_subscription.dim_crm_opportunity_id_current_open_renewal
  FROM mart_charge
  LEFT JOIN dim_subscription
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
  LEFT JOIN mart_crm_opportunity
    ON dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
  LEFT JOIN discounted_accounts
    ON mart_charge.dim_crm_account_id = discounted_accounts.dim_crm_account_id
  WHERE mart_crm_opportunity.close_date >= '2024-02-01'
    AND mart_crm_opportunity.is_closed = FALSE
    AND mart_charge.rate_plan_name LIKE '%Premium%'
    -- and
    -- mart_charge.term_start_date <= '2024-02-01'
    AND type_of_arr_change != 'New'
    AND mart_charge.term_start_date >= '2022-02-01'
    AND price_after_renewal > previous_price
    AND previous_quantity != 0
    AND quantity != 0
    AND discounted_accounts.dim_crm_account_id IS NULL
    /*mart_charge.dim_crm_account_id NOT IN
              (
                SELECT DISTINCT
                  dim_crm_account_id
                FROM restricted_safe_common_mart_sales.mart_charge charge
                WHERE subscription_start_date >= '2023-02-01'
                  AND rate_plan_charge_description = 'fo-discount-70-percent'
              )*/
    AND (
      LOWER(rate_plan_charge_description) LIKE '%eoa%'
      OR
      (
        (previous_price >= 5 AND previous_price <= 7)
        OR (previous_price >= 8 AND previous_price <= 10)
        OR (previous_price >= 14 AND previous_price <= 16)
      )
    )
),

open_eoa_renewal_data AS (
--Gets all future renewal opportunities where the account currently has EoA special pricing


  SELECT DISTINCT
    dim_crm_opportunity_id_current_open_renewal
    -- ,
    -- price_after_renewal,
    -- close_date
  FROM open_eoa_renewals


)

--select * from open_eoa_renewal_data;
,
renewal_self_service_data AS (--Uses the Order Action to determine if a Closed Won renewal was Autorenewed, Sales-Assisted, or Manual Portal Renew by the customer

  SELECT
    -- dim_order_action.dim_subscription_id,
    -- dim_subscription.subscription_name,
    -- dim_order_action.contract_effective_date,
    COALESCE (order_description != 'AutoRenew by CustomersDot', FALSE) AS manual_portal_renew_flag,
    -- mart_crm_opportunity.is_web_portal_purchase,
    mart_crm_opportunity.dim_crm_opportunity_id,
    CASE
      WHEN manual_portal_renew_flag AND is_web_portal_purchase THEN 'Manual Portal Renew'
      WHEN manual_portal_renew_flag = FALSE AND is_web_portal_purchase THEN 'Autorenew'
      ELSE 'Sales-Assisted Renew'
    END                                                                AS actual_manual_renew_flag
  FROM dim_order_action
  LEFT JOIN dim_order
    ON dim_order_action.dim_order_id = dim_order.dim_order_id
  LEFT JOIN dim_subscription
    ON dim_order_action.dim_subscription_id = dim_subscription.dim_subscription_id
  LEFT JOIN mart_crm_opportunity
    ON dim_subscription.dim_crm_opportunity_id = mart_crm_opportunity.dim_crm_opportunity_id
  WHERE order_action_type = 'RenewSubscription'
)

--select * from renewal_self_service_data;
,

price_increase_promo_renewal_open AS (
  SELECT
    charge.*,
    arr / NULLIFZERO(quantity)     AS actual_price,
    prod.annual_billing_list_price AS list_price,
    dim_subscription.dim_crm_opportunity_id_current_open_renewal
  FROM mart_charge AS charge
  INNER JOIN dim_product_detail AS prod
    ON charge.dim_product_detail_id = prod.dim_product_detail_id
  INNER JOIN dim_subscription
    ON charge.dim_subscription_id = dim_subscription.dim_subscription_id
  WHERE charge.term_start_date >= '2023-04-01'
    AND charge.term_start_date <= '2024-05-01'
    AND charge.type_of_arr_change != 'New'
    AND charge.subscription_start_date < '2023-04-01'
    AND charge.quantity > 0
    AND actual_price > 228
    AND actual_price < 290
    AND charge.rate_plan_charge_name LIKE '%Premium%'
),

price_increase_promo_renewal_open_data AS (
  SELECT DISTINCT dim_crm_opportunity_id_current_open_renewal
  FROM price_increase_promo_renewal_open

)
--SELECT * FROM price_increase_promo_renewal_open_data;
,
case_task_summary_data AS (
  SELECT
    opportunity_data.dim_crm_opportunity_id                                                       AS case_task_summary_id,

    LISTAGG(DISTINCT case_data.case_trigger, ', ') WITHIN GROUP (ORDER BY case_data.case_trigger)
      AS oppty_trigger_list,
    LISTAGG(DISTINCT case_data.case_id, ', ')
      AS oppty_case_id_list,

    COUNT(DISTINCT case_data.case_id)                                                             AS closed_case_count,
    COUNT(DISTINCT CASE
      WHEN case_data.status = 'Closed: Resolved' OR case_data.status = 'Closed'
        THEN case_data.case_id
    END)                                                                                          AS resolved_case_count,

    COUNT(DISTINCT CASE
      WHEN task_data.inbound_outbound_flag = 'Inbound' THEN task_data.task_id
    END)                                                                                          AS inbound_email_count,
    COUNT(DISTINCT CASE
      WHEN task_data.outbound_answered_flag THEN task_data.task_id
    END)                                                                                          AS completed_call_count,
    COUNT(DISTINCT CASE
      WHEN task_data.inbound_outbound_flag = 'Outbound' THEN task_data.task_id
    END)                                                                                          AS outbound_email_count,
    COALESCE (inbound_email_count > 0, FALSE)                                                     AS task_inbound_flag,
    COALESCE (completed_call_count > 0, FALSE)                                                    AS task_completed_call_flag,
    COALESCE (outbound_email_count > 0, FALSE)                                                    AS task_outbound_flag,
    COUNT(DISTINCT task_data.task_id)                                                             AS completed_task_count

  FROM opportunity_data
  LEFT JOIN case_data
    ON
      opportunity_data.dim_crm_account_id = case_data.dim_crm_account_id
      AND
      (
        (
          trx_type = 'First Order' AND opportunity_data.close_date >= case_data.created_date
          AND case_data.status = 'Closed: Resolved' AND case_data.created_date >= '2024-02-01'
        )
        OR
        (
          trx_type LIKE ANY ('%Growth%', '%QSR%') AND opportunity_data.close_date >= case_data.closed_date
          AND case_data.status = 'Closed: Resolved' AND case_data.closed_date >= opportunity_data.close_date - 90
          AND case_data.created_date >= '2024-02-01'
        )
        OR
        (
          trx_type LIKE ANY ('Renewal%', 'Churn') AND case_data.status = 'Closed: Resolved'
          AND case_data.closed_date <= opportunity_data.close_date + 30 AND case_data.created_date >= '2024-02-01'
        )
      )
  LEFT JOIN task_data
    ON
      opportunity_data.dim_crm_account_id = task_data.dim_crm_account_id
      AND
      (
        (
          trx_type = 'First Order' AND opportunity_data.close_date >= task_data.task_date
          AND task_data.task_date >= '2023-02-01'
        )
        OR
        (
          trx_type LIKE ANY ('%Growth%', '%QSR%') AND opportunity_data.close_date >= task_data.task_date
          AND task_data.task_date >= opportunity_data.close_date - 90 AND task_data.task_date >= '2023-02-01'
        )
        OR
        (
          trx_type LIKE ANY ('Renewal%', 'Churn') AND task_data.task_date <= opportunity_data.close_date + 30
          AND task_data.task_date >= '2023-02-01' AND task_data.task_date >= opportunity_data.close_date - 365
        )
      )
  --where case_data.account_id is not null
  GROUP BY 1
)
--select * from case_task_summary_data;
,
last_carr_data AS (
  SELECT
    snapshot_date,
    dim_crm_account_id,
    MAX(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END)
      OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS last_carr_date,
    MIN(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END) OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC)                                               AS first_carr_date
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date >= '2019-12-01'
    AND crm_account_type != 'Prospect'
  -- and crm_account_name like 'Natural Substances%'
  QUALIFY last_carr_date IS NOT NULL
)
--SELECT * FROM last_carr_data;
,
full_base_data AS (
  SELECT
    opportunity_data.*,
    account_base.calculated_tier,
    CASE
      WHEN account_base.team IS NULL OR account_base.team = 'Other'
        THEN
          case when opportunity_data.parent_crm_account_geo in ('AMER','APJ','APAC') 
or opportunity_data.parent_crm_account_region in ('AMER','APJ','APAC')
or opportunity_data.crm_opp_owner_area_stamped in ('AMER','APJ','APAC')
then 'AMER/APJ'
when opportunity_data.parent_crm_account_geo in ('EMEA') 
or opportunity_data.parent_crm_account_region in ('EMEA') 
or opportunity_data.crm_opp_owner_area_stamped in ('EMEA')
then 'EMEA'

            ELSE account_base.team
          END
      ELSE account_base.team
    END                                                                                                              AS team
    ,
    account_base.fo_fiscal_year,
    account_base.fo_close_date,
    account_base.fo_net_arr,
    account_base.fo_sqs,
    account_base.churn_net_arr,
    account_base.churn_close_date,
    account_base.new_fy25_fo_flag,
    account_base.high_value_account_owner,
    --account_base.high_value_account_team,
    account_base.high_value_manager_name,
    account_base.eoa_flag                                                                                            AS eoa_account_flag,
    account_base.free_promo_flag                                                                                     AS free_promo_account_flag,
    account_base.price_increase_promo_flag                                                                           AS price_increase_promo_account_flag,
    account_base.crm_account_owner,
    account_base.owner_role,
    account_base.account_tier,
    account_base.gds_account_flag,
    COALESCE ((
      opportunity_data.close_date >= '2024-02-01'
      -- not(opportunity_data.opportunity_owner like any ('%Taylor Lund%','%Miguel Nunes%','%Kazem Kutob%'))
      -- and
      AND (
        crm_opp_owner_sales_segment_stamped = 'SMB'
        OR (
          crm_opp_owner_sales_segment_stamped IS NULL
          AND (
            sales_type = 'New Business' OR crm_account_owner LIKE '%SMB Sales%'
            OR account_base.owner_role LIKE 'Advocate%'
          )
          AND account_base.carr_account_family <= 30000
          AND (
            account_base.parent_crm_account_max_family_employee <= 100
            OR account_base.parent_crm_account_max_family_employee IS NULL
          )
        )
      )
    )
    OR
    (
      opportunity_data.close_date < '2024-02-01'
      AND gds_account_flag
      AND (
        ultimate_customer_flag = FALSE OR upgrades.dim_crm_opportunity_id IS NOT NULL
        OR trx_type = 'First Order'
      )
    ), FALSE)                                                                                                        AS gds_oppty_flag,
    gs_health_user_engagement,
    gs_health_cd,
    gs_health_devsecops,
    gs_health_ci,
    gs_health_scm,
    carr_account_family,
    carr_this_account,
    pte_score,
    ptc_score,
    ptc_predicted_arr                                                                                                AS ptc_predicted_arr__c,
    ptc_predicted_renewal_risk_category                                                                              AS ptc_predicted_renewal_risk_category__c,
    -- upgrades.prior_product,
    -- upgrades.upgrade_product,
    COALESCE (upgrades.dim_crm_opportunity_id IS NOT NULL, FALSE)                                                    AS upgrade_flag,
    COALESCE (price_increase_promo_fo_data.dim_crm_opportunity_id IS NOT NULL, FALSE)                                AS price_increase_promo_fo_flag,
    COALESCE (free_limit_promo_fo_data.dim_crm_opportunity_id IS NOT NULL, FALSE)                                    AS free_limit_promo_fo_flag,
    COALESCE (past_eoa_uplift_opportunity_data.dim_crm_opportunity_id IS NOT NULL, FALSE)                            AS past_eoa_uplift_opportunity_flag,
    COALESCE (open_eoa_renewal_data.dim_crm_opportunity_id_current_open_renewal IS NOT NULL, FALSE)                  AS open_eoa_renewal_flag,
    COALESCE (price_increase_promo_renewal_open_data.dim_crm_opportunity_id_current_open_renewal IS NOT NULL, FALSE) AS price_increase_promo_open_renewal_flag,
    renewal_self_service_data.actual_manual_renew_flag                                                               AS actual_manual_renew_flag,
    case_task_summary_data.* EXCLUDE (case_task_summary_id),
    dim_subscription.turn_on_auto_renewal,
    ultimate_customer_flag,
    last_carr_date,
    first_carr_date,
    account_base.snapshot_date                                                                                       AS account_snapshot_date,
    last_carr_data.snapshot_date                                                                                     AS last_carr_snapshot_date,
    arr_basis                                                                                                        AS atr,
    case_history_output.dim_crm_user_id_owner_on_date,
    dim_crm_user.user_name                                                                                           AS case_owner_name_on_date,
    CASE
      WHEN trx_type = 'First Order' OR order_type_live LIKE '%First Order%' THEN 'First Order'
      WHEN trx_type IN ('Renewal - Uplift', 'Renewal - Flat') THEN 'Renewal Growth'
      WHEN qsr_flag OR trx_type = 'Growth - Uplift' THEN 'Nonrenewal Growth'
      WHEN trx_type IN ('Churn', 'Renewal - Contraction', 'Non-Renewal - Contraction') THEN 'C&C'
      ELSE 'Other'
    END                                                                                                              AS trx_type_grouping,
    opportunity_data.close_date                                                                                      AS test_date
  FROM opportunity_data
  LEFT JOIN last_carr_data
    ON opportunity_data.dim_crm_account_id = last_carr_data.dim_crm_account_id
      AND ((
        order_type NOT LIKE '%First Order%'
        AND is_closed
        AND
        last_carr_data.snapshot_date = opportunity_data.close_date - 1
      )
      OR
      (
        order_type LIKE '%First Order%'
        AND is_closed
        AND last_carr_data.snapshot_date = last_carr_data.first_carr_date
      )
      OR
      (is_closed = FALSE AND last_carr_data.snapshot_date = CURRENT_DATE)
      )
  LEFT JOIN account_base
    ON opportunity_data.dim_crm_account_id = account_base.dim_crm_account_id
      AND
      (
        (
          is_closed
          AND order_type LIKE '%First Order%'
          AND account_base.snapshot_date = first_carr_date
          AND last_carr_data.dim_crm_account_id = account_base.dim_crm_account_id
        )
        OR
        (
          is_closed
          AND order_type NOT LIKE '%First Order%'
          AND account_base.snapshot_date = last_carr_date
          AND last_carr_data.dim_crm_account_id = account_base.dim_crm_account_id
        )
        OR
        (
          is_closed = FALSE
          AND account_base.snapshot_date = CURRENT_DATE
        )
      )
  LEFT JOIN upgrades
    ON opportunity_data.dim_crm_opportunity_id = upgrades.dim_crm_opportunity_id
  LEFT JOIN price_increase_promo_fo_data
    ON opportunity_data.dim_crm_opportunity_id = price_increase_promo_fo_data.dim_crm_opportunity_id
  LEFT JOIN past_eoa_uplift_opportunity_data
    ON opportunity_data.dim_crm_opportunity_id = past_eoa_uplift_opportunity_data.dim_crm_opportunity_id
  LEFT JOIN free_limit_promo_fo_data
    ON opportunity_data.dim_crm_opportunity_id = free_limit_promo_fo_data.dim_crm_opportunity_id
  LEFT JOIN open_eoa_renewal_data
    ON opportunity_data.dim_crm_opportunity_id = open_eoa_renewal_data.dim_crm_opportunity_id_current_open_renewal
  LEFT JOIN renewal_self_service_data
    ON opportunity_data.dim_crm_opportunity_id = renewal_self_service_data.dim_crm_opportunity_id
  LEFT JOIN case_task_summary_data
    ON
      opportunity_data.dim_crm_opportunity_id = case_task_summary_data.case_task_summary_id
  LEFT JOIN price_increase_promo_renewal_open_data
    ON opportunity_data.dim_crm_opportunity_id
      = price_increase_promo_renewal_open_data.dim_crm_opportunity_id_current_open_renewal
  LEFT JOIN case_history_output on case_history_output.case_id = high_value_case_id
  and case_history_output.date_actual = opportunity_data.close_date
  LEFT JOIN dim_crm_user
    ON case_history_output.dim_crm_user_id_owner_on_date = dim_crm_user.dim_crm_user_id
  LEFT JOIN (select
distinct
dim_crm_opportunity_id_current_open_renewal, 
first_value(turn_on_auto_renewal) over(partition by dim_crm_opportunity_id_current_open_renewal order by turn_on_auto_renewal asc) as TURN_ON_AUTO_RENEWAL
from dim_subscription
where dim_crm_opportunity_id_current_open_renewal is not null) dim_subscription
    ON opportunity_data.dim_crm_opportunity_id = dim_subscription.dim_crm_opportunity_id_current_open_renewal
),
--select * from full_base_data
  

paid_namespace_data as (
select 
    distinct
    dim_namespace_id as paid_namespace_id,
    dim_crm_account_id,
    first_value(dim_subscription_id) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_id,
    first_value(product_tier_name_subscription) over(partition by dim_namespace_id order by subscription_start_date asc) as first_namespace_product,
    first_value(product_tier_name_order) over(partition by dim_namespace_id order by subscription_start_date asc) as first_order_product,
    first_value(subscription_start_date) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_start_date

from 
(select * from
bdg_namespace_order_subscription
where
dim_namespace_id = ultimate_parent_namespace_id
and product_tier_name_namespace not like '%Trial%'
and product_tier_name_namespace not like '%Free%'
and dim_crm_account_id is not null)
),

conversions as (
select
paid_namespace_data.* EXCLUDE dim_crm_account_id,
opps.*
from full_base_data opps
left join paid_namespace_data
on paid_namespace_data.dim_crm_account_id = opps.dim_crm_account_id
order by paid_namespace_id asc),

--select * from conversions

trials AS (
  
  SELECT
    dim_namespace_id,
    latest_trial_start_date
  FROM FCT_TRIAL_LATEST
  
), 

combined AS (

SELECT
  namespaces.*,
  conversions.*,
  latest_trial_start_date,
  case when dim_namespace_id is not null then true else false end as trial_flag,
  DATEDIFF(day,namespaces.namespace_created_at,first_subscription_start_date) AS days_from_creation_to_subscription,
  case when days_from_creation_to_subscription = 0 then true else false end as paid_from_free_within_day,
  DATEDIFF(day,namespaces.namespace_created_at,latest_trial_start_date) AS days_from_creation_to_trial,
  case when days_from_creation_to_trial  = 0 then true else false end as trial_from_free_within_day,
  DATEDIFF(day,latest_trial_start_date,first_subscription_start_date) AS days_from_trial_to_paid,
  case when days_from_trial_to_paid  = 0 then true else false end as paid_from_trial_within_day,
  case when days_from_creation_to_subscription < 0 then true else false end as paid_before_create
  --trial_namespace_id CANNOT TRUST THIS
FROM conversions
Left JOIN namespaces ON conversions.paid_namespace_id = namespaces.namespace_id
LEFT JOIN trials ON namespaces.namespace_id = trials.dim_namespace_id
-- Limit to only conversions after the created date (not created and paid at same time) and trials after creation
WHERE (dim_namespace_id is null or latest_trial_start_date >= date_trunc('day',namespace_created_at))
 -- and (first_subscription_start_date >= date_trunc('day',namespace_created_at) or first_subscription_start_date is null)

),

full_data as (
SELECT
  --date_trunc('week',namespace_created_at) as created_date,
  --date_trunc('week',first_subscription_start_date) as close_date,
  combined.* EXCLUDE (namespace_created_at,latest_trial_start_date,trial_flag),
  cast(namespace_created_at as date) as namespace_created_at,
  cast(latest_trial_start_date as date) as latest_trial_start_date,
  case when latest_trial_start_date is not null then true else false end as trial_flag,
  case when close_date is not null then 'Paid' else 'Free' end as is_paid_flag,
  case when days_from_creation_to_subscription <= 30 then TRUE else false end as convert_within_month_flag
FROM combined
where trx_type = 'First Order'
and is_won
  ),

--select * from full_data

assign_bucket as (
SELECT
--date_trunc('week',namespace_created_at + 1)-1 as week,
--  LT_Segment,
-- fiscal_year,
-- fiscal_quarter,  
--  close_month,
-- is_web_portal_purchase,
'Buy Now' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and (close_date >= namespace_created_at and close_date <= namespace_created_at + 1)
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
--    LT_Segment,
--   fiscal_year,
--   fiscal_quarter,  
--  close_month,
--   is_web_portal_purchase,
'Trial Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > latest_trial_start_date
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and trial_flag
and delivery like '%SaaS%'
and product_tier is not null
and datediff('day',latest_trial_start_date,close_date)<= 40
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
--    LT_Segment,
--   fiscal_year,
--   fiscal_quarter,  
--  close_month,
--   is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
and (trial_flag = false or datediff('day',latest_trial_start_date,close_date)>40)
 union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
--    LT_Segment,
--   fiscal_year,
--   fiscal_quarter,  
--  close_month,
--   is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
--is_paid_flag = 'Paid'
--and week > date_trunc('week',namespace_created_at + 1)-1
--is_web_portal_purchase and
delivery like '%SaaS%'
and (namespace_id is null or paid_before_create = true)
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
--    LT_Segment,
--   fiscal_year,
--   fiscal_quarter,  
--  close_month,
--   is_web_portal_purchase,
'S-M' as bucket,
dim_crm_opportunity_id
from conversions
where --is_web_portal_purchase and 
  delivery like '%Self%'
  and trx_type = 'First Order'
and is_won
)

select *
from assign_bucket
