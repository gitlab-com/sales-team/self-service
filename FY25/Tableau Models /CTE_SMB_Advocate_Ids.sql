-------to get SMB advocates 
------placeholder for sales segment 
------is there any other additional fields or filtering we will need for this when the segment is transitioned over? 
with SMB_advocates as (
SELECT 
case when user_name in ('Katie Wilkinson','Matthew Wyman Jr','Amanda Shim','Erica Wilson','Amanda Acosta','Lauren Kaible','Joseph Hudson','Maria Spada Rojas','Theo Aspro','Rasheed Power','Ellie Hickson','Luis Hernandez Calixto','Barbara Schreuder','Tom Elliott','Sarah Van Damme','Bastien Escudé','Hugo Barennes','Emma Szász','Paula Newport','Nga Nguyen') then true else false end as advocate_flag
,
*
FROM PROD.COMMON.DIM_CRM_USER
WHERE 
(CRM_USER_SALES_SEGMENT = 'SMB' or user_name like '%SMB Sales' or title like '%SMB%')
and is_active
and user_name not in ('Sales Admin [DO NOT CHATTER THIS USER]','System','Automated Process','Platform Integration User')
--and user_name like '%SMB%'
and ((title not like '%Manager%' and title not like '%VP%') or title is null)
)
