--Gets all opportunities associated with Free Limit 70% discount

select
distinct dim_crm_opportunity_id,
actual_price
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
max(arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
where
-- effective_start_date >= '2023-02-01'
-- and
rate_plan_charge_description like '%70%'
and type_of_arr_change = 'New'
)
