--Uses the Order Action to determine if a Closed Won renewal was Autorenewed, Sales-Assisted, or Manual Portal Renew by the customer

select dim_order_action.DIM_SUBSCRIPTION_ID,
  dim_subscription.subscription_name,
  dim_order_action.contract_effective_date,
    case when ORDER_DESCRIPTION <> 'AutoRenew by CustomersDot' then true else false end as manual_portal_renew_flag,
    mart_crm_opportunity.is_web_portal_purchase,
    mart_crm_opportunity.dim_crm_opportunity_id,
    case when manual_portal_renew_flag and is_web_portal_purchase then 'Manual Portal Renew'
when manual_portal_renew_flag = false and is_web_portal_purchase then 'Autorenew'
else 'Sales-Assisted Renew' end as actual_manual_renew_flag
from common.dim_order_action
    left join common.dim_order on dim_order_action.dim_order_id = dim_order.dim_order_id
    left join common.dim_subscription on dim_order_action.DIM_SUBSCRIPTION_ID = dim_subscription.DIM_SUBSCRIPTION_ID
    left join restricted_safe_common_mart_sales.mart_crm_opportunity on dim_subscription.dim_crm_opportunity_id = mart_crm_opportunity.dim_crm_opportunity_id
where order_action_type = 'RenewSubscription'
