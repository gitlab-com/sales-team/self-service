with
cdot_errors as (
SELECT 
behavior_at::date as event_date,
behavior_at as event_timestamp,
event_value AS customer_id,
event_property as error_message,
substr(event_property,position('Failed',event_property)+8,position(': {',event_property)-9) as product
 
  FROM 
  PROD.common_mart.mart_behavior_structured_event events
  WHERE events.behavior_at::DATE >= '2024-09-01'
   AND app_id IN ('gitlab','gitlab_customers')
  AND (
    event_category = 'Webstore' 
    OR
    event_category = 'SubscriptionsController'
  )
  and event_property like 'Failed%'
  and event_property not like 'Failed: 1,000%'
  and event_property not like '%Storage%'
),

customers_db_customers as
(select * from PREP.CUSTOMERS.CUSTOMERS_DB_CUSTOMERS_SOURCE),

mart_crm_opportunity as
(select * from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
where is_won and close_date >= '2024-09-01')

select
distinct
cdot_errors.*,
customers_db_customers.sfdc_account_id
from cdot_errors
inner join customers_db_customers on cdot_errors.customer_id = customers_db_customers.customer_id
left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_crm_account.dim_crm_account_id = 
customers_db_customers.sfdc_account_id
left join MART_CRM_OPPORTUNITY
on mart_crm_opportunity.dim_crm_account_id = customers_db_customers.sfdc_account_id
and mart_crm_opportunity.close_date <= dateadd('day',7,cdot_errors.event_date)
and mart_crm_opportunity.close_date >= cdot_errors.event_date
and mart_crm_opportunity.is_won
where mart_crm_account.crm_account_owner like '%SMB Sales'
and mart_crm_opportunity.dim_crm_account_id is null