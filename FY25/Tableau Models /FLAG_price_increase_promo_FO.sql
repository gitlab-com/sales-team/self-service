select distinct dim_crm_account_id from
(
 select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  subscription_start_date >= '2023-04-01'
  and subscription_start_date <= '2023-07-01'
  and TYPE_OF_ARR_CHANGE = 'New'
  and quantity > 0
  and actual_price > 228
  and actual_price < 290
  and rate_plan_charge_name like '%Premium%'
)
