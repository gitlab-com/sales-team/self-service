/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/

with

PREP_CRM_CASE as
(
select * from
PROD.COMMON_PREP.PREP_CRM_CASE
),

dim_crm_user as
(
select * from
PROD.COMMON.DIM_CRM_USER
),

case_history as
(
select * from
RAW.SALESFORCE_V2_STITCH.CASEHISTORY
),

dim_date as
(
select * from
prod.common.dim_date
),



case_data as
(
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
PREP_CRM_CASE.*
 ,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.user_role_type
from PREP_CRM_CASE
left join dim_crm_user on PREP_CRM_CASE.dim_crm_user_id = dim_crm_user.dim_crm_user_id

where
PREP_CRM_CASE.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and PREP_CRM_CASE.created_date >= '2024-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
)

select * from case_data