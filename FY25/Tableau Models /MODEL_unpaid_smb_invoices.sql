select 
count(*),
sum(balance),
sum(amount),
sum(payment_amount)
from 
PROD.RESTRICTED_SAFE_COMMON.FCT_INVOICE
left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
on fct_invoice.dim_crm_account_id = mart_crm_account.dim_crm_account_id
where --invoice_number = 'INV00325846'
--dim_invoice_id = '8a1289b98ee4c473018ee96d072f2965'
due_date_id >= 20240101
and due_date_id <= 20240801
and mart_crm_account.crm_account_owner like '%SMB Sales'
and balance > 0