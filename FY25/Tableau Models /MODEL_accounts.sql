WITH
dim_crm_user AS (
  SELECT *
  FROM prod.common.dim_crm_user
),

dim_date AS (
  SELECT *
  FROM prod.common.dim_date
),

dim_order AS (
  SELECT *
  FROM prod.common.dim_order
),

dim_order_action AS (
  SELECT *
  FROM prod.common.dim_order_action
),

dim_product_detail AS (
  SELECT *
  FROM prod.common.dim_product_detail
),

dim_subscription AS (
  SELECT *
  FROM prod.common.dim_subscription
),

mart_crm_task AS (
  SELECT *
  FROM prod.common_mart_sales.mart_crm_task
),

mart_arr AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_arr
),

mart_charge AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_charge
),

mart_crm_account AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_account
),

mart_crm_opportunity AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_opportunity
),

dim_crm_account_daily_snapshot AS (
  SELECT *
  FROM prod.restricted_safe_common.dim_crm_account_daily_snapshot
),

mart_delta_arr_subscription_month AS (
  SELECT *
  FROM prod.restricted_safe_legacy.mart_delta_arr_subscription_month
),

sfdc_case AS (
  SELECT *
  FROM prod.workspace_sales.sfdc_case
),

cm_organizations as (
  select * from PROD.WORKSPACE_MARKETING.WK_COMMONROOM_ORGANIZATIONS
),

account_raw as (
  select * from raw.salesforce_v2_stitch.account
),

case_data AS (
  SELECT

    CASE
      WHEN subject LIKE 'Multiyear Renewal%' THEN 'Multiyear Renewal'
      WHEN subject LIKE 'EOA Renewal%' THEN 'EOA Renewal'
      WHEN subject LIKE 'PO Required%' THEN 'PO Required'
      WHEN subject LIKE 'Auto-Renewal Will Fail%' THEN 'Auto-Renewal Will Fail'
      WHEN subject LIKE 'Overdue Renewal%' THEN 'Overdue Renewal'
      WHEN subject LIKE 'Auto-Renew Recently Turned Off%' THEN 'Auto-Renew Recently Turned Off'
      WHEN subject LIKE 'Failed QSR%' THEN 'Failed QSR'
      ELSE subject
    END                                                            AS case_trigger,
    sfdc_case.*
    ,
    DATEDIFF('day', sfdc_case.created_date, sfdc_case.closed_date) AS case_days_to_close,
    DATEDIFF('day', sfdc_case.created_date, CURRENT_DATE)          AS case_age_days,
    dim_crm_user.user_name                                         AS case_owner_name,
    dim_crm_user.department                                        AS case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name                                    AS case_user_role_name,
    dim_crm_user.user_role_type
  FROM sfdc_case
  LEFT JOIN dim_crm_user
    ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
  WHERE sfdc_case.record_type_id = '0128X000001pPRkQAM'
    AND sfdc_case.created_date >= '2024-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
)
,

task_data AS (

-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs
-- May need to be updated for new role names

  SELECT *
  FROM (
    SELECT
      task_id,
      task_status,
      task.dim_crm_account_id,
      task.dim_crm_user_id,
      task.dim_crm_person_id,
      task_subject,
      CASE
        WHEN LOWER(task_subject) LIKE '%email%' THEN 'Email'
        WHEN LOWER(task_subject) LIKE '%call%' THEN 'Call'
        WHEN LOWER(task_subject) LIKE '%linkedin%' THEN 'LinkedIn'
        WHEN LOWER(task_subject) LIKE '%inmail%' THEN 'LinkedIn'
        WHEN LOWER(task_subject) LIKE '%sales navigator%' THEN 'LinkedIn'
        WHEN LOWER(task_subject) LIKE '%drift%' THEN 'Chat'
        WHEN LOWER(task_subject) LIKE '%chat%' THEN 'Chat'
        ELSE
          task_type
      END                                                                                                                 AS type,
      CASE
        WHEN task_subject LIKE '%Outreach%' AND task_subject NOT LIKE '%Advanced Outreach%' THEN 'Outreach'
        WHEN task_subject LIKE '%Clari%' THEN 'Clari'
        WHEN task_subject LIKE '%Conversica%' THEN 'Conversica'
        ELSE 'Other'
      END                                                                                                                 AS outreach_clari_flag,
      task_created_date,
      task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      CASE
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[Out]%' OR task_subject LIKE '%utbound%')
          THEN 'Outbound'
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[In]%' OR task_subject LIKE '%nbound%')
          THEN 'Inbound'
        ELSE 'Other'
      END                                                                                                                 AS inbound_outbound_flag,
      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,
      task_date,
      CASE
        WHEN task.task_created_by_id LIKE '0054M000003Tqub%' THEN 'Outreach'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%GitLab Transactions%'
          THEN 'Post-Purchase'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Was Sent Email%'
          THEN 'SFDC Marketing Email Send'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Your GitLab License%'
          THEN 'Post-Purchase'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Advanced Outreach%'
          THEN 'Gainsight Marketing Email Send'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Filled Out Form%'
          THEN 'Marketo Form Fill'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Conversation in Drift%'
          THEN 'Drift'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Opened Email%'
          THEN 'Marketing Email Opened'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Sales Navigator%'
          THEN 'Sales Navigator'
        WHEN task.task_created_by_id NOT LIKE '0054M000003Tqub%' AND task_subject LIKE '%Clari - Email%'
          THEN 'Clari Email'
        ELSE
          'Other'
      END                                                                                                                 AS task_type,

      user.user_name                                                                                                      AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END                                                                                                                 AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM mart_crm_task AS task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id
    WHERE task.dim_crm_user_id IS NOT NULL
      AND is_deleted = FALSE
      AND task_date >= '2024-02-01'
      AND task_status = 'Completed'
  )

  WHERE (outreach_clari_flag = 'Outreach' OR task_created_by_id = dim_crm_user_id)
    AND outreach_clari_flag != 'Other'
    AND (
      user_role_name LIKE ANY ('%AE%', '%SDR%', '%BDR%', '%Advocate%')
      OR crm_user_sales_segment = 'SMB'
    )
)

--SELECT * from task_data; -- No records?
,

first_order AS (
  SELECT DISTINCT
    dim_crm_account_id,
    LAST_VALUE(net_arr) OVER (PARTITION BY dim_crm_account_id ORDER BY close_date ASC)     AS net_arr,
    LAST_VALUE(close_date) OVER (PARTITION BY dim_crm_account_id ORDER BY close_date ASC)  AS close_date,
    LAST_VALUE(fiscal_year) OVER (PARTITION BY dim_crm_account_id ORDER BY close_date ASC) AS fiscal_year,
    LAST_VALUE(sales_qualified_source_name)
      OVER (PARTITION BY dim_crm_account_id ORDER BY close_date ASC)                       AS sqs
  FROM mart_crm_opportunity
  LEFT JOIN dim_date
    ON mart_crm_opportunity.close_date = dim_date.date_actual
  WHERE is_won
    AND order_type = '1. New - First Order'
),

latest_churn AS (
  SELECT
    snapshot_date                                                                               AS close_date,
    dim_crm_account_id,
    carr_this_account,
    LAG(carr_this_account, 1) OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC) AS prior_carr,
    -prior_carr                                                                                 AS net_arr
    ,
    MAX(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END) OVER (PARTITION BY dim_crm_account_id)                                                 AS last_carr_date
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date >= '2019-02-01'
    AND snapshot_date = DATE_TRUNC('month', snapshot_date)
  -- and dim_crm_account_id = '0014M00001lbBt1QAE'
  QUALIFY
    prior_carr > 0
    AND carr_this_account = 0
    AND snapshot_date > last_carr_date
),

high_value_case AS  (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    sfdc_case.created_date,
    MAX(sfdc_case.created_date) over(partition by ACCOUNT_ID) as last_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
--    and account_id = '0014M00001gTGESQA4'
    and lower(subject) like '%fy26 high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date 
),

start_values AS ( -- This is a large slow to query table
  SELECT
    dim_crm_account_id,
    carr_account_family,
    carr_this_account,
    parent_crm_account_lam_dev_count,
    pte_score,
    ptc_score
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date = '2024-02-10' -----placeholder date for start of year
),

first_high_value_case AS (
  SELECT *
  FROM (
    SELECT
      account_id,
      case_id,
      owner_id,
      subject,
      dim_crm_user.user_name                                     AS case_owner_name,
      dim_crm_user.department                                    AS case_department,
      --    dim_crm_user.team,
      dim_crm_user.manager_name,
      dim_crm_user.user_role_name                                AS case_user_role_name,
      dim_crm_user.user_role_type,
      sfdc_case.created_date,
      MIN(sfdc_case.created_date) OVER (PARTITION BY account_id) AS first_high_value_date
    FROM sfdc_case
    LEFT JOIN dim_crm_user
      ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id IN ('0128X000001pPRkQAM')
      AND LOWER(subject) LIKE '%fy25 high value account%'
  ) -----subject placeholder - this could change
  WHERE created_date = first_high_value_date
),

eoa_cohorts AS (
   select distinct dim_crm_account_id from
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and mart_arr.quantity > 0
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
))),

free_promo AS (
  SELECT DISTINCT dim_crm_account_id
  FROM mart_charge
  WHERE subscription_start_date >= '2023-02-01'
    AND rate_plan_charge_description = 'fo-discount-70-percent'
),

smb_19_fo_promo as (
select distinct dim_crm_account_id
from mart_crm_opportunity
where lower(opportunity_name) like '%19promo%'
),

price_increase AS (
  SELECT DISTINCT dim_crm_account_id
  FROM (
    SELECT
      charge.*,
      arr / NULLIFZERO(quantity)     AS actual_price,
      prod.annual_billing_list_price AS list_price
    FROM mart_charge AS charge
    INNER JOIN dim_product_detail AS prod
      ON charge.dim_product_detail_id = prod.dim_product_detail_id
    WHERE subscription_start_date >= '2023-04-01'
      AND subscription_start_date <= '2023-07-01'
      AND type_of_arr_change = 'New'
      AND quantity > 0
      AND actual_price > 228
      AND actual_price < 290
      AND rate_plan_charge_name LIKE '%Premium%'
  )
),

ultimate AS (
  SELECT DISTINCT
    dim_crm_account_id,
    arr_month,
    MAX(arr_month) OVER (PARTITION BY dim_crm_account_id ORDER BY arr_month DESC) AS last_ultimate_arr_month
  FROM mart_arr
  WHERE product_tier_name LIKE '%Ultimate%'
    AND arr > 0
qualify (arr_month = last_ultimate_arr_month and arr_month <= date_trunc('month',current_date))
or (last_ultimate_arr_month > date_trunc('month',current_date) and arr_month = date_trunc('month',current_date))
),

amer_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('AMER SMB Sales')
    OR owner_role = 'Advocate_SMB_AMER'
),

emea_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('EMEA SMB Sales', 'APJ SMB Sales')
    OR owner_role = 'Advocate_SMB_EMEA'
),

cm_organizations_prep as (
select
cm_organizations.*,
account_raw.account_id_18__c as dim_crm_account_id
from cm_organizations
left join account_raw
on (cm_organizations.domain = account_raw.domains__c
or cm_organizations.domain = account_raw.tracrtc__website_domain__c
or cm_organizations.domain = account_raw.tracrtc__traction_complete_domain__c
or cm_organizations.domain = account_raw.zi_company_other_domains__c
or cm_organizations.domain = account_raw.WEBSITE
or cm_organizations.domain = account_raw.ZI_WEBSITE__C)
),

cm_organization_output as (
select
--distinct
dim_crm_account_id,
array_agg(distinct organization_name_hash) as all_cm_org_hashes
from
cm_organizations_prep
group by all
),

account_base AS (
  SELECT
    acct.*,
    CASE
      WHEN first_high_value_case.case_id IS NOT NULL THEN 'Tier 1'
      WHEN first_high_value_case.case_id IS NULL
        THEN
          CASE
            WHEN acct.carr_this_account > 7000 THEN 'Tier 1'
            WHEN acct.carr_this_account < 3000 AND acct.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
            ELSE 'Tier 2'
          END
    END                                                              AS calculated_tier,
    CASE
      WHEN (acct.snapshot_date >= '2024-02-01' AND amer_accounts.dim_crm_account_id IS NOT NULL
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('AMER SMB Sales', 'APAC SMB Sales')
            OR owner_role = 'Advocate_SMB_AMER'
        )*/)
      OR (
        acct.snapshot_date < '2024-02-01' AND (
          acct.parent_crm_account_geo IN ('AMER')
          OR acct.parent_crm_account_region IN ('AMER')
        )
      ) THEN 'AMER'
      WHEN (acct.snapshot_date >= '2024-02-01' AND emea_accounts.dim_crm_account_id IS NOT NULL
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('EMEA SMB Sales')
            OR owner_role = 'Advocate_SMB_EMEA'
        )*/)
      OR (
        acct.snapshot_date < '2024-02-01' AND (
          acct.parent_crm_account_geo IN ('EMEA', 'APJ', 'APAC')
          OR acct.parent_crm_account_region IN ('EMEA', 'APJ', 'APAC')
        )
      ) THEN 'EMEA'
      ELSE 'Other'
    END                                                              AS team,
    COALESCE ((acct.snapshot_date >= '2024-02-01' AND (emea_accounts.dim_crm_account_id IS NOT NULL OR amer_accounts.dim_crm_account_id IS NOT NULL)
    /*acct.dim_crm_account_id IN (
          SELECT dim_crm_account_id
          FROM mart_crm_account
          WHERE crm_account_owner IN
                ('AMER SMB Sales', 'APAC SMB Sales', 'EMEA SMB Sales')
            OR owner_role LIKE 'Advocate%'
        )*/)
    OR
    (
      acct.snapshot_date < '2024-02-01'
      AND (acct.carr_account_family <= 30000 --and acct.carr_this_account > 0
      )
      AND (
        acct.parent_crm_account_max_family_employee <= 100
        OR acct.parent_crm_account_max_family_employee IS NULL
      )
      AND ultimate.dim_crm_account_id IS NULL
      AND acct.parent_crm_account_sales_segment IN ('SMB', 'Mid-Market', 'Large')
      AND acct.parent_crm_account_upa_country != 'JP'
      AND acct.is_jihu_account = FALSE
    ), FALSE)                                                        AS gds_account_flag,
    fo.fiscal_year                                                   AS fo_fiscal_year,
    fo.close_date                                                    AS fo_close_date,
    fo.net_arr                                                       AS fo_net_arr,
    fo.sqs                                                           AS fo_sqs,
    churn.close_date                                                 AS churn_close_date,
    churn.net_arr                                                    AS churn_net_arr,
    NOT COALESCE (fo_fiscal_year <= 2024, FALSE)                     AS new_fy25_fo_flag,
    first_high_value_case.created_date                               AS first_high_value_case_created_date,
    high_value_case.case_owner_name                                  AS high_value_account_owner,
    --high_value_case.team as high_value_account_team,
    high_value_case.manager_name                                     AS high_value_manager_name,
    start_values.carr_account_family                                 AS starting_carr_account_family,
    start_values.carr_this_account                                   AS starting_carr_this_account,
    CASE
      WHEN start_values.carr_this_account > 7000 THEN 'Tier 1'
      WHEN start_values.carr_this_account < 3000 AND start_values.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
      ELSE 'Tier 2'
    END                                                              AS starting_calculated_tier,
    start_values.pte_score                                           AS starting_pte_score,
    start_values.ptc_score                                           AS starting_ptc_score,
    start_values.parent_crm_account_lam_dev_count                    AS starting_parent_crm_account_lam_dev_count,
    COALESCE (eoa.dim_crm_account_id IS NOT NULL, FALSE)             AS eoa_flag,
    COALESCE (free_promo.dim_crm_account_id IS NOT NULL, FALSE)      AS free_promo_flag,
    COALESCE (price_increase.dim_crm_account_id IS NOT NULL, FALSE)  AS price_increase_promo_flag,
    COALESCE (ultimate.dim_crm_account_id IS NOT NULL, FALSE) AS ultimate_customer_flag,
    COALESCE (smb_19_fo_promo.dim_crm_account_id IS NOT NULL, FALSE) AS smb_19_fo_promo_flag,
    cm_organization_output.all_cm_org_hashes,
  FROM dim_crm_account_daily_snapshot AS acct
  ------subquery that gets latest FO data
  LEFT JOIN first_order AS fo
    ON acct.dim_crm_account_id = fo.dim_crm_account_id
  ---------subquery that gets latest churn data
  LEFT JOIN latest_churn AS churn
    ON acct.dim_crm_account_id = churn.dim_crm_account_id
  --------subquery to get high tier case owner
  LEFT JOIN high_value_case
    ON acct.dim_crm_account_id = high_value_case.account_id
  --------------subquery to get start of FY25 values
  LEFT JOIN start_values
    ON acct.dim_crm_account_id = start_values.dim_crm_account_id
  -----subquery to get FIRST high value case
  LEFT JOIN first_high_value_case
    ON acct.dim_crm_account_id = first_high_value_case.account_id
  -----EOA cohort accounts
  LEFT JOIN eoa_cohorts AS eoa
    ON acct.dim_crm_account_id = eoa.dim_crm_account_id
  ------free limit promo cohort accounts
  LEFT JOIN free_promo
    ON acct.dim_crm_account_id = free_promo.dim_crm_account_id
  ------price increase promo cohort accounts
  LEFT JOIN price_increase
    ON acct.dim_crm_account_id = price_increase.dim_crm_account_id
  LEFT JOIN ultimate
    ON acct.dim_crm_account_id = ultimate.dim_crm_account_id
      AND (ultimate.arr_month = DATE_TRUNC('month', acct.snapshot_date)
      OR 
      (
      ultimate.last_ultimate_arr_month = dateadd('month', -1, DATE_TRUNC('month', acct.snapshot_date))
      and ultimate.last_ultimate_arr_month = dateadd('month', -1, DATE_TRUNC('month', current_date))
      ))
  ----- amer and apac accounts
  LEFT JOIN amer_accounts
    ON acct.dim_crm_account_id = amer_accounts.dim_crm_account_id
  ----- emea emea accounts
  LEFT JOIN emea_accounts
    ON acct.dim_crm_account_id = emea_accounts.dim_crm_account_id
  LEFT join cm_organization_output
    ON acct.dim_crm_account_id = cm_organization_output.dim_crm_account_id
  LEFT JOIN smb_19_fo_promo
    on acct.dim_crm_account_id = smb_19_fo_promo.dim_crm_account_id

  -------filtering to get current account data
  WHERE acct.snapshot_date = current_date
-- and(((acct.snapshot_date < '2024-02-01' or acct.snapshot_date >= '2024-03-01')
-- and  acct.snapshot_date = date_trunc('month',acct.snapshot_date))
-- or (date_trunc('month',acct.snapshot_date) = '2024-02-01' and acct.snapshot_date = current_date))
--and acct.dim_crm_account_id = '0014M00001ldLdiQAE'
),

--SELECT * from account_base;

case_task_summary as
(
select 
account_base.dim_crm_account_id as case_task_summary_id,

listagg(distinct case_data.case_trigger, ', ') within group (order by case_data.case_trigger)
as acct_trigger_list,
listagg(distinct case_data.case_id, ', ')
as acct_case_id_list,

count(distinct case_data.case_id) as closed_case_count,
count(distinct case when case_data.status = 'Closed: Resolved' or case_data.status = 'Closed' then case_data.case_id else null end) as resolved_case_count,

count(distinct case when task_data.inbound_outbound_flag = 'Inbound' then task_data.task_id else null end) as inbound_email_count,
count(distinct case when task_data.outbound_answered_flag then task_data.task_id else null end) as completed_call_count,
count(distinct case when task_data.inbound_outbound_flag = 'Outbound' then task_data.task_id else null end) as outbound_email_count,
case when inbound_email_count > 0 then true else false end as task_inbound_flag,
case when completed_call_count > 0 then true else false end as task_completed_call_flag,
case when outbound_email_count > 0 then true else false end as task_outbound_flag,
count(distinct task_data.task_id) as completed_task_count

from account_base
left join case_data
on account_base.dim_crm_account_id = case_data.account_id
and case_data.is_closed
left join task_data
on account_base.dim_crm_account_id = task_data.dim_crm_account_id
--where case_data.account_id is not null
group by 1
)

select
account_base.*,
case_task_summary.*
from account_base
left join case_task_summary on account_base.dim_crm_account_id = case_task_summary.case_task_summary_id

