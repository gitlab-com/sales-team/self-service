WITH
target_page_views AS 
(SELECT 
*
FROM PROD.COMMON.FCT_BEHAVIOR_WEBSITE_PAGE_VIEW
where 
(page_url like '%ultimate%'
OR
page_url like '%security%'
OR
page_url like '%compliance%'
OR
page_url like '%finance%'
OR
page_url like '%telecom%')
AND
page_view_start_at::DATE >= dateadd('day',-30,current_date)
AND
page_url NOT LIKE 'https://gitlab.com/%'
),

events as
(
select
distinct
dim_namespace_id,
ultimate_parent_namespace_id,
user_snowplow_domain_id
from
PROD.common_mart.mart_behavior_structured_event
WHERE behavior_at::DATE >= dateadd('day',-30,current_date)
),

smb_account_namespace_ids AS
(
select
mart_crm_account.dim_crm_account_id,
bdg_namespace_order_subscription.ultimate_parent_namespace_id
from PROD.COMMON.BDG_NAMESPACE_ORDER_SUBSCRIPTION
inner join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
on bdg_namespace_order_subscription.dim_crm_account_id = mart_crm_account.dim_crm_account_id
where mart_crm_account.crm_account_owner IN ('AMER SMB Sales','EMEA SMB Sales','APAC SMB Sales')
and mart_crm_account.carr_this_account > 0
)

select
target_page_views.clean_url_path,
smb_account_namespace_ids.dim_crm_account_id,
target_page_views.page_view_start_at::DATE as page_view_date
from
target_page_views
inner join events
on target_page_views.user_snowplow_domain_id = events.user_snowplow_domain_id
inner join smb_account_namespace_ids
on events.ultimate_parent_namespace_id = smb_account_namespace_ids.ultimate_parent_namespace_id