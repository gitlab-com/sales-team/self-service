with
get_account_id as
(
select
distinct
mart_crm_account.dim_crm_account_id,
subscription_name
from
common.dim_subscription
left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
on dim_subscription.dim_crm_account_id = mart_crm_account.dim_crm_account_id
where subscription_status = 'Active'
and parent_crm_account_sales_segment = 'SMB'
)

SELECT 
DATE_TRUNC ('MONTH', DATE(customers_db_provisions.CREATED_AT)) AS provision_creation_month,
*
FROM PROD.workspace_product.customers_db_provisions
inner join get_account_id
on customers_db_provisions.subscription_name = get_account_id.subscription_name
WHERE customers_db_provisions.created_at >= '2022-10-01'
AND DATEDIFF(MONTH,provision_creation_month,CURRENT_DATE) > 0
and state in ('skipped','failed')
and provision_creation_month >= '2024-07-01'