WITH run_results AS (
  SELECT
    *
  FROM "PREP"."DBT"."DBT_RUN_RESULTS_SOURCE"
)

SELECT
  SPLIT_PART(model_unique_id,'.',3) AS model_name,
  *
FROM run_results
WHERE uploaded_at > dateadd('day', -10, current_date)

--UPDATE MODEL NAME
AND model_name in ('wk_sales_gds_cases')
  -- AND status != 'success'
  -- AND status = 'error'
ORDER BY uploaded_at desc
;