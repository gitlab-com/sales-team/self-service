with task_data as (
WITH base_data AS (
  SELECT *
  FROM (
    SELECT *
  FROM (
    SELECT
      task_id,
      task_status,
      task.dim_crm_account_id,
      task.dim_crm_user_id,
      task.dim_crm_person_id,
      task_type,
      task_subject,
      CASE
        WHEN task_subject LIKE '%Outreach%' AND task_subject NOT LIKE '%Advanced Outreach%' THEN 'Outreach'
        WHEN task_subject LIKE '%Clari%' THEN 'Clari'
        WHEN task_subject LIKE '%Conversica%' THEN 'Conversica'
        ELSE 'Other'
      END AS outreach_clari_flag,
      -------flagging groove tasks
      CASE
        WHEN (contains(lower(task_subject), '[inbox]') or contains(lower(task_subject), '[flow]')) AND outreach_clari_flag = 'Other' then TRUE else FALSE end as groove_flag,
      --------categorizing groove tasks
            CASE
        WHEN groove_flag = True and task_type = 'Call' then 'Groove Call'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Flow]') then 'Groove Flow Email'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Inbox]') then 'Groove Inbox Email'
        WHEN groove_flag = True and task_type = 'Call' and contains(task_subject, 'Flow') then 'Groove Flow Call'
        End as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      CASE
        WHEN groove_flag = TRUE and contains(lower(task_subject), '<<') then 'Inbound' 
        WHEN groove_flag = True and contains(lower(task_subject), '>>') then 'Outbound'
        Else 'Other' 
        END as groove_inbound_outbound_flag,
      --------flagging gong tasks
      CASE 
        WHEN contains(lower(task_subject), 'gong') and outreach_clari_flag = 'Other' then TRUE Else FALSE end as gong_flag,
      CASE
        WHEN gong_flag = True and task_type = 'Meeting' then TRUE 
        WHEN contains(lower(task_subject), '(ii)') then TRUE
        WHEN contains(lower(task_subject), '#meeting') then TRUE 
        WHEN task_type = 'Meeting' then TRUE
        ELSE False End as meeting_flag, 
      -----------Flagging for groove task types
    
      task_created_date,
      task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      CASE
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[Out]%' OR task_subject LIKE '%utbound%')
          THEN 'Outbound'
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[In]%' OR task_subject LIKE '%nbound%')
          THEN 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email received') then 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email sent') then 'Outbound'
        WHEN groove_inbound_outbound_flag = 'Inbound' then 'Inbound'
        WHEN groove_inbound_outbound_flag = 'Outbound' then 'Outbound'
        ELSE 'Other'
      END                                                                                                                 AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,

      
      task_date,
      user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_TASK AS task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id
    WHERE task.dim_crm_user_id IS NOT NULL
      AND is_deleted = FALSE
      AND task_date >= '2024-02-01'
      AND task_status = 'Completed'

      UNION 

      SELECT 
      event_id as task_id,
      'Completed' as task_status,
      dim_crm_account_id,
      task.dim_crm_user_id,
      dim_crm_person_id,
      event_type as task_type,
      event_subject as task_subject,
      'Other' AS outreach_clari_flag,
      -------flagging groove tasks
      FALSE as groove_flag,
      --------categorizing groove tasks
      'Non-Groove - Event' as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      'Other' as groove_inbound_outbound_flag,
      --------flagging gong tasks
      FALSE as gong_flag,
      TRUE as meeting_flag, 
      -----------Flagging for groove task types
    
      event_created_date as task_created_date,
      event_created_by_id as task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      'Other' AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,
     event_date as task_date,
     user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_EVENT task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id 
    WHERE 
      lower(event_subject) like '%#meeting' or lower(event_subject) like '%#meeting%' or lower(event_subject) like '#meeting%'
      OR lower(event_subject) like '%(ii)%'
      AND event_date >= '2024-02-01'
  )

  WHERE (
      user_role_name LIKE ANY ('%AE%', '%SDR%', '%BDR%', '%Advocate%')
      OR crm_user_sales_segment = 'SMB')
)
  ),
meeting_deduped AS (
  SELECT *,
    CASE 
      WHEN meeting_flag = TRUE THEN
        ROW_NUMBER() OVER (
          PARTITION BY dim_crm_account_id, task_date  -- Partitioning by account AND date
          ORDER BY 
            task_created_date ASC,
            CASE 
              WHEN task_type = 'Meeting' THEN 1
              ELSE 2 
            END
        )
      ELSE 1  -- Non-meetings get a rank of 1 so they're always kept
    END as meeting_rank
  FROM base_data
)
SELECT *
FROM meeting_deduped
WHERE meeting_rank = 1) 
