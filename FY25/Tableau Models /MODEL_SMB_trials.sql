with 

rpt_namespace_onboarding as
(
    select * from common_mart_product.rpt_namespace_onboarding
),

mart_crm_person as
(
    select * from common_mart_marketing.mart_crm_person
),


trials_base AS (
    SELECT
        ultimate_parent_namespace_id,
        trial_start_date,
        creator_is_valuable_signup,
        namespace_contains_valuable_signup,
        has_team_activation,
        first_paid_subscription_start_date,
        is_namespace_created_within_2min_of_creator_invite_acceptance,
        account_demographics_sales_segment
    FROM rpt_namespace_onboarding
        join mart_crm_person
            on rpt_namespace_onboarding.sfdc_record_id = mart_crm_person.sfdc_record_id 
    WHERE is_namespace_created_within_2min_of_creator_invite_acceptance = false

-- 45-day free to paid conversion rate
), namespaces_on_day_45_of_trial AS (

    -- gets namespace ids that just started a trial based on the segment 
    SELECT  
        trials_base.*,
        DATEADD(DAY, 45, trial_start_date) AS day_45_of_trial
    FROM trials_base
        -- limmit to trials ON thier x date
    WHERE DATEADD(DAY, 45, trial_start_date) <= CURRENT_DATE
        AND creator_is_valuable_signup

), day_45_free_to_paid_numerator AS (

    SELECT
        DATE_TRUNC('week', day_45_of_trial)               AS metric_time,
        account_demographics_sales_segment,
        COUNT(DISTINCT 
            CASE WHEN 
                -- subscribed within 45 days
                first_paid_subscription_start_date <= day_45_of_trial 
                -- ensure the paid sub IS after the trial
                AND first_paid_subscription_start_date >= trial_start_date 
                THEN ultimate_parent_namespace_id END)     AS day_45_free_to_paid_numerator
    FROM namespaces_on_day_45_of_trial
    WHERE day_45_of_trial >= '2022-01-01'  
        AND creator_is_valuable_signup
    GROUP BY 1,2

),  day_45_free_to_paid_denominator AS (
    SELECT
        DATE_TRUNC('week', day_45_of_trial)               AS metric_time,
        account_demographics_sales_segment,
        COUNT(DISTINCT ultimate_parent_namespace_id)       AS day_45_free_to_paid_denominator
    FROM namespaces_on_day_45_of_trial
    WHERE day_45_of_trial >= '2022-01-01'  
        AND creator_is_valuable_signup
    GROUP BY 1,2
)
SELECT 
    day_45_free_to_paid_numerator.metric_time,
    day_45_free_to_paid_numerator.account_demographics_sales_segment,
    day_45_free_to_paid_denominator,
    day_45_free_to_paid_numerator
    
FROM day_45_free_to_paid_numerator
    join day_45_free_to_paid_denominator
        on day_45_free_to_paid_numerator.metric_time = day_45_free_to_paid_denominator.metric_time
        and day_45_free_to_paid_numerator.account_demographics_sales_segment = day_45_free_to_paid_denominator.account_demographics_sales_segment
WHERE Day_45_free_to_paid_numerator.account_demographics_sales_segment = 'SMB'