with SHEETLOAD_PARTNER_DISCOUNT as
(select DIM_CRM_OPPORTUNITY_ID,max(DISCOUNT_PERCENT) as Partner_Margin
from PREP.SHEETLOAD.SHEETLOAD_PARTNER_DISCOUNT_SUMMARY_SOURCE
group by 1
),
CHARGECONTRACTUALVALUE AS (
select distinct "ratePlanChargeId", ELP
from "RAW"."ZUORA_QUERY_API"."CHARGECONTRACTUALVALUE"
) ,

Discount_prep as (
SELECT
RATEPLANCHARGE.description,
BOOKINGTRANSACTION.listprice,
BOOKINGTRANSACTION.CURRENTELP,
-- BOOKINGTRANSACTION.listprice/NULLIFZERO(fct_invoice_item.Quantity) as List_price_per_unit,

CHARGECONTRACTUALVALUE.ELP as ELP,
(CASE WHEN '2020-02-29' BETWEEN RATEPLANCHARGE.EFFECTIVESTARTDATE and RATEPLANCHARGE.EFFECTIVEENDDATE
THEN datediff(d, RATEPLANCHARGE.EFFECTIVESTARTDATE,RATEPLANCHARGE.EFFECTIVEENDDATE)-1
WHEN '2024-02-29' between  RATEPLANCHARGE.EFFECTIVESTARTDATE and RATEPLANCHARGE.EFFECTIVEENDDATE
THEN datediff(d,RATEPLANCHARGE.EFFECTIVESTARTDATE,RATEPLANCHARGE.EFFECTIVEENDDATE)-1
ELSE datediff(d, RATEPLANCHARGE.EFFECTIVESTARTDATE,RATEPLANCHARGE.EFFECTIVEENDDATE) END) as Effective_days,

(CASE WHEN BOOKINGTRANSACTION.listprice is not null THEN BOOKINGTRANSACTION.listprice/NULLIFZERO(fct_invoice_item.Quantity)
ELSE ( CHARGECONTRACTUALVALUE.ELP*365/nullifzero(Effective_days) )/NULLIFZERO(fct_invoice_item.Quantity) END) as List_price_per_unit,

fct_invoice_item.arr/NULLIFZERO(fct_invoice_item.Quantity) as arpu,
fct_invoice_item.invoice_number,
fct_invoice_item.is_last_segment_version,
fct_invoice_item.DIM_CRM_ACCOUNT_ID_INVOICE,
fct_invoice_item.charge_id,
(fct_invoice_item.QUANTITY - ZEROIFNULL(LAG(fct_invoice_item.quantity) OVER (PARTITION BY fct_invoice_item.invoice_number, dim_product_detail.product_name
ORDER BY fct_invoice_item.effective_end_month))) AS delta_quantity,
fct_invoice_item.invoice_item_unit_price,
fct_invoice_item.INVOICE_DATE,
fct_invoice_item.dim_subscription_id,
fct_invoice_item.dim_product_detail_id,
fct_invoice_item.MRR,
fct_invoice_item.ARR,
fct_invoice_item.INVOICE_ITEM_CHARGE_AMOUNT,
fct_invoice_item.Quantity,

DIM_CRM_ACCOUNT.parent_crm_account_name,
DIM_CRM_ACCOUNT.is_jihu_account,
DIM_CRM_ACCOUNT.is_reseller,
DIM_CRM_ACCOUNT.PARENT_CRM_ACCOUNT_SALES_SEGMENT,
DIM_CRM_ACCOUNT.dim_parent_crm_account_id,
DIM_CRM_ACCOUNT.parent_crm_account_geo,
DIM_CRM_ACCOUNT.parent_crm_account_region,
DIM_CRM_ACCOUNT.PARENT_CRM_ACCOUNT_UPA_COUNTRY,
RATEPLANCHARGE.dmrc AS delta_mrr,
RATEPLANCHARGE.BILLINGPERIOD,
RATEPLANCHARGE.SPECIFICBILLINGPERIOD,
RATEPLANCHARGE.EFFECTIVESTARTDATE,
RATEPLANCHARGE.EFFECTIVEENDDATE,
-- rateplanchargetier.price,

DIM_SUBSCRIPTION.subscription_name,
DIM_SUBSCRIPTION.Term_Start_date,
DIM_SUBSCRIPTION.Term_End_date,

dim_product_detail.product_name,
dim_product_detail.product_tier_name,
dim_product_detail.PRODUCT_DELIVERY_TYPE,
dim_product_detail.PRODUCT_RATE_PLAN_CHARGE_NAME,
dim_product_detail.is_arpu,

MART_CRM_OPPORTUNITY.dim_crm_opportunity_id,
MART_CRM_OPPORTUNITY.Sales_type,
MART_CRM_OPPORTUNITY.RESALE_PARTNER_NAME,
MART_CRM_OPPORTUNITY.Opportunity_Category,
(CASE WHEN MART_CRM_OPPORTUNITY.Order_Type='1. New - First Order' THEN 'First Order' ELSE 'Growth' END) AS Order_type,
(CASE WHEN MART_CRM_OPPORTUNITY.deal_path_name is null THEN 'Other' ELSE MART_CRM_OPPORTUNITY.deal_path_name END) as deal_path_name,

SHEETLOAD_PARTNER_DISCOUNT.Partner_Margin

FROM "PROD".restricted_safe_common.fct_invoice_item as fct_invoice_item
INNER JOIN "PROD".common.dim_product_detail dim_product_detail
ON fct_invoice_item.dim_product_detail_id = dim_product_detail.dim_product_detail_id
left join RAW.ZUORA_STITCH.RATEPLANCHARGE RATEPLANCHARGE
on fct_invoice_item.charge_id = RATEPLANCHARGE.id
--  left join RAW.zuora_stitch.rateplanchargetier rateplanchargetier
--      ON RATEPLANCHARGE.id =rateplanchargetier.rateplanchargeid
left join "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT" DIM_CRM_ACCOUNT
on fct_invoice_item.DIM_CRM_ACCOUNT_ID_INVOICE=DIM_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
left join "RAW"."ZUORA_STITCH"."BOOKINGTRANSACTION" BOOKINGTRANSACTION
on RATEPLANCHARGE.Id = BOOKINGTRANSACTION.RATEPLANCHARGEID
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION
on fct_invoice_item.dim_subscription_id = DIM_SUBSCRIPTION.dim_subscription_id
left join "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY"
on DIM_SUBSCRIPTION.dim_crm_opportunity_id = MART_CRM_OPPORTUNITY.dim_crm_opportunity_id
left join SHEETLOAD_PARTNER_DISCOUNT
on MART_CRM_OPPORTUNITY.dim_crm_opportunity_id =SHEETLOAD_PARTNER_DISCOUNT.DIM_CRM_OPPORTUNITY_ID
left join CHARGECONTRACTUALVALUE
on RATEPLANCHARGE.Id = CHARGECONTRACTUALVALUE."ratePlanChargeId"
)
-- select product_name, count(*) from Discount_prep group by 1;
,
Discount_prep_step2 as (

select
DIM_DATE.FISCAL_YEAR,
DIM_DATE.FISCAL_QUARTER_NAME_FY,
DIM_DATE.FIRST_DAY_OF_MONTH,
Discount_prep.INVOICE_DATE,
Discount_prep.description,
Discount_prep.invoice_number,
Discount_prep.Product_name,
Discount_prep.PRODUCT_RATE_PLAN_CHARGE_NAME,
Discount_prep.product_tier_name,
Discount_prep.is_last_segment_version,
--Discount_prep.ARR,
Discount_prep.delta_quantity,
Discount_prep.quantity,
Discount_prep.delta_mrr,
Discount_prep.arpu ,
Discount_prep.CURRENTELP,
Discount_prep.INVOICE_ITEM_CHARGE_AMOUNT,
Discount_prep.BILLINGPERIOD,
Discount_prep.SPECIFICBILLINGPERIOD,
Discount_prep.DIM_CRM_ACCOUNT_ID_INVOICE,
Discount_prep.charge_id,
Discount_prep.EFFECTIVESTARTDATE,
Discount_prep.EFFECTIVEENDDATE,
Discount_prep.dim_subscription_id,
Discount_prep.subscription_name,
Discount_prep.Term_Start_date,
Discount_prep.Term_End_date,
Discount_prep.PRODUCT_DELIVERY_TYPE,
Discount_prep.Order_type,
Discount_prep.deal_path_name,
Discount_prep.dim_parent_crm_account_id,
Discount_prep.PARENT_CRM_ACCOUNT_SALES_SEGMENT as Segment,
Discount_prep.parent_crm_account_name,
Discount_prep.parent_crm_account_geo as Geo,
Discount_prep.parent_crm_account_region as Region,
Discount_prep.PARENT_CRM_ACCOUNT_UPA_COUNTRY as Country,
Discount_prep.dim_crm_opportunity_id,
Discount_prep.sales_type,
Discount_prep.Opportunity_Category,
Discount_prep.is_jihu_account,
Discount_prep.is_reseller,
Discount_prep.RESALE_PARTNER_NAME,
(CASE WHEN Sales_Type= 'Add-On Business' THEN delta_quantity ELSE QUANTITY END) AS QUANTITY_with_addon_update,

round ((CASE WHEN BILLINGPERIOD= 'Month' THEN List_price_per_unit*12
WHEN BILLINGPERIOD= 'Two Years' THEN List_price_per_unit/2
WHEN BILLINGPERIOD= 'Three Years' THEN List_price_per_unit/3
WHEN BILLINGPERIOD= 'Four Years' THEN List_price_per_unit/4
WHEN BILLINGPERIOD= 'Five Years' THEN List_price_per_unit/5
WHEN BILLINGPERIOD= 'Specific Months' THEN List_price_per_unit*12/SPECIFICBILLINGPERIOD
ELSE LIST_PRICE_PER_UNIT END),2) as List_price_per_unit_calc,

--Premium price increase was effective from 03-Apr-2023. However,
--our systems were not ready for 18% discount coupon by that time, and we had incoroprated 18% discount on List price itself until 18-May-2023.
--Note: add-on business are sold at customers existing price, that's the reason it's being handled differently on the below code.
(CASE WHEN lower(PRODUCT_NAME) like '%premium%' AND Sales_Type= 'Add-On Business' AND Invoice_date BETWEEN TERM_START_DATE AND TERM_END_DATE
and TERM_START_DATE <'2023-04-03' AND ARPU<=228 THEN 228
WHEN lower(PRODUCT_NAME) like '%premium%' AND Sales_Type= 'Add-On Business' AND Invoice_date BETWEEN TERM_START_DATE AND TERM_END_DATE
AND (TERM_START_DATE >='2023-04-03' OR ARPU>228 ) THEN 348
WHEN round(List_price_per_unit_calc)=285 THEN 348
ELSE List_price_per_unit_calc END
) as List_price_per_unit_overwrite,

(List_price_per_unit_overwrite * QUANTITY_with_addon_update ) as List_Price_calc,
(arpu * QUANTITY_with_addon_update) as ARR_calc,
(List_Price_calc-ARR_calc)/nullifzero(List_Price_calc) as discount_on_deal,

(CASE WHEN Delta_MRR=0 AND Sales_Type= 'Add-On Business' THEN 'NE: Add-on_duplicates'
WHEN is_arpu = FALSE THEN 'NE: EDU_Subscriptions'
WHEN INVOICE_ITEM_CHARGE_AMOUNT <0 THEN 'NE: Credit/Decommissions'
WHEN BILLINGPERIOD ='Month' AND IS_LAST_SEGMENT_VERSION= FALSE THEN 'NE: Duplicate_arr_on_monthly_billings'
WHEN List_price_per_unit is null THEN 'NE:List_Price_null'
WHEN zeroifnull(Arr) = 0 THEN 'NE: ARR_zero'
WHEN QUANTITY_with_addon_update < 0 THEN 'NE: Contractions'
WHEN QUANTITY_with_addon_update = 0 THEN 'NE: Quantity=0'
WHEN PARENT_CRM_ACCOUNT_NAME= 'CERN' THEN 'CERN Deal in FY24Q2 99.5% discount'
WHEN discount_on_deal <0 THEN 'Negative_discount_data_issue'
ELSE 'Eligible' END) as Discount_Eligible_flag,

--In case overall discount is lower than the partner margin, we consider lower of these 2 as partner margin.
(CASE WHEN discount_on_deal>= 0 and discount_on_deal <Discount_prep.Partner_Margin then discount_on_deal ELSE Discount_prep.Partner_Margin END) as Partner_Margin

from Discount_prep
LEFT JOIN "PROD"."COMMON"."DIM_DATE"
on Discount_prep.INVOICE_DATE = DIM_DATE.DATE_ACTUAL
where Discount_Eligible_flag ='Eligible'
),
Discount_prep_step3 as (
select *,
round(ARR_calc/(1-zeroifnull(Partner_Margin)) ,2) as ARR_incl_channel_margin,
(List_Price_calc-ARR_incl_channel_margin)/NULLIFZERO(List_Price_calc) as End_user_discount
from Discount_prep_step2
),

-- select * from Discount_prep_step2
Final as (
Select FISCAL_QUARTER_NAME_FY,
FIRST_DAY_OF_MONTH as Invoice_Month,
dim_parent_crm_account_id,
parent_crm_account_name,
dim_crm_opportunity_id,
Invoice_Number,
dim_subscription_id,
Subscription_name,
RESALE_PARTNER_NAME,
(CASE WHEN Segment is null THEN 'SMB' ELSE Segment END) as Segment,
Geo,
Region,
Country,
(CASE WHEN PRODUCT_TIER_NAME ilike '%Premium%' then 'Premium'
WHEN PRODUCT_TIER_NAME ilike '%Dedicate%' then 'Dedicated-Ultimate'
WHEN PRODUCT_TIER_NAME ilike '%Ultimate%' then 'Ultimate'
WHEN PRODUCT_NAME ilike '%duo%' then 'Duo Pro'
WHEN PRODUCT_NAME ilike '%Enterprise%Agile%Planning%' then 'Enterprise Agile Planning'
WHEN PRODUCT_TIER_NAME ilike '%Bronze%' or PRODUCT_TIER_NAME ilike '%Starter%' THEN 'Bronze/Starter'  else 'Others' end) as Product_Name,
PRODUCT_DELIVERY_TYPE,
product_tier_name,
(CASE WHEN Deal_Path_name is null or Deal_Path_name='Other' then 'Direct' else Deal_Path_name END) as Deal_Path_name,
(CASE WHEN Sales_Type is null then 'New Business' else Sales_Type END) as Sales_Type,
Order_type,
(CASE WHEN QUANTITY_with_addon_update <=10 THEN 'a) 1-10'
WHEN QUANTITY_with_addon_update <=25 THEN 'b) 11-25'
WHEN QUANTITY_with_addon_update <=50 THEN 'c) 25-50'
WHEN QUANTITY_with_addon_update <=99 THEN 'd) 50-99'
WHEN QUANTITY_with_addon_update >100 THEN 'e) >100' END) as Seats_Bucket,

(CASE WHEN List_Price_calc <=5000  THEN 'a) <$5K'
WHEN List_Price_calc <=25000 THEN 'b) $5K-$25K'
WHEN List_Price_calc <=50000 THEN 'c) $25K-$50K'
WHEN List_Price_calc <=100000 THEN 'd) $50K-$100K'
WHEN List_Price_calc <=300000 THEN 'e) $100K-$300K'
WHEN List_Price_calc <=500000 THEN 'f) $300K-$500K'
WHEN List_Price_calc <=1000000 THEN 'g) $500K-$1M'
WHEN List_Price_calc >1000000 THEN 'h) $1M+'
END) as List_Price_Buckets,

(CASE WHEN End_user_discount =0  THEN 'a) 0%'
WHEN End_user_discount <=0.05 THEN 'b) <5%'
WHEN End_user_discount <=0.1 THEN 'c) 5-10%'
WHEN End_user_discount <=0.2 THEN 'd) 10-20%'
WHEN End_user_discount <=0.3 THEN 'e) 20-30%'
WHEN End_user_discount <=0.4 THEN 'f) 30-40%'
WHEN End_user_discount <=0.5 THEN 'g) 40-50%'
WHEN End_user_discount <=0.6 THEN 'h) 50-60%'
WHEN End_user_discount <=0.7 THEN 'i) 60-70%'
WHEN End_user_discount <=0.8 THEN 'j) 70-80%'
WHEN End_user_discount <=0.9 THEN 'k) 80-90%'
WHEN End_user_discount >0.9 THEN 'l) >90%'
END) as End_user_discount_Buckets,

sum(QUANTITY_with_addon_update) as Quantity,
sum(List_Price_calc) as List_Price,
sum(ARR_calc) as ARR,
sum(ARR_incl_channel_margin) as ARR_with_channel_margin
,(ARR_with_channel_margin -ARR)/NULLIFZERO(ARR_with_channel_margin) as Programmatic_Discount,
(List_Price-ARR_with_channel_margin)/NULLIFZERO(List_Price) as Discount_End_user,
(List_Price-ARR)/NULLIFZERO(List_Price) as Discount_overall
from Discount_prep_step3
where
FISCAL_QUARTER_NAME_FY>='FY23-Q1'
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22
)
select * from Final