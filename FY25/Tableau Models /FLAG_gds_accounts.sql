case when (snapshot_date >= '2024-02-07' and crm_account_owner in ('AMER SMB Sales','APAC SMB Sales','EMEA SMB Sales'))
or
(
snapshot_date < '2024-02-07' and
(carr_account_family <= 30000 and carr_this_account > 0)
and (parent_crm_account_max_family_employee <= 100 or parent_crm_account_max_family_employee is null)
and ultimate_customer_flag = false
and parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
)
then true else false end as gds_account_flag,