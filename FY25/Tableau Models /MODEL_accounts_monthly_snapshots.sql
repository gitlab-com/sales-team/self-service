--Updates that will be needed:
--Task and case dates updated to just FY25 if that's what we want to look at
--FO SDR case flag

with 
rpt_product_usage_health_score as
(
  select * from PROD.common_mart_product.rpt_product_usage_health_score
),

mart_product_usage_paid_user_metrics_monthly as
(
  select * from PROD.common_mart_product.mart_product_usage_paid_user_metrics_monthly
),

dim_crm_user AS (
  SELECT *
  FROM prod.common.dim_crm_user
),

dim_date AS (
  SELECT *
  FROM prod.common.dim_date
),

dim_order AS (
  SELECT *
  FROM prod.common.dim_order
),

dim_order_action AS (
  SELECT *
  FROM prod.common.dim_order_action
),

dim_product_detail AS (
  SELECT *
  FROM prod.common.dim_product_detail
),

dim_subscription AS (
  SELECT *
  FROM prod.common.dim_subscription
),

mart_crm_task AS (
  SELECT *
  FROM prod.common_mart_sales.mart_crm_task
),

mart_arr AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_arr
),

mart_charge AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_charge
),

mart_crm_account AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_account
),

mart_crm_opportunity AS (
  SELECT *
  FROM prod.restricted_safe_common_mart_sales.mart_crm_opportunity
),

dim_crm_account_daily_snapshot AS (
  SELECT *
  FROM prod.restricted_safe_common.dim_crm_account_daily_snapshot
),

mart_delta_arr_subscription_month AS (
  SELECT *
  FROM prod.restricted_safe_legacy.mart_delta_arr_subscription_month
),

PREP_CRM_CASE as
(
select * from
PROD.COMMON_PREP.PREP_CRM_CASE
),

case_history as
(
select * from
RAW.SALESFORCE_V2_STITCH.CASEHISTORY
),

usage_data as
(
SELECT rpt.SNAPSHOT_MONTH,
rpt.DIM_CRM_ACCOUNT_ID,
rpt.CRM_ACCOUNT_NAME,
SCM_SCORE,
CI_SCORE,
EPICS_28_DAYS_USER,
ISSUES_28_DAYS_USER,
ISSUES_EDIT_28_DAYS_USER,
EPICS_USAGE_28_DAYS_USER
FROM rpt_product_usage_health_score rpt
LEFT JOIN mart_product_usage_paid_user_metrics_monthly mpu ON rpt.primary_key = mpu.primary_key
WHERE rpt.SNAPSHOT_MONTH > '2023-01-30'
),

account_base as (
SELECT 
case when date_trunc('month',acct.snapshot_date) = '2024-02-01' then '2024-02-01' else acct.snapshot_date end as snapshot_date,
acct.* EXCLUDE(snapshot_date), 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from MART_CRM_ACCOUNT
  where crm_account_owner in
  ('AMER SMB Sales')  or owner_role = 'Advocate_SMB_AMER'
  ))
or (acct.snapshot_date < '2024-02-01' and(acct.parent_crm_account_geo in ('AMER')
or acct.parent_crm_account_region in ('AMER'))
) then 'AMER' 
WHEN (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from MART_CRM_ACCOUNT
  where crm_account_owner in
  ('EMEA SMB Sales','APAC SMB Sales') or owner_role = 'Advocate_SMB_EMEA'))
or (acct.snapshot_date < '2024-02-01' and (acct.parent_crm_account_geo in ('EMEA', 'APJ','APAC')
or acct.parent_crm_account_region in ('EMEA', 'APJ','APAC'))
) then 'EMEA' 
     ELSE 'Other' end as Team,
case when (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from MART_CRM_ACCOUNT
  where crm_account_owner in
  ('AMER SMB Sales','APAC SMB Sales','EMEA SMB Sales')  or owner_role like 'Advocate%'))
or
(
acct.snapshot_date < '2024-02-01' and
(acct.carr_account_family <= 30000 --and acct.carr_this_account > 0
)
and (acct.parent_crm_account_max_family_employee <= 100 or acct.parent_crm_account_max_family_employee is null)
and ultimate.dim_parent_crm_account_id is null
and acct.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and acct.parent_crm_account_upa_country <> 'JP'
and acct.is_jihu_account = false
)
then true else false end as gds_account_flag,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
fo.SQS as fo_sqs,
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr, 
case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_owner_name as high_value_account_owner,
high_value_case.team as high_value_account_team,
high_value_case.manager_name as high_value_manager_name, 
high_value_case.case_id as high_value_case_id, 
start_values.carr_account_family as starting_carr_account_family, 
start_values.carr_this_account as starting_carr_this_account, 
CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
     WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
     else 'Tier 2' end as starting_calculated_tier,
start_values.pte_score as starting_pte_score, 
start_values.ptc_score as starting_ptc_score, 
start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag,
usage_data.* EXCLUDE (dim_crm_account_id,crm_account_name,SNAPSHOT_MONTH)
FROM DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
    last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS
    from
    mart_crm_opportunity
    LEFT JOIN dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo
    ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
LEFT JOIN 
  (
select snapshot_date as close_date, dim_crm_account_id, carr_this_account,
lag(carr_this_account,1) over(partition by dim_crm_account_id order by snapshot_date asc) as prior_carr,
-prior_carr as net_arr,
max(case when carr_this_account > 0 then snapshot_date else null end) over(partition by dim_crm_account_id) as last_carr_date
from DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
 where snapshot_date >= '2021-02-01'
-- and dim_crm_account_id = '0014M00001lbBt1QAE'
qualify prior_carr > 0 and carr_this_account = 0 and snapshot_date > last_carr_date
    ) churn 
    ON churn.DIM_CRM_ACCOUNT_ID = acct.DIM_CRM_ACCOUNT_ID
--------subquery to get high tier case owner
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    dim_crm_account_id,
    case_id,
    prep_crm_case.dim_crm_user_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
    MAX(prep_crm_case.created_date) over(partition by dim_crm_ACCOUNT_ID) as last_high_value_date
    FROM prep_crm_case
    LEFT JOIN common.dim_crm_user 
        ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
--    and account_id = '0014M00001gTGESQA4'
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date 
) high_value_case
    ON high_value_case.dim_crm_account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
LEFT JOIN 
    (SELECT
    *
    FROM DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    WHERE snapshot_date = '2024-02-10' -----placeholder date for start of year
    ) start_values
    ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    dim_crm_account_id,
    case_id,
    prep_crm_case.dim_crm_user_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
    MIN(prep_crm_case.created_date) over(partition by dim_crm_ACCOUNT_ID) as last_high_value_date
    FROM prep_crm_case
    LEFT JOIN common.dim_crm_user 
        ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
--    and account_id = '0014M00001gTGESQA4'
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date 
) first_high_value_case
    ON first_high_value_case.dim_crm_account_id = acct.dim_crm_account_id

-----EOA cohort accounts
    LEFT JOIN 
    ( select distinct dim_crm_account_id from
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM MART_ARR
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
))) EOA 
    ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
------free limit promo cohort accounts
LEFT JOIN 
    (select
  distinct
dim_crm_account_id
  from mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
    ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
------price increase promo cohort accounts
LEFT JOIN
    (select distinct dim_crm_account_id from
        (
         select
          charge.*,
          arr / quantity as actual_price,
          prod.annual_billing_list_price as list_price
          from mart_charge charge
          inner join DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
          where 
          subscription_start_date >= '2023-04-01'
          and subscription_start_date <= '2023-07-01'
          and TYPE_OF_ARR_CHANGE = 'New'
          and quantity > 0
          and actual_price > 228
          and actual_price < 290
          and rate_plan_charge_name like '%Premium%'
        )) price_increase
        ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
LEFT JOIN
(
    select
distinct
dim_parent_crm_account_id, arr_month
from
MART_ARR
where product_tier_name like '%Ultimate%'
and arr > 0
) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id
and ultimate.arr_month = date_trunc('month',acct.snapshot_date)
left join usage_data on usage_data.dim_crm_account_id = acct.dim_crm_account_id
and usage_data.SNAPSHOT_MONTH = acct.snapshot_date
WHERE acct.snapshot_date >= '2023-02-01'
and acct.snapshot_date = 
case when date_trunc('month',acct.snapshot_date) = '2024-02-01' then '2024-02-10' else
date_trunc('month',acct.snapshot_date) end
and acct.DIM_parent_CRM_ACCOUNT_ID in
(select
distinct dim_parent_crm_account_id
from mart_arr
where arr_month >= '2023-02-01'
and arr > 0
)
),

/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/


case_data as
(
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
PREP_CRM_CASE.*
 ,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.user_role_type
from PREP_CRM_CASE
left join dim_crm_user on PREP_CRM_CASE.dim_crm_user_id = dim_crm_user.dim_crm_user_id

where
PREP_CRM_CASE.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and PREP_CRM_CASE.created_date >= '2024-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
),

with task_data as (
WITH base_data AS (
  SELECT *
  FROM (
    SELECT *
  FROM (
    SELECT
      task_id,
      task_status,
      task.dim_crm_account_id,
      task.dim_crm_user_id,
      task.dim_crm_person_id,
      task_type,
      task_subject,
      CASE
        WHEN task_subject LIKE '%Outreach%' AND task_subject NOT LIKE '%Advanced Outreach%' THEN 'Outreach'
        WHEN task_subject LIKE '%Clari%' THEN 'Clari'
        WHEN task_subject LIKE '%Conversica%' THEN 'Conversica'
        ELSE 'Other'
      END AS outreach_clari_flag,
      -------flagging groove tasks
      CASE
        WHEN (contains(lower(task_subject), '[inbox]') or contains(lower(task_subject), '[flow]')) AND outreach_clari_flag = 'Other' then TRUE else FALSE end as groove_flag,
      --------categorizing groove tasks
            CASE
        WHEN groove_flag = True and task_type = 'Call' then 'Groove Call'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Flow]') then 'Groove Flow Email'
        WHEN groove_flag = True and task_type = 'Email' and contains(task_subject, '[Inbox]') then 'Groove Inbox Email'
        WHEN groove_flag = True and task_type = 'Call' and contains(task_subject, 'Flow') then 'Groove Flow Call'
        End as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      CASE
        WHEN groove_flag = TRUE and contains(lower(task_subject), '<<') then 'Inbound' 
        WHEN groove_flag = True and contains(lower(task_subject), '>>') then 'Outbound'
        Else 'Other' 
        END as groove_inbound_outbound_flag,
      --------flagging gong tasks
      CASE 
        WHEN contains(lower(task_subject), 'gong') and outreach_clari_flag = 'Other' then TRUE Else FALSE end as gong_flag,
      CASE
        WHEN gong_flag = True and task_type = 'Meeting' then TRUE 
        WHEN contains(lower(task_subject), '(ii)') then TRUE
        WHEN contains(lower(task_subject), '#meeting') then TRUE 
        WHEN task_type = 'Meeting' then TRUE
        ELSE False End as meeting_flag, 
      -----------Flagging for groove task types
    
      task_created_date,
      task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      CASE
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[Out]%' OR task_subject LIKE '%utbound%')
          THEN 'Outbound'
        WHEN outreach_clari_flag = 'Outreach' AND (task_subject LIKE '%[In]%' OR task_subject LIKE '%nbound%')
          THEN 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email received') then 'Inbound'
        WHEN outreach_clari_flag = 'Clari' and contains(lower(task_subject), '[email sent') then 'Outbound'
        WHEN groove_inbound_outbound_flag = 'Inbound' then 'Inbound'
        WHEN groove_inbound_outbound_flag = 'Outbound' then 'Outbound'
        ELSE 'Other'
      END                                                                                                                 AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,

      
      task_date,
      user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_TASK AS task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id
    WHERE task.dim_crm_user_id IS NOT NULL
      AND is_deleted = FALSE
      AND task_date >= '2024-02-01'
      AND task_status = 'Completed'

      UNION 

      SELECT 
      event_id as task_id,
      'Completed' as task_status,
      dim_crm_account_id,
      task.dim_crm_user_id,
      dim_crm_person_id,
      event_type as task_type,
      event_subject as task_subject,
      'Other' AS outreach_clari_flag,
      -------flagging groove tasks
      FALSE as groove_flag,
      --------categorizing groove tasks
      'Non-Groove - Event' as Groove_Task_Category,
     --------flagging groove tasks as inbound or outbound
      'Other' as groove_inbound_outbound_flag,
      --------flagging gong tasks
      FALSE as gong_flag,
      TRUE as meeting_flag, 
      -----------Flagging for groove task types
    
      event_created_date as task_created_date,
      event_created_by_id as task_created_by_id,

      --This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

      'Other' AS inbound_outbound_flag,

      COALESCE ((
        inbound_outbound_flag = 'Outbound' AND task_subject LIKE '%Answered%'
        AND task_subject NOT LIKE '%Not Answer%'
        AND task_subject NOT LIKE '%No Answer%'
      )
      OR (LOWER(task_subject) LIKE '%call%' AND task_subject NOT LIKE '%Outreach%' AND task_status = 'Completed'), FALSE) AS outbound_answered_flag,
     event_date as task_date,
     user.user_name  AS task_user_name,
      CASE
        WHEN user.department LIKE '%arketin%' THEN 'Marketing'
        ELSE user.department
      END AS department,
      user.is_active,
      user.crm_user_sales_segment,
      user.crm_user_geo,
      user.crm_user_region,
      user.crm_user_area,
      user.crm_user_business_unit,
      user.user_role_name
    FROM PROD.COMMON_MART_SALES.MART_CRM_EVENT task
    INNER JOIN dim_crm_user AS user
      ON task.dim_crm_user_id = user.dim_crm_user_id 
    WHERE 
      lower(event_subject) like '%#meeting' or lower(event_subject) like '%#meeting%' or lower(event_subject) like '#meeting%'
      OR lower(event_subject) like '%(ii)%'
      AND event_date >= '2024-02-01'
  )

  WHERE (
      user_role_name LIKE ANY ('%AE%', '%SDR%', '%BDR%', '%Advocate%')
      OR crm_user_sales_segment = 'SMB')
)
  ),
meeting_deduped AS (
  SELECT *,
    CASE 
      WHEN meeting_flag = TRUE THEN
        ROW_NUMBER() OVER (
          PARTITION BY dim_crm_account_id, task_date  -- Partitioning by account AND date
          ORDER BY 
            task_created_date ASC,
            CASE 
              WHEN task_type = 'Meeting' THEN 1
              ELSE 2 
            END
        )
      ELSE 1  -- Non-meetings get a rank of 1 so they're always kept
    END as meeting_rank
  FROM base_data
)
SELECT *
FROM meeting_deduped
WHERE meeting_rank = 1) ,


get_case_owner_history as
(
select 
iff(first_value(OLDVALUE) over(partition by caseid order by createddate asc) is null
or first_value(OLDVALUE) over(partition by caseid order by createddate asc) = '00G8X000006WmU3UAK',
first_value(NEWVALUE) over(partition by caseid order by createddate asc),
first_value(OLDVALUE) over(partition by caseid order by createddate asc))
as dim_crm_user_id_first_owner,
date(first_value(createddate) over(partition by caseid order by createddate asc)) as first_change_date,
first_value(NEWVALUE) over(partition by caseid,date(createddate) order by createddate desc) as dim_crm_user_id_last_owner_on_date,
date(lag(createddate,1) over(partition by caseid order by createddate asc)) as prior_owner_change_date,
date(lead(createddate,1) over(partition by caseid order by createddate asc)) as next_owner_change_date,
--first_value(NEWVALUE) over(partition by caseid order by createddate asc) as user_name_first_owner,
case_data.dim_crm_user_id as dim_crm_user_id_current_owner,
case_history.* EXCLUDE createddate,
date(createddate) as change_date
from case_history
inner join case_data on case_history.caseid = case_data.case_id
 where 
 field = 'Owner'
and datatype = 'EntityId'
--and case_id = '500PL000008CLxBYAW'
),

case_owner_logic as
(
select
distinct
dim_date.date_actual,
case_data.case_id,
case_data.created_date::date as created_date,
get_case_owner_history.change_date,
get_case_owner_history.dim_crm_user_id_first_owner,
get_case_owner_history.first_change_date,
get_case_owner_history.prior_owner_change_date,
get_case_owner_history.next_owner_change_date,
get_case_owner_history.dim_crm_user_id_last_owner_on_date,
case 
when get_case_owner_history.caseid is null then case_data.dim_crm_user_id
else
case
when date_actual < created_date then null
when date_actual >= created_date and date_actual < first_change_date then dim_crm_user_id_first_owner
when date_actual >= created_date and date_actual >= change_date and (date_actual < next_owner_change_date or next_owner_change_date is null) then dim_crm_user_id_last_owner_on_date
else null end
end
as dim_crm_user_id_owner_on_date
from
dim_date
left join case_data
left join get_case_owner_history on get_case_owner_history.caseid = case_data.case_id
--and change_date = date_actual
where
dim_date.fiscal_year = 2025
--and case_id = '500PL000005mnhTYAQ'
order by 1 asc
),

case_history_output as
(
select
distinct
date_actual,
case_id,
dim_crm_user_id_first_owner,
created_date,
dim_crm_user_id_owner_on_date
from
case_owner_logic
where
(dim_crm_user_id_owner_on_date is not null or date_actual < created_date)
order by 1 asc
),

case_task_summary as
(
select 
account_base.dim_crm_account_id as case_task_summary_id,

listagg(distinct case_data.case_trigger, ', ') within group (order by case_data.case_trigger)
as acct_trigger_list,
listagg(distinct case_data.case_id, ', ')
as acct_case_id_list,

count(distinct case_data.case_id) as closed_case_count,
count(distinct case when case_data.status = 'Closed: Resolved' or case_data.status = 'Closed' then case_data.case_id else null end) as resolved_case_count,

count(distinct case when task_data.inbound_outbound_flag = 'Inbound' then task_data.task_id else null end) as inbound_email_count,
count(distinct case when task_data.outbound_answered_flag then task_data.task_id else null end) as completed_call_count,
count(distinct case when task_data.inbound_outbound_flag = 'Outbound' then task_data.task_id else null end) as outbound_email_count,
case when inbound_email_count > 0 then true else false end as task_inbound_flag,
case when completed_call_count > 0 then true else false end as task_completed_call_flag,
case when outbound_email_count > 0 then true else false end as task_outbound_flag,
count(distinct task_data.task_id) as completed_task_count

from account_base
left join case_data
on account_base.dim_crm_account_id = case_data.dim_crm_account_id
and case_data.is_closed
and case_data.closed_date <= account_base.snapshot_date
left join task_data
on account_base.dim_crm_account_id = task_data.dim_crm_account_id
and task_data.task_date <= account_base.snapshot_date
--where case_data.account_id is not null
group by 1
)

select
account_base.*,
case_task_summary.*,
   dim_crm_user.user_name                                                                                           AS case_owner_name_on_date
from account_base
left join case_task_summary on account_base.dim_crm_account_id = case_task_summary.case_task_summary_id
left join case_history_output on account_base.high_value_case_id = case_history_output.case_id
and account_base.snapshot_date = date_trunc('month', case_history_output.date_actual)
and case_history_output.date_actual = date_trunc('month', case_history_output.date_actual)
  LEFT JOIN dim_crm_user
    ON case_history_output.dim_crm_user_id_owner_on_date = dim_crm_user.dim_crm_user_id
