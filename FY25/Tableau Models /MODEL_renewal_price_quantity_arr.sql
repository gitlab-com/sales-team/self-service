with
previous_data as
(
select
dim_crm_account_id,
date_trunc('month',snapshot_date) as snapshot_month,
arr_month,
dim_subscription_id,
product_rate_plan_name,
sum(arr) as arr,
sum(quantity) as quantity
from
prod.restricted_safe_common_mart_sales.mart_arr_snapshot_model 
where 
product_rate_plan_name like any ('%Premium%','%Ultimate%','%Silver%','%Gold%','%Starter%','%Bronze%')
and 
arr_month = date_trunc('month',snapshot_date)
and snapshot_date = date_trunc('month',snapshot_date)
and snapshot_date >= '2021-01-01'
group by 1,2,3,4,5)

select
MART_CRM_OPPORTUNITY.dim_crm_account_id,
MART_CRM_OPPORTUNITY.dim_crm_opportunity_id,
MART_CRM_OPPORTUNITY.sales_type,
mart_crm_opportunity.order_type,
MART_CRM_OPPORTUNITY.close_date,
MART_CRM_OPPORTUNITY.close_month,
current_data.arr_month,
row_number() over(partition by MART_CRM_OPPORTUNITY.dim_crm_account_id,MART_CRM_OPPORTUNITY.dim_crm_opportunity_id order by current_data.arr desc) as arr_rank,
current_data.arr as total_new_arr,
previous_data.arr as total_previous_arr,
previous_data.quantity as total_previous_quantity,
current_data.quantity as total_new_quantity,
total_previous_arr / total_previous_quantity as total_previous_price,
total_new_arr / total_new_quantity as total_new_price
,
(total_new_price - total_previous_price) * total_previous_quantity as price_increase_arr,
(total_new_quantity - total_previous_quantity) * total_new_price as quantity_increase_arr
,
current_data.product_rate_plan_name as new_product,
previous_data.product_rate_plan_name as previous_product

from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
left join previous_data
on mart_crm_opportunity.dim_crm_account_id = previous_data.dim_crm_account_id
and 
(
(mart_crm_opportunity.close_month < date_trunc('month',current_date) and
((order_type not like '%Churn%' and 
previous_data.arr_month = dateadd('month',-1,mart_crm_opportunity.close_month))
or
(order_type like '%Churn%' and 
previous_data.arr_month = dateadd('month',-3,mart_crm_opportunity.close_month))
))
or
(
mart_crm_opportunity.close_month >= date_trunc('month',current_date) and
previous_data.arr_month = date_trunc('month',current_date))
)
left join previous_data current_data
on mart_crm_opportunity.dim_crm_account_id = current_data.dim_crm_account_id
and current_data.arr_month = dateadd('month',1,mart_crm_opportunity.close_month)
where sales_type = 'Renewal'
and close_month >= '2022-02-01'
and close_month < date_trunc('month',current_date)
and mart_crm_opportunity.arr_basis_for_clari > 0
qualify (arr_rank = 1 or arr_rank is null)