with task_data as
(
select * from

(select 
    task_id,
task_status,
iff(task.dim_crm_account_id like '6bb%',null,task.dim_crm_account_id) as dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_created_by_id,

--This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when (inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%') or (lower(task_subject) like '%call%' and task_subject not like '%Outreach%' and task_status = 'Completed' ) then true else false end as outbound_answered_flag,
task_date,
      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name as task_user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
        user.crm_user_geo,
user.crm_user_region,
user.crm_user_area,
user.crm_user_business_unit,
      user.user_role_name
      from
 common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2024-02-01'
and task_status = 'Completed')

where (outreach_clari_flag = 'Outreach' or task_created_by_id = dim_crm_user_id)
and outreach_clari_flag <> 'Other'
and (user_role_name like any ( '%AE%','%SDR%','%BDR%','%Advocate%')
or crm_user_sales_segment = 'SMB')
),

lead_account_ids as (
select dim_crm_account_id, sfdc_record_id, dim_crm_person_id
from PROD.COMMON_MART_MARKETING.MART_CRM_PERSON
where sfdc_record_type = 'lead'
and dim_crm_account_id is not null
)

select
lead_account_ids.dim_crm_account_id as lead_account_id,
task_data.* exclude dim_crm_account_id,
coalesce(task_data.dim_crm_account_id,lead_account_id) as dim_crm_account_id_prep,
dim_crm_account_id_prep as dim_crm_account_id

from task_data
left join lead_account_ids on task_data.dim_crm_person_id = lead_account_ids.dim_crm_person_id
where dim_crm_account_id_prep is not null