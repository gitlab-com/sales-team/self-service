select distinct dim_crm_account_id from
(
 select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  term_start_date >= '2023-04-01'
  and term_start_date <= '2024-05-01'
  and TYPE_OF_ARR_CHANGE <> 'New'
  and subscription_start_date < '2023-04-01'
  and rate_plan_charge_description like '%18%'
)
