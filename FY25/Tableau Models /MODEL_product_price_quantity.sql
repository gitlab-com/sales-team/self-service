with arr_data as
(
SELECT 
mart_arr.*
--,monthly_mart.snapshot_month 
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,DIV0(mart_arr.ARR, mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
--,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
LEFT JOIN 
  (select
  --DISTINCT
  --  snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    max(date_trunc('month',ping_created_at)) as latest_ping,
    max(LICENSE_USER_COUNT) as license_user_count,
    MAX(BILLABLE_USER_COUNT)
   
    as max_billable_user_count

    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active' 
    group by 1,2,3,4,5,6
 --   and (snapshot_month = DATE_TRUNC('month',CURRENT_DATE) or snapshot_month = dateadd('month',-1,DATE_TRUNC('month',CURRENT_DATE)))
  having (latest_ping = DATE_TRUNC('month',CURRENT_DATE) or latest_ping = dateadd('month',-1,DATE_TRUNC('month',CURRENT_DATE)))
   order by 1
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE arr_month = dateadd('month',-1,DATE_TRUNC('month',CURRENT_DATE))
and PRODUCT_TIER_NAME like '%Premium%'
)

select *
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,DIV0(ARR , (case when license_user_count is null then quantity else license_user_count end))  as arr_per_user,
greatest(zeroifnull(max_billable_user_count),quantity) as current_user_count,
arr_per_user/12 as monthly_price_per_user,
case when monthly_price_per_user < (case when subscription_end_month > '2024-04-01' then 29 else 23.78 end) and product_tier_name like '%Premium%' then true else false end as fy25_premium_price_increase_flag,
case when fy25_premium_price_increase_flag then (
    (case when subscription_end_month > '2024-04-01' then 29 else 23.78 end)
     - monthly_price_per_user) * current_user_count * 12 else 0 end as fy25_estimated_price_increase_impact,
case when arr + fy25_estimated_price_increase_impact >= 7000 and arr < 7000 then true else false end as fy25_likely_price_increase_uptier_flag,
arr as test_arr,
current_user_count * (case when subscription_end_month > '2024-04-01' then 29 else 23.78 end) * 12 as next_arr
FROM arr_data
where
arr > 0