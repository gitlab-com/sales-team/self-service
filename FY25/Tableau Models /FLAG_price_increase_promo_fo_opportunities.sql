--Gets all FO opportunities associated with Price Increase promo

select
distinct dim_crm_opportunity_id,
actual_price
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
max(arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
where
-- effective_start_date >= '2023-02-01'
-- and
dim_subscription.subscription_start_date >= '2023-04-01'
  and dim_subscription.subscription_start_date <= '2023-07-01'
  and TYPE_OF_ARR_CHANGE = 'New'
  and quantity > 0
  -- and actual_price > 228
  -- and actual_price < 290
  and rate_plan_charge_name like '%Premium%'
qualify actual_price > 228
  and actual_price < 290
)
