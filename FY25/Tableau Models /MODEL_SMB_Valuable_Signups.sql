with 

rpt_namespace_onboarding as
(
    select * from common_mart_product.rpt_namespace_onboarding
),

mart_crm_person as
(
    select * from common_mart_marketing.mart_crm_person
)

SELECT
    count (distinct ultimate_parent_namespace_id) as Trial_namespace_signups,
    count (distinct rpt_namespace_onboarding.sfdc_record_id) as Trial_SFDC_signups,
    trial_start_date,
    creator_is_valuable_signup,
    namespace_contains_valuable_signup,
    is_namespace_created_within_2min_of_creator_invite_acceptance,
    account_demographics_sales_segment
FROM rpt_namespace_onboarding
    join mart_crm_person
        on rpt_namespace_onboarding.sfdc_record_id = mart_crm_person.sfdc_record_id 
WHERE is_namespace_created_within_2min_of_creator_invite_acceptance = false
AND creator_is_valuable_signup
AND account_demographics_sales_segment = 'SMB'
GROUP BY 3,4,5,6,7