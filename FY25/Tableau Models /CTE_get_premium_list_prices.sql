select effective_start_month,
mode(actual_price) as mode_price
from
(select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  effective_start_month >= '2022-02-01'
  and parent_account_cohort_month = effective_start_month
  and charge.effective_start_date < current_date
  and TYPE_OF_ARR_CHANGE = 'New'
  and quantity > 0
  and actual_price > 120
  and rate_plan_charge_name like '%Premium%'-- or rate_plan_charge_name like '%Ultimate%'
  )
  group by 1