with gds_accounts as

(select * from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
where crm_account_owner like '%SMB Sales'
and carr_this_account > 0
)

select 
snapshot_date,
MART_CRM_OPPORTUNITY_DAILY_SNAPSHOT.dim_crm_account_id,
dim_crm_opportunity_id,
ptc_predicted_renewal_risk_category,
ptc_predicted_arr,
is_closed,
is_won,
net_arr,
lag(ptc_predicted_renewal_risk_category,1) over(partition by dim_crm_opportunity_id order by snapshot_date asc) as prior_predicted_risk,
lag(ptc_predicted_arr,1) over(partition by dim_crm_opportunity_id order by snapshot_date asc) as prior_predicted_arr
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY_DAILY_SNAPSHOT
left join gds_accounts on MART_CRM_OPPORTUNITY_DAILY_SNAPSHOT.dim_crm_account_id = gds_accounts.dim_crm_account_id
where sales_type = 'Renewal'
and close_date >= '2024-02-01'
and snapshot_date >= '2023-06-01'
and ptc_predicted_renewal_risk_category is not null
and snapshot_date = date_trunc('month',snapshot_date)
qualify prior_predicted_risk is not null
and ptc_predicted_renewal_risk_category <> prior_predicted_risk
