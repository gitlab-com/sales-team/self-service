select 
distinct
dim_crm_account_id,
dim_subscription_id,
product_rate_plan_name,
subscription_end_month,
turn_on_auto_renewal,
count(distinct product_tier_name) over(partition by dim_subscription_id) as sub_prod_count
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = '2024-01-01'
and parent_crm_account_sales_segment = 'SMB'
and turn_on_auto_renewal = 'Yes'
qualify sub_prod_count > 1
