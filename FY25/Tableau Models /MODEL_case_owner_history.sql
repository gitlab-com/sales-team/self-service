/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/

with

PREP_CRM_CASE as
(
select * from
PROD.COMMON_PREP.PREP_CRM_CASE
),

dim_crm_user as
(
select * from
PROD.COMMON.DIM_CRM_USER
),

case_history as
(
select * from
RAW.SALESFORCE_V2_STITCH.CASEHISTORY
),

dim_date as
(
select * from
prod.common.dim_date
),



case_data as
(
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
PREP_CRM_CASE.*
 ,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.user_role_type
from PREP_CRM_CASE
left join dim_crm_user on PREP_CRM_CASE.dim_crm_user_id = dim_crm_user.dim_crm_user_id

where
PREP_CRM_CASE.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and PREP_CRM_CASE.created_date >= '2024-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
),

-- select * from case_data
-- where case_id = '500PL00000CGBQRYA5'

get_case_owner_history as
(
select 
iff(first_value(OLDVALUE) over(partition by caseid order by createddate asc) is null
or first_value(OLDVALUE) over(partition by caseid order by createddate asc) = '00G8X000006WmU3UAK',
first_value(NEWVALUE) over(partition by caseid order by createddate asc),
first_value(OLDVALUE) over(partition by caseid order by createddate asc))
as dim_crm_user_id_first_owner,
date(first_value(createddate) over(partition by caseid order by createddate asc)) as first_change_date,
first_value(NEWVALUE) over(partition by caseid,date(createddate) order by createddate desc) as dim_crm_user_id_last_owner_on_date,
date(lag(createddate,1) over(partition by caseid order by createddate asc)) as prior_owner_change_date,
date(lead(createddate,1) over(partition by caseid order by createddate asc)) as next_owner_change_date,
--first_value(NEWVALUE) over(partition by caseid order by createddate asc) as user_name_first_owner,
case_data.dim_crm_user_id as dim_crm_user_id_current_owner,
case_history.* EXCLUDE createddate,
date(createddate) as change_date
from case_history
inner join case_data on case_history.caseid = case_data.case_id
 where 
 field = 'Owner'
and datatype = 'EntityId'
--and case_id = '500PL000008CLxBYAW'
),

case_owner_logic as
(
select
distinct
dim_date.date_actual,
case_data.case_id,
case_data.created_date::date as created_date,
get_case_owner_history.change_date,
get_case_owner_history.dim_crm_user_id_first_owner,
get_case_owner_history.first_change_date,
get_case_owner_history.prior_owner_change_date,
get_case_owner_history.next_owner_change_date,
get_case_owner_history.dim_crm_user_id_last_owner_on_date,
case 
when get_case_owner_history.caseid is null then case_data.dim_crm_user_id
else
case
when date_actual < created_date then null
when date_actual >= created_date and date_actual < first_change_date then dim_crm_user_id_first_owner
when date_actual >= created_date and date_actual >= change_date and (date_actual < next_owner_change_date or next_owner_change_date is null) then dim_crm_user_id_last_owner_on_date
else null end
end
as dim_crm_user_id_owner_on_date
from
dim_date
left join case_data
left join get_case_owner_history on get_case_owner_history.caseid = case_data.case_id
--and change_date = date_actual
where
dim_date.fiscal_year = 2025
--and case_id = '500PL000005mnhTYAQ'
order by 1 asc
),

case_history_output as
(
select
distinct
date_actual,
case_id,
dim_crm_user_id_first_owner,
created_date,
dim_crm_user_id_owner_on_date
from
case_owner_logic
where
(dim_crm_user_id_owner_on_date is not null or date_actual < created_date)
order by 1 asc
)
select * from case_history_output