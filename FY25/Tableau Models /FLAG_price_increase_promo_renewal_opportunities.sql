select distinct dim_crm_opportunity_id_current_open_renewal from
(
 select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price,
  dim_subscription.dim_crm_opportunity_id_current_open_renewal
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  inner join common.dim_subscription on dim_subscription.dim_subscription_id = charge.dim_subscription_id
  where 
  charge.term_start_date >= '2023-04-01'
  and charge.term_start_date <= '2024-05-01'
  and charge.TYPE_OF_ARR_CHANGE <> 'New'
  and charge.subscription_start_date < '2023-04-01'
  and charge.quantity > 0
  and actual_price > 228
  and actual_price < 290
  and charge.rate_plan_charge_name like '%Premium%'
)
