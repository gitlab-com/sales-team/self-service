with 

account_base as (
SELECT distinct
acct.*, 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN crm_account_owner in
  ('AMER SMB Sales') then 'AMER' 
WHEN crm_account_owner in
  ('EMEA SMB Sales','APAC SMB Sales') then 'EMEA' 
     ELSE 'Other' end as Team,
case when crm_account_owner in
  ('AMER SMB Sales','APAC SMB Sales','EMEA SMB Sales')
-- or
-- (
-- acct.snapshot_date < '2024-02-01' and
-- (acct.carr_account_family <= 30000 --and acct.carr_this_account > 0
-- )
-- and (acct.parent_crm_account_max_family_employee <= 100 or acct.parent_crm_account_max_family_employee is null)
-- and ultimate.dim_parent_crm_account_id is null
-- and acct.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
-- and acct.parent_crm_account_upa_country <> 'JP'
-- and acct.is_jihu_account = false
-- )
then true else false end as gds_account_flag,
-- fo.fiscal_year as fo_fiscal_year,
-- fo.close_date as fo_close_date,
-- fo.net_arr as fo_net_arr,
-- fo.SQS as fo_sqs,
-- churn.close_date as churn_close_date, 
-- churn.net_arr as churn_net_arr, 
-- case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_id as high_value_case_id,
high_value_case.case_owner_id as high_value_case_owner,
high_value_case.team as high_value_case_owner_team,
high_value_case.manager_name as high_value_case_owner_manager, 
FY25_high_value_case.owner_id as FY25_case_owner_id,
-- start_values.carr_account_family as starting_carr_account_family, 
-- start_values.carr_this_account as starting_carr_this_account, 
-- CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
--      WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
--      else 'Tier 2' end as starting_calculated_tier,
-- start_values.pte_score as starting_pte_score, 
-- start_values.ptc_score as starting_ptc_score, 
-- start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
-- CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
-- CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
-- CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
-- CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag
FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
-- LEFT JOIN 
--   (
--  select
--     distinct
--     DIM_parent_CRM_ACCOUNT_ID,
--     last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
--     last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
--     last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
--     last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS
--     from
--     restricted_safe_common_mart_sales.mart_crm_opportunity
--     LEFT JOIN common.dim_date
--         ON mart_crm_opportunity.close_date = dim_date.date_actual
--     where
--     is_won
--     and order_type = '1. New - First Order'
--     ) fo
--     ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
-- LEFT JOIN 
--   (
-- select snapshot_date as close_date, dim_crm_account_id, carr_this_account,
-- lag(carr_this_account,1) over(partition by dim_crm_account_id order by snapshot_date asc) as prior_carr,
-- -prior_carr as net_arr,
-- max(case when carr_this_account > 0 then snapshot_date else null end) over(partition by dim_crm_account_id) as last_carr_date
-- from PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
--  where snapshot_date >= '2021-02-01'
-- -- and dim_crm_account_id = '0014M00001lbBt1QAE'
-- qualify prior_carr > 0 and carr_this_account = 0 and snapshot_date > last_carr_date
--     ) churn 
--     ON churn.DIM_CRM_ACCOUNT_ID = acct.DIM_CRM_ACCOUNT_ID
--------subquery to get current high tier case owner
LEFT JOIN 
    (SELECT * 
    FROM 
    (SELECT
    account_id,
    case_id,
    owner_id as case_owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type, 
    sfdc_case.created_date,
    MAX(sfdc_case.created_date) over(partition by ACCOUNT_ID) as latest_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    AND lower(subject) like '%high value account%'
    AND lower(subject) != 'high value account check in') 
    WHERE created_date = latest_high_value_date) high_value_case
    ON high_value_case.account_id = acct.dim_crm_account_id
--------------getting the previous high value case owner/etc
LEFT JOIN 
    (SELECT
    account_id,
    owner_id, 
    case_id,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and subject = 'FY25 High Value Account' -----subject placeholder - this could change
    ) FY25_high_value_case
    ON FY25_high_value_case.account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
-- LEFT JOIN 
--     (SELECT
--     *
--     FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
--     WHERE snapshot_date = '2024-02-09' 
--     ) start_values
--     ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    sfdc_case.created_date,
    MIN(sfdc_case.created_date) over(partition by ACCOUNT_ID) as first_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    AND lower(subject) like '%high value account%'
    AND lower(subject) != 'high value account check in') -----subject placeholder - this could change
    WHERE created_date = first_high_value_date 
    ) first_high_value_case
    ON first_high_value_case.account_id = acct.dim_crm_account_id

-- -----EOA cohort accounts
--     LEFT JOIN 
--     (SELECT
-- mart_arr.ARR_MONTH
-- --,ping_created_at
-- ,mart_arr.SUBSCRIPTION_END_MONTH
-- ,mart_arr.DIM_CRM_ACCOUNT_ID
-- ,mart_arr.CRM_ACCOUNT_NAME
-- --,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
-- ,mart_arr.DIM_SUBSCRIPTION_ID
-- ,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
-- ,mart_arr.SUBSCRIPTION_NAME
-- ,mart_arr.subscription_sales_type
-- ,mart_arr.AUTO_PAY
-- ,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
-- ,mart_arr.CONTRACT_AUTO_RENEWAL
-- ,mart_arr.TURN_ON_AUTO_RENEWAL
-- ,mart_arr.TURN_ON_CLOUD_LICENSING
-- ,mart_arr.CONTRACT_SEAT_RECONCILIATION
-- ,mart_arr.TURN_ON_SEAT_RECONCILIATION
-- ,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
-- ,mart_arr.PRODUCT_TIER_NAME
-- ,mart_arr.PRODUCT_DELIVERY_TYPE
-- ,mart_arr.PRODUCT_RATE_PLAN_NAME
-- ,mart_arr.ARR
-- --,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
-- ,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
-- arr_per_user/12 as monthly_price_per_user,
-- mart_arr.mrr/mart_arr.quantity as mrr_check
-- FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- -- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
-- --     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
-- WHERE ARR_MONTH = '2023-01-01'
-- and PRODUCT_TIER_NAME like '%Premium%'
-- and ((monthly_price_per_user >= 14
-- and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
-- and dim_crm_account_id in
-- (
-- select
-- DIM_CRM_ACCOUNT_ID
-- --,product_rate_plan_name
-- from
-- RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- where arr_month >= '2020-02-01'
-- and arr_month <= '2022-02-01'
-- and product_rate_plan_name like any ('%Bronze%','%Starter%')
-- )) EOA 
--     ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
-- ------free limit promo cohort accounts
-- LEFT JOIN 
--     (select
--   distinct
-- dim_crm_account_id
--   from restricted_safe_common_mart_sales.mart_charge charge
--   where 
--   subscription_start_date >= '2023-02-01'
--   and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
--     ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
-- ------price increase promo cohort accounts
-- LEFT JOIN
--     (select distinct dim_crm_account_id from
--         (
--          select
--           charge.*,
--           arr / quantity as actual_price,
--           prod.annual_billing_list_price as list_price
--           from restricted_safe_common_mart_sales.mart_charge charge
--           inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
--           where 
--           subscription_start_date >= '2023-04-01'
--           and subscription_start_date <= '2023-07-01'
--           and TYPE_OF_ARR_CHANGE = 'New'
--           and quantity > 0
--           and actual_price > 228
--           and actual_price < 290
--           and rate_plan_charge_name like '%Premium%'
--         )) price_increase
--         ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
-- LEFT JOIN
-- (
--     select
-- distinct
-- dim_parent_crm_account_id
-- from
-- PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- where arr_month = date_trunc('month',current_date)
-- and product_tier_name like '%Ultimate%'
-- ) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id    
WHERE acct.snapshot_date = current_date
and gds_account_flag = True
and acct.is_deleted = false),





-- ---------------need to add fields to create timeframes for case pulls 
-- --------------this table pulls in all of the data for the accounts (from account CTE), opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_blended as (
SELECT 
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.calculated_tier, 
a.team, 
a.high_value_case_id,
a.high_value_case_owner,
a.high_value_case_owner_team,
a.high_value_case_owner_manager, 
a.count_active_subscriptions,
a.PARENT_CRM_ACCOUNT_UPA_COUNTRY, 
a.PARENT_CRM_ACCOUNT_UPA_STATE,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
-- DATEDIFF('day', last_upload_date, a.next_renewal_date) as days_from_last_upload_to_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.CARR_THIS_ACCOUNT,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.GS_LAST_CSM_ACTIVITY_DATE,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
-- DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, last_upload_date) as days_since_6sense_account_update_last_upload,
-- o.dim_crm_opportunity_id as opportunity_id,
-- o.owner_id as opportunity_owner_id,
-- CASE WHEN o.sales_type = 'Renewal' and o.close_date >= '2024-02-01' and o.close_date <= '2025-01-31' THEN True ELSE False end as FY25_Renewal,
-- o.is_closed, 
-- o.is_won,
-- o.sales_type, 
-- o.close_date, 
-- o.opportunity_term, 
-- o.opportunity_name,
-- o.QSR_status,
-- o.stage_name,
-- o.net_arr,
-- -- qsr.qsr_failed_last_75,
-- s.dim_subscription_id as sub_subscription_id, 
-- s.dim_subscription_id_original as sub_subscription_id_original,
-- s.subscription_status, 
-- s.term_start_date as current_subscription_start_date,
-- s.term_end_date as current_subscription_end_date,
-- DATEDIFF('day', s.term_start_date, current_date) as days_into_current_subscription
FROM account_base a
-- LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
--     ON a.dim_crm_account_id = o.dim_crm_account_id
--     --AND o.is_closed = False
--     AND DATEDIFF('day', current_date, o.close_date) <= 365
-- LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
--     ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
--     and subscription_status = 'Active'
-- LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
--     ON b.dim_crm_account_id = a.dim_crm_account_id
),



-- sub_date as (
-- SELECT 
-- account_id,
-- next_sub_end_date
-- FROM(
-- SELECT 
-- *, 
-- MIN(current_subscription_end_date) OVER (PARTITION BY ACCOUNT_ID) as next_sub_end_date,
-- 'FY25 High Value Account' as case_trigger
-- FROM account_blended
-- --WHERE calculated_tier = 'Tier 1'
-- WHERE FY25_renewal = True
-- and opportunity_id IS NOT NULL
-- and current_subscription_end_date >= current_date)
-- WHERE current_subscription_end_date = next_sub_end_date
-- ),



new_tier_1 as (
SELECT *, 
'FY25 High Value Account' as case_trigger_name
FROM account_base
WHERE calculated_tier = 'Tier 1' and high_value_case_id IS NULL
) --

-- ultimate as (
-- select
-- distinct
-- dim_crm_account_id
-- from
-- PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- where arr_month = date_trunc('month',current_date)
-- and product_tier_name like '%Ultimate%'
-- ),

-- ultimate_SMB as (
-- SELECT u.*, 
-- a.crm_account_owner, 
-- a.carr_account_family, 
-- a.carr_this_account, 
-- a. PARENT_CRM_ACCOUNT_LAM_DEV_COUNT, 
-- a.PARENT_CRM_ACCOUNT_UPA_COUNTRY,
-- a.PARENT_CRM_ACCOUNT_UPA_STATE
-- FROM ultimate u
-- JOIN PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT a
--     ON u.dim_crm_account_id = a.dim_crm_account_id
-- WHERE a.crm_account_owner like '%SMB%'
-- ),

-- ultimate_final as (
-- SELECT *,
-- 'FY25 High Value Account' as case_trigger_name
-- FROM ultimate_smb us
-- LEFT JOIN PROD.WORKSPACE_SALES.SFDC_CASE c
--     ON us.dim_crm_account_id = c.account_id
--     AND c.subject = 'FY25 High Value Account'
-- WHERE case_id IS NULL
-- AND PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 30 
-- )



SELECT 
n.case_trigger_name as case_trigger,
n.dim_crm_account_id,
--n.calculated_tier as account_tier,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
case_subject, 
NULL as high_value_case_owner,
cd.case_reason,
cd.record_type_id,
cd.priority,
cd.case_cta, 
'' context, 
--CONCAT(CAST(next_sub_end_date as date), ' ', '(Next Renewal Date)') as next_sub_end_date, 
PARENT_CRM_ACCOUNT_UPA_COUNTRY,
PARENT_CRM_ACCOUNT_UPA_STATE,
crm_account_owner
FROM new_tier_1 n
-- LEFT JOIN sub_date s
--     ON s.account_id = n.dim_crm_account_id
LEFT JOIN boneyard.case_data cd
    ON n.case_trigger_name = cd.case_trigger


-- UNION 

-- SELECT 
-- n.case_trigger_name as case_trigger,
-- n.dim_crm_account_id,
-- --n.calculated_tier as account_tier,
-- cd.case_trigger_id,  
-- cd.status, 
-- cd.case_origin, 
-- cd.type, 
-- case_subject, 
-- NULL as high_value_case_owner,
-- cd.case_reason,
-- cd.record_type_id,
-- cd.priority,
-- cd.case_cta, 
-- '' context, 
-- --CONCAT(CAST(next_sub_end_date as date), ' ', '(Next Renewal Date)') as next_sub_end_date, 
-- PARENT_CRM_ACCOUNT_UPA_COUNTRY,
-- PARENT_CRM_ACCOUNT_UPA_STATE,
-- crm_account_owner
-- FROM ultimate_final n
-- LEFT JOIN sub_date s
--     ON s.account_id = n.dim_crm_account_id
-- LEFT JOIN boneyard.case_data cd
--     ON n.case_trigger_name = cd.case_trigger




