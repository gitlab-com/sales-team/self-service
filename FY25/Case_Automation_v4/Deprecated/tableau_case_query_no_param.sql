with   prep_crm_case as (select * from common_prep.prep_crm_case),
    dim_crm_user as (select * from common.dim_crm_user),
    mart_crm_opportunity as (select * from restricted_safe_common_mart_sales.mart_crm_opportunity),
    mart_arr as (select * from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR),
    dim_date as (select * from common.dim_date),
    sfdc_case as (select * from PROD.WORKSPACE_SALES.SFDC_CASE),
    dim_crm_account_daily_snapshot as (select * from PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    where snapshot_date = CURRENT_DATE),
    dim_product_detail as (select * from common.dim_product_detail),
    mart_charge as (select * from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE),
    mart_crm_account as (select * from restricted_safe_common_mart_sales.mart_crm_account),
    dim_subscription as (select * from common.dim_subscription),
    dim_billing_account as (select * from common.dim_billing_account),
    mart_product_usage_paid_user_metrics_monthly as (select * from PROD.COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY),
    dim_subscription_snapshot_bottom_up as (select * from PROD.COMMON.DIM_SUBSCRIPTION_SNAPSHOT_BOTTOM_UP
    where snapshot_id >= 20240201),
    mart_crm_task as (select * from PROD.COMMON_MART_SALES.MART_CRM_TASK),
    prep_billing_account_user as (select * from PROD.RESTRICTED_SAFE_COMMON_PREP.PREP_BILLING_ACCOUNT_USER),
    sfdc_contact_snapshots_source as (select * from PROD.LEGACY.SFDC_CONTACT_SNAPSHOTS_SOURCE
    where dbt_valid_from::DATE >= '2024-02-01'),
    case_data as (select * from PROD.BONEYARD.CASE_DATA),
    zuora_subscription_source as (select * from PREP.ZUORA.ZUORA_SUBSCRIPTION_SOURCE),
--    wk_sales_gds_cases as (select * from PROD.RESTRICTED_SAFE_WORKSPACE_SALES.WK_SALES_GDS_CASES),
    mart_behavior_structured_event as (select *
,contexts['data'][0]['data']['customer_id'] as customer_id
 from  PROD.COMMON_MART.MART_BEHAVIOR_STRUCTURED_EVENT
where behavior_date >= '2024-07-20'
and event_category = 'Webstore'
and event_action like 'cancel%'
and event_property <> 'Testing Purpose'),
    customers_db_customers_source as (select * from PREP.CUSTOMERS.CUSTOMERS_DB_CUSTOMERS_SOURCE),

-------to get SMB advocates 
------placeholder for sales segment 
------is there any other additional fields or filtering we will need for this when the segment is transitioned over? 
SMB_advocates as (
SELECT 
case when user_name in ('Katie Wilkinson','Matthew Wyman Jr','Amanda Shim','Erica Wilson','Joseph Hudson','Rasheed Power','Ellie Hickson','Luis Hernandez Calixto','Barbara Schreuder','Tom Elliott','Sarah Van Damme','Bastien Escudé','Hugo Barennes','Emma Szász','Nga Nguyen','Corey Lund','Devon Speth','Arthur Gabor') then true else false end as advocate_flag
,
*
FROM DIM_CRM_USER
WHERE 
(CRM_USER_SALES_SEGMENT = 'SMB' or user_name like '%SMB Sales' or title like '%SMB%')
and is_active
and user_name not in ('Sales Admin [DO NOT CHATTER THIS USER]','System','Automated Process','Platform Integration User')
--and user_name like '%SMB%'
and ((title not like '%Manager%' and title not like '%VP%') or title is null)
),

current_leave as
(
select distinct employee_id from PROD.COMMON.FCT_TEAM_MEMBER_ABSENCE
where current_date >= fct_team_member_absence.absence_start
and current_date <= fct_team_member_absence.absence_end
),

team_totals as
(
select 
distinct
count(distinct case when manager_name = 'Taylor Lund' then employee_number else null end) as amer_count,
count(distinct case when manager_name = 'Miguel Nunes' then employee_number else null end)  as emea_count
from smb_advocates 
left join current_leave
on smb_advocates.employee_number = current_leave.employee_id
where advocate_flag
and current_leave.employee_id is null
),

rollover_case_last_90 as (
SELECT distinct
dim_crm_account_id, 
True as rollover_last_90
FROM (
SELECT distinct
dim_crm_account_id,
CASE WHEN lower(subject) like ('rollover%') THEN True else FALSE END as rollover_Last_90 ------- placeplacer until confirmation of case name 
FROM prep_crm_case
WHERE ((DATEDIFF('day', created_date, current_date) <= 90 AND status = 'Closed - Unresponsive') 
OR (DATEDIFF ('day', closed_date, current_date) <= 90 AND status = 'Closed - Resolved')
OR status in ('Open', 'In Progress'))
and record_type_id in ('0128X000001pPRkQAM'))
WHERE rollover_last_90 = True
),

--Pulls all current High Value account cases
high_value_case_one AS (
  SELECT DISTINCT
    dim_crm_account_id,
    ------- placeplacer until confirmation of case name 
    COALESCE(
      LOWER(subject) = 'high value account check in'
      OR CONTAINS(LOWER(case_type), 'inbound request')
      OR (LOWER(subject) = 'fy25 high value account' AND status IN ('Open', 'In Progress') AND DATEDIFF('day', created_date, CURRENT_DATE) <= 90),
      FALSE
    ) AS high_value_last_90
  FROM prep_crm_case
  WHERE (
    (DATEDIFF('day', created_date, CURRENT_DATE) <= 90 AND status = 'Closed: Unresponsive')
    OR (DATEDIFF('day', closed_date, CURRENT_DATE) <= 90 AND status = 'Closed: Resolved')
    OR status IN ('Open', 'In Progress')
  )
  AND record_type_id IN ('0128X000001pPRkQAM')
),

--Identifies accounts with a High Value case in last 90 days
high_value_case_last_90 AS (
  SELECT DISTINCT
    dim_crm_account_id,
    TRUE AS high_value_last_90
  FROM high_value_case_one
  WHERE high_value_last_90 = TRUE
),

--Identifies accounts with any cases in the last 90 days
any_case_last_90 AS (
  SELECT DISTINCT
    dim_crm_account_id,
    TRUE AS any_case_last_90 ------- placeplacer until confirmation of case name 
  FROM prep_crm_case
  WHERE (
    (DATEDIFF('day', created_date, CURRENT_DATE) <= 90 AND status = 'Closed: Unresponsive')
    OR (DATEDIFF('day', closed_date, CURRENT_DATE) <= 90 AND status = 'Closed: Resolved')
    OR status IN ('Open', 'In Progress')
  )
  AND record_type_id IN ('0128X000001pPRkQAM')
),

--Pulls info about the most recent High Value case
-- last_high_value_one AS (
--   SELECT
--     prep_crm_case.dim_crm_account_id,
--     --opportunity_id,
--     prep_crm_case.case_id,
--     prep_crm_case.dim_crm_user_id                                                        AS owner_id, -----------check to make sure this is owner id 
--     prep_crm_case.subject,
--     dim_crm_user.user_name                                                               AS case_owner_name,
--     dim_crm_user.department                                                              AS case_department,
--     dim_crm_user.team,
--     dim_crm_user.manager_name,
--     dim_crm_user.user_role_name                                                          AS case_user_role_name,
--     dim_crm_user.user_role_type,
--     prep_crm_case.created_date,
--     MAX(prep_crm_case.created_date) OVER (PARTITION BY prep_crm_case.dim_crm_account_id) AS last_high_value_date
--   FROM prep_crm_case
--   LEFT JOIN dim_crm_user
--     ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
--   WHERE prep_crm_case.record_type_id IN ('0128X000001pPRkQAM')
--     AND LOWER(prep_crm_case.subject) LIKE '%high value account%'
-- ),


-- last_high_value_case AS (
--   SELECT *
--   FROM last_high_value_one
--   WHERE created_date = last_high_value_date
-- ),

--Identifies accounts with High Adoption cases in last 90 days
high_adoption_one AS (
  SELECT DISTINCT
    dim_crm_account_id,
    COALESCE((
      CONTAINS(LOWER(subject), 'duo trial')
      OR CONTAINS(LOWER(subject), 'high adoption')
      OR CONTAINS(LOWER(subject), 'overage')
      OR CONTAINS(LOWER(subject), 'future tier 1')
    ),
    FALSE) AS ha_last_90
  FROM prep_crm_case
  WHERE DATEDIFF('day', created_date, CURRENT_DATE) <= 90
    AND record_type_id IN ('0128X000001pPRkQAM')
),

high_adoption_last_90 AS (
  SELECT DISTINCT
    dim_crm_account_id,
    ha_last_90
  FROM high_adoption_one
  WHERE ha_last_90 = TRUE
),

--Identifies accounts with Low Adoption cases in last 180 days
low_adoption_one AS (
  SELECT DISTINCT
    dim_crm_account_id,
    COALESCE((
      CONTAINS(LOWER(subject), 'ptc')
      OR CONTAINS(LOWER(subject), 'low adoption')
      OR CONTAINS(LOWER(subject), 'underutilization')
      OR CONTAINS(LOWER(subject), 'future tier 1')
    ),
    FALSE) AS la_last_180
  FROM prep_crm_case
  WHERE DATEDIFF('day', created_date, CURRENT_DATE) <= 180
    AND record_type_id IN ('0128X000001pPRkQAM')
),


low_adoption_last_180 AS (
  SELECT DISTINCT
    dim_crm_account_id,
    TRUE AS la_last_180
  FROM low_adoption_one
  WHERE la_last_180 = TRUE
),

--Identifies accounts with at least 1 Failed QSR case
qsr_failed_one AS (
  SELECT DISTINCT
    dim_crm_account_id,
    dim_crm_opportunity_id,
    COALESCE(CONTAINS(LOWER(subject), 'failed qsr'), FALSE) AS qsr_failed_last_75
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND CONTAINS(LOWER(subject), 'failed qsr')
),

qsr_failed_last_75 AS (
  SELECT DISTINCT
    dim_crm_account_id,
    dim_crm_opportunity_id,
    TRUE AS qsr_failed_last_75
  FROM qsr_failed_one
  WHERE qsr_failed_last_75
),

--Retrieves the most recent case 
last_case_data_one AS (
  SELECT
    *,
    ROW_NUMBER() OVER (PARTITION BY dim_crm_account_id ORDER BY created_date DESC) AS rn
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND status IN ('Open', 'In Progress')
),

last_case_data AS (
  SELECT *
  FROM last_case_data_one
  WHERE rn = 1
),

--Retrieves the most recent Renewal case
last_renewal_case_data_one AS (
  SELECT
    *,
    ROW_NUMBER() OVER (PARTITION BY dim_crm_account_id ORDER BY created_date DESC) AS rn
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND CONTAINS(LOWER(subject), 'renew')
),

last_renewal_case_data AS (
  SELECT *
  FROM last_renewal_case_data_one
  WHERE rn = 1
),

--Gets count of all open cases on each account
open_cases AS (
  SELECT
    dim_crm_account_id,
    COUNT(DISTINCT case_id) AS count_cases
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND status IN ('Open', 'In Progress')
  GROUP BY 1
),

--Counts open renewal opportunities per account
open_renewal_opps AS (
  SELECT
    dim_crm_account_id,
    COUNT(DISTINCT dim_crm_opportunity_id) AS count_open_opps
  FROM mart_crm_opportunity
  WHERE sales_type = 'Renewal'
    AND is_closed = FALSE
  GROUP BY 1
),

--Identifies subscriptions where autorenew will fail due to having multiple non-Storage products
auto_renew_will_fail_one AS (
  SELECT
    dim_crm_account_id,
    dim_subscription_id,
    product_rate_plan_name,
    subscription_end_month                                                    AS auto_renewal_sub_end_month,
    turn_on_auto_renewal,
    COUNT(DISTINCT product_tier_name) OVER (PARTITION BY dim_subscription_id) AS sub_prod_count
  FROM mart_arr
  WHERE (arr_month = DATE_TRUNC('month', CURRENT_DATE()) OR arr_month = DATE_TRUNC('month', CURRENT_DATE - INTERVAL '1 month'))
    --and parent_crm_account_sales_segment = 'SMB'
    AND LOWER(product_tier_name) NOT LIKE '%storage%'
    AND turn_on_auto_renewal = 'Yes'
  QUALIFY sub_prod_count > 1
),

auto_renew_will_fail AS (
  SELECT DISTINCT
    dim_crm_account_id,
    dim_subscription_id
  FROM auto_renew_will_fail_one
),

--Identifies which High Value accounts have had their cases closed due to churn
tier1_full_churns AS (
  SELECT dim_crm_account_id
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND subject = 'FY25 High Value Account'
    AND status = 'Closed: Resolved'
    AND resolution_action = 'Request Not Possible'
),

--Pulls existing Duo Trial lead cases
existing_duo_trial AS (
  SELECT dim_crm_account_id
  FROM prep_crm_case
  WHERE record_type_id IN ('0128X000001pPRkQAM')
    AND subject = 'Duo Trial Started' OR (subject = 'Customer MQL' AND dim_crm_account_id IN ('0014M00001sEismQAC', '001PL000004lgERYAY'))
),

--Identifies which subscriptions have Duo on them
duo_on_sub AS (
  SELECT DISTINCT
    dim_crm_account_id,
    dim_subscription_id,
    product_rate_plan_name,
    subscription_end_month                                          AS auto_renewal_sub_end_month,
    turn_on_auto_renewal,
    COALESCE(CONTAINS(LOWER(product_rate_plan_name), 'duo'), FALSE) AS duo_flag
  --arr_month,
  --max(arr_month) over(partition by dim_subscription_id) as latest_arr_month
  FROM mart_arr
  WHERE (arr_month = DATE_TRUNC('month', CURRENT_DATE()) OR arr_month = DATE_TRUNC('month', CURRENT_DATE - INTERVAL '1 month'))
    AND duo_flag = TRUE
),

--Pulls information about the most recent First Order on each account
first_order AS (
  SELECT DISTINCT
    mart_crm_opportunity.dim_parent_crm_account_id,
    LAST_VALUE(mart_crm_opportunity.net_arr)
      OVER (PARTITION BY mart_crm_opportunity.dim_parent_crm_account_id ORDER BY mart_crm_opportunity.close_date ASC)
      AS net_arr,
    LAST_VALUE(mart_crm_opportunity.close_date)
      OVER (PARTITION BY mart_crm_opportunity.dim_parent_crm_account_id ORDER BY mart_crm_opportunity.close_date ASC)
      AS close_date,
    LAST_VALUE(dim_date.fiscal_year)
      OVER (PARTITION BY mart_crm_opportunity.dim_parent_crm_account_id ORDER BY mart_crm_opportunity.close_date ASC)
      AS fiscal_year,
    LAST_VALUE(mart_crm_opportunity.sales_qualified_source_name)
      OVER (PARTITION BY mart_crm_opportunity.dim_parent_crm_account_id ORDER BY mart_crm_opportunity.close_date ASC)
      AS sqs,
    LAST_VALUE(mart_crm_opportunity.opportunity_name)
      OVER (PARTITION BY mart_crm_opportunity.dim_parent_crm_account_id ORDER BY mart_crm_opportunity.close_date ASC)
      AS fo_opp_name
  FROM mart_crm_opportunity
  LEFT JOIN dim_date
    ON mart_crm_opportunity.close_date = dim_date.date_actual
  WHERE mart_crm_opportunity.is_won
    AND mart_crm_opportunity.order_type = '1. New - First Order'
),

--Pulls information about the most recent Churn on each account
latest_churn AS (
  SELECT
    snapshot_date                                                                               AS close_date,
    dim_crm_account_id,
    carr_this_account,
    LAG(carr_this_account, 1) OVER (PARTITION BY dim_crm_account_id ORDER BY snapshot_date ASC) AS prior_carr,
    -prior_carr                                                                                 AS net_arr,
    MAX(CASE
      WHEN carr_this_account > 0 THEN snapshot_date
    END) OVER (PARTITION BY dim_crm_account_id)                                                 AS last_carr_date
  FROM dim_crm_account_daily_snapshot
  WHERE snapshot_date >= '2019-02-01'
    AND snapshot_date = DATE_TRUNC('month', snapshot_date)
  QUALIFY
    prior_carr > 0
    AND carr_this_account = 0
    AND snapshot_date > last_carr_date
),

high_value_case_prep AS (
  SELECT
    prep_crm_case.dim_crm_account_id                                                     AS account_id,
    prep_crm_case.case_id,
    prep_crm_case.dim_crm_user_id                                                        AS owner_id, ---------check that this is the same as owner ID 
    prep_crm_case.subject,
    dim_crm_user.user_name                                                               AS case_owner_name,
    dim_crm_user.department                                                              AS case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name                                                          AS case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
    MAX(prep_crm_case.created_date) OVER (PARTITION BY prep_crm_case.dim_crm_account_id) AS last_high_value_date
  FROM prep_crm_case
  LEFT JOIN dim_crm_user
    ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
  WHERE prep_crm_case.record_type_id IN ('0128X000001pPRkQAM')
    --    and account_id = '0014M00001gTGESQA4'
    AND LOWER(prep_crm_case.subject) LIKE '%high value account%'
),

--Duplicative - gets most recent High Value case
high_value_case AS (
  SELECT *
  FROM high_value_case_prep
  WHERE created_date = last_high_value_date
),

--Information about the account from the beginning of the fiscal year
-- start_values AS ( -- This is a large slow to query table
--   SELECT
--     dim_crm_account_id,
--     carr_account_family,
--     carr_this_account,
--     parent_crm_account_lam_dev_count,
--     pte_score,
--     ptc_score
--   FROM dim_crm_account_daily_snapshot
--   WHERE snapshot_date = '2024-02-10'
-- ),

--First High Value case on each account (probably unneeded)
first_high_value_case_one AS (
  SELECT
    prep_crm_case.dim_crm_account_id                                                     AS account_id,
    prep_crm_case.case_id,
    prep_crm_case.dim_crm_user_id                                                        AS owner_id, ---------check that this is the same as owner ID 
    prep_crm_case.subject,
    dim_crm_user.user_name                                                               AS case_owner_name,
    dim_crm_user.department                                                              AS case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name                                                          AS case_user_role_name,
    dim_crm_user.user_role_type,
    prep_crm_case.created_date,
    MIN(prep_crm_case.created_date) OVER (PARTITION BY prep_crm_case.dim_crm_account_id) AS first_high_value_date
  FROM prep_crm_case
  LEFT JOIN dim_crm_user
    ON prep_crm_case.dim_crm_user_id = dim_crm_user.dim_crm_user_id
  WHERE prep_crm_case.record_type_id IN ('0128X000001pPRkQAM')
    AND LOWER(prep_crm_case.subject) LIKE '%high value account%'
),

first_high_value_case AS (
  SELECT *
  FROM first_high_value_case_one
  WHERE created_date = first_high_value_date
),

eoa_cohorts_prep AS (
  SELECT
    mart_arr.arr_month,
    --,ping_created_at
    mart_arr.subscription_end_month,
    mart_arr.dim_crm_account_id,
    mart_arr.crm_account_name,
    --,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
    mart_arr.dim_subscription_id,
    mart_arr.dim_subscription_id_original,
    mart_arr.subscription_name,
    mart_arr.subscription_sales_type,
    mart_arr.auto_pay,
    mart_arr.default_payment_method_type,
    mart_arr.contract_auto_renewal,
    mart_arr.turn_on_auto_renewal,
    mart_arr.turn_on_cloud_licensing,
    mart_arr.contract_seat_reconciliation,
    mart_arr.turn_on_seat_reconciliation,
    COALESCE(mart_arr.contract_seat_reconciliation = 'Yes' AND mart_arr.turn_on_seat_reconciliation = 'Yes', FALSE) AS qsr_enabled_flag,
    mart_arr.product_tier_name,
    mart_arr.product_delivery_type,
    mart_arr.product_rate_plan_name,
    mart_arr.arr,
    --,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
    DIV0(mart_arr.arr, mart_arr.quantity)                                                                           AS arr_per_user,
    arr_per_user
    / 12                                                                                                            AS monthly_price_per_user,
    DIV0(mart_arr.mrr, mart_arr.quantity)                                                                           AS mrr_check
  FROM mart_arr
  -- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
  --     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
  WHERE mart_arr.arr_month = '2023-01-01'
    AND mart_arr.quantity > 0
    AND mart_arr.product_tier_name LIKE '%Premium%'
    AND ((
      monthly_price_per_user >= 14
      AND monthly_price_per_user <= 16
    ) OR (monthly_price_per_user >= 7.5 AND monthly_price_per_user <= 9.5
    ))
    AND mart_arr.dim_crm_account_id IN
    (
      SELECT dim_crm_account_id
      --,product_rate_plan_name
      FROM mart_arr
      WHERE arr_month >= '2020-02-01'
        AND arr_month <= '2022-02-01'
        AND product_rate_plan_name LIKE ANY ('%Bronze%', '%Starter%')
    )
),

--Identifies accounts that had EoA pricing in the past few years
eoa_cohorts AS (
  SELECT DISTINCT dim_crm_account_id FROM
    eoa_cohorts_prep
),

--Identifies accounts that had their First Order discounted 70% as part of the Free User limits promotion
free_promo AS (
  SELECT DISTINCT dim_crm_account_id
  FROM mart_charge
  WHERE subscription_start_date >= '2023-02-01'
    AND rate_plan_charge_description = 'fo-discount-70-percent'
),

price_increase_prep AS (
  SELECT
    mart_charge.*,
    DIV0(mart_charge.arr, mart_charge.quantity) AS actual_price,
    prod.annual_billing_list_price              AS list_price
  FROM mart_charge
  INNER JOIN dim_product_detail AS prod
    ON mart_charge.dim_product_detail_id = prod.dim_product_detail_id
  WHERE mart_charge.subscription_start_date >= '2023-04-01'
    AND mart_charge.subscription_start_date <= '2023-07-01'
    AND mart_charge.type_of_arr_change = 'New'
    AND mart_charge.quantity > 0
    AND actual_price > 228
    AND actual_price < 290
    AND mart_charge.rate_plan_charge_name LIKE '%Premium%'
),

--Identifies accounts that received a First Order discount as part of the Premium price increase promotion
price_increase AS (
  SELECT DISTINCT dim_crm_account_id
  FROM price_increase_prep
),

--Identifies any accounts with Ultimate ARR
ultimate AS (
  SELECT DISTINCT
    dim_parent_crm_account_id,
    arr_month
  FROM mart_arr
  WHERE product_tier_name LIKE '%Ultimate%'
    AND arr > 0
),

--All AMER/APJ team accounts
amer_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('AMER SMB Sales', 'APAC SMB Sales')
    OR owner_role = 'Advocate_SMB_AMER'
),

--All EMEA team accounts
emea_accounts AS (
  SELECT dim_crm_account_id
  FROM mart_crm_account
  WHERE crm_account_owner IN
    ('EMEA SMB Sales')
    OR owner_role = 'Advocate_SMB_EMEA'
),


--------------using the account base CTE to get tiering and beginning of FY25 info joined to the account id
account_base AS (
  SELECT
    acct.*,
    CASE
      WHEN first_high_value_case.case_id IS NOT NULL THEN 'Tier 1'
      WHEN first_high_value_case.case_id IS NULL
        THEN
          CASE
            WHEN acct.carr_this_account > 7000 THEN 'Tier 1'
            WHEN acct.carr_this_account < 3000 AND acct.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
            ELSE 'Tier 2'
          END
    END                                                             AS calculated_tier,
    CASE
      WHEN (acct.snapshot_date >= '2024-02-01' AND amer_accounts.dim_crm_account_id IS NOT NULL)
        OR (
          acct.snapshot_date < '2024-02-01'
          AND (acct.parent_crm_account_geo IN ('AMER', 'APJ', 'APAC') OR acct.parent_crm_account_region IN ('AMER', 'APJ', 'APAC'))
        )
        THEN 'AMER/APJ'
      WHEN
        (acct.snapshot_date >= '2024-02-01' AND emea_accounts.dim_crm_account_id IS NOT NULL)
        OR (acct.snapshot_date < '2024-02-01' AND (acct.parent_crm_account_geo IN ('EMEA') OR acct.parent_crm_account_region IN ('EMEA')))
        THEN 'EMEA'
      ELSE 'Other'
    END                                                             AS team,
    COALESCE(
      (acct.snapshot_date >= '2024-02-01' AND (emea_accounts.dim_crm_account_id IS NOT NULL OR amer_accounts.dim_crm_account_id IS NOT NULL))
      OR
      (
        acct.snapshot_date < '2024-02-01'
        AND (acct.carr_account_family <= 30000)
        AND (acct.parent_crm_account_max_family_employee <= 100 OR acct.parent_crm_account_max_family_employee IS NULL)
        AND ultimate.dim_parent_crm_account_id IS NULL
        AND acct.parent_crm_account_sales_segment IN ('SMB', 'Mid-Market', 'Large')
        AND acct.parent_crm_account_upa_country != 'JP'
        AND acct.is_jihu_account = FALSE
      ), FALSE
    )                                                               AS gds_account_flag,
    first_order.fiscal_year                                         AS fo_fiscal_year,
    first_order.close_date                                          AS fo_close_date,
    first_order.net_arr                                             AS fo_net_arr,
    first_order.sqs                                                 AS fo_sqs,
    first_order.fo_opp_name,
    churn.close_date                                                AS churn_close_date,
    churn.net_arr                                                   AS churn_net_arr,
    NOT COALESCE(fo_fiscal_year <= 2024, FALSE)                     AS new_fy25_fo_flag,
    first_high_value_case.created_date                              AS first_high_value_case_created_date,
    high_value_case.case_owner_name                                 AS high_value_case_owner,
    high_value_case.owner_id                                        AS high_value_case_owner_id,
    high_value_case.case_id                                         AS high_value_case_id,
    high_value_case.team                                            AS high_value_case_owner_team,
    high_value_case.manager_name                                    AS high_value_case_owner_manager,
    -- start_values.carr_account_family                                AS starting_carr_account_family,
    -- start_values.carr_this_account                                  AS starting_carr_this_account,
    -- CASE
    --   WHEN start_values.carr_this_account > 7000 THEN 'Tier 1'
    --   WHEN start_values.carr_this_account < 3000 AND start_values.parent_crm_account_lam_dev_count < 10 THEN 'Tier 3'
    --   ELSE 'Tier 2'
    -- END                                                             AS starting_calculated_tier,
    -- start_values.pte_score                                          AS starting_pte_score,
    -- start_values.ptc_score                                          AS starting_ptc_score,
    -- start_values.parent_crm_account_lam_dev_count                   AS starting_parent_crm_account_lam_dev_count,
    COALESCE(eoa_cohorts.dim_crm_account_id IS NOT NULL, FALSE)     AS eoa_flag,
    COALESCE(free_promo.dim_crm_account_id IS NOT NULL, FALSE)      AS free_promo_flag,
    COALESCE(price_increase.dim_crm_account_id IS NOT NULL, FALSE)  AS price_increase_promo_flag,
    COALESCE(ultimate.dim_parent_crm_account_id IS NOT NULL, FALSE) AS ultimate_customer_flag,
    team_totals.amer_count,
    team_totals.emea_count
  FROM dim_crm_account_daily_snapshot AS acct
  ------subquery that gets latest FO data
  LEFT JOIN team_totals
  LEFT JOIN first_order
    ON acct.dim_parent_crm_account_id = first_order.dim_parent_crm_account_id
  ---------subquery that gets latest churn data
  LEFT JOIN latest_churn AS churn
    ON acct.dim_crm_account_id = churn.dim_crm_account_id
  --------subquery to get high tier case owner
  LEFT JOIN high_value_case
    ON acct.dim_crm_account_id = high_value_case.account_id
  --------------subquery to get start of FY25 values
--   LEFT JOIN start_values
--     ON acct.dim_crm_account_id = start_values.dim_crm_account_id
  -----subquery to get FIRST high value case
  LEFT JOIN first_high_value_case
    ON acct.dim_crm_account_id = first_high_value_case.account_id
  -----EOA cohort accounts
  LEFT JOIN eoa_cohorts
    ON acct.dim_crm_account_id = eoa_cohorts.dim_crm_account_id
  ------free limit promo cohort accounts
  LEFT JOIN free_promo
    ON acct.dim_crm_account_id = free_promo.dim_crm_account_id
  ------price increase promo cohort accounts
  LEFT JOIN price_increase
    ON acct.dim_crm_account_id = price_increase.dim_crm_account_id
  LEFT JOIN ultimate
    ON acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id
      AND ultimate.arr_month = DATE_TRUNC('month', acct.snapshot_date)
  ----- amer and apac accounts
  LEFT JOIN amer_accounts
    ON acct.dim_crm_account_id = amer_accounts.dim_crm_account_id
  ----- emea emea accounts
  LEFT JOIN emea_accounts
    ON acct.dim_crm_account_id = emea_accounts.dim_crm_account_id
  -------filtering to get current account data
  WHERE acct.snapshot_date = CURRENT_DATE
),

subscription_prep_one as (
select
dim_crm_account_id,
dim_subscription_id, 
subscription_name,
dim_subscription_id_original,
subscription_status, 
term_start_date,
term_end_date,
DATEDIFF('day', term_start_date, current_date),
-- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
turn_on_auto_renewal,
dim_crm_opportunity_id,
dim_crm_opportunity_id_current_open_renewal,
dim_crm_opportunity_id_closed_lost_renewal,
subscription_version,
max(case when dim_crm_opportunity_id_closed_lost_renewal is not null then subscription_version else null end) over(partition by dim_subscription_id_original) as latest_lost_version,
max(case when dim_crm_opportunity_id_current_open_renewal is not null then subscription_version else null end) over(partition by dim_subscription_id_original) as latest_open_version
from dim_subscription
where subscription_status = 'Active'
and term_end_date::date > term_start_date::date
),

subscription_prep_two as (
select *
from subscription_prep_one
where
(dim_crm_opportunity_id_current_open_renewal is not null 
and subscription_version = latest_open_version)
OR
(dim_crm_opportunity_id_closed_lost_renewal is not null 
and subscription_version = latest_lost_version)

),

--------------this table pulls in all of the data for the accounts (from account CTE), opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_blended as (
SELECT 
dateadd('day',-7,current_date),
DATEADD('day', 30, dateadd('day',-7,current_date)) as last_upload_30_days,
DATEADD('day', 60, dateadd('day',-7,current_date)) as last_upload_60_days,
DATEADD('day', 80, dateadd('day',-7,current_date)) as last_upload_80_days, 
DATEADD('day', 90, dateadd('day',-7,current_date)) as last_upload_90_days, 
DATEADD('day', 180, dateadd('day',-7,current_date)) as last_upload_180_days, 
DATEADD('day', 270, dateadd('day',-7,current_date)) as last_upload_270_days, 
DATEADD('day', 30, current_date) as current_date_30_days,
DATEADD('day', 60, current_date) as current_date_60_days,
DATEADD('day', 80, current_date) as current_date_80_days, 
DATEADD('day', 90, current_date) as current_date_90_days, 
DATEADD('day', 180, current_date) as current_date_180_days,
DATEADD('day', 270, current_date) as current_date_270_days,
DATEDIFF('day', dateadd('day',-7,current_date),  current_date) as days_since_last_upload,
a.dim_crm_account_id as account_id, 
gds_account_flag,
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.calculated_tier, 
a.amer_count,
a.emea_count,
a.team,
a.high_value_case_id,
a.high_value_case_owner,
a.high_value_case_owner_id,
a.high_value_case_owner_team,
a.high_value_case_owner_manager, 
a.count_active_subscriptions,
a.fo_opp_name,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
-- DATEDIFF('day', last_upload_date, a.next_renewal_date) as days_from_last_upload_to_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.CARR_THIS_ACCOUNT,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.GS_LAST_CSM_ACTIVITY_DATE,
a.free_promo_flag, 
a.Price_Increase_Promo_Flag,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
-- DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, last_upload_date) as days_since_6sense_account_update_last_upload,
o.dim_crm_opportunity_id as opportunity_id,
o.owner_id as opportunity_owner_id,
CASE WHEN o.sales_type = 'Renewal' and o.close_date >= '2024-02-01' and o.close_date <= '2025-01-31' THEN True ELSE False end as FY25_Renewal,
o.is_closed, 
o.is_won,
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
o.QSR_notes,
o.stage_name,
o.net_arr,
o.arr_basis,
o.PTC_PREDICTED_RENEWAL_RISK_CATEGORY,
CASE WHEN o.deal_path_name = 'Partner' then True ELSE false end as partner_opp_flag,
CASE WHEN qsr.qsr_failed_last_75 = True then True ELSE False end as qsr_failed_last_75,
s.dim_subscription_id as sub_subscription_id, 
s.subscription_name as sub_subscription_name,
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_start_date as current_subscription_start_date,
s.term_end_date as current_subscription_end_date,
DATEDIFF('day', s.term_start_date, current_date) as days_into_current_subscription,
-- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
s.turn_on_auto_renewal,
-- ptc.score_date as ptc_score_date, 
-- DATEDIFF('day', ptc.score_date, current_date) as days_since_last_ptc_score, 
-- -- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
-- ptc.score as ptc_score, 
-- ptc.score_group as ptc_score_group,
-- ptc.latest_score_date as ptc_latest_score_date,
-- ptc.insights as ptc_insights,
-- pte.score_date as pte_score_date, 
-- DATEDIFF('day', pte.score_date, current_date) as days_since_last_pte_score,
-- -- DATEDIFF('day', pte.score_date, last_upload_date) as days_since_last_pte_score_last_upload,
-- pte.score as pte_score, 
-- pte.score_group as pte_score_group,
-- pte.latest_score_date as pte_latest_score_date,
-- pte.insights as pte_insights,
-- lpte.most_recent_pte, 
-- lptc.most_recent_ptc,
cd.created_date as last_case_created_date, 
DATEDIFF('day', CAST(cd.created_date as date), current_date) as days_since_last_case_created,
cd.case_id as last_case_id, 
cd.subject as last_case_subject, 
cd.status as last_case_status, 
cd.dim_crm_user_id as last_case_owner_id, 
DATEDIFF('day', CAST(lr.created_date as date), current_date) as days_since_last_renewal_case_created,
lr.case_id as last_renewal_case_id, 
lr.subject as last_renewal_case_subject, 
lr.status as last_renewal_case_status, 
lr.dim_crm_user_id as last_renewal_case_owner_id,
oc.count_cases as current_open_cases,
oo.count_open_opps,
COUNT(distinct s.DIM_SUBSCRIPTION_ID) OVER (PARTITION BY s.dim_crm_account_id) AS count_subscriptions,
CASE WHEN HA_last_90 = True then TRUE else FALSE end as HA_LAST_90, 
CASE WHEN LA_last_180 = TRUE then TRUE else FALSE end as LA_last_180,
CASE WHEN High_Value_Last_90 = True then True Else False end as High_Value_Last_90, 
CASE WHEN any_case_last_90 = True then True ELSE False end as Any_Case_Last_90,
CASE WHEN rollover_last_90 = True then True Else False end as rollover_last_90,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
-- DATEDIFF('day', s.term_end_date, last_upload_date) as days_since_current_sub_end_date_last_upload,
s.dim_subscription_id,
CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, o.close_date) else null end as days_till_close,
-- CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, last_upload_date) else null end as days_till_close_last_upload,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
a.EOA_flag,
b.PO_required,  
o.auto_renewal_status,
-- CASE WHEN overdue_renewal.account_id IS NOT NULL then True Else False end as overdue_renewal_last_300_flag,
CASE WHEN auto_renew_will_fail.dim_crm_account_id IS NOT NULL then True else False end as auto_renew_will_fail_mult_products,
CASE WHEN duo_on_sub.dim_crm_account_id IS NOT NULL then true else false end as duo_on_sub_flag,
--CASE WHEN FY26_high_value_case.account_id IS NOT NULL THEN True ELSE False end as FY26_High_Value_Account_Exists_Flag,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ultimate_Upgrade_Oppty_Flag, 
CASE WHEN tier1_full_churns.dim_crm_account_ID IS NOT NULL THEN True ELSE False end as Tier1_Full_Churn_Flag, 
CASE WHEN e.dim_crm_account_id IS NOT NULL then TRUE else FALSE end as existing_duo_trial_flag
FROM account_base a
LEFT JOIN mart_crm_opportunity o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    --AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN dim_subscription s
    ON (o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
 --   and subscription_status = 'Active'
    and o.is_closed = false)
    -- or (o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_closed_lost_renewal
    -- and o.close_date >= '2024-07-01' and o.is_closed)
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
-- LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"  QUALIFY score_date = latest_score_date) ptc 
--     ON ptc.crm_account_id = a.dim_crm_account_id
-- LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  QUALIFY score_date = latest_score_date) pte 
    -- ON pte.crm_account_id = a.dim_crm_account_id
LEFT JOIN last_case_data cd 
    ON cd.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN last_renewal_case_data lr 
    ON lr.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN open_cases oc
    ON oc.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN open_renewal_opps oo
    ON oo.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN low_adoption_last_180 
    ON low_adoption_last_180.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN high_adoption_last_90
    ON high_adoption_last_90.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN qsr_failed_last_75 qsr
    ON qsr.dim_crm_account_id = a.dim_crm_account_id
    AND qsr.dim_crm_opportunity_id = o.dim_crm_opportunity_id
-- LEFT JOIN latest_ptc_score lptc
-- LEFT JOIN latest_pte_score lpte
LEFT JOIN high_value_case_last_90
    ON high_value_case_last_90.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN any_case_last_90 
    ON any_case_last_90.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN rollover_case_last_90 
    ON rollover_case_last_90.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN auto_renew_will_fail
    ON auto_renew_will_fail.dim_crm_account_id = a.dim_crm_account_id
    AND auto_renew_will_fail.dim_subscription_id = s.dim_subscription_id
LEFT JOIN tier1_full_churns 
    ON tier1_full_churns.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN overdue_renewal
--     ON overdue_renewal.account_id = a.dim_crm_account_id
--     AND overdue_renewal.opportunity_id = o.dim_crm_opportunity_id
LEFT JOIN existing_duo_trial e
    ON e.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN duo_on_sub 
    ON duo_on_sub.dim_crm_account_id = a.dim_crm_account_id
    AND s.dim_subscription_id = duo_on_sub.dim_subscription_id
-- LEFT JOIN FY26_high_value_case
--     ON FY26_high_value_case.account_id = a.dim_crm_account_id
--JOIN last_created_date
---------------------currently a placeholder and will need to be substituted in for all SMB sements--------
-- WHERE a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'
-- AND carr_account_family < 30000 and carr_account
-- WHERE (a.carr_account_family <= 30000 and a.carr_this_account > 0)
-- and (a.parent_crm_account_max_family_employee <= 100 or a.parent_crm_account_max_family_employee is null)
-- and a.ultimate_customer_flag = false
-- and a.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
-- and a.parent_crm_account_upa_country <> 'JP'
-- and a.is_jihu_account = false
WHERE (gds_account_flag = True AND sales_type != 'Renewal')
OR ((gds_account_flag = True and sales_type = 'Renewal' and opportunity_name not like '%EDU PROGRAM%')
OR (gds_account_flag = True AND sales_type = 'Renewal' and opportunity_name not like '%OSS PROGRAM%'))),





utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,DIV0(mart_arr.ARR , mart_arr.QUANTITY) as arr_per_user
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,DIV0(mart_arr.ARR , mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM mart_arr
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM mart_product_usage_paid_user_metrics_monthly 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
), 

-- mart_behavior_structured_event_prep as
-- (
-- select *
-- ,contexts['data'][0]['data']['customer_id'] as customer_id
--  from mart_behavior_structured_event
-- where behavior_date >= '2024-07-20'
-- and event_category = 'Webstore'
-- and event_action like 'cancel%'
-- and event_property <> 'Testing Purpose'
-- ),

portal_cancel_reasons as (
select 
mart_behavior_structured_event.* ,
customers_db_customers_source.sfdc_account_id as dim_crm_account_id
from
mart_behavior_structured_event
left join customers_db_customers_source
on 
mart_behavior_structured_event.customer_id = customers_db_customers_source.customer_id
),

----------------------------get the date on a subscription when a customer switches from auto-renew on to auto-renew off
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
cancel_reason,
cancel_comments,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*,
portal_cancel_reasons.event_label as cancel_reason,
portal_cancel_reasons.event_property as cancel_comments
from dim_subscription_snapshot_bottom_up sub
inner join dim_date on sub.snapshot_id = dim_date.date_id
inner join prep_billing_account_user on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
left join portal_cancel_reasons on sub.dim_crm_account_id = portal_cancel_reasons.dim_crm_account_id
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4,5,6
 ),


--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
eoa_accounts_fy24 as (
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_arr.ARR_MONTH
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,DIV0(mart_arr.ARR , mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
DIV0(mart_arr.mrr,mart_arr.quantity) as mrr_check
FROM mart_arr
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
from
mart_arr
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
) order by dim_crm_account_id asc)
),
 

 
prior_sub_25_eoa as (
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
dim_subscription.dim_crm_opportunity_id_current_open_renewal,
DIV0(previous_mrr , previous_quantity) as previous_price,
DIV0(mrr , quantity) as price_after_renewal
FROM mart_charge
LEFT JOIN dim_subscription
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
INNER JOIN eoa_accounts_fy24 on eoa_accounts_fy24.dim_crm_account_id = mart_charge.dim_crm_account_id
-- left join mart_crm_opportunity on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
WHERE mart_charge.rate_plan_name like '%Premium%'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2023-02-01'
and mart_charge.term_start_date < current_date
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and (lower(rate_plan_charge_description) like '%eoa%'
or ((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
and quantity < 25)
),

duo_trials as (
SELECT *
FROM (
SELECT 
*, 
ROW_NUMBER() OVER(PARTITION BY account_id ORDER BY marketo_last_interesting_moment_date) as order_number
FROM (
select distinct
contact_id,
account_id,
marketo_last_interesting_moment_date, 
from sfdc_contact_snapshots_source
where marketo_last_interesting_moment like '%Duo Pro SaaS Trial%'
and marketo_last_interesting_moment_date >= '2024-02-01' ))
WHERE order_number = 1),

failure_sub as (
select 
dim_subscription.dim_subscription_id as failure_subscription_id, 
dim_subscription.subscription_name as failure_subscription_name,
dim_subscription.dim_crm_opportunity_id as failed_sub_oppty, 
dim_subscription.dim_crm_opportunity_id_current_open_renewal as failed_sub_renewal_opp, 
dim_subscription.dim_crm_opportunity_id_closed_lost_renewal as failed_sub_closed_opp,
dim_subscription.dim_crm_account_id
,right(REGEXP_SUBSTR(ZUORA_SUBSCRIPTION_SOURCE.notes, 'RENEWAL_ERROR.{12}'),10)::DATE as failure_date
from 
dim_subscription
left join zuora_subscription_source
on dim_subscription.dim_subscription_id = ZUORA_SUBSCRIPTION_SOURCE.subscription_id
where  lower(ZUORA_SUBSCRIPTION_SOURCE.notes) like '%renewal_error%'
and REGEXP_SUBSTR(ZUORA_SUBSCRIPTION_SOURCE.notes, 'RENEWAL_ERROR.{12}') like '%(20%'
),



-----------brings together account blended date, utilization data, and autorenewal switch data 
all_data as(
SELECT a.*, 
CASE WHEN subscription_end_month > '2024-04-01' and u.product_tier_name like '%Premium%' THEN 29 
when u.product_tier_name like '%Ultimate%' then 1188
ELSE 0 END as future_price,
u.overage_count,
u.overage_amount,
u.latest_overage_month,
u.qsr_enabled_flag,
u.max_billable_user_count,
u.LICENSE_USER_COUNT,
u.ARR,
u.arr_per_user,
turn_on_seat_reconciliation,
--turn_on_auto_renewal,
s.latest_switch_date, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch, 
CASE WHEN prior_sub_25_eoa.dim_crm_account_id IS NOT NULL then True else False end as prior_year_eoa_under_25_flag, 
CASE WHEN d.contact_id IS NOT NULL then True ELSE False end as duo_trial_on_account_flag, 
d.marketo_last_interesting_moment_date::DATE as duo_trial_start_date, 
d.contact_id, 
d.order_number, 
CASE WHEN f.dim_crm_account_id IS NOT NULL THEN TRUE ELSE FALSE end as payment_failure_flag, 
failed_sub_oppty,
failure_subscription_name,
failed_sub_renewal_opp, 
failed_sub_closed_opp, 
failure_date,
cancel_reason,
cancel_comments
FROM account_blended a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id
LEFT JOIN prior_sub_25_eoa 
    ON prior_sub_25_eoa.dim_crm_account_id = a.account_id
LEFT JOIN duo_trials d
    ON d.account_id = a.account_id
LEFT JOIN failure_sub f
    ON f.dim_crm_account_id = a.account_id
    AND (f.failure_subscription_name = sub_subscription_name or (sub_subscription_name is null and failure_date >= dateadd('day',-7,current_date)))
    ),




------------flags each account/opportunity with any applicable flags
case_flags as (
SELECT *,
--CASE WHEN calculated_tier = 'Tier 1' and ((future_price*LICENSE_USER_COUNT) - (arr_per_user*LICENSE_USER_COUNT) < 3000) and High_Value_Last_90 = False AND Tier1_Full_Churn_Flag = False then True ELSE False end as check_in_case_needed_flag,
--CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') AND any_case_last_90 = False and (future_price*max_billable_user_count)*12 >= 7000 THEN True ELSE False end as future_tier_1_account_flag, 
CASE WHEN sales_type = 'Renewal' AND PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
CASE WHEN opportunity_term > 12 OR (CONTAINS(opportunity_name, '2 year') or CONTAINS(opportunity_name, '3 year')) then TRUE else FALSE end as multiyear_renewal_flag,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN TRUE ELSE FALSE end as EOA_Auto_Renewal_Will_Fail_Flag,
CASE WHEN ((auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) AND EOA_Auto_Renewal_Will_Fail_Flag = False) THEN TRUE ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag, 
CASE WHEN auto_renew_will_fail_mult_products = True then True else False end as auto_renewal_will_fail_logic_flag,
--CASE WHEN (eoa_flag = True and prior_year_eoa_under_25_flag = True and max_BILLABLE_USER_COUNT >= 25) OR (eoa_flag = True AND turn_on_auto_renewal = 'No') THEN TRUE ELSE FALSE end as EOA_RENEWAL_FLAG,
CASE WHEN QSR_flag = True and QSR_status = 'Failed' and QSR_notes like '%card%' and is_closed = False and close_date >= '2024-02-01' then TRUE ELSE FALSE END AS QSR_Failed_Flag, 
-- CASE WHEN sales_type = 'Renewal' and days_since_current_sub_end_date > 0 and is_closed = False and overdue_renewal_last_300_flag = False then TRUE ELSE FALSE END as Overdue_Renewal_Flag,
CASE WHEN calculated_tier in ('Tier 2') and sales_type = 'Renewal' and PTC_PREDICTED_RENEWAL_RISK_CATEGORY = 'Will Churn (Actionable)' then TRUE else FALSE END AS Will_Churn_Flag,
--CASE WHEN days_since_autorenewal_switch = 2 then TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag, ----- commenting out to remove timeframe filter (add this in another query
CASE WHEN latest_switch_date IS NOT NULL THEN TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag,
-- CASE WHEN overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) and DATEDIFF('day', current_date, term_end_date) > 120 then TRUE ELSE FALSE END AS Underutiliation_Flag, 
-- CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) then TRUE ELSE FALSE END AS Underutilization_Flag, 
--CASE WHEN ptc_score > .5 = false and ptc_last_90 = False and days_since_last_ptc_score = 2 then TRUE ELSE FALSE END AS High_PTC_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
-- CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and ptc_score > .5 and ptc_latest_score_date = most_recent_ptc and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTC_Score_Flag,
--CASE WHEN pte_score > .5 = false and pte_last_90 = False and days_since_last_pte_score = 2 then TRUE ELSE FALSE END AS High_PTE_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
-- CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and pte_score > .5 and pte_latest_score_date = most_recent_pte and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTE_Score_Flag,
--CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and overage_count > 0 and overage_amount > 0 and latest_overage_month = DATE_TRUNC('month', current_date) and qsr_enabled_flag = false then TRUE ELSE FALSE END AS Overage_QSR_Off_Flag,
CASE WHEN duo_trial_on_account_flag = True then TRUE else False end as duo_trial_flag,
CASE WHEN duo_on_sub_flag = True and sales_type = 'Renewal' then True else False end as duo_renewal_flag,
CASE WHEN (calculated_tier in ('Tier 1','Tier 2') or arr_basis >= 3000) and payment_failure_flag = True and sales_type = 'Renewal' then True else False end as Renewal_Payment_Failure,
-- CASE WHEN overage_amount > 0 and turn_on_seat_reconciliation <> 'Yes' and (latest_overage_month = DATE_TRUNC('month', current_date) or latest_overage_month = date_trunc('month', current_date - interval '1 month')) and contains(lower(fo_opp_name), '#smb19promo') then TRUE else FALSE end as Overage_with_QSR_Off_19_Offer,
CASE WHEN overage_amount> 1000 and turn_on_seat_reconciliation <> 'Yes' and latest_overage_month = DATE_TRUNC('month', current_date) then TRUE else FALSE end as Overage_with_QSR_Off
FROM all_data
WHERE (is_closed = False or (Renewal_Payment_Failure = True and failure_date >= dateadd('day',-7,current_date)))
),

cases as (
SELECT *,
--CASE WHEN check_in_case_needed_flag = True and ((current_subscription_end_date > last_upload_60_days AND current_subscription_end_date <= current_date_60_days) OR (current_subscription_end_date > last_upload_270_days AND current_subscription_end_date <= current_date_270_days)) then 'High Value Account Check In' end as Check_In_Trigger_Name,
CASE WHEN duo_renewal_flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) then 'Renewal with Duo'
--     WHEN future_tier_1_account_flag = True and (current_subscription_end_date > last_upload_90_days AND current_subscription_end_date <= current_date_90_days) and rollover_last_90 = False and any_case_last_90 = False then 'Future Tier 1 Account Check In' 
end as Future_Tier_1_Trigger_Name,
CASE WHEN PO_REQUIRED_Flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days)  and close_date > '2024-09-08' and arr_basis > 3000 then 'PO Required'
     WHEN multiyear_renewal_flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and rollover_last_90 = False and arr_basis > 3000 then 'Multiyear Renewal'
     WHEN EOA_Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and rollover_last_90 = False then 'Auto-Renewal Will Fail'
     WHEN Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and rollover_last_90 = False and arr_basis > 3000 then 'Auto-Renewal Will Fail'
     WHEN auto_renewal_will_fail_logic_flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and rollover_last_90 = False then 'Auto-Renewal Will Fail'
--     WHEN EOA_RENEWAL_FLAG = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and rollover_last_90 = False then 'EOA Renewal'
     WHEN will_churn_flag = True and (close_date > last_upload_60_days AND close_date <= current_date_60_days) and arr_basis > 2000 then 'Renewal Risk: Will Churn'
     WHEN Renewal_Payment_Failure = True and current_subscription_end_date <= current_date and (failure_date > DATEADD('day', -2, dateadd('day',-7,current_date)) AND failure_date <= current_date) then 'Renewal with Payment Failure'

     -- WHEN Overdue_Renewal_Flag = True and (current_subscription_end_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND current_subscription_end_date <= DATEADD('day', -2, current_date)) and rollover_last_90 = False and (eoa_flag = False AND arr_basis > 3000) then 'Overdue Renewal'
     -- WHEN Overdue_Renewal_Flag = True and (current_subscription_end_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND current_subscription_end_date <= DATEADD('day', -2, current_date)) and rollover_last_90 = False and eoa_flag = True then 'EOA Account - Overdue Renewal'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and (latest_switch_date > DATEADD('day', -2, dateadd('day',-7,current_date))) and rollover_last_90 = False and (eoa_flag = False AND arr_basis > 3000) then 'Auto-Renew Recently Turned Off'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and (latest_switch_date > DATEADD('day', -2, dateadd('day',-7,current_date))) and rollover_last_90 = False and eoa_flag = True then 'EOA Account - Auto-Renew Recently Turned Off'
     WHEN QSR_Failed_Flag = True and qsr_failed_last_75 = False and net_arr >1000 then 'Failed QSR' 
     ELSE NULL END AS Renewal_Trigger_Name, 
CASE 
     --WHEN Overage_with_QSR_Off_19_Offer = True and ((current_subscription_end_date > last_upload_90_days AND current_subscription_end_date <= current_date_90_days) OR (current_subscription_end_date > last_upload_180_days AND current_subscription_end_date <= current_date_180_days) OR (current_subscription_end_date > last_upload_270_days AND current_subscription_end_date <= current_date_270_days)) then 'Overage and QSR Off' 
     WHEN Overage_with_QSR_Off and ha_last_90 = False then 'Overage and QSR Off'
     WHEN duo_trial_flag = True and existing_duo_trial_flag = False and (duo_trial_start_date > DATEADD('day', -2, dateadd('day',-7,current_date)) AND duo_trial_start_date <= DATEADD('day', -2, current_date)) then 'Duo Trial Started'
ELSE NULL END AS High_Adoption_Case_Trigger_Name
FROM case_flags
),






--------------prioritizes renewals first and then low adoption over high adoption
final as (
SELECT *, 
CASE --WHEN Check_In_Trigger_name IS NOT NULL then check_in_trigger_name
     --WHEN Future_Tier_1_Trigger_Name IS NOT NULL THEN Future_Tier_1_Trigger_name
     WHEN renewal_trigger_name IS NOT NULL then renewal_trigger_name
     WHEN (renewal_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     --WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     ELSE NULL end as case_trigger, 
CASE WHEN renewal_trigger_name IS NOT NULL then True else False end as renewal_case
FROM cases
WHERE case_trigger is not null
),

output_prep as 
(SELECT * EXCLUDE context, 
CASE WHEN owner_id != '00G8X000006WmU3' then CONCAT(context, ' Skip Queue Flag: True')
    ELSE CONCAT(context, ' Skip Queue Flag: False') end as context_final,
CASE WHEN owner_id IS NULL and case_trigger = 'High Value Account Check In' then TRUE else FALSE end as remove_flag
FROM (
SELECT --distinct
f.case_trigger as case_trigger,
f.calculated_tier as account_tier,
f.* EXCLUDE (case_trigger,calculated_tier),
IFNULL(ROUND(arr_per_user,2),0) as current_price,
IFNULL(future_price*12,0) as renewal_price,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
CASE WHEN cd.case_trigger_id in (6, 26) then CONCAT(cd.case_subject, ' ', f.latest_switch_date)
     WHEN cd.case_trigger_id in (28) then CONCAT(cd.case_subject, ' ', f.duo_trial_start_date)
     when cd.case_trigger_id in (31) then concat(cd.case_subject, ' ',f.failure_date)
     when cd.case_trigger_id in (32) then concat(cd.case_subject, ' ',f.close_date)
     WHEN renewal_case = true and cd.case_trigger_id not in (31,32) then CONCAT(cd.case_subject, ' ', f.close_date) ELSE cd.case_subject end as case_subject, 
COALESCE(
CASE WHEN calculated_tier = 'Tier 1' then high_value_case_owner_id 
     WHEN current_open_cases > 0 then last_case_owner_id 
     ELSE cd.owner_id end
     , '00G8X000006WmU3')
      as owner_id,
cd.case_reason,
cd.record_type_id,
cd.priority,
CASE WHEN cd.case_trigger_id in (10, 11, 12, 28) or f.case_trigger = 'Duo Trial Started' then NULL else f.opportunity_id end as case_opportunity_id, 
CASE WHEN cd.case_trigger_id in (28) then f.contact_id else NULL end as case_contact_id,
cd.case_cta, 
CASE 
     -- WHEN cd.case_trigger_id = 9 then CONCAT(cd.CASE_CONTEXT, ' ', ptc_insights, ' EOA Account:', eoa_flag)
     -- WHEN cd.case_trigger_id = 11 then CONCAT(cd.CASE_CONTEXT, ' ', pte_insights, ' EOA Account:', eoa_flag)
     WHEN cd.case_trigger_id in (6,26) then CONCAT(cd.CASE_CONTEXT, ' ', latest_switch_date, ' Cancel Reason:',ifnull(cancel_reason,''), ' Cancel Comments:',ifnull(cancel_comments,''),  ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0))
     WHEN cd.case_trigger_id in  (5,25) then CONCAT(cd.case_context, ' ', current_subscription_end_date, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0))
     WHEN cd.case_trigger_id = 7 then CONCAT(cd.CASE_CONTEXT, ' ', opportunity_id, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0))
     WHEN cd.case_trigger_id = 4 and auto_renewal_will_fail_logic_flag = False then CONCAT(cd.CASE_CONTEXT, ' ', COALESCE(auto_renewal_status, 'Null'), ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0))
     WHEN cd.case_trigger_id = 4 and auto_renewal_will_fail_logic_flag = True then CONCAT('Auto Renewal Will Fail Due to Multiple Products on Subscription ', cd.CASE_CONTEXT, ' ', COALESCE(auto_renewal_status, 'Null'), ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0))
     WHEN cd.case_trigger_id = 12 then CONCAT(cd.CASE_CONTEXT, ' ', SIX_SENSE_ACCOUNT_INTENT_SCORE, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0)) 
     WHEN cd.case_trigger_id = 16 then CONCAT('Current Billable Quantity: ', max_billable_user_count, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0), ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
    --WHEN cd.case_trigger_id = 10 then CONCAT(cd.case_context, '$19 Promo:', Overage_with_QSR_Off_19_Offer, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     ELSE CONCAT(cd.CASE_CONTEXT, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag, ' Current Price: ', IFNULL(ROUND(arr_per_user,2),0), ' Renewal Price: ', IFNULL(future_price*12,0)) END AS context 
FROM FINAL f
LEFT JOIN case_data cd
    ON f.case_trigger = cd.case_trigger
    )
    where remove_flag = false
)

select
output_prep.account_id,
output_prep.owner_id,
output_prep.case_origin,
output_prep.type,
output_prep.case_subject,
output_prep.case_reason,
output_prep.record_type_id,
output_prep.priority,
output_prep.case_opportunity_id,
output_prep.case_cta,
output_prep.context_final,
output_prep.status,
random() as random_seed,
'Data Loaded Case' as description,
output_prep.* EXCLUDE (
    account_id,
owner_id,
case_origin,
type,
case_subject,
case_reason,
record_type_id,
priority,
case_opportunity_id,
case_cta,
context_final,
status
)


from output_prep
left join PREP_CRM_CASE
on output_prep.account_ID = PREP_CRM_CASE.dim_crm_account_id
and output_prep.case_subject = PREP_CRM_CASE.subject
-- and (output_prep.case_opportunity_id = PREP_CRM_CASE.dim_crm_opportunity_id or (output_prep.case_opportunity_id is null and prep_crm_case.created_date >= dateadd('day',-30,current_date))
-- or
-- (prep_crm_case.dim_crm_opportunity_id is null  and prep_crm_case.created_date >= dateadd('day',-30,current_date))
-- )
where PREP_CRM_CASE.dim_crm_account_id is null