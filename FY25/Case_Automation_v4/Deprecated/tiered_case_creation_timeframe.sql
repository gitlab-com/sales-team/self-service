SET LAST_UPLOAD_DATE = '2024-05-09';
--CAST(current_date - 30 as date);


------90 days since last case Closed - Resolved OR 90 days since last case created if Closed - Unresponsive
with high_value_case_last_90 as (
SELECT distinct
account_id, 
True as High_Value_Last_90
FROM(
SELECT distinct
account_id,
CASE WHEN lower(subject) = 'high value account check in' OR case_type = 'Inbound Request' OR (lower(subject) = 'fy25 high value account' AND status in ('Open', 'In Progress') AND DATEDIFF('day', created_date, current_date) <= 90) then True else False END AS High_Value_Last_90 ------- placeplacer until confirmation of case name 
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE ((DATEDIFF('day', created_date, current_date) <= 90 AND status = 'Closed - Unresponsive') 
OR (DATEDIFF ('day', closed_date, current_date) <= 90 AND status = 'Closed - Resolved')
OR status in ('Open', 'In Progress'))
and record_type_id in ('0128X000001pPRkQAM'))
WHERE high_value_last_90 = True
),

any_case_last_90 as (
SELECT distinct
account_id,
True as Any_case_Last_90 ------- placeplacer until confirmation of case name 
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE ((DATEDIFF('day', created_date, current_date) <= 90 AND status = 'Closed - Unresponsive') 
OR (DATEDIFF ('day', closed_date, current_date) <= 90 AND status = 'Closed - Resolved')
OR status in ('Open', 'In Progress'))
and record_type_id in ('0128X000001pPRkQAM')
),


rollover_case_last_90 as (
SELECT distinct
account_id, 
True as rollover_last_90
FROM (
SELECT distinct
account_id,
CASE WHEN lower(subject) like ('rollover%') THEN True else FALSE END as rollover_Last_90 ------- placeplacer until confirmation of case name 
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE ((DATEDIFF('day', created_date, current_date) <= 90 AND status = 'Closed - Unresponsive') 
OR (DATEDIFF ('day', closed_date, current_date) <= 90 AND status = 'Closed - Resolved')
OR status in ('Open', 'In Progress'))
and record_type_id in ('0128X000001pPRkQAM'))
WHERE rollover_last_90 = True
),


last_high_value_case as (
SELECT *
FROM(
SELECT
    account_id,
    --opportunity_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    sfdc_case.created_date,
    MAX(sfdc_case.created_date) over(partition by ACCOUNT_ID) as last_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date
), 

high_adoption_Last_90 as (
SELECT distinct
account_id, 
True as HA_last_90
FROM
(
SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'duo trial') OR contains(lower(subject), 'high adoption') OR contains(lower(subject), 'overage') OR contains(lower(subject), 'future tier 1')) then True else False END AS HA_Last_90
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 90
and record_type_id in ('0128X000001pPRkQAM'))
WHERE HA_last_90 = True
),

low_adoption_Last_180 as (
SELECT distinct
account_id, 
True as LA_last_180
FROM
(SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'ptc') OR contains(lower(subject), 'low adoption') OR contains(lower(subject), 'underutilization') OR contains(lower(subject), 'future tier 1')) then True else False END AS LA_Last_180
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 180
and record_type_id in ('0128X000001pPRkQAM')) 
WHERE la_last_180 = True
),

qsr_failed_last_75 as (
SELECT distinct
account_id, 
opportunity_id,
True as QSR_failed_last_75 
FROM (
SELECT distinct
account_id,
opportunity_id, -----placeholder to get this until opportunity ID is it's own field
CASE WHEN contains(lower(subject), 'failed qsr') then True else False end as QSR_Failed_last_75
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM') 
and contains(lower(subject), 'failed qsr'))
WHERE QSR_failed_last_75
),


last_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and status in ('Open', 'In Progress')
)
WHERE rn = 1
),

last_renewal_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and contains(subject, 'Renewal') ----currently a placeholder until we can determine what we need to be inclusive of all renewal cases
)
WHERE rn = 1
),

open_cases as (
SELECT
account_id, 
COUNT(distinct case_id) as count_cases
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM') 
AND status in ('Open', 'In Progress')
GROUP BY 1
),

open_renewal_opps as (
SELECT
dim_crm_account_id, 
count(distinct dim_crm_opportunity_id) as count_open_opps
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
WHERE sales_type = 'Renewal'
AND IS_Closed = False
GROUP BY 1
),


auto_renew_will_fail as (
select 
distinct
dim_crm_account_id,
dim_subscription_id,
product_rate_plan_name,
subscription_end_month as auto_renewal_sub_end_month,
turn_on_auto_renewal,
count(distinct product_tier_name) over(partition by dim_subscription_id) as sub_prod_count
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = '2024-01-01'
--and parent_crm_account_sales_segment = 'SMB'
and lower(product_tier_name) not like '%storage%'
and turn_on_auto_renewal = 'Yes'
qualify sub_prod_count > 1
),

tier1_full_churns as (
SELECT
account_id 
FROM PROD.WORKSPACE_SALES.SFDC_CASE
WHERE record_type_id in ('0128X000001pPRkQAM')
AND subject = 'FY25 High Value Account'
AND status = 'Closed - Resolved'
and resolution_action = 'Request Not Possible'
), 


existing_duo_trial as (
SELECT 
account_id 
FROM PROD.WORKSPACE_SALES.SFDC_CASE
WHERE record_type_id in ('0128X000001pPRkQAM')
AND subject = 'Duo Trial Started' OR (subject = 'Customer MQL' and account_id in ('0014M00001sEismQAC', '001PL000004lgERYAY'))
),

duo_on_sub as (
SELECT
distinct
dim_crm_account_id,
dim_subscription_id,
product_rate_plan_name,
subscription_end_month as auto_renewal_sub_end_month,
turn_on_auto_renewal,
CASE WHEN contains(lower(product_rate_plan_name), 'duo') then TRUE else FALSE end as duo_flag, 
--arr_month,
--max(arr_month) over(partition by dim_subscription_id) as latest_arr_month
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where (arr_month = date_trunc('month', current_date()) or arr_month = date_trunc('month', current_date - interval '1 month'))
and duo_flag = TRUE
),

-- FY26_high_value_case as (
-- SELECT distinct
-- account_id
-- FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
-- WHERE record_type_id in ('0128X000001pPRkQAM') 
-- and lower(subject) = 'FY26 High Value Account' ---
-- ),

--------------using the account base CTE to get tiering and beginning of FY25 info joined to the account id
account_base as (
SELECT acct.*, 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
  where crm_account_owner in
  ('AMER SMB Sales','APAC SMB Sales')))
or (acct.snapshot_date < '2024-02-01' and(acct.parent_crm_account_geo in ('AMER', 'APAC')
or acct.parent_crm_account_region in ('AMER', 'APAC'))
) then 'AMER/APAC' 
WHEN (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
  where crm_account_owner in
  ('EMEA SMB Sales')))
or (acct.snapshot_date < '2024-02-01' and (acct.parent_crm_account_geo in ('EMEA')
or acct.parent_crm_account_region in ('EMEA'))
) then 'EMEA' 
     ELSE 'Other' end as Team,
case when (acct.snapshot_date >= '2024-02-01' and acct.dim_crm_account_id in (
  select dim_crm_account_id from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
  where crm_account_owner in
  ('AMER SMB Sales','APAC SMB Sales','EMEA SMB Sales')))
or
(
acct.snapshot_date < '2024-02-01' and
(acct.carr_account_family <= 30000 --and acct.carr_this_account > 0
)
and (acct.parent_crm_account_max_family_employee <= 100 or acct.parent_crm_account_max_family_employee is null)
and ultimate.dim_parent_crm_account_id is null
and acct.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and acct.parent_crm_account_upa_country <> 'JP'
and acct.is_jihu_account = false
)
then true else false end as gds_account_flag,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
fo.SQS as fo_sqs,
fo.fo_opp_name as fo_opp_name,
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr, 
case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_id as HIGH_VALUE_CASE_ID,
high_value_case.case_owner_id as high_value_case_owner,
high_value_case.team as high_value_case_owner_team,
high_value_case.manager_name as high_value_case_owner_manager, 
FY25_high_value_case.owner_id as FY25_case_owner_id,
start_values.carr_account_family as starting_carr_account_family, 
start_values.carr_this_account as starting_carr_this_account, 
CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
     WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
     else 'Tier 2' end as starting_calculated_tier,
start_values.pte_score as starting_pte_score, 
start_values.ptc_score as starting_ptc_score, 
start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag
FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
    last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS,
    last_value(opportunity_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fo_opp_name
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo
    ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
LEFT JOIN 
  (
select snapshot_date as close_date, dim_crm_account_id, carr_this_account,
lag(carr_this_account,1) over(partition by dim_crm_account_id order by snapshot_date asc) as prior_carr,
-prior_carr as net_arr,
max(case when carr_this_account > 0 then snapshot_date else null end) over(partition by dim_crm_account_id) as last_carr_date
from PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
 where snapshot_date >= '2021-02-01'
-- and dim_crm_account_id = '0014M00001lbBt1QAE'
qualify prior_carr > 0 and carr_this_account = 0 and snapshot_date > last_carr_date
    ) churn 
    ON churn.DIM_CRM_ACCOUNT_ID = acct.DIM_CRM_ACCOUNT_ID
--------subquery to get current high tier case owner
LEFT JOIN 
    (SELECT * 
    FROM 
    (SELECT
    account_id,
    case_id,
    owner_id as case_owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type, 
    sfdc_case.created_date,
    MAX(sfdc_case.created_date) over(partition by ACCOUNT_ID) as latest_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    AND lower(subject) like '%high value account%'
    AND lower(subject) != 'high value account check in') 
    WHERE created_date = latest_high_value_date) high_value_case
    ON high_value_case.account_id = acct.dim_crm_account_id
--------------getting the previous high value case owner/etc
LEFT JOIN 
    (SELECT
    account_id,
    owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and subject = 'FY25 High Value Account' -----subject placeholder - this could change
    ) FY25_high_value_case
    ON FY25_high_value_case.account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
LEFT JOIN 
    (SELECT
    *
    FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    WHERE snapshot_date = '2024-02-09' 
    ) start_values
    ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.user_role_type,
    sfdc_case.created_date,
    MIN(sfdc_case.created_date) over(partition by ACCOUNT_ID) as first_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    AND lower(subject) like '%high value account%'
    AND lower(subject) != 'high value account check in') -----subject placeholder - this could change
    WHERE created_date = first_high_value_date 
    ) first_high_value_case
    ON first_high_value_case.account_id = acct.dim_crm_account_id

-----EOA cohort accounts
    LEFT JOIN 
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
)) EOA 
    ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
------free limit promo cohort accounts
LEFT JOIN 
    (select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
    ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
------price increase promo cohort accounts
LEFT JOIN
    (select distinct dim_crm_account_id from
        (
         select
          charge.*,
          arr / quantity as actual_price,
          prod.annual_billing_list_price as list_price
          from restricted_safe_common_mart_sales.mart_charge charge
          inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
          where 
          subscription_start_date >= '2023-04-01'
          and subscription_start_date <= '2023-07-01'
          and TYPE_OF_ARR_CHANGE = 'New'
          and quantity > 0
          and actual_price > 228
          and actual_price < 290
          and rate_plan_charge_name like '%Premium%'
        )) price_increase
        ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
LEFT JOIN
(
    select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id    
WHERE acct.snapshot_date = current_date),



---------------need to add fields to create timeframes for case pulls 
--------------this table pulls in all of the data for the accounts (from account CTE), opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_blended as (
SELECT 
$LAST_UPLOAD_DATE,
DATEADD('day', 30, $LAST_UPLOAD_DATE) as last_upload_30_days,
DATEADD('day', 80, $LAST_UPLOAD_DATE) as last_upload_80_days, 
DATEADD('day', 90, $LAST_UPLOAD_DATE) as last_upload_90_days, 
DATEADD('day', 180, $LAST_UPLOAD_DATE) as last_upload_180_days, 
DATEADD('day', 270, $LAST_UPLOAD_DATE) as last_upload_270_days, 
DATEADD('day', 30, current_date) as current_date_30_days, 
DATEADD('day', 80, current_date) as current_date_80_days, 
DATEADD('day', 90, current_date) as current_date_90_days, 
DATEADD('day', 180, current_date) as current_date_180_days,
DATEADD('day', 270, current_date) as current_date_270_days,
DATEDIFF('day', $LAST_UPLOAD_DATE, current_date) as days_since_last_upload,
a.dim_crm_account_id as account_id, 
gds_account_flag,
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.calculated_tier, 
a.team,
a.high_value_case_id,
a.high_value_case_owner,
a.high_value_case_owner_team,
a.high_value_case_owner_manager, 
a.count_active_subscriptions,
a.fo_opp_name,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
-- DATEDIFF('day', last_upload_date, a.next_renewal_date) as days_from_last_upload_to_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.CARR_THIS_ACCOUNT,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.GS_LAST_CSM_ACTIVITY_DATE,
a.free_promo_flag, 
a.Price_Increase_Promo_Flag,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
-- DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, last_upload_date) as days_since_6sense_account_update_last_upload,
o.dim_crm_opportunity_id as opportunity_id,
o.owner_id as opportunity_owner_id,
CASE WHEN o.sales_type = 'Renewal' and o.close_date >= '2024-02-01' and o.close_date <= '2025-01-31' THEN True ELSE False end as FY25_Renewal,
o.is_closed, 
o.is_won,
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
o.QSR_notes,
o.stage_name,
o.net_arr,
o.arr_basis,
o.PTC_PREDICTED_RENEWAL_RISK_CATEGORY,
CASE WHEN o.deal_path_name = 'Partner' then True ELSE false end as partner_opp_flag,
CASE WHEN qsr.qsr_failed_last_75 = True then True ELSE False end as qsr_failed_last_75,
s.dim_subscription_id as sub_subscription_id, 
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_start_date as current_subscription_start_date,
s.term_end_date as current_subscription_end_date,
DATEDIFF('day', s.term_start_date, current_date) as days_into_current_subscription,
-- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
s.turn_on_auto_renewal,
cd.created_date as last_case_created_date, 
DATEDIFF('day', CAST(cd.created_date as date), current_date) as days_since_last_case_created,
cd.case_id as last_case_id, 
cd.subject as last_case_subject, 
cd.status as last_case_status, 
cd.owner_id as last_case_owner_id, 
DATEDIFF('day', CAST(lr.created_date as date), current_date) as days_since_last_renewal_case_created,
lr.case_id as last_renewal_case_id, 
lr.subject as last_renewal_case_subject, 
lr.status as last_renewal_case_status, 
lr.owner_id as last_renewal_case_owner_id,
oc.count_cases as current_open_cases,
oo.count_open_opps,
COUNT(distinct s.DIM_SUBSCRIPTION_ID) OVER (PARTITION BY s.dim_crm_account_id) AS count_subscriptions,
CASE WHEN HA_last_90 = True then TRUE else FALSE end as HA_LAST_90, 
CASE WHEN LA_last_180 = TRUE then TRUE else FALSE end as LA_last_180,
CASE WHEN High_Value_Last_90 = True then True Else False end as High_Value_Last_90, 
CASE WHEN any_case_last_90 = True then True ELSE False end as Any_Case_Last_90,
CASE WHEN rollover_last_90 = True then True Else False end as rollover_last_90,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
-- DATEDIFF('day', s.term_end_date, last_upload_date) as days_since_current_sub_end_date_last_upload,
s.dim_subscription_id,
CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, o.close_date) else null end as days_till_close,
-- CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, last_upload_date) else null end as days_till_close_last_upload,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
a.EOA_flag,
b.PO_required,  
o.auto_renewal_status,
-- CASE WHEN overdue_renewal.account_id IS NOT NULL then True Else False end as overdue_renewal_last_300_flag,
CASE WHEN auto_renew_will_fail.dim_crm_account_id IS NOT NULL then True else False end as auto_renew_will_fail_mult_products,
CASE WHEN duo_on_sub.dim_crm_account_id IS NOT NULL then true else false end as duo_on_sub_flag,
--CASE WHEN FY26_high_value_case.account_id IS NOT NULL THEN True ELSE False end as FY26_High_Value_Account_Exists_Flag,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ultimate_Upgrade_Oppty_Flag, 
CASE WHEN tier1_full_churns.account_ID IS NOT NULL THEN True ELSE False end as Tier1_Full_Churn_Flag, 
CASE WHEN e.account_id IS NOT NULL then TRUE else FALSE end as existing_duo_trial_flag
FROM account_base a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    --AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
    and subscription_status = 'Active'
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
LEFT JOIN last_case_data cd 
    ON cd.account_id = a.dim_crm_account_id
LEFT JOIN last_renewal_case_data lr 
    ON lr.account_id = a.dim_crm_account_id
LEFT JOIN open_cases oc
    ON oc.account_id = a.dim_crm_account_id
LEFT JOIN open_renewal_opps oo
    ON oo.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN low_adoption_last_180 
    ON low_adoption_last_180.account_id = a.dim_crm_account_id
LEFT JOIN high_adoption_last_90
    ON high_adoption_last_90.account_id = a.dim_crm_account_id
LEFT JOIN qsr_failed_last_75 qsr
    ON qsr.account_id = a.dim_crm_account_id
    AND qsr.opportunity_id = o.dim_crm_opportunity_id
LEFT JOIN high_value_case_last_90
    ON high_value_case_last_90.account_id = a.dim_crm_account_id
LEFT JOIN any_case_last_90 
    ON any_case_last_90.account_id = a.dim_crm_account_id
LEFT JOIN rollover_case_last_90 
    ON rollover_case_last_90.account_id = a.dim_crm_account_id
LEFT JOIN auto_renew_will_fail
    ON auto_renew_will_fail.dim_crm_account_id = a.dim_crm_account_id
    AND auto_renew_will_fail.dim_subscription_id = s.dim_subscription_id
LEFT JOIN tier1_full_churns 
    ON tier1_full_churns.account_id = a.dim_crm_account_id
LEFT JOIN existing_duo_trial e
    ON e.account_id = a.dim_crm_account_id
LEFT JOIN duo_on_sub 
    ON duo_on_sub.dim_crm_account_id = a.dim_crm_account_id
    AND s.dim_subscription_id = duo_on_sub.dim_subscription_id
WHERE (gds_account_flag = True AND sales_type != 'Renewal')
OR ((gds_account_flag = True and sales_type = 'Renewal' and opportunity_name not like '%EDU PROGRAM%')
OR (gds_account_flag = True AND sales_type = 'Renewal' and opportunity_name not like '%OSS PROGRAM%'))),




utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,(mart_arr.ARR / mart_arr.QUANTITY) as arr_per_user
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
), 

----------------------------get the date on a subscription when a customer switches from auto-renew on to auto-renew off
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4
 ),



--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
eoa_accounts_fy24 as (
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_arr.ARR_MONTH
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
) order by dim_crm_account_id asc)
),
 

 
prior_sub_25_eoa as (
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
dim_subscription.dim_crm_opportunity_id_current_open_renewal,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
INNER JOIN eoa_accounts_fy24 on eoa_accounts_fy24.dim_crm_account_id = mart_charge.dim_crm_account_id
-- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
WHERE mart_charge.rate_plan_name like '%Premium%'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2023-02-01'
and mart_charge.term_start_date < current_date
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and (lower(rate_plan_charge_description) like '%eoa%'
or ((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
and quantity < 25)
),

duo_trials as (
SELECT *
FROM (
SELECT 
*, 
ROW_NUMBER() OVER(PARTITION BY account_id ORDER BY marketo_last_interesting_moment_date) as order_number
FROM (
select distinct
contact_id,
account_id,
marketo_last_interesting_moment_date, 
from PROD.LEGACY.SFDC_CONTACT_SNAPSHOTS_SOURCE
where marketo_last_interesting_moment like '%Duo Pro SaaS Trial%'
and marketo_last_interesting_moment_date >= '2024-02-01' ))
WHERE order_number = 1),

failure_sub as (
SELECT t.*, 
s.dim_subscription_id as failure_subscription_id, 
s.dim_crm_opportunity_id as failed_sub_oppty, 
s.dim_crm_opportunity_id_current_open_renewal as failed_sub_renewal_opp, 
s.dim_crm_opportunity_id_closed_lost_renewal as failed_sub_closed_opp
FROM (SELECT distinct
t.dim_crm_account_id, 
task_id, 
task_date,
--dim_crm_opportunity_id, 
RIGHT(REGEXP_SUBSTR(full_comments, 'GitLab subscription.{12}'),11) AS extracted_subscription_name
FROM PROD.COMMON_MART_SALES.MART_CRM_TASK t
WHERE (task_subject like '%Payment Failed%' or task_subject like '%Payment failed%')
--AND task_id = '00TPL000006KMiV2AW'
and task_date >= '2024-02-01') t
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON t.extracted_subscription_name = s.subscription_name
WHERE subscription_status = 'Active'),




-----------brings together account blended date, utilization data, and autorenewal switch data 
all_data as(
SELECT a.*, 
CASE WHEN subscription_end_month > '2024-04-01' THEN 29 ELSE 23.78 END as future_price,
u.overage_count,
u.overage_amount,
u.latest_overage_month,
u.qsr_enabled_flag,
u.max_billable_user_count,
u.LICENSE_USER_COUNT,
u.ARR,
u.arr_per_user,
turn_on_seat_reconciliation,
--turn_on_auto_renewal,
s.latest_switch_date, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch, 
CASE WHEN prior_sub_25_eoa.dim_crm_account_id IS NOT NULL then True else False end as prior_year_eoa_under_25_flag, 
CASE WHEN d.contact_id IS NOT NULL then True ELSE False end as duo_trial_on_account_flag, 
d.marketo_last_interesting_moment_date as duo_trial_start_date, 
d.contact_id, 
d.order_number, 
CASE WHEN f.dim_crm_account_id IS NOT NULL THEN TRUE ELSE FALSE end as payment_failure_flag, 
failed_sub_oppty,
failed_sub_renewal_opp, 
failed_sub_closed_opp, 
task_date
FROM account_blended a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id
LEFT JOIN prior_sub_25_eoa 
    ON prior_sub_25_eoa.dim_crm_account_id = a.account_id
LEFT JOIN duo_trials d
    ON d.account_id = a.account_id
LEFT JOIN failure_sub f
    ON f.dim_crm_account_id = a.account_id
    AND f.failure_subscription_id = sub_subscription_id
    ),




------------flags each account/opportunity with any applicable flags
case_flags as (
SELECT *,
CASE WHEN calculated_tier = 'Tier 1' and High_Value_Last_90 = False AND Tier1_Full_Churn_Flag = False then True ELSE False end as check_in_case_needed_flag,
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') AND any_case_last_90 = False and (future_price*max_billable_user_count)*12 >= 7000 THEN True ELSE False end as future_tier_1_account_flag, 
CASE WHEN sales_type = 'Renewal' AND PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
CASE WHEN opportunity_term > 12 OR (CONTAINS(opportunity_name, '2 year') or CONTAINS(opportunity_name, '3 year')) then TRUE else FALSE end as multiyear_renewal_flag,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN TRUE ELSE FALSE end as EOA_Auto_Renewal_Will_Fail_Flag,
CASE WHEN ((auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) AND EOA_Auto_Renewal_Will_Fail_Flag = False) THEN TRUE ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag, 
CASE WHEN auto_renew_will_fail_mult_products = True then True else False end as auto_renewal_will_fail_logic_flag,
CASE WHEN (eoa_flag = True and prior_year_eoa_under_25_flag = True and max_BILLABLE_USER_COUNT >= 25) OR (eoa_flag = True AND turn_on_auto_renewal = 'No') THEN TRUE ELSE FALSE end as EOA_RENEWAL_FLAG,
CASE WHEN QSR_flag = True and QSR_status = 'Failed' and QSR_notes like '%card%' and is_closed = False and close_date >= '2024-02-01' then TRUE ELSE FALSE END AS QSR_Failed_Flag, 
CASE WHEN calculated_tier in ('Tier 2') and sales_type = 'Renewal' and PTC_PREDICTED_RENEWAL_RISK_CATEGORY = 'Will Churn (Actionable)' then TRUE else FALSE END AS Will_Churn_Flag,
CASE WHEN latest_switch_date IS NOT NULL THEN TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag,
--CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and overage_count > 0 and overage_amount > 0 and latest_overage_month = DATE_TRUNC('month', current_date) and qsr_enabled_flag = false then TRUE ELSE FALSE END AS Overage_QSR_Off_Flag,
CASE WHEN duo_trial_on_account_flag = True then TRUE else False end as duo_trial_flag,
CASE WHEN duo_on_sub_flag = True and sales_type = 'Renewal' then True else False end as duo_renewal_flag,
CASE WHEN calculated_tier in ('Tier 2') and payment_failure_flag = True and sales_type = 'Renewal' then True else False end as Renewal_Payment_Failure,
-- CASE WHEN overage_amount > 0 and turn_on_seat_reconciliation <> 'Yes' and (latest_overage_month = DATE_TRUNC('month', current_date) or latest_overage_month = date_trunc('month', current_date - interval '1 month')) and contains(lower(fo_opp_name), '#smb19promo') then TRUE else FALSE end as Overage_with_QSR_Off_19_Offer,
CASE WHEN overage_amount> 1000 and turn_on_seat_reconciliation <> 'Yes' and latest_overage_month = DATE_TRUNC('month', current_date) then TRUE else FALSE end as Overage_with_QSR_Off
FROM all_data
WHERE is_closed = False
),



-------uses the case flags to create cases and adds in the timing they should be triggering 
cases as (
SELECT *,
CASE WHEN check_in_case_needed_flag = True and ((current_subscription_end_date > last_upload_80_days AND current_subscription_end_date <= current_date_80_days) OR (current_subscription_end_date > last_upload_180_days AND current_subscription_end_date <= current_date_180_days) OR (current_subscription_end_date > last_upload_270_days AND current_subscription_end_date <= current_date_270_days)) then 'High Value Account Check In' end as Check_In_Trigger_Name,
CASE WHEN duo_renewal_flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'Renewal with Duo'
     WHEN future_tier_1_account_flag = True and (current_subscription_end_date > last_upload_80_days AND current_subscription_end_date <= current_date_80_days) and rollover_last_90 = False and any_case_last_90 = False then 'Future Tier 1 Account Check In' end as Future_Tier_1_Trigger_Name,
CASE WHEN PO_REQUIRED_Flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) and close_date > '2024-09-08' and arr_basis > 3000 then 'PO Required'
     WHEN multiyear_renewal_flag = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and rollover_last_90 = False and arr_basis > 3000 then 'Multiyear Renewal'
     WHEN EOA_Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and rollover_last_90 = False then 'Auto-Renewal Will Fail'
     WHEN Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and rollover_last_90 = False and arr_basis > 3000 then 'Auto-Renewal Will Fail'
     WHEN auto_renewal_will_fail_logic_flag = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and rollover_last_90 = False then 'Auto-Renewal Will Fail'
     WHEN EOA_RENEWAL_FLAG = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and rollover_last_90 = False then 'EOA Renewal'
     WHEN will_churn_flag = True and (close_date > last_upload_90_days AND close_date <= current_date_90_days) and arr_basis > 2000 then 'Renewal Risk: Will Churn'
     WHEN Renewal_Payment_Failure = True and (task_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND task_date <= DATEADD('day', -2, current_date)) then 'Renewal with Payment Failure'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and (latest_switch_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND latest_switch_date <= DATEADD('day', -2, current_date)) and rollover_last_90 = False and (eoa_flag = False AND arr_basis > 3000) then 'Auto-Renew Recently Turned Off'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and (latest_switch_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND latest_switch_date <= DATEADD('day', -2, current_date)) and rollover_last_90 = False and eoa_flag = True then 'EOA Account - Auto-Renew Recently Turned Off'
     WHEN QSR_Failed_Flag = True and qsr_failed_last_75 = False and net_arr >1000 then 'Failed QSR' 
     ELSE NULL END AS Renewal_Trigger_Name, 
CASE 
     --WHEN Overage_with_QSR_Off_19_Offer = True and ((current_subscription_end_date > last_upload_90_days AND current_subscription_end_date <= current_date_90_days) OR (current_subscription_end_date > last_upload_180_days AND current_subscription_end_date <= current_date_180_days) OR (current_subscription_end_date > last_upload_270_days AND current_subscription_end_date <= current_date_270_days)) then 'Overage and QSR Off' 
     WHEN Overage_with_QSR_Off and ha_last_90 = True then 'Overage and QSR Off'
     WHEN duo_trial_flag = True and existing_duo_trial_flag = False and (duo_trial_start_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND duo_trial_start_date <= DATEADD('day', -2, current_date)) then 'Duo Trial Started'
ELSE NULL END AS High_Adoption_Case_Trigger_Name
FROM case_flags
),





--------------prioritizes renewals first and then low adoption over high adoption
final as (
SELECT *, 
CASE WHEN Check_In_Trigger_name IS NOT NULL then check_in_trigger_name
     WHEN Future_Tier_1_Trigger_Name IS NOT NULL THEN Future_Tier_1_Trigger_name
     WHEN renewal_trigger_name IS NOT NULL then renewal_trigger_name
     WHEN (renewal_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     --WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     ELSE NULL end as case_trigger, 
CASE WHEN renewal_trigger_name IS NOT NULL then True else False end as renewal_case
FROM cases
WHERE case_trigger is not null)



SELECT * EXCLUDE context, 
CASE WHEN owner_id != '00G8X000006WmU3' then CONCAT(context, ' Skip Queue Flag: True')
    ELSE CONCAT(context, ' Skip Queue Flag: False') end as context_final,
CASE WHEN owner_id IS NULL and case_trigger = 'High Value Account Check In' then TRUE else FALSE end as remove_flag
FROM (
SELECT distinct
f.case_trigger as case_trigger,
f.account_id,
f.calculated_tier as account_tier,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
CASE WHEN cd.case_trigger_id in (6, 26) then CONCAT(cd.case_subject, ' ', f.latest_switch_date)
     WHEN cd.case_trigger_id in (28) then CONCAT(cd.case_subject, ' ', f.duo_trial_start_date)
     WHEN renewal_case = true then CONCAT(cd.case_subject, ' ', f.close_date) ELSE cd.case_subject end as case_subject, 
CASE WHEN calculated_tier = 'Tier 1' then high_value_case_owner 
     WHEN current_open_cases > 0 then last_case_owner_id 
     ELSE cd.owner_id end as owner_id,
cd.case_reason,
cd.record_type_id,
cd.priority,
CASE WHEN cd.case_trigger_id in (10, 11, 12, 28) or f.case_trigger = 'Duo Trial Started' then NULL else f.opportunity_id end as case_opportunity_id, 
CASE WHEN cd.case_trigger_id in (28) then f.contact_id else NULL end as case_contact_id,
cd.case_cta, 
CASE 
     -- WHEN cd.case_trigger_id = 9 then CONCAT(cd.CASE_CONTEXT, ' ', ptc_insights, ' EOA Account:', eoa_flag)
     -- WHEN cd.case_trigger_id = 11 then CONCAT(cd.CASE_CONTEXT, ' ', pte_insights, ' EOA Account:', eoa_flag)
     WHEN cd.case_trigger_id in (6,26) then CONCAT(cd.CASE_CONTEXT, ' ', latest_switch_date, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     WHEN cd.case_trigger_id in  (5,25) then CONCAT(cd.case_context, ' ', current_subscription_end_date, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     WHEN cd.case_trigger_id = 7 then CONCAT(cd.CASE_CONTEXT, ' ', opportunity_id, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     WHEN cd.case_trigger_id = 4 and auto_renewal_will_fail_logic_flag = False then CONCAT(cd.CASE_CONTEXT, ' ', COALESCE(auto_renewal_status, 'Null'), ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     WHEN cd.case_trigger_id = 4 and auto_renewal_will_fail_logic_flag = True then CONCAT('Auto Renewal Will Fail Due to Multiple Products on Subscription ', cd.CASE_CONTEXT, ' ', COALESCE(auto_renewal_status, 'Null'), ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     WHEN cd.case_trigger_id = 12 then CONCAT(cd.CASE_CONTEXT, ' ', SIX_SENSE_ACCOUNT_INTENT_SCORE, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag) 
     WHEN cd.case_trigger_id = 16 then CONCAT('Current Billable Quantity: ', max_billable_user_count, ' Current Price: ', ROUND(arr_per_user,2), ' Renewal Price: ', future_price*12, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     --WHEN cd.case_trigger_id = 10 then CONCAT(cd.case_context, '$19 Promo:', Overage_with_QSR_Off_19_Offer, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag)
     ELSE CONCAT(cd.CASE_CONTEXT, ' EOA Account:', eoa_flag, ' Partner Opp:', partner_opp_flag, ' Free Promo Flag:', free_promo_flag, ' Price Increase Flag:', Price_Increase_Promo_Flag) END AS context 
FROM FINAL f
LEFT JOIN boneyard.case_data cd
    ON f.case_trigger = cd.case_trigger
    )
    where remove_flag = false 






