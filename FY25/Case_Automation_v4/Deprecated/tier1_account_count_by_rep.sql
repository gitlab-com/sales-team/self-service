SELECT u.name,
u.user_id,
COUNT(DISTINCT case_id)
FROM PROD.WORKSPACE_SALES.SFDC_CASE c
LEFT JOIN PROD.LEGACY.SFDC_USERS u
    ON u.user_id = c.owner_id
WHERE subject = 'FY25 High Value Account'
AND u.name IS NOT NULL
GROUP BY 1,2
