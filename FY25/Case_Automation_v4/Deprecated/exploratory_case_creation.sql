with

wk_sales_gds_cases as (
select * from PROD.RESTRICTED_SAFE_WORKSPACE_SALES.WK_SALES_GDS_CASES
),

wk_sales_gds_account_snapshots as (
select * from PROD.RESTRICTED_SAFE_WORKSPACE_SALES.WK_SALES_GDS_ACCOUNT_SNAPSHOTS
where 
gds_account_flag
and snapshot_date = dateadd('day',-1,current_date)
and is_deleted = False
),

wk_sales_gds_opportunities as (
select * from PROD.RESTRICTED_SAFE_WORKSPACE_SALES.WK_SALES_GDS_OPPORTUNITIES
),

wk_sales_gds_account_snapshots as (
select * from PROD.RESTRICTED_SAFE_WORKSPACE_SALES.WK_SALES_GDS_ACCOUNT_SNAPSHOTS
where snapshot_date = dateadd('day',-1,current_date)
),

dim_subscription as (
select * from PROD.COMMON.DIM_SUBSCRIPTION
),

case_summary_prep as (
select
dim_crm_account_id,
count(distinct case when is_closed = False and subject not like '%High Value%' then case_id else null end) as open_nonhva_case_count,
count(distinct case when is_closed then case_id else null end) as closed_case_count,
count(distinct case when is_closed and status = 'Closed: Resolved' then case_id else null end) as resolved_case_count,
max(created_date)::DATE as most_recent_case_created_date,
max(closed_date)::DATE as most_recent_case_closed_date,
max(case when is_closed and status = 'Closed: Resolved' then closed_date else null end)::DATE as most_recent_case_resolved_date
from wk_sales_gds_cases
where spam_checkbox = 0
and status not like '%Duplicate%'
group by 1
),

case_summary_trigger_prep as (
select
dim_crm_account_id,
case when case_trigger like '%2024%' then 
trim(left(case_trigger,charindex('2024',case_trigger)-1))
else coalesce(case_trigger,subject,'No subject or trigger') end
as trigger_clean,
count(distinct case when is_closed = False then case_id else null end) over(partition by dim_crm_account_id, trigger_clean) as open_case_count,
count(distinct case when is_closed then case_id else null end) over(partition by dim_crm_account_id, trigger_clean) as closed_case_count,
count(distinct case when is_closed and status = 'Closed: Resolved' then case_id else null end) over(partition by dim_crm_account_id, trigger_clean) as resolved_case_count,
max(created_date) over(partition by dim_crm_account_id, trigger_clean)::DATE as most_recent_case_created_date,
max(closed_date) over(partition by dim_crm_account_id, trigger_clean)::DATE as most_recent_case_closed_date,
max(case when is_closed and status = 'Closed: Resolved' then closed_date else null end) over(partition by dim_crm_account_id, trigger_clean)::DATE as most_recent_case_resolved_date,
first_value(trigger_clean) over(partition by dim_crm_account_id order by created_date desc) as account_most_recent_created_trigger,
first_value(case when is_closed and status = 'Closed: Resolved' then trigger_clean else null end) over(partition by dim_crm_account_id order by created_date desc) as account_most_recent_resolved_trigger

from wk_sales_gds_cases
where spam_checkbox = 0
and status not like '%Duplicate%'
and dim_crm_account_id is not null
),

distinct_trigger_summary as (
select
distinct
dim_crm_account_id,
account_most_recent_created_trigger,
account_most_recent_resolved_trigger
from case_summary_trigger_prep
),

--Initial portion of the query gets the case disposition of each GDS account, for later filtering
account_case_summary as (
select
wk_sales_gds_account_snapshots.*,
case_summary_prep.* EXCLUDE (dim_crm_account_id),
distinct_trigger_summary.* EXCLUDE (dim_crm_account_id)
from wk_sales_gds_account_snapshots
left join case_summary_prep
on wk_sales_gds_account_snapshots.dim_crm_account_id = case_summary_prep.dim_crm_account_id
left join distinct_trigger_summary
on wk_sales_gds_account_snapshots.dim_crm_account_id = distinct_trigger_summary.dim_crm_account_id
),

account_case_flags as (
select 
dateadd('day',-180,current_date) as current_date_180_days,
dateadd('day',-90,current_date) as current_date_90_days,
dateadd('day',-60,current_date) as current_date_60_days,
dateadd('day',-30,current_date) as current_date_30_days,

account_case_summary.*,
coalesce(high_value_case_id is not null,FALSE) as has_high_value_case,
coalesce(most_recent_case_resolved_date >= current_date_180_days, FALSE) as has_resolved_case_last_180,
coalesce(most_recent_case_resolved_date >= current_date_90_days, FALSE) as has_resolved_case_last_90,
coalesce(most_recent_case_resolved_date >= current_date_60_days, FALSE) as has_resolved_case_last_60,
coalesce(most_recent_case_resolved_date >= current_date_30_days, FALSE) as has_resolved_case_last_30,
coalesce(open_nonhva_case_count > 0, FALSE) as has_open_non_highvalue_cases

from account_case_summary

),

opportunity_renewal_prep as
(
select
*,
rank() over(partition by dim_crm_account_id order by close_date asc) as close_date_rank_order
from wk_sales_gds_opportunities
where sales_type = 'Renewal'
and is_closed = False
and close_date > current_date
and arr_basis_for_clari > 0
),

opportunity_renewal_output as
(
select
*
from opportunity_renewal_prep
where close_date_rank_order = 1
),

opportunity_growth_prep as
(
select
*,
rank() over(partition by dim_crm_account_id order by close_date desc) as close_date_rank_order,
rank() over(partition by dim_crm_account_id, close_date order by net_arr desc) as close_date_multiple_narr_order
from wk_sales_gds_opportunities
where sales_type = 'Add-On Business'
and is_closed
and is_won
and net_arr > 0
),

opportunity_growth_output as
(
select
*
from opportunity_growth_prep
where close_date_rank_order = 1 
and close_date_multiple_narr_order = 1
),

subscription_prep as (
select
* ,
max(subscription_version) over (partition by subscription_name order by subscription_version desc) as latest_version,
max(case when dim_crm_opportunity_id_current_open_renewal is not null then subscription_version else null end)  over (partition by subscription_name order by subscription_version desc) as latest_version_with_open_renewal
from dim_subscription
qualify (
dim_crm_opportunity_id_current_open_renewal is not null and subscription_version = latest_version_with_open_renewal)
or
subscription_version = latest_version
)

--select * from opportunity_renewal_output order by 2

select 
account_case_flags.*, 
opportunity_renewal_output.dim_crm_opportunity_id as dim_crm_opportunity_id_next_renewal,
opportunity_renewal_output.close_date as close_date_next_renewal,
opportunity_renewal_output.arr_basis_for_clari as arr_basis_next_renewal,
opportunity_growth_output.dim_crm_opportunity_id as dim_crm_opportunity_id_most_recent_growth,
opportunity_growth_output.close_date as close_date_most_recent_growth,
opportunity_growth_output.net_arr as net_arr_most_recent_growth,
subscription_prep.dim_subscription_id as sub_subscription_id, 
subscription_prep.dim_subscription_id_original as sub_subscription_id_original,
subscription_prep.subscription_status, 
subscription_prep.term_start_date as current_subscription_start_date,
subscription_prep.term_end_date as current_subscription_end_date,
DATEDIFF('day', subscription_prep.term_start_date, current_date) as days_into_current_subscription,
subscription_prep.turn_on_auto_renewal,
subscription_prep.turn_on_seat_reconciliation

from account_case_flags
left join opportunity_renewal_output
on account_case_flags.dim_crm_account_id = opportunity_renewal_output.dim_crm_account_id 
left join opportunity_growth_output
on account_case_flags.dim_crm_account_id = opportunity_growth_output.dim_crm_account_id 
LEFT JOIN subscription_prep
    ON opportunity_renewal_output.dim_crm_opportunity_id = subscription_prep.dim_crm_opportunity_id_current_open_renewal
or (opportunity_growth_output.dim_crm_opportunity_id = subscription_prep.dim_crm_opportunity_id and subscription_prep.dim_crm_opportunity_id_current_open_renewal is null)

where 
account_case_flags.carr_this_account > 0
and sub_subscription_id is null
and churn_close_date is null
order by carr_this_account desc

--select * from dim_subscription where dim_crm_account_id = '0014M00001ldaZ7QAI'