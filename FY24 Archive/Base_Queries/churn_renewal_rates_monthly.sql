/*
******Additional version of churn and renewal rates by month (rather than quarter)
-﻿ NET_RETENTION = ARR in the Pooled account set in a given month / ARR in that account set 1 year prior
-﻿ NET_LOGO_RETENTION = What % of account set from a year prior are still customers 
-﻿ CHURN_RATE_NARR = Churned nARR / ARR Basis for the time period
-﻿ CHURN_RATE_LOGO = What % of accounts with a renewal in this period churned
-﻿ UPLIFT_RATE_RENEWAL = nARR from won renewals in this period (net of Contraction) / ARR Basis (renewal uplift rate)
-﻿ CONTRACTION_RATE_RENEWAL = nARR from Contractions at Renewal / ARR Basis
-﻿ GROWTH_RATE_NARR = nARR from won Growth + QSR / total acct CARR
-﻿ CONTRACTION_RATE_NARR = nARR from Contractions / total acct CARR
*/

with oppty_base as
(
[FY24 Opportunity Baseline with LT]
  and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)
 ),

 acct_data as 
(
  select
  snapshot_date,
  sum(carr_this_account) as carr_this_account
  from
([FY24 Account Snapshot with LT]
                  where fiscal_year = 2024
               and LT_segment = 'Pooled'
               and carr_this_account > 0
-- and account_tier_calculated = 1
               and (snapshot_date = last_day_of_month or snapshot_date = CURRENT_DATE)
              )
  group by 1
  )

select
close_month,
sum(ARR_Basis) as total_arr_basis,
sum(renewal_count) as total_renewal_basis,
min(carr) as total_carr,
sum(case when trx_type = 'Churn' then net_arr else null end) as churn_narr,
sum(case when trx_type like 'Renewal - Uplift%' then net_arr else null end) as renewal_uplift_narr,
sum(case when trx_type like any ('Growth%','QSR%')  then net_arr else null end) as growth_narr,
sum(case when trx_type like any ('%Contract%')  then net_arr else null end) as contraction_narr,
sum(case when trx_type like any ('Renewal - Contract%')  then net_arr else null end) as renewal_contraction_narr,
churn_narr / total_arr_basis as renewal_churn_rate,
--sum(case when trx_type = 'Churn' then renewal_count else null end) / total_renewal_basis as churn_rate_logo,
renewal_uplift_narr / total_arr_basis as renewal_uplift_rate,
growth_narr / total_carr as growth_rate_carr,
contraction_narr / total_carr as contraction_rate_carr,
renewal_uplift_narr / total_carr as renewal_uplift_carr,
churn_narr / total_carr as churn_carr,
renewal_contraction_narr / total_arr_basis as renewal_contraction_rate,
(churn_narr + renewal_uplift_narr + renewal_contraction_narr) / total_arr_basis as net_renewal_rate

from
(
select
close_month,
trx_type,
sum(net_arr) as net_arr,
sum(ARR_Basis) as ARR_Basis,
min(acct_data.carr_this_account) as carr,
count(DISTINCT case when trx_type like any ('Churn%','Renew%') then dim_crm_opportunity_id else null end) as renewal_count

from oppty_base
inner join acct_data on 
--   oppty_base.dim_crm_account_id = acct_base.dim_crm_account_id
--   and 
  oppty_base.close_month = date_trunc('month',acct_data.snapshot_date)
where
trx_type like any ('Churn%','%Renew%','Growth%','QSR%')
and oppty_base.LT_segment = 'Pooled'
and oppty_base.fiscal_year = 2024
--   and account_tier_calculated = 1
and close_date <= CURRENT_DATE
and is_closed
  and (net_arr <> 0 or trx_type like '%Renew%')
group by 1,2
 )
group by 1
-- --qualify churn_rate_narr is not null
 order by 1 asc
