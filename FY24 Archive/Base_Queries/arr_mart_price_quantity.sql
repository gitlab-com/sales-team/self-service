with accts as
(
  select
  dim_crm_account_id as pooled_acct_id,
  fo_close_date,
  churn_close_date,
lt_segment,
snapshot_date,
account_tier_calculated
  from
  ( with acct_base as (SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
   where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
      
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
              ),
/*
How it should work:
- Pooled AE manual tier updates should persist (after 3/1)
- Tier 1/1.5s should not get downtiered
- If churned - accts keep getting tiered but Tier 1/1.5 no longer locked
*/

calc as (SELECT b.*, 
case when b.account_tier in ('Rank 1','Rank 1.5') and (b.churn_close_date is null or b.churn_close_date > b.SNAPSHOT_date) then 1
when b.account_tier_notes not like '%Ops%' and b.snapshot_date >= '2023-03-01' then 
        case when b.account_tier = 'Rank 2' then 2
         when b.account_tier = 'Rank 3' then 3
         else 1 end
else
calc.account_tier_calculated end as account_tier_calculated
FROM acct_base b
LEFT JOIN (SELECT
      dim_crm_account_id,
           snapshot_date as calc_snapshot_date,
        2964 as carr_tier_1,
        2400 as carr_tier_2,
        1916 as carr_tier_3,
        134985000 as funding_tier_1,
        44909650 as funding_tier_2,
        20624700 as funding_tier_3,
        103 as dev_count_tier_1,
        44 as dev_count_tier_2,
        29 as dev_count_tier_3,
        CASE
        WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
        AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 ,
        case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end as account_tier_calculated 
     from acct_base
      where --lt_segment = 'Pooled'
     -- and
           fiscal_year = 2024) calc
      on calc.dim_crm_account_id = b.dim_crm_account_id
      and calc.calc_snapshot_date = b.snapshot_date) 
 

 SELECT calc.*, 
 first_value(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as initial_tier,
LAST_VALUE(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as final_tier,
case when initial_tier = 1 and final_tier > 1 then 1 else 0 end as downtier_flag,
case when initial_tier > 1 and final_tier = 1 then 1 else 0 end as uptier_flag  
 FROM calc
   )
   where
    (snapshot_date = last_day_of_month or snapshot_date = current_date)
--   LT_Segment = 'Pooled'
--   and 
--   carr_this_account > 0
 ),


arr_data as
(
 select
  distinct
  MART_ARR.*,
  DIM_PRODUCT_DETAIL.product_rate_plan_name as prod_detail_rate_plan_name,
  annual_billing_list_price,
  fo_close_date,
  churn_close_date,
  lt_segment,
account_tier_calculated
  from
  RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
  inner join accts on MART_ARR.DIM_CRM_ACCOUNT_ID = accts.pooled_acct_id
and mart_arr.arr_month = date_trunc('month',snapshot_date)
  inner join common.DIM_PRODUCT_DETAIL on MART_ARR.DIM_PRODUCT_DETAIL_ID = DIM_PRODUCT_DETAIL.DIM_PRODUCT_DETAIL_ID
  where 
  arr_month >= '2022-02-01'
  and prod_detail_rate_plan_name like '%Premium%'
  and fo_close_date < '2023-04-01'
--  and churn_close_date is null
 )

select 

* from
(select
arr_data.*,
dim_subscription.term_start_month,
dim_subscription.term_end_month,
arr / quantity as actual_price,
case when actual_price >= 229 and product_rate_plan_name like '%Premium%' and arr_month >= '2023-04-01' then 1 else 0 end as price_increase_flag,
arr - lag(arr,1) over(partition by arr_data.dim_crm_account_id order by arr_month asc) as arr_change,
quantity - lag(quantity,1) over(partition by arr_data.dim_crm_account_id order by arr_month asc) as quantity_change,
actual_price - lag(actual_price,1) over(partition by arr_data.dim_crm_account_id order by arr_month asc) as price_change,
quantity_change * actual_price as quantity_change_arr,
price_change * (lag(quantity,1) over(partition by arr_data.dim_crm_account_id order by arr_month asc)) as price_change_arr,
 case when churn_close_date >= '2023-04-03' then true else false end as affected_churn_flag
from
arr_data
inner join common.dim_subscription on arr_data.dim_subscription_id = dim_subscription.dim_subscription_id

order by arr_month asc

 )
where
arr_month >= '2022-02-01'
and arr_month <= DATE_TRUNC('month',CURRENT_DATE)
