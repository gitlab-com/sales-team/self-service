-------------------------------------------------------
/*
- Expands on the Price Points query to derive impacts from price and quantity
- Splits metrics by Web, Nonweb, Low Touch, Non-Low Touch
- Will need to be updated once price point changes

*/
-------------------------------------------------------
[fo_asp_price_points_LT]
  ,


pre_summary as (
select
 MEDIAN(quantity) as MEDIAN_quantity_comp,
  avg(quantity) as avg_quantity_comp,
sum(quantity) as Total_quantity_comp,
  mode(quantity) as mode_quantity_comp,
 count(DISTINCT dim_crm_opportunity_id)::int as fo_count_comp,
sum(net_arr)::int as total_narr_comp,
median(case when is_web_portal_purchase then quantity else null end) as web_median_comp,
median(case when is_web_portal_purchase = false then quantity else null end) as nonweb_median_comp,
median(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_median_comp,
median(case when LT_Segment = false then quantity else null end) as non_lt_fo_median_comp,
avg(case when is_web_portal_purchase then quantity else null end) as web_avg_comp,
avg(case when is_web_portal_purchase = false then quantity else null end) as nonweb_avg_comp,
avg(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_avg_comp,
avg(case when LT_Segment = false then quantity else null end) as non_lt_fo_avg_comp,
sum(case when is_web_portal_purchase then quantity else null end) as web_total_quantity_comp,
sum(case when is_web_portal_purchase = false then quantity else null end) as nonweb_total_quantity_comp,
sum(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_total_quantity_comp,
sum(case when LT_Segment = false then quantity else null end) as non_lt_fo_total_quantity_comp,
count(DISTINCT case when is_web_portal_purchase then dim_crm_opportunity_id else null end) as web_fo_count_comp,
count(DISTINCT case when is_web_portal_purchase = false then dim_crm_opportunity_id else null end) as nonweb_fo_count_comp,
count(DISTINCT case when LT_Segment = 'FO' then dim_crm_opportunity_id else null end) as lt_fo_fo_count_comp,
count(DISTINCT case when LT_Segment = false then dim_crm_opportunity_id else null end) as non_lt_fo_count_comp
from
subs
where list_price <= 348
  and actual_price_point in ('Pre-Matterhorn')
-- and close_date < '2023-05-01'
--  and quantity < 100
  ),

monthly_inc_summary as 
(
  select
actual_price_point,
  close_month,
 MEDIAN(quantity) as MEDIAN_quantity,
  avg(quantity) as avg_quantity,
sum(quantity) as Total_quantity,
  mode(quantity) as mode_quantity,
 count(DISTINCT dim_crm_opportunity_id)::int as fo_count,
sum(net_arr)::int as total_narr,
sum(case when is_web_portal_purchase then net_arr else null end)::int as web_narr,
sum(case when is_web_portal_purchase = false then net_arr else null end)::int as nonweb_narr,
sum(case when LT_Segment = 'FO' then net_arr else null end)::int as lt_fo_narr,
sum(case when LT_Segment = false then net_arr else null end)::int as non_lt_fo_narr,
median(case when is_web_portal_purchase then quantity else null end) as web_median,
median(case when is_web_portal_purchase = false then quantity else null end) as nonweb_median,
median(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_median,
median(case when LT_Segment = false then quantity else null end) as non_lt_fo_median,
avg(case when is_web_portal_purchase then quantity else null end) as web_avg,
avg(case when is_web_portal_purchase = false then quantity else null end) as nonweb_avg,
avg(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_avg,
avg(case when LT_Segment = false then quantity else null end) as non_lt_fo_avg,
sum(case when is_web_portal_purchase then quantity else null end) as web_total_quantity,
sum(case when is_web_portal_purchase = false then quantity else null end) as nonweb_total_quantity,
sum(case when LT_Segment = 'FO' then quantity else null end) as lt_fo_total_quantity,
sum(case when LT_Segment = false then quantity else null end) as non_lt_fo_total_quantity,
count(DISTINCT case when is_web_portal_purchase then dim_crm_opportunity_id else null end) as web_fo_count,
count(DISTINCT case when is_web_portal_purchase = false then dim_crm_opportunity_id else null end) as nonweb_fo_count,
count(DISTINCT case when LT_Segment = 'FO' then dim_crm_opportunity_id else null end) as lt_fo_fo_count,
count(DISTINCT case when LT_Segment = false then dim_crm_opportunity_id else null end) as non_lt_fo_count
from
subs
where list_price <= 348
  and actual_price_point in ('Matterhorn Temp FO Discount','Matterhorn')
-- and close_date < '2023-05-01'
-- and quantity < 100
group by 1,2
order by 1
  
),

combined as (
select 
monthly_inc_summary.*,
MEDIAN_quantity_comp,
avg_quantity_comp,
Total_quantity_comp,
mode_quantity_comp,
fo_count_comp,
total_narr_comp,
web_median_comp,
nonweb_median_comp,
lt_fo_median_comp,
non_lt_fo_median_comp,
web_avg_comp,
nonweb_avg_comp,
lt_fo_avg_comp,
non_lt_fo_avg_comp,
web_total_quantity_comp,
nonweb_total_quantity_comp,
lt_fo_total_quantity_comp,
non_lt_fo_total_quantity_comp,
web_fo_count_comp,
nonweb_fo_count_comp,
lt_fo_fo_count_comp,
non_lt_fo_count_comp  
from monthly_inc_summary
  join pre_summary
  ),

calcs as 
(
  select *,
 case when actual_price_point = 'Matterhorn Temp FO Discount' then 285
 when actual_price_point = 'Matterhorn' then 348
 when actual_price_point = 'Free User Promo' then 104
 else 0 end as price_point_quantity_multiplier,
 case when actual_price_point = 'Matterhorn Temp FO Discount' then 57
 when actual_price_point = 'Matterhorn' then 120
 when actual_price_point = 'Free User Promo' then -244
 else 0 end as price_point_price_multiplier,
MEDIAN_quantity - MEDIAN_quantity_comp as median_quantity_impact_overall,
web_median - web_median_comp as median_quantity_impact_web,
nonweb_median - nonweb_median_comp as median_quantity_impact_nonweb,
lt_fo_median - lt_fo_median_comp as median_quantity_impact_lt_fo,
avg_quantity - avg_quantity_comp as avg_quantity_impact_overall,
web_avg - web_avg_comp as avg_quantity_impact_web,
nonweb_avg - nonweb_avg_comp as avg_quantity_impact_nonweb,
lt_fo_avg - lt_fo_avg_comp as avg_quantity_impact_lt_fo,
median_quantity_impact_overall * price_point_quantity_multiplier as median_quantity_impact_overall_ASP,
median_quantity_impact_web * price_point_quantity_multiplier as median_quantity_impact_web_ASP,
median_quantity_impact_nonweb * price_point_quantity_multiplier as median_quantity_impact_nonweb_ASP,
median_quantity_impact_lt_fo * price_point_quantity_multiplier as median_quantity_impact_lt_fo_ASP,
avg_quantity_impact_overall * price_point_quantity_multiplier as avg_quantity_impact_overall_ASP,
avg_quantity_impact_web * price_point_quantity_multiplier as avg_quantity_impact_web_ASP,
avg_quantity_impact_nonweb * price_point_quantity_multiplier as avg_quantity_impact_nonweb_ASP,
avg_quantity_impact_lt_fo * price_point_quantity_multiplier as avg_quantity_impact_lt_fo_ASP,
MEDIAN_quantity * price_point_price_multiplier as overall_price_impact_ASP,
web_avg * price_point_price_multiplier as web_price_impact_ASP,
nonweb_avg * price_point_price_multiplier as nonweb_price_impact_ASP,
lt_fo_avg * price_point_price_multiplier as lt_fo_price_impact_ASP,
Total_quantity * price_point_price_multiplier as overall_price_impact_narr,
web_Total_quantity * price_point_price_multiplier as web_price_impact_narr,
nonweb_Total_quantity * price_point_price_multiplier as nonweb_price_impact_narr,
lt_fo_Total_quantity * price_point_price_multiplier as lt_fo_price_impact_narr,
fo_count * median_quantity_impact_overall_ASP as overall_quantity_impact_narr,
web_fo_count * median_quantity_impact_web_ASP as web_quantity_impact_narr,
nonweb_fo_count * median_quantity_impact_nonweb_ASP as nonweb_quantity_impact_narr,
lt_fo_fo_count * median_quantity_impact_lt_fo_ASP as lt_fo_quantity_impact_narr
from combined
--where actual_price_point in ('Pre-Matterhorn','Matterhorn Temp FO Discount')
--qualify actual_price_point = 'Matterhorn Temp FO Discount'
--and LT_Segment <> 'Pooled'
)