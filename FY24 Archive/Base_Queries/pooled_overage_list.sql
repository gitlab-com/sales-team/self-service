/*
Base query from https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/monthly_subscription_arr_snapshot_LT.sql
Filters to just Pooled accounts with overages with QSR enabled
Summarize by QSR date to see when the QSR opportunities will close
Look for RENEWAL_FLAG to see if the account with overage has a renewal coming up that the overage should impact
Look for MATTERHORN_IMPACTED_RENEWAL_FLAG for subscriptions ending in the $24 renewal period
Check: https://gitlab.my.salesforce.com/00O8X000008gqxV
Not all accounts report monthly usage data, so the query output will be less than what is shown in SFDC (10-15%)
*/

with arr_data 
as 
(
SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
--     ,mart_arr.QUANTITY
        --  , monthly_mart.PING_CREATED_AT
        --  , monthly_mart.HOSTNAME
        --  , monthly_mart.DIM_NAMESPACE_ID
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
         , monthly_mart.subscription_start_date
         , monthly_mart.subscription_end_date
         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
        , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
    FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
 LEFT JOIN 
  (
    select
    DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date
    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
    and PING_CREATED_AT::date > '2022-02-01'
  ) monthly_mart
                       ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
    WHERE ARR_MONTH >= DATE_TRUNC('month',date('2022-02-01'))
   --   and lower(user_role_name) like 'pooled-smb-system%'
      and mart_arr.subscription_status = 'Active'
  and ARR_MONTH = snapshot_month
      and overage_count > 0
        and PRODUCT_TIER_NAME not like '%Storage%'
--  and qsr_enabled_flag
  ),

pooled_accts as
(
  [FY24 Account Snapshot with LT]
 WHERE LT_Segment = 'Pooled'
 and fiscal_year = 2024
  ),

all_overages as
(
  select
  arr.*,
  account_tier_calculated,
  LT_Segment,
  sum(overage_count * price) over(partition by arr.DIM_CRM_ACCOUNT_ID,ARR_MONTH) as total_overage,
case when
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH)=  3
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 6
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 9
then --date_trunc('month',CURRENT_DATE)::date
  dateadd('day',7,
  dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH), subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 4
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 7
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 10
then --dateadd('month',1,date_trunc('month',CURRENT_DATE))::date
   dateadd('day',7,
  dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) + 1, subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 5
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 8
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 11
then --dateadd('month',2,date_trunc('month',CURRENT_DATE))::date
   dateadd('day',7,
dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) + 2, subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 1
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 2
or date_trunc('month',CURRENT_DATE)=
SUBSCRIPTION_END_MONTH
then subscription_end_date::date
  else null 
end as qsr_date,
  DATE_TRUNC('month',qsr_date) as qsr_month,
case when qsr_date <= CURRENT_DATE then true else false end as oppty_should_exist
  from
  arr_data arr
  LEFT join pooled_accts p on arr.DIM_CRM_ACCOUNT_ID = p.DIM_CRM_ACCOUNT_ID
  and arr.snapshot_month = p.snapshot_date
  where (latest_overage_month = DATE_TRUNC('month',CURRENT_DATE) or 
        ( qsr_date >= DATE_TRUNC('month',CURRENT_DATE)
        and latest_overage_month = dateadd('month', -1, DATE_TRUNC('month',CURRENT_DATE)))
        )
  and snapshot_month = latest_overage_month
)

select
*
from all_overages
where
LT_Segment = 'Pooled'
and overage_count > 0
and qsr_enabled_flag



