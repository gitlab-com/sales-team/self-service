---------------------------------------------------
-- Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/fo_asp_price_points_LT/5523ced30be244e6b30907e3d368959c/edit
-- Snippet last updated 2023/05/12


---------------------------------------------------

with oppty_base as
(
  [FY24 Opportunity Baseline with LT]
and is_won
  and trx_type = 'First Order'
  and dim_date.fiscal_year >= 2023
  and product_tier <> 'Storage'
    and [mart_crm_opportunity.crm_user_sales_segment=user_segment]
  and [mart_crm_opportunity.crm_user_business_unit=user_business_unit]
  and [mart_crm_opportunity.crm_user_region=user_region]
  and [mart_crm_opportunity.crm_user_role_type=user_role_type]
),

charges as (
  select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  subscription_start_date >= '2022-02-01'
  and TYPE_OF_ARR_CHANGE = 'New'
  and quantity > 0
  ),

discounts as (
  select
  charge.dim_subscription_id as discount_subscription_id
--  arr / quantity as actual_price,
--  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
--  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  subscription_start_date >= '2022-02-01'
  and rate_plan_name = 'First Order Free User Premium Discount'
  ),

subs as
(
 select
  oppty_base.trx_type,
  charges.actual_price,
  charges.list_price,
  charges.quantity,
  charges.rate_plan_name,
  charges.RATE_PLAN_CHARGE_NAME,
  case when discounts.discount_subscription_id is not null then true else false end as free_discount_flag,
  oppty_base.is_web_portal_purchase,
  oppty_base.LT_Segment,
  oppty_base.fiscal_year,
  oppty_base.fiscal_quarter_name_fy,
  oppty_base.close_month,
  oppty_base.close_date,
  oppty_base.net_arr,
  oppty_base.product_tier,
  sub.*,
  case when (free_discount_flag or (close_date > '2023-04-04' and actual_price < 105)) then 'Free User Promo'
else
case when actual_price::INT <= 228 then 'Pre-Matterhorn'
 when actual_price::INT > 228 and actual_price::INT <= 288 then 'Matterhorn Temp FO Discount'
when actual_price::INT >= 289 and RATE_PLAN_CHARGE_NAME like any ('%Premium%') then 'Matterhorn'
else actual_price::string end end
as actual_price_point
  from
  common.dim_subscription sub
  inner join oppty_base on sub.dim_crm_opportunity_id = oppty_base.dim_crm_opportunity_id
  inner join charges on sub.dim_subscription_id = charges.dim_subscription_id
  left join discounts on sub.dim_subscription_id = discounts.discount_subscription_id
  where
  sub.subscription_version = 1
  and oppty_base.close_date >= '2023-02-01'
  and [LT_Segment=Low_Touch]
  and RATE_PLAN_CHARGE_NAME like any ('%Premium%')
  and net_arr > 0
)