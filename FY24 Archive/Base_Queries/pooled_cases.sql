---query pulls all cases with a record type Id confirmed as the pooled sales case record type ID = 0128X000001pPRkQAM
---most origin of cases is internal pooled sales or through pooled sales web form 

SELECT *
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" cases
WHERE cases.record_type_id in ('0128X000001pPRkQAM')
