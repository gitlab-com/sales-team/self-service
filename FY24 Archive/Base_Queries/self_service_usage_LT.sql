with
base_oppty_data as
(
[FY24 Opportunity Baseline with LT]
  
),

get_upgrade_opportunities as (
SELECT
   arr_month,
  type_of_arr_change,
  arr.product_category,
  previous_month_product_category,
  opp.is_web_portal_purchase,
  arr.dim_crm_account_id, 
  opp.dim_crm_opportunity_id,
  SUM(beg_arr)                      AS beg_arr,
  SUM(end_arr)                      AS end_arr,
  SUM(end_arr) - SUM(beg_arr)       AS delta_arr,
  SUM(seat_change_arr)              AS seat_change_arr,
  SUM(price_change_arr)             AS price_change_arr,
  SUM(tier_change_arr)              AS tier_change_arr,
  SUM(beg_quantity)                 AS beg_quantity,
  SUM(end_quantity)                 AS end_quantity,
  SUM(seat_change_quantity)         AS delta_seat_change,
  COUNT(*)                          AS nbr_customers_upgrading
FROM restricted_safe_legacy.mart_delta_arr_subscription_month arr
left join base_oppty_data opp
  on (arr.DIM_CRM_ACCOUNT_ID = opp.DIM_CRM_ACCOUNT_ID
  and arr.arr_month = date_trunc('month',opp.subscription_start_date)
  and opp.order_type = '3. Growth'
  and opp.is_won)
WHERE  type_of_arr_change = 'Expansion'
 -- and opp.CRM_OPP_OWNER_SALES_SEGMENT_GEO_REGION_AREA_STAMPED like '%SMB%'
--  and opp.crm_opp_owner_user_role_type_stamped = 'Pooled'
 -- and arr.dim_parent_crm_account_id != '0016100000SEZArAAP'
   AND (ARRAY_CONTAINS('Self-Managed - Starter'::VARIANT, previous_month_product_category)
          OR ARRAY_CONTAINS('SaaS - Bronze'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('SaaS - Premium'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('Self-Managed - Premium'::VARIANT, previous_month_product_category)
       )
   AND tier_change_arr > 0
GROUP BY 1,2,3,4,5,6,7
),

get_sub_info as (
  select
  sub.DIM_CRM_ACCOUNT_ID as sub_acct_id,
  sub.subscription_name,
  sub.subscription_name_slugify,
  sub.subscription_status,
  sub.CONTRACT_AUTO_RENEWAL,
  sub.TURN_ON_AUTO_RENEWAL,
  sub.term_end_date,
  sub.subscription_end_date,
  sub.dim_crm_opportunity_id,
  prod.product_delivery_type,
  prod.product_rate_plan_charge_name
  from
  common.dim_subscription sub
  inner join common.bdg_subscription_product_rate_plan prod
  on sub.dim_subscription_id = prod.dim_subscription_id
  where
  --sub.subscription_status = 'Active'
  --and 
  sub.DIM_CRM_ACCOUNT_ID is not null
 -- and sub.term_end_date < CURRENT_DATE
  and prod.product_rate_plan_charge_name not like '%CI%'
  and prod.product_rate_plan_charge_name not like '%Service%'
   and prod.product_rate_plan_charge_name not like '%Trueup%'
  and prod.product_delivery_type <> 'Others'
  ),

logic as
(
select
base.*,
upgrade.product_category as upgrade_product_category,
upgrade.previous_month_product_category as upgrade_prior_product,
case when upgrade.dim_crm_opportunity_id is not null then true else false end as upgrade_flag,
case when sub.dim_crm_opportunity_id is not null then true else false end as sub_flag,  
case when upgrade_flag then '4. Upgrade'
when trx_type = 'First Order' and is_won then '1. First Order'
when trx_type like any ('Growth%','Non-R%')  and net_arr > 0 then '2. Growth - Uplift'
when trx_type like any ('QSR%') then '3. QSR'  
  when trx_type like any ('Growth%','Non-R%','Renew%')  and net_arr < 0 then '7. Contraction'
when trx_type like 'Renew%' and net_arr >= 0 and TURN_ON_AUTO_RENEWAL = 'Yes' then '5. Renewal - AutoRenew On'
  when trx_type like 'Renew%'  and net_arr >= 0 and TURN_ON_AUTO_RENEWAL <> 'Yes' then '6. Renewal - AutoRenew Off'
  else 'Other' end as job_type
from
base_oppty_data base
left join get_upgrade_opportunities upgrade on base.dim_crm_opportunity_id = upgrade.dim_crm_opportunity_id
left join get_sub_info sub on base.dim_crm_opportunity_id = sub.dim_crm_opportunity_id
where
trx_type not in ('Churn','PS/CI/CD')
--and is_closed
and (is_won or (is_closed and job_type = '3. QSR'))
and close_date < CURRENT_DATE
and (product_tier <> 'Storage' or product_tier is null)
and job_type <> 'Other'
)  


select 
job_type,
count(distinct case 
      when job_type = '3. QSR' and is_web_portal_purchase and is_won then dim_crm_opportunity_id
      when job_type <> '3. QSR' and is_web_portal_purchase then dim_crm_opportunity_id else null end) as trx_self_service,
sum(case 
      when job_type = '3. QSR' and is_web_portal_purchase and is_won then net_arr
    when is_web_portal_purchase then net_arr else null end) as nARR_self_service,
trx_self_service / 
count(distinct dim_crm_opportunity_id) as trx_perc_self_service,
nARR_self_service / 
sum(net_arr) as narr_perc_self_service
from logic
where fiscal_year = 2024
and [LT_segment=Low_Touch]
group by 1
order by 1 asc