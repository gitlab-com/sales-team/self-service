with 
usage as
(
select 
dim_crm_account_id,
arr_month,
subscription_name,
sum(overage_count) as net_license_usage,
sum(underutilization_amount) as net_license_amount,
sum(license_user_count) as license_user_count,
sum(billable_user_count) as billable_user_count,
min(price) as price
from

(SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month 
  ,  monthly_mart.term_end_date
     --     , ping_created_at 
     --     ,max(ping_created_at) over(partition by monthly_mart.SUBSCRIPTION_NAME, monthly_mart.snapshot_month) as latest_ping
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
        , MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
         , mart_arr.DIM_SUBSCRIPTION_ID
--         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT

         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
        , (mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as underutilization_amount
   --     , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID) as latest_underutilization_month
    FROM 
 (
   select * from
 RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
   where   PRODUCT_TIER_NAME not like '%Storage%')
   mart_arr
  left join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID

             LEFT JOIN 
  (
    select
    snapshot_month,
    dim_crm_account_id,
   SUBSCRIPTION_NAME,
    term_end_date,
    max(BILLABLE_USER_COUNT) as billable_user_count,
    max(LICENSE_USER_COUNT) as license_user_count

    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
      -- and BILLABLE_USER_COUNT > 0
       and LICENSE_USER_COUNT > 0
 --   and subscription_status = 'Active'
group by 1,2,3,4
  
  ) monthly_mart
  ON mart_arr.dim_crm_account_id = monthly_mart.dim_crm_account_id and mart_arr.subscription_name = monthly_mart.subscription_name
    WHERE --ARR_MONTH >= DATE_TRUNC('month',date('2022-01-01'))
-- and 
arr_month = snapshot_month

    -- and LICENSE_USER_COUNT > BILLABLE_USER_COUNT
  
--  and qsr_enabled_flag
order by dim_subscription_id, arr_month desc)
-- where arr_month = dateadd('month',-1,date_trunc('month',term_end_date))
--where ping_created_at = latest_ping
group by 1,2,3
),

subscription_with_usage as (
select sub.*,
max(sub.subscription_version) over(partition by sub.subscription_name) as latest_subscription_version,
prior.turn_on_auto_renewal as prior_auto_renewal,
usage.net_license_usage,
usage.net_license_amount,
usage.license_user_count,
usage.billable_user_count,
usage.price,
open_usage.net_license_usage as net_license_usage_current,
open_usage.net_license_amount as net_license_amount_current,
open_usage.license_user_count as license_user_count_current,
open_usage.billable_user_count as billiable_user_count_current,
open_usage.price as price_current
--,
--case when
--usage.arr_month = date_trunc('month',dateadd('month',-1,sub.term_start_date)) and sub.term_start_date < current_date then 'Renewal'
--when usage.arr_month = date_trunc('month',sub.term_end_date) then 'Churn'
--when usage.arr_month = date_trunc('month',current_date) and sub.term_end_date > current_date then 'Open'
--else null end as usage_data_flag
from common.dim_subscription sub
left join common.dim_subscription prior
on sub.dim_subscription_id_previous = prior.dim_subscription_id
left join usage
on (
 (usage.arr_month = date_trunc('month',dateadd('month',-1,date_trunc('month',sub.term_end_date))) and sub.dim_crm_opportunity_id_closed_lost_renewal is not null)
or (usage.arr_month = date_trunc('month',dateadd('month',-1,date_trunc('month',sub.term_start_date))) and sub.dim_crm_opportunity_id_closed_lost_renewal is null)
)
and usage.subscription_name = sub.subscription_name
left join 
(select * from
( select *,
         max(arr_month) over(partition by SUBSCRIPTION_NAME) as latest_month
from
usage)
where arr_month = latest_month)
 open_usage
on open_usage.subscription_name = sub.subscription_name
and sub.term_end_date > current_date
),

renewal_actions as
(select dim_order_action.DIM_SUBSCRIPTION_ID,
  dim_subscription.subscription_name,
  dim_order_action.contract_effective_date,
    case when ORDER_DESCRIPTION <> 'AutoRenew by CustomersDot' then true else false end as manual_portal_renew_flag
from common.dim_order_action
    left join common.dim_order on dim_order_action.dim_order_id = dim_order.dim_order_id
    left join common.dim_subscription on dim_order_action.DIM_SUBSCRIPTION_ID = dim_subscription.DIM_SUBSCRIPTION_ID
where order_action_type = 'RenewSubscription'
 -- and ORDER_ACTION_CREATED_DATE > '2023-01-31'
 ),

oppty_base as (SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area in ('LOWTOUCHPOOL', 'EAST','WEST') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag
--     ,
-- -- Adding account-level summary fields here 

-- -- FO Fields:

-- fo_net_arr,
-- fo_close_date,
-- fo_fiscal_year,

-- --Churn Fields

-- Churn_net_arr,
-- Churn_close_date,
-- Churn_fiscal_year,

-- -- Acct Fields

-- current_customer_flag,  
-- account_tier,
-- account_tier_notes


FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id
--   left join 
-- /*
-- NOTES
-- - Intended to be used for comparing account states over time, for example Tier or CARR
-- - Limited to just FY23-24 as we aren't looking at account state prior to that period
-- - Layers FO information where it exists, not all accounts have FO due to prospects, parent accounts, merged accounts, etc.
-- - MUST filter by snapshot date when used otherwise there will be duplicates
-- - MOST USE CASES will filter by CARR > 0 to look at just customer accounts
-- -3/21/23: added additional query/logic to add calculated account tier to pooled accounts in FY24 using the FY24 thresholds and logic (query will return the original results of all account snapshots from 2023 and forward but calculated account tier will be added to pooled FY24 accounts only)
-- - 3/22/23: Cleaned up calc subquery to improve performance and reduce duplicate field issues. Added logic to determine if the account is currently a Pooled account so that we can do historical reporting on the current Pooled account set.
-- - 3/23: Added logic to leave Tier 1 and 1.5 alone in the ACCOUNT_TIER_CALCULATED field
-- - 4/7: Removed Oppty snippet references and changed to just pull from Oppty Mart
-- - 7/19: Added smb_acct_expand_flag to track the Pool expansion accounts for FY23H2. Calc tier won't run after end of H1.
-- - Source: https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/pooled_acct_snapshot.sql
-- - SNIPPET UPDATED: 07/19/2023
-- - Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/FY24-Account-Snapshot-with-LT/554966ce5a1f4fb4bf78a3ceec5e0207/edit
-- */




-- (SELECT acct.*, 
-- dim_date.fiscal_year,
-- dim_date.fiscal_quarter_name_fy,
-- dim_date.last_day_of_month,
-- fo.fiscal_year as fo_fiscal_year,
-- fo.close_date as fo_close_date,
-- fo.net_arr as fo_net_arr,
-- churn.fiscal_year as churn_fiscal_year, 
-- churn.close_date as churn_close_date, 
-- churn.net_arr as churn_net_arr,
-- CASE
--     when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
--     when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
-- case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
-- case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
-- PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
-- crm_account_industry as final_industry,
-- TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
-- FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
-- LEFT JOIN common.dim_date 
--   ON acct.snapshot_date = dim_date.date_actual
-- left join
--   (            
--    select    
--     distinct
--    dim_crm_account_id,
--     CARR_THIS_ACCOUNT as curr_carr
--     from
--               "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
--    where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
--    )        curr_acct
--   on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
-- left join 
--   (
--  select
--     distinct
--     DIM_parent_CRM_ACCOUNT_ID,
--     last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
--     last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
--     last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
--     from
--     restricted_safe_common_mart_sales.mart_crm_opportunity
--     LEFT JOIN common.dim_date
--   ON mart_crm_opportunity.close_date = dim_date.date_actual
--     where
--     is_won
--     and order_type = '1. New - First Order'
--     ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
-- left join 
--   (
--  select
--     distinct
--     DIM_parent_CRM_ACCOUNT_ID,
--     last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
--     last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
--     last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
--     from
--     restricted_safe_common_mart_sales.mart_crm_opportunity
--     LEFT JOIN common.dim_date
--   ON mart_crm_opportunity.close_date = dim_date.date_actual
--     where
--     is_closed
--     and order_type like any ('%5%','%6%')
--        and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')
--     ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
-- --WHERE dim_date.fiscal_year >= 2024
--   ) acct_snap 
--   on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
-- and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 1 = acct_snap.snapshot_date) 
--  or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
--    or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
-- )

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND 
stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)
),

won_renewals as (
select
'Won Renewals' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
renewal_actions.manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 then 'Flat'
else null end as usage_flag,
case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
else 'Sales-Assisted Renew' end as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id = oppty_base.dim_crm_opportunity_id
inner join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
),

churns as
(
select
'Churns' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
null as manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 then 'Flat'
else null end as usage_flag,
-- case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
-- when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
-- else 'Sales-Assisted Renew' end 
null as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id_closed_lost_renewal = oppty_base.dim_crm_opportunity_id
--left join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
where
is_latest_version_flag
and oppty_base.trx_type is not null
and oppty_base.product_category like any ('%Bronze%','%Silver%','%Gold%','%Starter%','%Premium%','%Ultimate%')
and oppty_base.close_date < current_date
and oppty_base.is_closed
),

open_renewals as (
select
'Open Renewals' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
null as manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 then 'Flat'
else null end as usage_flag,
-- case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
-- when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
-- else 'Sales-Assisted Renew' end 
null as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id_current_open_renewal = oppty_base.dim_crm_opportunity_id
--left join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
where
is_latest_version_flag
and oppty_base.trx_type is not null
and oppty_base.close_date > current_date
and oppty_base.is_closed = False
)


select * from open_renewals
union all
select * from churns
union all
select * from won_renewals
