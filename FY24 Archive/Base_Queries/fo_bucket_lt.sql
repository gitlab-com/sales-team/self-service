/*
Pulls CW FO counts by Fiscal Quarter for FY24
Separates all SaaS FO into buckets:
Buy Now - purchased within 1 day of namespace creation
Trial - purchased from Trial namespace from 2-40 days after trial start
Free - purchased from Free namespace at least 2 days after namespace creation
Storage
Self-Managed is included as a bucket for totaling purposes
LT_Segment field is either “FO” or “false” to specify FO AEs
*/


WITH namespaces AS 
(SELECT DISTINCT
  namespace.dim_namespace_id as namespace_id,
  namespace.creator_id,
  created_at as namespace_created_at,
  CASE is_setup_for_company
    WHEN TRUE THEN 'True'
    WHEN FALSE THEN 'False'
    ELSE 'None'
  END AS company_setup_filter,
  namespace.visibility_level AS namespace_visibility_level -- bringing in the visibility level for namespaces
FROM common.dim_namespace namespace 
WHERE NAMESPACE_IS_ULTIMATE_PARENT -- this ensures the subgroup = group namespace 
AND namespace_is_internal = FALSE -- this blocks internal namespaces
),
  
  accounts_with_first_order_opps AS (

 [FY24 Opportunity Baseline with LT]
    and dim_date.fiscal_year = 2024 ---changing this to "and" so it runs in sisense
    and trx_type = 'First Order' ----adding first order filter to only pull opptys with a trx_type of FO
    and is_won
  ), 
  

paid_namespace_data as (
select 
    distinct
    dim_namespace_id as paid_namespace_id,
    dim_crm_account_id,
    first_value(dim_subscription_id) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_id,
    first_value(product_tier_name_subscription) over(partition by dim_namespace_id order by subscription_start_date asc) as first_namespace_product,
    first_value(product_tier_name_order) over(partition by dim_namespace_id order by subscription_start_date asc) as first_order_product,
    first_value(subscription_start_date) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_start_date

from 
(select * from
prod.common.bdg_namespace_order_subscription
where
dim_namespace_id = ultimate_parent_namespace_id
and product_tier_name_namespace not like '%Trial%'
and product_tier_name_namespace not like '%Free%'
and dim_crm_account_id is not null)
),

conversions as (
select
paid_namespace_id,
opps.dim_crm_account_id,
first_subscription_id,
  first_namespace_product,
  first_order_product,
  first_subscription_start_date,
dim_crm_opportunity_id,
  cast(close_date as date) as close_date,
  close_month,
  is_won,
  is_new_logo_first_order,
  is_web_portal_purchase,
crm_user_sales_segment as sales_segment,
  LT_Segment,
  product_category,
  product_tier,
  delivery,
  opps.fiscal_year,
  opps.fiscal_quarter_name_fy as fiscal_quarter,
case when opps.dim_crm_account_id is null then 'No FO Match' else 'Has FO' end as fo_flag,
case when date_trunc('week',opps.close_date) = date_trunc('week',paid_namespace_data.first_subscription_start_date) then 'Fuzzy Close Date Match'
else 'No Close Date match' end as close_date_match_flag,
case when (date_part('month',opps.close_date) = date_part('month',paid_namespace_data.first_subscription_start_date)
          and date_part('year',opps.close_date) < date_part('year',paid_namespace_data.first_subscription_start_date)) then 'Prior non-SaaS likely' else 'no prior sub likely' end as prior_sub_flag
from accounts_with_first_order_opps opps
left join paid_namespace_data
on paid_namespace_data.dim_crm_account_id = opps.dim_crm_account_id
order by paid_namespace_id asc),


trials AS (
  
  SELECT
    gl_namespace_id,
    MIN(start_date) AS first_trial_start_date
  FROM legacy.customers_db_trial_histories
  GROUP BY 1
  
), combined AS (

SELECT
  namespaces.*,
  conversions.*,
  first_trial_start_date,
  case when gl_namespace_id is not null then true else false end as trial_flag,
  DATEDIFF(day,namespaces.namespace_created_at,first_subscription_start_date) AS days_from_creation_to_subscription,
  case when days_from_creation_to_subscription = 0 then true else false end as paid_from_free_within_day,
  DATEDIFF(day,namespaces.namespace_created_at,first_trial_start_date) AS days_from_creation_to_trial,
  case when days_from_creation_to_trial  = 0 then true else false end as trial_from_free_within_day,
  DATEDIFF(day,first_trial_start_date,first_subscription_start_date) AS days_from_trial_to_paid,
  case when days_from_trial_to_paid  = 0 then true else false end as paid_from_trial_within_day,
  case when days_from_creation_to_subscription < 0 then true else false end as paid_before_create
  --trial_namespace_id CANNOT TRUST THIS
FROM conversions
Left JOIN namespaces ON conversions.paid_namespace_id = namespaces.namespace_id
LEFT JOIN trials ON namespaces.namespace_id = trials.gl_namespace_id
-- Limit to only conversions after the created date (not created and paid at same time) and trials after creation
WHERE (gl_namespace_id is null or first_trial_start_date >= date_trunc('day',namespace_created_at))
 -- and (first_subscription_start_date >= date_trunc('day',namespace_created_at) or first_subscription_start_date is null)

),

full_data as (
SELECT
  --date_trunc('week',namespace_created_at) as created_date,
  --date_trunc('week',first_subscription_start_date) as close_date,
  namespace_id,
  cast(namespace_created_at as date) as namespace_created_at,
  first_subscription_start_date,
  dim_crm_account_id,
  dim_crm_opportunity_id,
  fo_flag,
  close_date_match_flag,
  prior_sub_flag,
  is_web_portal_purchase,
  close_date,
close_month,
  sales_segment,
  first_namespace_product,
  product_category,
  product_tier,
  fiscal_year,
  fiscal_quarter,
    delivery,
  LT_Segment,
  cast(first_trial_start_date as date) as first_trial_start_date,
  case when first_trial_start_date is not null then true else false end as trial_flag,
  paid_from_free_within_day,
  trial_from_free_within_day,
  paid_before_create,
  case when close_date is not null then 'Paid' else 'Free' end as is_paid_flag,
  case when days_from_creation_to_subscription <= 30 then TRUE else false end as convert_within_month_flag,
  days_from_creation_to_subscription
--  median(days_from_creation_to_subscription) as median_create_to_paid,
 -- count(distinct namespace_id) as namespace_count
FROM combined
  ),

all_metrics as (
SELECT
--date_trunc('week',namespace_created_at + 1)-1 as week,
 LT_Segment,
fiscal_year,
fiscal_quarter,  
 close_month,
is_web_portal_purchase,
'Buy Now' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and (close_date >= namespace_created_at and close_date <= namespace_created_at + 1)
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Trial Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > first_trial_start_date
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and trial_flag
and delivery like '%SaaS%'
and product_tier is not null
and datediff('day',first_trial_start_date,close_date)<= 40
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
and (trial_flag = false or datediff('day',first_trial_start_date,close_date)>40)
 union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
--is_paid_flag = 'Paid'
--and week > date_trunc('week',namespace_created_at + 1)-1
--is_web_portal_purchase and
delivery like '%SaaS%'
and (namespace_id is null or paid_before_create = true)
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'S-M' as bucket,
dim_crm_opportunity_id
from conversions
where --is_web_portal_purchase and 
  delivery like '%Self%'
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Storage' as bucket,
dim_crm_opportunity_id
from conversions
where --is_web_portal_purchase and
  delivery like '%Storage%'
union all
SELECT
--date_trunc('week',cast(close_date as date) + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter_name_fy as fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'All FO' as bucket,
dim_crm_opportunity_id
from accounts_with_first_order_opps
--where is_web_portal_purchase
)



select 
all_metrics.fiscal_year,
all_metrics.fiscal_quarter,  
all_metrics.close_month,
is_web_portal_purchase,
count(distinct case when bucket = 'All FO' then dim_crm_opportunity_id else null end) as all_fo,
count(distinct case when bucket = 'Buy Now' then dim_crm_opportunity_id else null end) as buy_now,
count(distinct case when bucket = 'S-M' then dim_crm_opportunity_id else null end) as s_m,
count(distinct case when bucket = 'Storage' then dim_crm_opportunity_id else null end) as storage_10gb,
count(distinct case when bucket = 'Trial Convert' then dim_crm_opportunity_id else null end) as trial_convert,
count(distinct case when bucket = 'Free Convert' then dim_crm_opportunity_id else null end) as free_convert
from all_metrics
where [LT_Segment=Low_Touch]
and close_month <= CURRENT_DATE
group by 1,2,3,4
order by 1,2,3 asc

 



