/*
- Pulls all Pooled Sales Case type 
- Categorizes cases by parsing the subject line
- Have to manually exclude spam cases using https://gitlab.my.salesforce.com/00O8X0000096DtA
- Trigger types last updated: 04/25/2023
*/

with 

narr_data as
(
  select
  *
  from
  (
    [FY24 Opportunity Baseline with LT]
  )
  where
(is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal'))
  and fiscal_year >= 2024
 ),

acct_data as
(
  select
  *
  from
  (
    [FY24 Account Snapshot with LT]
   )
  where
  snapshot_date >= '2023-02-01'
 ),

case_data as

(select 
case 
when subject like '%Renewal Due to Fail%' then 'Failed Auto Renewals'
when subject like '%RT: Failed QSR%' then 'Failed QSRs - 14 days overdue'
when subject like '%EoA%' then 'EoA Renewals'
when subject like '%Multi Year%' then 'Multi Year Renewals'
when subject like '%Upcoming PO Required%' then 'PO Purchases'
when subject like '%Non Auto Renewal%' then 'Auto Renewal Turned Off'
when subject like '%RT: Past Due Renewal%' then 'Overdue Renewals'
when subject like '%PRO: New 5* PTE Account%' then 'New 4/5* PTE Accounts'
 when subject like '%PRO: New 4/5* PTE Account%' then 'New 4/5* PTE Accounts'
when subject like '%Likely to Downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%likely to downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%Upgrade%' then 'Accounts likely to upgrade to Ultimate'
when subject like '%PRO: New Overage Detected%' then 'Accounts with Overages'
when subject like '%PRO: New overage detected%' then 'Accounts with Overages'
when subject like '%Contact Sales Request%' or subject like '%Contact Us Request%' then 'Contact Sales Request'
when subject like '%NPS/CSAT%' then 'NPS/ CSAT Survey Response'
when subject like '%PRO: New 1/2%' then 'New 1/2* PTC Accounts'
when subject like '%SDR%' then 'SDR Created'
when subject like '%Support Ticket%' then 'Support Ticket'
when subject like '%Test%' then 'Internal Test'
when subject like '%New Case: %' then 'New Case:'
 when subject like '%New case: %' then 'New Case:'
 when subject like '%Upcoming SM Renewal not on 14%' then 'SM Renewal not on GL 14'
 
 else
 case when subject like '%Internal transfer%' then 'Internal Transfer'
 else
 'Other' end end as trigger_type,
sfdc_case.*
 ,
 dim_crm_user.user_name,
 dim_crm_user.department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name,
 dim_crm_user.crm_user_role_type
from workspace_sales.sfdc_case
left join common.dim_crm_user on sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
where
sfdc_case.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and sfdc_case.created_date >= '2023-02-01'
and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
 and sfdc_case.CASE_NUMBER not in ('00431774','00431781','00431783','00432055','00434891','00434927','00446212','00446427','00449130','00449137','00449142','00449145','00449149','00449159','00449174','00449193','00462545')
),

case_summary as (
select
case_data.*,
acct_data.DIM_CRM_ACCOUNT_ID,
acct_data.dim_crm_user_id,
acct_data.crm_account_owner_id,
acct_data.crm_account_name,
acct_data.fiscal_year,
acct_data.fiscal_quarter_name_fy,
acct_data.last_day_of_month,
acct_data.fo_fiscal_year,
acct_data.fo_close_date,
acct_data.fo_net_arr,
acct_data.churn_fiscal_year, 
acct_data.churn_close_date, 
acct_data.churn_net_arr,
acct_data.current_customer_flag,
acct_data.current_pool_flag,
acct_data.account_tier as tier_before_case,
acct_data.account_tier_calculated as calculated_tier_before_case,
acct_data.account_tier_notes as tier_notes_before_case,
current_acct.account_tier as current_tier,
current_acct.account_tier_calculated as current_calculated_tier,
current_acct.account_tier_notes as current_tier_notes,
acct_data.carr_this_account as carr_before_case,
current_acct.carr_this_account as current_carr,
min(post_narr.close_date) over(partition by acct_data.DIM_CRM_ACCOUNT_ID) as next_close_date,
DATEDIFF('day',case_data.closed_date,next_close_date) as days_case_to_oppty_close,
first_value(trx_type) over(partition by post_narr.DIM_CRM_ACCOUNT_ID order by close_date asc) as next_trx_type,
case when next_trx_type like any ('%Churn%','%Contract%') then 'Churn or Contraction'
  when next_trx_type like '%Uplift%' then 'Growth'
  when not(next_trx_type like any ('%Churn%','%Contract%','%Uplift%')) then 'Other'
  else null end as next_trx_group,
  case when next_trx_type = 'Churn' or acct_data.churn_close_date >= case_data.closed_date then true else false end as acct_churned_after_case_flag
from
case_data
left join acct_data on (case_data.account_id = acct_data.DIM_CRM_ACCOUNT_ID
and case_data.created_date::date = acct_data.snapshot_date::date)
left join narr_data post_narr on post_narr.dim_crm_account_id = case_data.account_id
and post_narr.close_date >= case_data.closed_date
left join acct_data current_acct on (case_data.account_id = current_acct.DIM_CRM_ACCOUNT_ID
and current_acct.snapshot_date::date = current_date)
)