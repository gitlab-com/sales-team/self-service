with all_renewals as
(
 select dim_order_action.DIM_SUBSCRIPTION_ID,
    case when ORDER_DESCRIPTION <> 'AutoRenew by CustomersDot' then true else false end as manual_portal_renew_flag
from common.dim_order_action
    left join common.dim_order on dim_order_action.dim_order_id = dim_order.dim_order_id
where order_action_type = 'RenewSubscription'
  and ORDER_ACTION_CREATED_DATE > '2022-10-31'
  ),

full_subs as 
(
  select
  subs.*,
  all_renewals.manual_portal_renew_flag
  from
  common.dim_subscription subs
  inner join all_renewals on subs.DIM_SUBSCRIPTION_ID = all_renewals.DIM_SUBSCRIPTION_ID
),

oppty_base as
(
  [FY24 Opportunity Baseline with LT no filters]
),

add_oppty_data as
(
select
full_subs.*,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.LT_Segment,
oppty_base.fiscal_year,
oppty_base.fiscal_quarter_name_fy,
oppty_base.close_month,
oppty_base.close_date,
oppty_base.subscription_start_date as opp_sub_start_date,
oppty_base.net_arr,
oppty_base.product_tier
from full_subs
left join oppty_base on full_subs.dim_crm_opportunity_id = oppty_base.dim_crm_opportunity_id
  ),

grouping as (
select
trx_type,
is_web_portal_purchase,
manual_portal_renew_flag,
count(DISTINCT dim_crm_opportunity_id) as trx_count,
sum(net_arr) as net_arr
from add_oppty_data
where trx_type like 'Renew%'
--and is_web_portal_purchase
and fiscal_year = 2024
group by 1,2,3)

select
trx_type,
sum(trx_count) as total_renewals,
sum(net_arr) as total_net_arr,
sum(case when is_web_portal_purchase and manual_portal_renew_flag = false then trx_count else null end) / 
sum(trx_count) as trx_perc_autorenew,
sum(case when is_web_portal_purchase and manual_portal_renew_flag then trx_count else null end) / 
sum(trx_count) as trx_perc_manual_portal_renew,
1 - trx_perc_autorenew - trx_perc_manual_portal_renew as trx_perc_sales_assist,
sum(case when is_web_portal_purchase and manual_portal_renew_flag = false then net_arr else null end) / 
sum(net_arr) as narr_perc_autorenew,
sum(case when is_web_portal_purchase and manual_portal_renew_flag then net_arr else null end) / 
sum(net_arr) as narr_perc_manual_portal_renew,
1 - narr_perc_autorenew - narr_perc_manual_portal_renew as narr_perc_sales_assist
from grouping
group by 1

--select * from add_oppty_data where trx_type is null