/*
Gets NET, GROSS, and LOGO Retention rates
Use the LT_Segment field to filter to just “Pooled” for Pooled accounts
Uses snapshot from 1/31/2023 to get Pooled account set for FY23, to have a good comparison - this leads to a big drop in the retention rates shown in the chart
Compare to the SMB retention rates in the Churn and Contraction dashboard
Can use the IS_WEB_FO field to filter to just Web FO accounts
*/


WITH base AS (
  
WITH arr_type_of_change AS (

SELECT 
  
  retention_month,
  COALESCE(next_renewal_month, last_renewal_month) AS next_renewal_month,
  accounts.parent_crm_account_name,
  retention.dim_parent_crm_account_id,
  accounts.parent_crm_account_owner_team,
  CASE
    WHEN accounts.parent_crm_account_sales_segment IS NULL THEN 'SMB'
    ELSE accounts.parent_crm_account_sales_segment
  END                                   AS parent_crm_account_sales_segment,
  accounts.parent_crm_account_billing_country,
  accounts.parent_crm_account_industry,
  accounts.parent_crm_account_sales_territory,
  prior_year_product_category,
  net_retention_product_category,
  type_of_arr_change,
  prior_year_arr,
  net_retention_arr,
  gross_retention_arr,
  seat_change_arr,
  price_change_arr,
  tier_change_arr,
  net_retention_arr - prior_year_arr                                                                          AS delta_arr,
  LAG(delta_arr) OVER (PARTITION BY retention.dim_parent_crm_account_id ORDER BY retention_month)             AS previous_delta_arr,
  delta_arr - previous_delta_arr                                                                              AS delta_delta_arr,
--  delta_delta_arr / previous_delta_arr                                                                        AS delta_delta_arr_percent,
  RANK() OVER (PARTITION BY retention_month ORDER BY prior_year_arr DESC)                                     AS rank_of_parent,
  RATIO_TO_REPORT(prior_year_arr) OVER (PARTITION BY retention_month)                                         AS percentage_total_amount,
  SUM(prior_year_arr) OVER (PARTITION BY retention_month)                                                     AS total_prior_year_arr,
  COUNT(DISTINCT retention.dim_parent_crm_account_id) OVER (PARTITION BY retention_month)                     AS total_customers,
  LAG(type_of_arr_change) OVER (PARTITION BY retention.dim_parent_crm_account_id ORDER BY retention_month)    AS previous_type_of_arr_change,
  prior_year_parent_customer_count,
  net_retention_parent_customer_count,
  CASE 
    WHEN COALESCE(net_retention_arr,0) > 5000 THEN 'ARR > $5K'
    WHEN COALESCE(net_retention_arr,0) <= 5000 THEN 'ARR <= $5K'
  END                                                                                                         AS retention_arr_band,
  case when retention_month >= '2023-02-01' and accounts.CRM_ACCOUNT_OWNER like any ('%Pooled%','%Amanda Shim%','%Erica Wilson%','%Matthew Wyman%') then 'Pooled' 
  when retention_month >= '2022-02-01' and retention_month < '2023-02-01' and acct_snap.CRM_ACCOUNT_OWNER like '%Pooled%'
  then 'Pooled'
  else false end as lt_segment

FROM restricted_safe_common_mart_sales.mart_retention_parent_account retention
LEFT JOIN restricted_safe_common.dim_crm_account accounts
  ON retention.dim_parent_crm_account_id = accounts.dim_crm_account_id
left join restricted_safe_common.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct_snap
  on retention.dim_parent_crm_account_id = acct_snap.dim_crm_account_id
  and acct_snap.snapshot_date = '2023-01-31'
ORDER BY 1 DESC
  
)

SELECT 
  arr_type_of_change.*,
  CASE 
    WHEN rank_of_parent BETWEEN 1 AND 20 THEN 'Top 20'
    ELSE 'Below Top 20'
  END AS customer_rank
FROM arr_type_of_change
  
),
get_acct_fo_type as (
SELECT 
    DIM_PARENT_CRM_ACCOUNT_ID as DIM_CRM_ACCOUNT_ID_OG,
    IS_WEB_PORTAL_PURCHASE as is_web_fo
FROM "PROD"."RESTRICTED_SAFE_WORKSPACE_FINANCE"."RPT_CRM_OPPORTUNITY_CLOSED_PERIOD"
WHERE
    IS_CLOSED_WON = 'TRUE' AND
    ORDER_TYPE = '1. New - First Order' AND
    DATE_TRUNC('day',CLOSE_DATE)::date >= '2/1/2018'
GROUP BY
     1,2

)

SELECT 
  retention_month, 
--  case when get_acct_fo_type.is_web_fo then 'Web FO' else 'Non-Web FO' end as is_web_fo,
--lt_segment,
  SUM(prior_year_arr)                           AS prior_year_arr,
  SUM(net_retention_arr)                        AS net_retention_arr,
  SUM(gross_retention_arr)                      AS gross_retention_arr,
  SUM(net_retention_arr)/SUM(prior_year_arr)    AS net_retention,
  SUM(gross_retention_arr)/SUM(prior_year_arr)  AS gross_retention,
    SUM(prior_year_parent_customer_count)                                           AS prior_year_parent_customer_count,
  SUM(net_retention_parent_customer_count)                                        AS net_retention_parent_customer_count,
  SUM(net_retention_parent_customer_count)/SUM(prior_year_parent_customer_count)  AS net_logo_retention
FROM base
INNER JOIN get_acct_fo_type on base.DIM_PARENT_CRM_ACCOUNT_ID = get_acct_fo_type.DIM_CRM_ACCOUNT_ID_OG
WHERE retention_month >= '2/1/2022'
  AND [parent_crm_account_sales_segment=ultimate_parent_sales_segment]
  AND [parent_crm_account_owner_team=account_owner_team]
  AND [parent_crm_account_industry=industry]
  AND [parent_crm_account_sales_territory=tsp_territory]
  AND [get_acct_fo_type.is_web_fo=Is_Web_FO_Filter]
and lt_segment = 'Pooled'
GROUP BY 1--,2
ORDER BY 1 DESC


