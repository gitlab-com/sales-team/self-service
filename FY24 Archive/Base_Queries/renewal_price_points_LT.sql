with oppty_base as
(
  [FY24 Opportunity Baseline with LT]
and is_won
  and trx_type like 'Renew%'
  and dim_date.fiscal_year >= 2024
    and [mart_crm_opportunity.crm_user_sales_segment=user_segment]
  and [mart_crm_opportunity.crm_user_business_unit=user_business_unit]
  and [mart_crm_opportunity.crm_user_region=user_region]
  and [mart_crm_opportunity.crm_user_role_type=user_role_type]
--  and product_tier = 'Premium'
),

charges as (
  select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  where 
  --CHARGE_CREATED_DATE >= '2022-02-01'
  --and 
--  TYPE_OF_ARR_CHANGE in ('No Impact','Expansion')
--  and
--  SUBSCRIPTION_AMENDMENT_TYPE in ('Renewal','Composite')
-- 
--  and charge_type = 'Recurring'
--and 
  charge.effective_start_date >= CHARGE_CREATED_DATE
and charge.effective_end_date > charge.effective_start_date
   and quantity > 0
  ),

subs as
(
 select
  oppty_base.trx_type,
  charges.actual_price,
  charges.list_price,
  charges.quantity,
  charges.rate_plan_name,
  charges.RATE_PLAN_CHARGE_NAME,
  charges.SUBSCRIPTION_AMENDMENT_TYPE,
  charges.subscription_start_date as charge_sub_start_date,
  charges.subscription_end_date as charge_sub_end_date,
  charges.term_start_date as charge_term_start_date,
  charges.term_end_date as charge_term_end_date,
  charges.CHARGE_CREATED_DATE,
  oppty_base.is_web_portal_purchase,
  oppty_base.LT_Segment,
  oppty_base.fiscal_year,
  oppty_base.fiscal_quarter_name_fy,
  oppty_base.close_month,
  oppty_base.close_date,
  oppty_base.subscription_start_date as opp_sub_start_date,
  oppty_base.net_arr,
  oppty_base.product_tier,
  case when opp_sub_start_date = charge_term_start_date then true else false end as sub_term_flag,
  sub.*
  from
  common.dim_subscription sub
  inner join oppty_base on sub.dim_crm_opportunity_id = oppty_base.dim_crm_opportunity_id
  inner join charges on sub.dim_subscription_id = charges.dim_subscription_id
 -- and oppty_base.subscription_end_date = charges.subscription_end_date
  where
 [LT_Segment=Low_Touch]
  and sub_term_flag
 -- and RATE_PLAN_CHARGE_NAME like any ('%Premium%')
--  and net_arr > 0
)


select
fiscal_quarter_name_fy as fq,
trx_type,
-- is_web_portal_purchase,
case when actual_price <= 228 then 'Pre-Matterhorn'
when actual_price <= 348 and actual_price >= 289 then 'Matterhorn - Full Price'
when actual_price::int <= 288 and actual_price > 228 then 'Matterhorn - Discounted'
else actual_price::string end
as actual_price_point,
 count(DISTINCT dim_crm_opportunity_id)::int as renewal_count,
sum(net_arr)::int as total_narr
from
subs
where
product_tier = 'Premium'
and
actual_price <= 348
and close_date < CURRENT_DATE
--and close_date >= '2023-04-01'
group by 1,2,3--,4
having fiscal_quarter_name_fy like 'FY24%'
order by 1,2,3--,4