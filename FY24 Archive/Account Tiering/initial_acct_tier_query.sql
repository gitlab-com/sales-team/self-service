-----query to get thresholds and run initail tier setting for pooled accounts
------used in FY2024 for initial tier setting on Feb 28, 2023

  
with exclude as (
SELECT * FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
where crm_account_owner in ('Amanda Shim', 'Erica Wilson', 'Matthew Wyman', 'Pooled Sales User [ DO NOT CHATTER ]') and (account_tier in  ('Rank 1', 'Rank 1.5') and notcontains(account_tier_notes, 'Updated by Self Service Ops'))
  ),

pooled_accts_24 as 
(
select *, 
  PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
  TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT, 
  case when zoom_info_company_industry = 'Finance' then 'FinTech'
when zoom_info_company_industry = 'Media & Internet' then 'Internet Software & Services'
when zoom_info_company_industry = 'Software' then 'Internet Software & Services'
when zoom_info_company_industry = 'Business Services' then 'Internet Software & Services'
when zoom_info_company_industry = 'Telecommunications' then 'Internet Software & Services'
when zoom_info_company_industry = 'Hospitals & Physicians Clinics' then 'Healthcare'
else crm_account_industry end as final_industry, 
  case when carr_account_family > 0 then 'Customer' else 'Prospect or Former Customer' end as customer_flag
  from "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
  where crm_account_owner in ('Amanda Shim', 'Erica Wilson', 'Matthew Wyman', 'Pooled Sales User [ DO NOT CHATTER ]')
  and dim_crm_account_id not in (select dim_crm_account_id from exclude)
),

  
 tiering as (
select
*,
PERCENTILE_CONT ( 0.99 )   
    WITHIN GROUP ( ORDER BY TOTAL_FUNDING_AMOUNT asc )  
    OVER ()  as funding_tier_1,
PERCENTILE_CONT ( 0.99)   
    WITHIN GROUP ( ORDER BY carr_account_family asc )  
    OVER (  )  as carr_tier_1,
PERCENTILE_DISC ( 0.99 )   
    WITHIN GROUP ( ORDER BY LAM_Dev_count asc )  
    OVER (  )  as dev_count_tier_1 ,
PERCENTILE_CONT ( 0.97)   
    WITHIN GROUP ( ORDER BY TOTAL_FUNDING_AMOUNT asc )  
    OVER (  )  as funding_tier_2,
PERCENTILE_CONT (0.97)   
    WITHIN GROUP ( ORDER BY carr_account_family asc )  
    OVER (  )  as carr_tier_2,
PERCENTILE_DISC ( 0.97)   
    WITHIN GROUP ( ORDER BY LAM_Dev_count asc )  
    OVER (  )  as dev_count_tier_2,
  PERCENTILE_CONT ( 0.92)   
    WITHIN GROUP ( ORDER BY TOTAL_FUNDING_AMOUNT asc )  
    OVER (  )  as funding_tier_3,
PERCENTILE_CONT (0.92)   
    WITHIN GROUP ( ORDER BY carr_account_family asc )  
    OVER ( )  as carr_tier_3,
PERCENTILE_DISC ( 0.92)   
    WITHIN GROUP ( ORDER BY lam_Dev_count asc )  
    OVER (  )  as dev_count_tier_3,
   case when carr_account_family >= carr_tier_1 then 1
   when carr_account_family >= carr_tier_2 then 2
   when carr_account_family >= carr_tier_3 then 3
   else 0 end as carr_tier,
   case when lam_dev_count >= dev_count_tier_1 then 1
   when lam_dev_count >= dev_count_tier_2 then 2
   when lam_dev_count >= dev_count_tier_3 then 3
   else 0 end as dev_count_tier,
   case when TOTAL_FUNDING_AMOUNT >= funding_tier_1 then 1
   when TOTAL_FUNDING_AMOUNT >= funding_tier_2 then 2
   when TOTAL_FUNDING_AMOUNT >= funding_tier_3 then 3
   else 0 end as funding_tier,
   case when final_industry in ('FinTech', 'Healthcare', 'Internet Software & Services') then 1 else 0 end as industry_tier
from pooled_accts_24
--where carr_this_account > 0
),

------query to get the tier threshold numbers 
-- initial_tiers as (
-- select
-- carr_tier_1,
-- carr_tier_2,
-- carr_tier_3,
-- funding_tier_1,
-- funding_tier_2,
-- funding_tier_3,
-- dev_count_tier_1,
-- dev_count_tier_2,
-- dev_count_tier_3
-- from tiering
-- limit 1
-- )
-- select * from initial_tiers



tier_setting as (
SELECT 
case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 'Tier 1'
else
    case when carr_account_family < 1700 and lam_dev_count <28 then 'Tier 3'
    else 'Tier 2' end
    end as account_tier,
    *
FROM(

SELECT 
DIM_CRM_ACCOUNT_ID, 
CARR_account_family, 
LAM_DEV_COUNT,
TOTAL_FUNDING_AMOUNT,
FINAL_INDUSTRY,
  customer_flag,
CASE 
WHEN (carr_account_family >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
CASE 
WHEN 
  (((carr_account_family >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2) 
    OR (carr_account_family >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2) 
    OR (carr_account_family >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
    OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2) 
    OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')) 
    OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
CASE 
WHEN 
  (((carr_account_family >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3) 
    OR (carr_account_family >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
    OR (carr_account_family >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')) 
    OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
   AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3     
FROM tiering

) a
)

SELECT * FROM tier_setting
ORDER BY account_tier


------query to get a count of the number of customers that fall into each tier to make sure porportions of tiers are correct (~10%, 15%, 75%)
//select
//customer_flag,
//account_tier,
//acct_count,
//acct_count / sum(acct_count) over(partition by customer_flag) as perc_of_cust_or_pros
//
//from
//(
//select
//customer_flag,
//account_tier,
//count(*) as acct_count
//from tier_setting
//group by 1,2
//order by 2 ,1
//)
//where customer_flag = 'Customer'
