-----using the oppty base query in order to segment into pooled and SMB/MM opptys 
DROP TABLE IF EXISTS all_opps;
CREATE TEMPORARY TABLE all_opps as (
SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area in ('LOWTOUCH', 'EAST','WEST') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.parent_crm_account_upa_country in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.parent_crm_account_upa_country in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag,
-- Adding account-level summary fields here 
-- FO Fields:
fo_net_arr,
fo_close_date,
fo_fiscal_year,
--Churn Fields
Churn_net_arr,
Churn_close_date,
Churn_fiscal_year,
-- Acct Fields
current_customer_flag,  
account_tier,
account_tier_notes,
account_tier_calculated
FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id
  left join 
(
/*
NOTES
- Intended to be used for comparing account states over time, for example Tier or CARR
- Limited to just FY23-24 as we aren't looking at account state prior to that period
- Layers FO information where it exists, not all accounts have FO due to prospects, parent accounts, merged accounts, etc.
- MUST filter by snapshot date when used otherwise there will be duplicates
- MOST USE CASES will filter by CARR > 0 to look at just customer accounts
-3/21/23: added additional query/logic to add calculated account tier to pooled accounts in FY24 using the FY24 thresholds and logic (query will return the original results of all account snapshots from 2023 and forward but calculated account tier will be added to pooled FY24 accounts only)
- 3/22/23: Cleaned up calc subquery to improve performance and reduce duplicate field issues. Added logic to determine if the account is currently a Pooled account so that we can do historical reporting on the current Pooled account set.
- 3/23: Added logic to leave Tier 1 and 1.5 alone in the ACCOUNT_TIER_CALCULATED field
- 4/7: Removed Oppty snippet references and changed to just pull from Oppty Mart
- Source: https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/pooled_acct_snapshot.sql
- SNIPPET UPDATED: 04/07/2023
- Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/FY24-Account-Snapshot-with-LT/554966ce5a1f4fb4bf78a3ceec5e0207/edit
*/
with acct_base as (SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
   where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
              ),
/*
How it should work:
- Pooled AE manual tier updates should persist (after 3/1)
- Tier 1/1.5s should not get downtiered
- If churned - accts keep getting tiered but Tier 1/1.5 no longer locked
*/
calc as (SELECT b.*, 
case when b.account_tier in ('Rank 1','Rank 1.5') and (b.churn_close_date is null or b.churn_close_date > b.SNAPSHOT_date) then 1
when b.account_tier_notes not like '%Ops%' and b.snapshot_date >= '2023-03-01' then 
        case when b.account_tier = 'Rank 2' then 2
         when b.account_tier = 'Rank 3' then 3
         else 1 end
else
calc.account_tier_calculated end as account_tier_calculated
FROM acct_base b
LEFT JOIN (SELECT
      dim_crm_account_id,
           snapshot_date as calc_snapshot_date,
        2964 as carr_tier_1,
        2400 as carr_tier_2,
        1916 as carr_tier_3,
        134985000 as funding_tier_1,
        44909650 as funding_tier_2,
        20624700 as funding_tier_3,
        103 as dev_count_tier_1,
        44 as dev_count_tier_2,
        29 as dev_count_tier_3,
        CASE
        WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
        AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 ,
        case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end as account_tier_calculated 
     from acct_base
      where --lt_segment = 'Pooled'
     -- and
           fiscal_year = 2024) calc
      on calc.dim_crm_account_id = b.dim_crm_account_id
      and calc.calc_snapshot_date = b.snapshot_date) 
 SELECT calc.*, 
 first_value(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as initial_tier,
LAST_VALUE(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as final_tier,
case when initial_tier = 1 and final_tier > 1 then 1 else 0 end as downtier_flag,
case when initial_tier > 1 and final_tier = 1 then 1 else 0 end as uptier_flag  
 FROM calc
  ) acct_snap 
  on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 1 = acct_snap.snapshot_date) 
 or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
   or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
 )
WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false);



----creating a table with all LT renewal opps since October 2022 that have auto-renewal turned on
-----changing filter to get SMB/MM opps within the same time frame for larger scope analysis
----join this to subscription table through the opportunity_id (closed won) OR through the opportunity_close_lost_renewal_opp (closed lost) to get the auto-renewal on filter
----using sales_type to ensure we are just looking at renewal opptys
-----timeframe is 10-01-2022 through current date
-----count of opptys as well as associated net_arr from opptys
SELECT 
close_month, 
COUNT(dim_crm_opportunity_id) as total_opportunties, 
SUM(CC_failure) as total_cc_failures, 
total_cc_failures/total_opportunties as percent_cc_fail, 
SUM(net_arr) as total_narr, 
SUM(cc_failure_narr) as total_cc_failure_narr, 
total_cc_failure_narr/total_narr as percent_cc_failure_narr
FROM(
SELECT o.*, 
z.notes, 
CASE WHEN z.notes like '%card%' then 1 else 0 end as cc_failure,
CASE WHEN z.notes like '%card%' then o.net_arr else 0 end as cc_failure_narr
FROM all_opps o
LEFT JOIN common.dim_subscription d
ON  o.dim_crm_opportunity_id = d.DIM_CRM_OPPORTUNITY_ID_CLOSED_LOST_RENEWAL ----joining on what should be closed lost opportunity
LEFT JOIN "PROD"."LEGACY"."ZUORA_SUBSCRIPTION" z
ON d.dim_subscription_id = z.subscription_id
WHERE 
--lt_segment in ('Pooled')
CRM_USER_SALES_SEGMENT in ('SMB', 'MID-MARKET')
AND close_date <= CURRENT_DATE
AND close_date >= '2022-10-01'
AND sales_type = 'Renewal'
AND d.TURN_ON_AUTO_RENEWAL = 'Yes'
  )
  GROUP BY 1
  ORDER BY 1