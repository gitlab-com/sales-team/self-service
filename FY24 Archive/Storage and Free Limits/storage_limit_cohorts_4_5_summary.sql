with private_free_namespaces as (
  select
    dim_namespace.created_at::date as created_at, 
    dim_namespace.creator_id, 
    dim_namespace.dim_namespace_id as namespace_id,
    dim_namespace.is_setup_for_company,
    dim_namespace.GITLAB_PLAN_TITLE,
    dim_namespace.namespace_name,
    dim_namespace.namespace_type,
    dim_namespace.visibility_level
  from prod.common.dim_namespace
  where dim_namespace.NAMESPACE_IS_ULTIMATE_PARENT = TRUE
    and dim_namespace.namespace_type in ( 'Group','User')
    and dim_namespace.GITLAB_PLAN_TITLE like any('Free')
    and dim_namespace.namespace_creator_is_blocked = FALSE -- same as blocked.user_id IS NULLL
    and dim_namespace.namespace_is_internal = FALSE

), namespace_storage_usage as (
  select
    private_free_namespaces.namespace_id as ultimate_parent_namespace_id,
    snapshot_month,
    coalesce(fct_usage_storage.storage_gib,0) as total_size_gb
  from private_free_namespaces
  inner join common.fct_usage_storage
    on private_free_namespaces.namespace_id = fct_usage_storage.ULTIMATE_PARENT_NAMESPACE_ID 
    and fct_usage_storage.snapshot_month = date_trunc('month',current_date)
  where 1=1
    and total_size_gb > 5
), namespaces_with_subscriptions as (
    -- treated as seperate step to combined rtp onboarding and dim_subscription models 
    select
        coalesce(wk_rpt_namespace_onboarding.ultimate_parent_namespace_id::varchar, dim_subscription.namespace_id::varchar) as    namespace_id
    from
    common.dim_subscription
        full join (
                select ultimate_parent_namespace_id 
                from workspace_product.wk_rpt_namespace_onboarding
                where first_paid_subscription_start_date IS NOT NULL
            ) wk_rpt_namespace_onboarding
            on dim_subscription.namespace_id = wk_rpt_namespace_onboarding.ultimate_parent_namespace_id
), storage_namespaces_with_subscriptions as (
        select
            namespace_id
        from
        namespaces_with_subscriptions
        join namespace_storage_usage
            on namespace_storage_usage.ultimate_parent_namespace_id = namespaces_with_subscriptions.namespace_id
    group by 1
),

big_projects as
(
select
projects.namespace_id,
max(storage_size) as highest_storage
from
PROD.LEGACY.GITLAB_DOTCOM_PROJECT_STATISTICS projects
where projects.storage_size >= 536870912000
--and projects.storage_size <= 2147483648000
group by 1
order by highest_storage desc
),

namespace_list as
(
select
    private_free_namespaces.*,
    namespace_storage_usage.total_size_gb,
    case when big_projects.namespace_id is not null then true else false end as large_project_flag
from
namespace_storage_usage 
    join private_free_namespaces 
        on private_free_namespaces.namespace_id = namespace_storage_usage.ultimate_parent_namespace_id
    left join storage_namespaces_with_subscriptions
        on namespace_storage_usage.ultimate_parent_namespace_id = storage_namespaces_with_subscriptions.namespace_id
    left join big_projects on big_projects.namespace_id = namespace_storage_usage.ultimate_parent_namespace_id
where storage_namespaces_with_subscriptions.namespace_id is null
--and big_projects.namespace_id is null
),

namespace_owners as
(
select 
distinct
ns.*,memberships.user_id,																						users.email,
        users.is_email_opted_in,
        user_detail.country
        ,
        case when marketing.dim_marketing_contact_id is not null then true else false end as marketing_mart_flag,
        marketing.*
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
          inner  join legacy.gitlab_dotcom_members members --join to get timestamp -- xxxxx inner join reduces the row count from 15M to 9M, but we decided to do this for now, as the impact to group namespace is <2% 																									
            ON memberships.user_id = members.user_id																									
        and memberships.membership_source_id = members.source_id 																									
        inner join namespace_list ns--limit to just the top-level namespaces we care about										
            ON memberships.ultimate_parent_id = ns.namespace_id 	

        inner join PROD.LEGACY.GITLAB_DOTCOM_USERS_XF users
        on users.user_id = memberships.user_id
        inner join prod.common.dim_user user_detail on user_detail.user_id = memberships.user_id
        left join RAW.DRIVELOAD.MARKETING_DNC_LIST on lower(MARKETING_DNC_LIST.address) = lower(users.email)
         left join 
         (select
dim_marketing_contact_id,
sfdc_record_id,
dim_crm_account_id,
country as marketo_country,
marketo_lead_id,
is_marketo_opted_in,
has_marketo_unsubscribed,
is_sfdc_lead_contact,
sfdc_lead_contact,
is_sfdc_opted_out,
gitlab_dotcom_user_id
from 
PROD.COMMON_MART_MARKETING.MART_MARKETING_CONTACT_NO_PII
where gitlab_dotcom_user_id is not null) marketing on 
       memberships.user_id = marketing.gitlab_dotcom_user_id
             where memberships.is_billable = TRUE																	
             and memberships.access_level = 50
             and MARKETING_DNC_LIST.address is null

)

select cohort,marketing_mart_flag,
is_sfdc_lead_contact,
count(distinct namespace_id) as ns_count, count(distinct email) as email_count,
count(distinct case when ns_per_email_count > 1 then email else null end) as emails_multiple_ns_count
from
(
select *,
case when namespace_type = 'Group' and large_project_flag = false then 'Cohort 4A'
when namespace_type = 'Group' and large_project_flag = true then 'Cohort 4B'
when namespace_type = 'User' then 'Cohort 5'
else 'other' end as cohort,
count(distinct namespace_id) over(partition by email) as ns_per_email_count
from namespace_owners
)
group by 1,2,3



