with namespace_owners as (
    select
        gitlab_dotcom_users_source.email,
        gitlab_dotcom_users_source.user_id,
        disco_namespaces.namespace_id,
        -- don't worry about this feild - For the email, everyone in each group is the same but we're splitting per email lower
        disco_namespaces.namespace_type_detailed,
        is_email_opted_in
    from
    -- This is the output of https://gitlab.com/gitlab-com/business-analytics/-/snippets/2546593
    free_limit_list_1_06_12_2023 disco_namespaces
    
        -- get the memebership information for the namespace
        join PROD.legacy.gitlab_dotcom_members
            on disco_namespaces.namespace_id = gitlab_dotcom_members.source_id 
                -- 50 is an owner
                and gitlab_dotcom_members.access_level = 50 
                -- ensure its still a vaild link
                and gitlab_dotcom_members.is_currently_valid
        -- Going low in the edm to get the email address
        join PREP.gitlab_dotcom.gitlab_dotcom_users_source
            on gitlab_dotcom_members.user_id = gitlab_dotcom_users_source.user_id
        -- remove marketing Do not contact list
        left join RAW.DRIVELOAD.MARKETING_DNC_LIST
            on lower(gitlab_dotcom_users_source.email) = lower(MARKETING_DNC_LIST.address)
    where MARKETING_DNC_LIST.address is null
), final as ( 
    select
    email,
    is_email_opted_in,
    -- get a list of namespace IDs and comma seperate it
    listagg(namespace_id, ', ') as namespace_ids,
    --How many namespace is this email an owner of?
    count(namespace_id) > 1 as is_multi_level,
    left(namespace_ids, 100) as namespace_id_100
    from namespace_owners
    where 1=1
    group by 1,2
)
select
*
from
final
--I updated this where to get each list
where is_multi_level

