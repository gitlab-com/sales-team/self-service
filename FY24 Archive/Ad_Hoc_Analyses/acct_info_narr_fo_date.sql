-- Gets current account info and appends:
-- FY23 total nARR
-- FY24 total nARR
-- FO close date
-- Most recent product category

select
acct.*,
fy23.net_arr as fy23_net_arr,
fy24.net_arr as fy24_net_arr,
fo.close_date as fo_close_date,
case when prod.most_recent_product_category like any ('%Ultimate%','%Gold%')
then true else false end as ultimate_flag
from
restricted_safe_common_mart_sales.mart_crm_account acct
left join
(
select
dim_crm_account_id,
sum(net_arr) as net_arr
from
restricted_safe_common_mart_sales.mart_crm_opportunity
where
is_closed
and opportunity_category not like 'Decom%'
and close_date >= '2022-02-01'
and close_date < '2023-02-01'
group by 1
) fy23
on acct.dim_crm_account_id = fy23.dim_crm_account_id
left join
(
select
dim_crm_account_id,
sum(net_arr) as net_arr
from
restricted_safe_common_mart_sales.mart_crm_opportunity
where
is_closed
and opportunity_category not like 'Decom%'
and close_date >= '2023-02-01'
and close_date < current_date
group by 1
) fy24
on acct.dim_crm_account_id = fy24.dim_crm_account_id
left join
(
select
distinct
dim_crm_account_id,
close_date
from
restricted_safe_common_mart_sales.mart_crm_opportunity
where
is_closed
and opportunity_category not like 'Decom%'
and is_won
and order_type = '1. New - First Order'
) fo
on acct.dim_crm_account_id = fo.dim_crm_account_id
left join
(
select
distinct
dim_crm_account_id,
first_value(product_category) 
over(partition by dim_crm_account_id order by close_date desc) as most_recent_product_category,
first_value(product_details) 
over(partition by dim_crm_account_id order by close_date desc) as most_recent_product_details
from
restricted_safe_common_mart_sales.mart_crm_opportunity
where
is_won
and
order_type not like '7%'
and net_arr > 0
and product_category is not null
) prod on acct.dim_crm_account_id = prod.dim_crm_account_id
where carr_this_account > 0
