------90s days from renewal date

SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Urget Renewal & TRX Support' as TYPE,
CONCAT('Multiyear Renewal - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Multiyear Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'H/M/L' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
ON o.dim_crm_account_id = a.dim_crm_account_id
WHERE account_tier not in ('Rank 1', 'Rank 1.5')
and opportunity_term > 12
and (opportunity_name like '%2 year%' OR opportunity_name like '%3 year%')
and opportunity_owner like '%Pooled%'
and is_closed = False
and datediff('day', close_date, current_date) = 90 