SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Urgent Renewal & TRX Support' as TYPE,
CONCAT('Auto-Renewal Will Fail - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Failed Autorenewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'H/M/L' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
ON o.dim_crm_account_id = a.dim_crm_account_id
where account_tier not in ('Rank 1', 'Rank 1.5')
AND is_closed = False
AND opportunity_owner like '%Pooled%' 
//AND auto_renewal_status != ''
//AND auto_renewal_status not in ('on', 'off') -----autorenewal field not pulling into snowflake
and datediff('day', close_date, current_date) <= 90