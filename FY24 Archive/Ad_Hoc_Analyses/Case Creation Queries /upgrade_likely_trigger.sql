---------likely to upgrade from pte score table 
----------this query grabs likely to upgrade from the last 30 days -- we can update the cadence on that (same as PTE score 4/5)
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, --hardcoded this case type to align with other internal created cases (this can be changed/edited in the code)
'Expansion' as TYPE,
'Likley to Upgrade' as CASE_SUBJECT, 
'Likely to Upgrade Flag = True' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, --hardcoding 'Pooled Sales Case' as record type
'H/M/L' as PRIORITY, 
'Xyz' as CASE_REASON_SUBTYPE, 
-----additional fields?
account_tier, 
pte.score_group, 
carr_this_account
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES" pte
    ON a.dim_crm_account_id = pte.crm_account_id 
WHERE carr_this_account > 0 
AND account_tier in ('Rank 2', 'Rank 3')
AND pte.uptier_likely = 'True'
-- AND CRM_ACCOUNT_OWNER_AREA = 'LOWTOUCH'
-- AND USER_ROLE_TYPE = 'POOL'
AND owner_role = 'SystemUser_COMM_AMER_SMB_AMER_LOWTOUCH_POOL'
AND datediff('day', score_date, current_date) < 30 ----determine cadence for pulling this
