drop table if exists last_create_date;
create temporary table last_create_date as (
SELECT 
MAX(created_date) as last_case_creation_date
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
AND case_type != 'Inbound Request'
ORDER BY created_date desc);