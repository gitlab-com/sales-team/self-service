--------OVERDUE RENEWALS---------------
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Urgent Renewal & TRX Support' as TYPE,
CONCAT('Overdue Renewal - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Overdue Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'H/M/L' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
ON o.dim_crm_account_id = a.dim_crm_account_id
WHERE account_tier not in ('Rank 1', 'Rank 1.5')
AND close_date <= current_date-1 
AND is_closed = False
AND opportunity_owner like '%Pooled%' 
AND NOT(opportunity_name LIKE ANY ('%storage%', '%Storage%'))
AND NOT (opportunity_name LIKE ANY ('%Refund%', '%refund%', '%debook%', '%Debook%'))


---and close_date > '2023-06-08' ---- PLACEHOLDER: this is close_date(2) in SFDC using this to fill in for date filter 
-- likely can just make it eventually close date is past in the last week, or x timeframe
 
