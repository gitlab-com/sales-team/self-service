-------HIGH PTC-------------------------------------------------------------------------------------
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, --hardcoded this case type to align with other internal created cases (this can be changed/edited in the code)
'Churn & Contraction Mitigation' as TYPE,
'High PTC Score' as CASE_SUBJECT, ----need to specifiy this 
'PTC Score 1*' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID, --hardcoding 'Pooled Sales Case' as record type
'H/M/L' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES" ptc
ON ptc.crm_account_id = a.dim_crm_account_id
where carr_this_account >0 
and account_tier not in ('Rank 1', 'Rank 1.5')
and score_group = 1
//AND CRM_ACCOUNT_OWNER_AREA = 'LOWTOUCH'
//AND USER_ROLE_TYPE = 'POOL' 
AND owner_role = 'SystemUser_COMM_AMER_SMB_AMER_LOWTOUCH_POOL'
AND datediff('day', score_date, current_date) < 30