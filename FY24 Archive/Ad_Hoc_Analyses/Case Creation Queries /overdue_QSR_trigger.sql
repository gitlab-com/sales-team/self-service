SELECT
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Churn & Contraction Mitigation' as TYPE,
CONCAT('Overdue QSR - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Failed QSR' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'H/M/L' as PRIORITY
FROM(
SELECT o.*, 
CASE WHEN opportunity_name like '%QSR%' then true else false end as qsr_flag, 
datediff('day', created_date, current_date)+1 as age
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
ON  o.dim_crm_account_id = a.dim_crm_account_id
WHERE qsr_flag = true
AND opportunity_owner like '%Pooled%'
AND a.account_tier not in ('Rank 1', 'Rank 1.5')
and close_date > current_date ---need to change this depending on cadence of case creation
and datediff('day', created_date, current_date)+1 > 7
and is_closed = False)