SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case when ultimate.dim_crm_account_id is not null then true else false end as curr_ultimate_customer_flag,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag,
-- Adding account-level summary fields here 

-- FO Fields:

fo_net_arr,
fo_close_date,
fo_fiscal_year,

--Churn Fields

Churn_net_arr,
Churn_close_date,
Churn_fiscal_year,

-- Acct Fields

current_customer_flag,  
account_tier,
account_tier_notes,
pte_score,
pte_decile,
pte_score_group,
ptc_score,
ptc_decile,
ptc_score_group,
license_utilization,
health_number,
health_score_color,
gs_health_user_engagement,
gs_health_cd,
gs_health_devsecops,
gs_health_ci,
gs_health_scm,
current_employee_count,
current_sales_segment,
current_business_unit,
current_geo,
current_region,
curr_carr,
carr_this_account as carr_at_oppty,
current_active_subscriptions


FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

left join
(
select
distinct
dim_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_crm_account_id = 
mart_crm_opportunity.dim_crm_account_id

  left join 
(
/*
NOTES
- Intended to be used for comparing account states over time, for example Tier or CARR
- Limited to just FY23-24 as we aren't looking at account state prior to that period
- Layers FO information where it exists, not all accounts have FO due to prospects, parent accounts, merged accounts, etc.
- MUST filter by snapshot date when used otherwise there will be duplicates
- MOST USE CASES will filter by CARR > 0 to look at just customer accounts
-3/21/23: added additional query/logic to add calculated account tier to pooled accounts in FY24 using the FY24 thresholds and logic (query will return the original results of all account snapshots from 2023 and forward but calculated account tier will be added to pooled FY24 accounts only)
- 3/22/23: Cleaned up calc subquery to improve performance and reduce duplicate field issues. Added logic to determine if the account is currently a Pooled account so that we can do historical reporting on the current Pooled account set.
- 3/23: Added logic to leave Tier 1 and 1.5 alone in the ACCOUNT_TIER_CALCULATED field
- 4/7: Removed Oppty snippet references and changed to just pull from Oppty Mart
- 7/19: Added smb_acct_expand_flag to track the Pool expansion accounts for FY23H2. Calc tier won't run after end of H1.
- Source: https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/pooled_acct_snapshot.sql
- SNIPPET UPDATED: 07/19/2023
- Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/FY24-Account-Snapshot-with-LT/554966ce5a1f4fb4bf78a3ceec5e0207/edit
*/




SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
curr_acct.curr_carr,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,      
 curr_acct.crm_account_employee_count as current_employee_count,
    curr_acct.crm_account_owner_user_segment as current_sales_segment,
    curr_acct.parent_crm_account_business_unit as current_business_unit,
    curr_acct.parent_crm_account_geo as current_geo,
    curr_acct.parent_crm_account_region as current_region,   
curr_acct.count_active_subscriptions as current_active_subscriptions,
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr,
    crm_account_employee_count,
    crm_account_owner_user_segment,
    parent_crm_account_business_unit,
    parent_crm_account_geo,
    parent_crm_account_region,
    count_active_subscriptions
    from
              "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id

left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
       and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
  ) acct_snap 
  on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 1 = acct_snap.snapshot_date) 
 or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
   or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
 )

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)
