   select    
    distinct
   MART_CRM_ACCOUNT.dim_crm_account_id
    from
              "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
left join
(
select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_parent_crm_account_id = 
MART_CRM_ACCOUNT.dim_parent_crm_account_id

   where      
(PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE <= 100 or PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE is null)
and 
CARR_ACCOUNT_FAMILY > 0
and 
CARR_ACCOUNT_FAMILY <= 30000
--and parent_crm_account_business_unit in ('ENTG','COMM')
and PARENT_CRM_ACCOUNT_SALES_SEGMENT in ('SMB','Mid-Market','Large')
and ultimate.dim_parent_crm_account_id is null
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
