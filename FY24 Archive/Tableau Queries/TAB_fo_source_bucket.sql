/*
Pulls CW FO counts by Fiscal Quarter for FY24
Separates all SaaS FO into buckets:
Buy Now - purchased within 1 day of namespace creation
Trial - purchased from Trial namespace from 2-40 days after trial start
Free - purchased from Free namespace at least 2 days after namespace creation
Storage
Self-Managed is included as a bucket for totaling purposes
LT_Segment field is either “FO” or “false” to specify FO AEs
*/


WITH namespaces AS 
(SELECT DISTINCT
  namespace.dim_namespace_id as namespace_id,
  namespace.creator_id,
  created_at as namespace_created_at,
  CASE is_setup_for_company
    WHEN TRUE THEN 'True'
    WHEN FALSE THEN 'False'
    ELSE 'None'
  END AS company_setup_filter,
  namespace.visibility_level AS namespace_visibility_level -- bringing in the visibility level for namespaces
FROM common.dim_namespace namespace 
WHERE NAMESPACE_IS_ULTIMATE_PARENT -- this ensures the subgroup = group namespace 
AND namespace_is_internal = FALSE -- this blocks internal namespaces
),
  
  accounts_with_first_order_opps AS (



SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area in ('LOWTOUCH', 'EAST','WEST') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag,
-- Adding account-level summary fields here 

-- FO Fields:

fo_net_arr,
fo_close_date,
fo_fiscal_year,

--Churn Fields

Churn_net_arr,
Churn_close_date,
Churn_fiscal_year,

-- Acct Fields

current_customer_flag,  
account_tier,
account_tier_notes,
account_tier_calculated


FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id
  left join 
(
/*
NOTES
- Intended to be used for comparing account states over time, for example Tier or CARR
- Limited to just FY23-24 as we aren't looking at account state prior to that period
- Layers FO information where it exists, not all accounts have FO due to prospects, parent accounts, merged accounts, etc.
- MUST filter by snapshot date when used otherwise there will be duplicates
- MOST USE CASES will filter by CARR > 0 to look at just customer accounts
-3/21/23: added additional query/logic to add calculated account tier to pooled accounts in FY24 using the FY24 thresholds and logic (query will return the original results of all account snapshots from 2023 and forward but calculated account tier will be added to pooled FY24 accounts only)
- 3/22/23: Cleaned up calc subquery to improve performance and reduce duplicate field issues. Added logic to determine if the account is currently a Pooled account so that we can do historical reporting on the current Pooled account set.
- 3/23: Added logic to leave Tier 1 and 1.5 alone in the ACCOUNT_TIER_CALCULATED field
- 4/7: Removed Oppty snippet references and changed to just pull from Oppty Mart
- Source: https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/pooled_acct_snapshot.sql
- SNIPPET UPDATED: 04/07/2023
- Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/FY24-Account-Snapshot-with-LT/554966ce5a1f4fb4bf78a3ceec5e0207/edit
*/




with acct_base as (SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
   where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
      
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
              ),
/*
How it should work:
- Pooled AE manual tier updates should persist (after 3/1)
- Tier 1/1.5s should not get downtiered
- If churned - accts keep getting tiered but Tier 1/1.5 no longer locked
*/

calc as (SELECT b.*, 
case when b.account_tier in ('Rank 1','Rank 1.5') and (b.churn_close_date is null or b.churn_close_date > b.SNAPSHOT_date) then 1
when b.account_tier_notes not like '%Ops%' and b.snapshot_date >= '2023-03-01' then 
        case when b.account_tier = 'Rank 2' then 2
         when b.account_tier = 'Rank 3' then 3
         else 1 end
else
calc.account_tier_calculated end as account_tier_calculated
FROM acct_base b
LEFT JOIN (SELECT
      dim_crm_account_id,
           snapshot_date as calc_snapshot_date,
        2964 as carr_tier_1,
        2400 as carr_tier_2,
        1916 as carr_tier_3,
        134985000 as funding_tier_1,
        44909650 as funding_tier_2,
        20624700 as funding_tier_3,
        103 as dev_count_tier_1,
        44 as dev_count_tier_2,
        29 as dev_count_tier_3,
        CASE
        WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
        AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 ,
        case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end as account_tier_calculated 
     from acct_base
      where --lt_segment = 'Pooled'
     -- and
           fiscal_year = 2024) calc
      on calc.dim_crm_account_id = b.dim_crm_account_id
      and calc.calc_snapshot_date = b.snapshot_date) 
 

 SELECT calc.*, 
 first_value(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as initial_tier,
LAST_VALUE(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as final_tier,
case when initial_tier = 1 and final_tier > 1 then 1 else 0 end as downtier_flag,
case when initial_tier > 1 and final_tier = 1 then 1 else 0 end as uptier_flag  
 FROM calc
  ) acct_snap 
  on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 1 = acct_snap.snapshot_date) 
 or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
   or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
 )

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

    and dim_date.fiscal_year = 2024 ---changing this to "and" so it runs in sisense
    and trx_type = 'First Order' ----adding first order filter to only pull opptys with a trx_type of FO
    and is_won
 
    
  ), 
  

paid_namespace_data as (
select 
    distinct
    dim_namespace_id as paid_namespace_id,
    dim_crm_account_id,
    first_value(dim_subscription_id) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_id,
    first_value(product_tier_name_subscription) over(partition by dim_namespace_id order by subscription_start_date asc) as first_namespace_product,
    first_value(product_tier_name_order) over(partition by dim_namespace_id order by subscription_start_date asc) as first_order_product,
    first_value(subscription_start_date) over(partition by dim_namespace_id order by subscription_start_date asc) as first_subscription_start_date

from 
(select * from
prod.common.bdg_namespace_order_subscription
where
dim_namespace_id = ultimate_parent_namespace_id
and product_tier_name_namespace not like '%Trial%'
and product_tier_name_namespace not like '%Free%'
and dim_crm_account_id is not null)
),

conversions as (
select
paid_namespace_id,
opps.dim_crm_account_id,
first_subscription_id,
  first_namespace_product,
  first_order_product,
  first_subscription_start_date,
dim_crm_opportunity_id,
  cast(close_date as date) as close_date,
  close_month,
  is_won,
  is_new_logo_first_order,
  is_web_portal_purchase,
crm_user_sales_segment as sales_segment,
  LT_Segment,
  product_category,
  product_tier,
  delivery,
  opps.fiscal_year,
  opps.fiscal_quarter_name_fy as fiscal_quarter,
case when opps.dim_crm_account_id is null then 'No FO Match' else 'Has FO' end as fo_flag,
case when date_trunc('week',opps.close_date) = date_trunc('week',paid_namespace_data.first_subscription_start_date) then 'Fuzzy Close Date Match'
else 'No Close Date match' end as close_date_match_flag,
case when (date_part('month',opps.close_date) = date_part('month',paid_namespace_data.first_subscription_start_date)
          and date_part('year',opps.close_date) < date_part('year',paid_namespace_data.first_subscription_start_date)) then 'Prior non-SaaS likely' else 'no prior sub likely' end as prior_sub_flag
from accounts_with_first_order_opps opps
left join paid_namespace_data
on paid_namespace_data.dim_crm_account_id = opps.dim_crm_account_id
order by paid_namespace_id asc),


trials AS (
  
  SELECT
    gl_namespace_id,
    MIN(start_date) AS first_trial_start_date
  FROM legacy.customers_db_trial_histories
  GROUP BY 1
  
), combined AS (

SELECT
  namespaces.*,
  conversions.*,
  first_trial_start_date,
  case when gl_namespace_id is not null then true else false end as trial_flag,
  DATEDIFF(day,namespaces.namespace_created_at,first_subscription_start_date) AS days_from_creation_to_subscription,
  case when days_from_creation_to_subscription = 0 then true else false end as paid_from_free_within_day,
  DATEDIFF(day,namespaces.namespace_created_at,first_trial_start_date) AS days_from_creation_to_trial,
  case when days_from_creation_to_trial  = 0 then true else false end as trial_from_free_within_day,
  DATEDIFF(day,first_trial_start_date,first_subscription_start_date) AS days_from_trial_to_paid,
  case when days_from_trial_to_paid  = 0 then true else false end as paid_from_trial_within_day,
  case when days_from_creation_to_subscription < 0 then true else false end as paid_before_create
  --trial_namespace_id CANNOT TRUST THIS
FROM conversions
Left JOIN namespaces ON conversions.paid_namespace_id = namespaces.namespace_id
LEFT JOIN trials ON namespaces.namespace_id = trials.gl_namespace_id
-- Limit to only conversions after the created date (not created and paid at same time) and trials after creation
WHERE (gl_namespace_id is null or first_trial_start_date >= date_trunc('day',namespace_created_at))
 -- and (first_subscription_start_date >= date_trunc('day',namespace_created_at) or first_subscription_start_date is null)

),

full_data as (
SELECT
  --date_trunc('week',namespace_created_at) as created_date,
  --date_trunc('week',first_subscription_start_date) as close_date,
  namespace_id,
  cast(namespace_created_at as date) as namespace_created_at,
  first_subscription_start_date,
  dim_crm_account_id,
  dim_crm_opportunity_id,
  fo_flag,
  close_date_match_flag,
  prior_sub_flag,
  is_web_portal_purchase,
  close_date,
close_month,
  sales_segment,
  first_namespace_product,
  product_category,
  product_tier,
  fiscal_year,
  fiscal_quarter,
    delivery,
  LT_Segment,
  cast(first_trial_start_date as date) as first_trial_start_date,
  case when first_trial_start_date is not null then true else false end as trial_flag,
  paid_from_free_within_day,
  trial_from_free_within_day,
  paid_before_create,
  case when close_date is not null then 'Paid' else 'Free' end as is_paid_flag,
  case when days_from_creation_to_subscription <= 30 then TRUE else false end as convert_within_month_flag,
  days_from_creation_to_subscription
--  median(days_from_creation_to_subscription) as median_create_to_paid,
 -- count(distinct namespace_id) as namespace_count
FROM combined
  ),

all_metrics as (
SELECT
--date_trunc('week',namespace_created_at + 1)-1 as week,
 LT_Segment,
fiscal_year,
fiscal_quarter,  
 close_month,
is_web_portal_purchase,
'Buy Now' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and (close_date >= namespace_created_at and close_date <= namespace_created_at + 1)
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Trial Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > first_trial_start_date
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and trial_flag
and delivery like '%SaaS%'
and product_tier is not null
and datediff('day',first_trial_start_date,close_date)<= 40
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
is_paid_flag = 'Paid'
and close_date > namespace_created_at + 1
--and is_web_portal_purchase
and delivery like '%SaaS%'
and product_tier is not null
and (trial_flag = false or datediff('day',first_trial_start_date,close_date)>40)
 union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Free Convert' as bucket,
dim_crm_opportunity_id
from full_data
where 
--is_paid_flag = 'Paid'
--and week > date_trunc('week',namespace_created_at + 1)-1
--is_web_portal_purchase and
delivery like '%SaaS%'
and (namespace_id is null or paid_before_create = true)
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'S-M' as bucket,
dim_crm_opportunity_id
from conversions
where --is_web_portal_purchase and 
  delivery like '%Self%'
union all
SELECT
--date_trunc('week',close_date + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'Storage' as bucket,
dim_crm_opportunity_id
from conversions
where --is_web_portal_purchase and
  delivery like '%Storage%'
union all
SELECT
--date_trunc('week',cast(close_date as date) + 1)-1 as week,
   LT_Segment,
  fiscal_year,
  fiscal_quarter_name_fy as fiscal_quarter,  
 close_month,
  is_web_portal_purchase,
'All FO' as bucket,
dim_crm_opportunity_id
from accounts_with_first_order_opps
--where is_web_portal_purchase
)



select 
all_metrics.fiscal_year,
all_metrics.fiscal_quarter,  
all_metrics.close_month,
is_web_portal_purchase,
count(distinct case when bucket = 'All FO' then dim_crm_opportunity_id else null end) as all_fo,
count(distinct case when bucket = 'Buy Now' then dim_crm_opportunity_id else null end) as buy_now,
count(distinct case when bucket = 'S-M' then dim_crm_opportunity_id else null end) as s_m,
count(distinct case when bucket = 'Storage' then dim_crm_opportunity_id else null end) as storage_10gb,
count(distinct case when bucket = 'Trial Convert' then dim_crm_opportunity_id else null end) as trial_convert,
count(distinct case when bucket = 'Free Convert' then dim_crm_opportunity_id else null end) as free_convert
from all_metrics
where -- [LT_Segment=Low_Touch]
--and 
close_month <= CURRENT_DATE
group by 1,2,3,4
order by 1,2,3 asc
