SELECT acct.*, 
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
case when ultimate.dim_parent_crm_account_id is not null then true else false end as current_ultimate_customer_flag,
case when crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then true else false end as current_pool_flag,
case when carr_this_account > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT,
  case when current_date >= '2023-08-01' then acct_flag.Pooled_SMB_Expand_Flag else null end as Pooled_SMB_Expand_Flag,
case when current_date >= '2023-08-01' then acct_flag.account_tier_fy23h1 else null end as account_tier_fy23h1,
case when herbie.dim_crm_account_id is not null then true else false end as herbie_flag


FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" acct
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
     and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')  
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join
(
  -----snippet last updated 7/19/2023


select
--a.crm_account_owner_region,
--a.crm_account_owner,
a.dim_crm_account_id,
b.previous_account_owner__c,
b.previous_account_owner_email__c,
u.user_role_name as prior_owner_role,
case when prior_owner_role like any ('%_EXP%','%_KEY%') then 'SMB Expand'
when not(prior_owner_role like any ('%_EXP%','%_KEY%')) and b.FIRST_ORDER_DATE__C >=  '2023-08-01' then 'New via FO'
else 'Pooled' end as Pooled_SMB_Expand_Flag,
CASE 
    when Pooled_SMB_Expand_Flag = 'Pooled' and account_tier IS NULL then 'Rank 3'
    else account_tier end as account_tier_fy23h1
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT a
left join RAW.salesforce_V2_stitch.account b on a.dim_crm_account_id = b.account_id_18__c
left join 
( select distinct user_email, user_role_name
from
PROD.COMMON.DIM_CRM_USER 
where is_active
and user_role_name not like 'Executive%'
and user_role_name <> 'EMEA House Account'
)
u on u.user_email = b.previous_account_owner_email__c
where a.crm_account_owner like '%Pooled%'
--and prior_owner_role like any ('%_EXP%','%_KEY%')
and a.carr_this_account > 0
) acct_flag on acct.dim_crm_account_id = acct_flag.dim_crm_account_id
left join
(
select distinct dim_parent_crm_account_id
from
restricted_safe_common_mart_sales.mart_arr
where arr_month = date_trunc('month',current_date)
and arr > 0
and product_tier_name like '%Ultimate%'
--and parent_crm_account_sales_segment in ('SMB','Mid-Market')
) ultimate on ultimate.dim_parent_crm_account_id = acct.dim_parent_crm_account_id
left join
  (            
       select    
    distinct
   MART_CRM_ACCOUNT.dim_crm_account_id
    from
              "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
left join
(
select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_parent_crm_account_id = 
MART_CRM_ACCOUNT.dim_parent_crm_account_id

   where      
(PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE <= 100 or PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE is null)
and 
CARR_ACCOUNT_FAMILY > 0
and 
CARR_ACCOUNT_FAMILY <= 30000
--and parent_crm_account_business_unit in ('ENTG','COMM')
and PARENT_CRM_ACCOUNT_SALES_SEGMENT in ('SMB','Mid-Market','Large')
and ultimate.dim_parent_crm_account_id is null
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false

   )       herbie
on herbie.dim_crm_account_id = acct.dim_crm_account_id
