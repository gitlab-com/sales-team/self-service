/*
- Pulls all Pooled Sales Case type 
- Categorizes cases by parsing the subject line
- Have to manually exclude spam cases using https://gitlab.my.salesforce.com/00O8X0000096DtA
- Trigger types last updated: 11/14/2023
*/

with 

task_data as (

-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs

select * from

(select 
    task_id,
task_status,
task.dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_created_by_id,

--This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when (inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%') or (lower(task_subject) like '%call%' and task_subject not like '%Outreach%' and task_status = 'Completed' ) then true else false end as outbound_answered_flag,
task_date,
   row_number() over(partition by dim_crm_person_id order by task_date asc) as task_order,

      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name as case_user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
        user.crm_user_geo,
user.crm_user_region,
user.crm_user_area,
user.crm_user_business_unit,
      user.user_role_name--,
--  case when count(distinct case when outreach_clari_flag in ('Clari','Outreach') then task_id else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) > 1  and
--   count(distinct case when outreach_clari_flag in ('Clari','Outreach') then outreach_clari_flag else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) = 2
--   then true else false end as likely_dupe
  --,
 -- case when likely_dupe and outreach_clari_flag = 'Outreach' and account_or_opportunity_id is null then
 -- first_value(account_or_opportunity_id) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id order by outreach_clari_flag asc) else account_or_opportunity_id end as account_or_opportunity_id
      from
  prod.common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where --createdbyid like '0054M000003Tqub%' and
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2022-02-01'
and task_status = 'Completed')

where outreach_clari_flag = 'Outreach' or task_created_by_id = dim_crm_user_id

  ),

--Baseline opportunity query

narr_data as
(
  select
  *
  from
  (
  

SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area like any ('%LOWTOUCH%', '%EAST%','%WEST%') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag,
-- Adding account-level summary fields here 

-- FO Fields:

fo_net_arr,
fo_close_date,
fo_fiscal_year,

--Churn Fields

Churn_net_arr,
Churn_close_date,
Churn_fiscal_year,

-- Acct Fields

current_customer_flag,  
account_tier,
account_tier_notes


FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id
  left join 
(
/*
NOTES
- Intended to be used for comparing account states over time, for example Tier or CARR
- Limited to just FY23-24 as we aren't looking at account state prior to that period
- Layers FO information where it exists, not all accounts have FO due to prospects, parent accounts, merged accounts, etc.
- MUST filter by snapshot date when used otherwise there will be duplicates
- MOST USE CASES will filter by CARR > 0 to look at just customer accounts
-3/21/23: added additional query/logic to add calculated account tier to pooled accounts in FY24 using the FY24 thresholds and logic (query will return the original results of all account snapshots from 2023 and forward but calculated account tier will be added to pooled FY24 accounts only)
- 3/22/23: Cleaned up calc subquery to improve performance and reduce duplicate field issues. Added logic to determine if the account is currently a Pooled account so that we can do historical reporting on the current Pooled account set.
- 3/23: Added logic to leave Tier 1 and 1.5 alone in the ACCOUNT_TIER_CALCULATED field
- 4/7: Removed Oppty snippet references and changed to just pull from Oppty Mart
- Source: https://gitlab.com/gitlab-com/sales-team/self-service/-/blob/main/SSOT%20Queries/Base%20Queries%20and%20Code%20Snippets/pooled_acct_snapshot.sql
- SNIPPET UPDATED: 04/07/2023
- Snippet: https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/FY24-Account-Snapshot-with-LT/554966ce5a1f4fb4bf78a3ceec5e0207/edit
*/



SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
   where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
     and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')  
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
  ) acct_snap 
  on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 1 = acct_snap.snapshot_date) 
 or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
   or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
 )

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

  )
  where
(is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = false)
  and fiscal_year >= 2024
 ),

-- We limit to only Pooled accounts (including EMEA pool to limit query size)

acct_data as
(
  select
  *
  from
  (
 with

acct_list as
(
select distinct dim_crm_account_id
from
"PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT"
where (crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%', 
                                  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%','%Debroux%','%Nga Nguyen%','%Obakin%','%Raphael Werner%','%Bajura%')
  or (crm_account_owner = 'Rasheed Power' and snapshot_date < '2023-02-01')
)
 and snapshot_date >= '2022-02-01'
 and crm_account_type <> 'Prospect'
),

acct_base as 

(SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%','%Debroux%','%Nga Nguyen%','%Obakin%','%Raphael Werner%','%Bajura%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%','%Debroux%','%Nga Nguyen%','%Obakin%','%Raphael Werner%','%Bajura%') then 'Pooled' end as LT_Segment,      
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
 
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
INNER JOIN acct_list on acct.dim_crm_account_id = acct_list.dim_crm_account_id
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
WHERE snapshot_date >= '2022-02-01'
              )
              
 SELECT *
 FROM acct_base

 

   )
  where
  snapshot_date >= '2023-02-01'
 ),

/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

Early query iterations manually listed Spam cases to exclude given that the SFDC_CASE table is lacking the spam checkbox from SFDC.
However this is not scalable, instead we filter afterwards in Tableau based on case Status.
*/


case_data as

(select 
case 
when subject like '%Q3%' then 'Q3 AE Play'
when subject like '%Q4 Atlassian%' then 'Q4 AE Play'
when subject like '%Renewal Due to Fail%' or subject like '%Will Fail%' then 'Auto Renewal Will Fail'
when subject like '%Failed QSR%' or subject like '%Overdue QSR%' then 'Failed QSRs - 14 days overdue'
when subject like '%Overage%' and subject like '%QSR%' then 'Overage and QSR Off'
when subject like '%EoA%' or subject like '%EOA%' then 'EoA Renewals'
when subject like '%Multi Year%' then 'Multi Year Renewals'
when subject like '%PO%' then 'PO Purchases'
when subject like '%Non Auto Renewal%' then 'Auto Renewal Turned Off'
when subject like '%Auto Renew Recently Turned Off%' then 'Auto Renewal Turned Off'
when subject like '%Auto Renew recently%' then 'Auto Renewal Turned Off'
when subject like '%6sense%' then '6Sense Growth Signal'
when subject like '%Past Due Renewal%' or subject like '%Overdue Renewal%' then 'Overdue Renewals'
when subject like '%PTE%' then 'High PTE Account'
when subject like '%Likely to Downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%likely to downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%Upgrade%' then 'Accounts likely to upgrade to Ultimate'
when subject like '%PRO: New Overage Detected%' then 'Accounts with Overages'
when subject like '%PRO: New overage detected%' then 'Accounts with Overages'
when subject like '%Contact Sales Request%' or subject like '%Contact Us Request%' then 'Contact Sales Request'
when subject like '%NPS/CSAT%' then 'NPS/ CSAT Survey Response'
when subject like '%PTC%' then 'High PTC Account'
when subject like '%SDR%' then 'SDR Created'
when subject like '%PQL%' then 'Hand Raise PQL'
when subject like '%Support Ticket%' then 'Support Ticket'
when subject like '%Test%' then 'Internal Test'
when subject like '%New Case: %' then 'New Case:'
 when subject like '%New case: %' then 'New Case:'
 when subject like '%Upcoming SM Renewal not on 14%' then 'SM Renewal not on GL 14'
 when subject like '%Cancelled%' then 'Auto Renewal Turned Off'
 when subject like '%Under%' then 'Underutilization'
 when subject like '%High Adoption%' then 'High Adoption/Growth'
 when subject like '%AR Request%' then 'AR Request'
 else
 (case when lower(subject) like '%internal transfer%' then 'Internal Transfer'
 else
 'Other' end) end as trigger_type,
sfdc_case.*
 ,
 dim_crm_user.user_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.crm_user_role_type
from workspace_sales.sfdc_case
left join common.dim_crm_user on sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
where
sfdc_case.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and sfdc_case.created_date >= '2023-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
 and sfdc_case.CASE_NUMBER not in ('00431774','00431781','00431783','00432055','00434891','00434927','00446212','00446427','00449130','00449137','00449142','00449145','00449149','00449159','00449174','00449193','00462545')
),

/*
We join acct and opportunity data to the case data above. Only keeping accounts and opportunities where they are 
linked to Pooled cases.

The first acct_data join is to get the account info at the time the case was created, the second join gets current account
data. This can help identify if tiers or CARR changed after the case.

We associate opportunities based on the case and opportunity types. Generally we only want opportunities where 
the case is closed ahead of the opportunity close date. However for renewals, sometimes the close date is earlier (e.g. overdue renewal)
and so we allow for any opportunity closing within 95 days of the case creation date.
*/

case_summary as (
select
case_data.*,
--acct_data.DIM_CRM_ACCOUNT_ID as case_account_id,
--acct_data.dim_crm_user_id,
acct_data.crm_account_owner_id,
acct_data.crm_account_name,
acct_data.crm_account_owner_geo,
acct_data.crm_account_owner_region,
acct_data.crm_account_owner_area,
acct_data.fiscal_year,
acct_data.fiscal_quarter_name_fy,
acct_data.last_day_of_month,
acct_data.account_tier as tier_before_case,
acct_data.account_tier_notes as tier_notes_before_case,
current_acct.account_tier as current_tier,
current_acct.account_tier_notes as current_tier_notes,
acct_data.carr_this_account as carr_before_case,
current_acct.carr_this_account as current_carr,
post_narr.dim_crm_opportunity_id,
post_narr.close_date,
post_narr.trx_type,
post_narr.sales_type,
post_narr.opportunity_name,
post_narr.is_won,
post_narr.is_closed as oppty_is_closed,
post_narr.stage_name,
post_narr.sales_qualified_source_name,
post_narr.order_type,
post_narr.is_web_portal_purchase,
post_narr.net_arr,
post_narr.arr_basis
from
case_data
left join acct_data on (case_data.account_id = acct_data.DIM_CRM_ACCOUNT_ID
and case_data.created_date::date = acct_data.snapshot_date::date)
left join narr_data post_narr on post_narr.dim_crm_account_id = case_data.account_id
and (post_narr.close_date >= case_data.closed_date or (post_narr.sales_type = 'Renewal' and post_narr.is_closed 
and post_narr.close_date <= dateadd('day',95,case_data.created_date) and post_narr.close_date >= case_data.created_date))
left join acct_data current_acct on (case_data.account_id = current_acct.DIM_CRM_ACCOUNT_ID
and current_acct.snapshot_date::date = current_date)
),

/*
We only want AE activities/tasks that occur during the resolution of the case, so after the creation date
and before the closed date. We also only want tasks carried out by the same AE that owns the case.
*/

task_join as (
select 
case_summary.case_id as task_case_id,
task_data.*
from case_summary
left join task_data
on case_summary.account_id = task_data.dim_crm_account_id
and case_summary.owner_id = task_data.dim_crm_user_id
and task_data.task_created_date::DATE >= case_summary.created_date::DATE
and (task_data.task_created_date::DATE < case_summary.closed_date::DATE
or case_summary.closed_date is null)
),

/*
To reduce dimensionality, we don't join every task to the corresponding case. Instead we create a summary view
here adding up inbound customer emails and completed customer calls. For this purpose, these are the only tasks we care about.
*/

task_summary as (
select
task_case_id,
count(distinct case when inbound_outbound_flag = 'Inbound' then task_id else null end) as inbound_email_count,
count(distinct case when outbound_answered_flag then task_id else null end) as completed_call_count,
count(distinct case when inbound_outbound_flag = 'Outbound' then task_id else null end) as outbound_email_count,
case when inbound_email_count > 0 then true else false end as inbound_flag,
case when completed_call_count > 0 then true else false end as completed_call_flag,
case when outbound_email_count > 0 then true else false end as outbound_flag
from task_join
group by 1
)


select case_summary.*,
task_summary.inbound_email_count,
task_summary.completed_call_count,
task_summary.inbound_flag,
task_summary.completed_call_flag,
task_summary.outbound_email_count,
task_summary.outbound_flag
from case_summary
left join task_summary on case_summary.case_id = task_summary.task_case_id
