/*
This query is intended to link the outcomes of renewal opportunities (Uplift, Flat, Contraction, Churn) with
usage level prior to the renewal. Usage level is the difference between billable users and license users, and can be 
either flat (difference = 0), overage (billable > licensed), or underutilized (billable < licensed).

The complexity in this query comes with linking the usage to the correct renewal opportunity using the logic present
in the DIM_SUBSCRIPTION table, which does not have 100% coverage.
*/


/*
First we gather usage information at the account-subscription-month level, using Subscription Name 
so that we don't have to worry about getting the exact DIM_SUBSCRIPTION_ID.

We also need the next month's subscription info, corresponding to the subscription AFTER successful renewal.
Given that the last MART_ARR entry for a given subscription is the month ahead
of its renewal date, we go 2 months forward, trying to line up the same account-product combinations,
and also allowing for product changes/upgrades. 
*/

with 
usage as
(
select 
dim_crm_account_id,
arr_month,
subscription_name,
sum(overage_count) as net_license_usage,
sum(underutilization_amount) as net_license_amount,
sum(license_user_count) as license_user_count,
sum(billable_user_count) as billable_user_count,
min(price) as price,
sum(quantity) as quantity,
sum(next_sub_quantity) as next_sub_quantity,
sum(next_sub_arr) as next_sub_arr,
min(next_sub_price) as next_sub_price
 from

(SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month 
  ,  monthly_mart.term_end_date
     --     , ping_created_at 
     --     ,max(ping_created_at) over(partition by monthly_mart.SUBSCRIPTION_NAME, monthly_mart.snapshot_month) as latest_ping
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
        , MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
         , mart_arr.DIM_SUBSCRIPTION_ID
--         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.quantity
         , mart_arr.ARR / mart_arr.QUANTITY as price
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
            
         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
        , (mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as underutilization_amount,
        next_month.arr as next_sub_arr,
        next_month.quantity as next_sub_quantity,
        case when next_sub_quantity > 0 then next_sub_arr / next_sub_quantity else null end as next_sub_price
   --     , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID) as latest_underutilization_month
    FROM 
 (
   select * from
 RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
   where   PRODUCT_TIER_NAME not like '%Storage%')
   mart_arr
   left join 
   (
   select * from
 RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
   where   PRODUCT_TIER_NAME not like '%Storage%') next_month
   on mart_arr.dim_crm_account_id = next_month.dim_crm_account_id 
and (mart_arr.product_tier_name = next_month.product_tier_name
or (mart_arr.product_tier_name like '%Premium%' and next_month.product_tier_name like '%Ultimate%')
or (mart_arr.product_tier_name like '%Premium%' and next_month.product_tier_name like '%Premium%')
or (mart_arr.product_tier_name like '%Ultimate%' and next_month.product_tier_name like '%Ultimate%'))
   and mart_arr.arr_month = dateadd('month',-2,next_month.arr_month)
  left join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID

             LEFT JOIN 
  (
    select
    snapshot_month,
    dim_crm_account_id,
   SUBSCRIPTION_NAME,
    term_end_date,
    max(BILLABLE_USER_COUNT) as billable_user_count,
    max(LICENSE_USER_COUNT) as license_user_count

    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 

/*
Many entries in the monthly metrics table are missing key values, so we limit to just PRODUCTION instances
where there are recording licensed users.
*/

  where INSTANCE_TYPE = 'Production'
      -- and BILLABLE_USER_COUNT > 0
       and LICENSE_USER_COUNT > 0
 --   and subscription_status = 'Active'
group by 1,2,3,4
  
  ) monthly_mart
  ON mart_arr.dim_crm_account_id = monthly_mart.dim_crm_account_id and mart_arr.subscription_name = monthly_mart.subscription_name
    WHERE --ARR_MONTH >= DATE_TRUNC('month',date('2022-01-01'))
-- and 
mart_arr.arr_month = snapshot_month or snapshot_month is null

    -- and LICENSE_USER_COUNT > BILLABLE_USER_COUNT
  
--  and qsr_enabled_flag
order by mart_arr.dim_subscription_id, mart_arr.arr_month desc)
-- where arr_month = dateadd('month',-1,date_trunc('month',term_end_date))
--where ping_created_at = latest_ping
group by 1,2,3
),

/*
We layer the DIM_SUBSCRIPTION table info with usage data from the above Usage query.

1. We join to the DIM_SUBSCRIPTION table again to get auto renewal information for the prior subscription (as that was the autorenew status that matters).

2. We join to the Usage data above either looking at the final month prior to Churn (DIM_CRM_OPPORTUNITY_ID_CLOSED_LOST_RENEWAL is not null),
or the final month prior to the Renewal (CLOSED_LOST_RENEWAL is null).

3. We join to the Usage data again, but only where the TERM_END_DATE > current date, to capture current usage for future renewals.
*/

subscription_with_usage as (
select sub.*,
max(sub.subscription_version) over(partition by sub.subscription_name) as latest_subscription_version,
prior.turn_on_auto_renewal as prior_auto_renewal,
usage.net_license_usage,
usage.net_license_amount,
usage.license_user_count,
usage.billable_user_count,
usage.price,
usage.quantity,
usage.next_sub_quantity,
usage.next_sub_arr,
usage.next_sub_price,
open_usage.net_license_usage as net_license_usage_current,
open_usage.net_license_amount as net_license_amount_current,
open_usage.license_user_count as license_user_count_current,
open_usage.billable_user_count as billiable_user_count_current,
open_usage.price as price_current
from common.dim_subscription sub
left join common.dim_subscription prior
on sub.dim_subscription_id_previous = prior.dim_subscription_id
left join usage
on (
 (usage.arr_month = date_trunc('month',dateadd('month',-1,date_trunc('month',sub.term_end_date))) and sub.dim_crm_opportunity_id_closed_lost_renewal is not null)
or (usage.arr_month = date_trunc('month',dateadd('month',-1,date_trunc('month',sub.term_start_date))) and sub.dim_crm_opportunity_id_closed_lost_renewal is null)
)
and usage.subscription_name = sub.subscription_name
left join 
(select * from
( select *,
         max(arr_month) over(partition by SUBSCRIPTION_NAME) as latest_month
from
usage)
where arr_month = latest_month)
 open_usage
on open_usage.subscription_name = sub.subscription_name
and sub.term_end_date > current_date
),

/*
Here we leverage the DIM_ORDER/ACTION info captured from Zuora to definitely identify which DIM_SUBSCRIPTION_ID was actually renewed.

We can identify AutoRenews using the ORDER_DESCRIPTION, and combine this info later with IS_WEB_PORTAL_PURCHASE
on the Opportunity to flag customer manual portal renews.
*/

renewal_actions as
(select dim_order_action.DIM_SUBSCRIPTION_ID,
  dim_subscription.subscription_name,
  dim_order_action.contract_effective_date,
    case when ORDER_DESCRIPTION <> 'AutoRenew by CustomersDot' then true else false end as manual_portal_renew_flag
from common.dim_order_action
    left join common.dim_order on dim_order_action.dim_order_id = dim_order.dim_order_id
    left join common.dim_subscription on dim_order_action.DIM_SUBSCRIPTION_ID = dim_subscription.DIM_SUBSCRIPTION_ID
where order_action_type = 'RenewSubscription'
 -- and ORDER_ACTION_CREATED_DATE > '2023-01-31'
 ),

 /*
Standard Opportunity baseline query with all the account fields and calculated tier logic removed
 */

oppty_base as (SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area in ('LOWTOUCHPOOL', 'EAST','WEST') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag


FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND 
stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)
),

/*
1st output: Won Renewals

We inner join to the Subscriptions info above on the main DIM_CRM_OPPORTUNITY_ID, because that ID corresponds to the 
won renewal that renews the Subscription. We also inner join to the Renewal Actions table above so that we 
get the autorenewal flag info.
*/

won_renewals as (
select
'Won Renewals' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
oppty_base.is_closed,
oppty_base.is_won,
oppty_base.stage_name,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
renewal_actions.manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 or net_license_usage is null then 'Flat'
else null end as usage_flag,
case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
else 'Sales-Assisted Renew' end as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 or net_license_usage_current is null then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id = oppty_base.dim_crm_opportunity_id
inner join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
),

/*
2nd output: Churns

We join to the DIM_CRM_OPPORTUNITY_ID_CLOSED_LOST_RENEWAL with only the Churns in the Opportunity table.
*/

churns as
(
select
'Churns' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
oppty_base.is_closed,
oppty_base.is_won,
oppty_base.stage_name,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
null as manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 or net_license_usage is null then 'Flat'
else null end as usage_flag,
-- case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
-- when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
-- else 'Sales-Assisted Renew' end 
null as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 or net_license_usage_current is null then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id_closed_lost_renewal = oppty_base.dim_crm_opportunity_id
--left join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
where
is_latest_version_flag
and oppty_base.trx_type is not null
and oppty_base.product_category like any ('%Bronze%','%Silver%','%Gold%','%Starter%','%Premium%','%Ultimate%')
and oppty_base.close_date < current_date
and oppty_base.is_closed
),

/*
3rd output: Open Renewals

We join to the DIM_CRM_OPPORTUNITY_ID_CURRENT_OPEN_RENEWAL to get the future dated renewal opportunity that will renew the subscription.
*/

open_renewals as (
select
'Open Renewals' as type_flag,
subscription_with_usage.*,
oppty_base.sales_type,
oppty_base.trx_type,
oppty_base.is_web_portal_purchase,
oppty_base.net_arr,
oppty_base.arr_basis,
oppty_base.order_type,
oppty_base.close_date,
oppty_base.close_month,
oppty_base.is_closed,
oppty_base.is_won,
oppty_base.stage_name,
fiscal_year,
fiscal_quarter_name_fy,
fiscal_month_name_fy,
null as manual_portal_renew_flag,
case when net_license_usage < 0 then 'Underutilization'
when net_license_usage > 0 then 'Overage'
when net_license_usage = 0 or net_license_usage is null then 'Flat'
else null end as usage_flag,
-- case when renewal_actions.manual_portal_renew_flag and oppty_base.is_web_portal_purchase then 'Manual Portal Renew'
-- when renewal_actions.manual_portal_renew_flag = false and oppty_base.is_web_portal_purchase then 'Autorenew'
-- else 'Sales-Assisted Renew' end 
null as actual_manual_renew_flag,
case when subscription_with_usage.subscription_version = latest_subscription_version then true else false end as is_latest_version_flag,
case when net_license_usage_current < 0 then 'Underutilization'
when net_license_usage_current > 0 then 'Overage'
when net_license_usage_current = 0 or net_license_usage_current is null then 'Flat'
else null end as usage_flag_current
from
subscription_with_usage
inner join oppty_base on subscription_with_usage.dim_crm_opportunity_id_current_open_renewal = oppty_base.dim_crm_opportunity_id
--left join renewal_actions on subscription_with_usage.dim_subscription_id = renewal_actions.dim_subscription_id
where
is_latest_version_flag
and oppty_base.trx_type is not null
and oppty_base.close_date > current_date
and oppty_base.is_closed = False
)


select * from open_renewals
union all
select * from churns
union all
select * from won_renewals
