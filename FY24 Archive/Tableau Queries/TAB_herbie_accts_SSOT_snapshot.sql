   select    
    distinct
   DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_crm_account_id
    from
              PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
left join
(
select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR_SNAPSHOT_BOTTOM_UP
where arr_month = '2023-01-01'
and snapshot_date = '2023-01-31'
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_parent_crm_account_id = 
DIM_CRM_ACCOUNT_DAILY_SNAPSHOT.dim_parent_crm_account_id

   where      
(PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE <= 100 or PARENT_CRM_ACCOUNT_MAX_FAMILY_EMPLOYEE is null)
and 
CARR_ACCOUNT_FAMILY > 0
and 
CARR_ACCOUNT_FAMILY <= 30000
--and parent_crm_account_business_unit in ('ENTG','COMM')
and PARENT_CRM_ACCOUNT_SALES_SEGMENT in ('SMB','Mid-Market','Large','MM','LRG','MID-MARKET','LARGE')
and ultimate.dim_parent_crm_account_id is null
and snapshot_date = '2023-02-01'
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
