with

acct_list as
(
select distinct dim_crm_account_id
from
"PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT"
where (crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%', 
                                  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
  or (crm_account_owner = 'Rasheed Power' and snapshot_date < '2023-02-01')
  or (user_role_type = 'KEY' and carr_this_account < 4000 and snapshot_date > '2023-07-31')
  or (crm_account_owner = 'Julia Hill-Wright' and carr_this_account < 4000 and snapshot_date > '2023-07-31')
  or (user_role_type = 'EXP' and carr_this_account < 2000 and snapshot_date > '2023-07-31')
)
 and snapshot_date >= '2022-02-01'
 and crm_account_type <> 'Prospect'
),

acct_flag as
(
  SELECT 
dim_crm_account_id, 
--snapshot_date,
crm_account_owner,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' 
     when user_role_type = 'KEY' and carr_this_account < 4000 then 'SMB Expand'
    when crm_account_owner = 'Julia Hill-Wright' and carr_this_account < 4000 then 'SMB Expand'
    when user_role_type = 'EXP' and carr_this_account < 2000 then 'SMB Expand'
    else 'False' end as Pooled_SMB_Expand_Flag,
-- CASE 
--     when user_role_type = 'KEY' and carr_this_account < 4000 then 'SMB Expand'
--     when crm_account_owner = 'Julia Hill-Wright' and carr_this_account < 4000 then 'SMB Expand'
--     when user_role_type = 'EXP' and carr_this_account < 2000 then 'SMB Expand'
--     else 'False' end as SMB_Expand_Flag, 
CASE 
    when Pooled_SMB_Expand_Flag = 'Pooled' and account_tier IS NULL then 'Rank 3'
    else account_tier end as account_tier_fy23h1
FROM  "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
where snapshot_date = '2023-07-31'
and cARR_this_account > 0 
and CRM_ACCOUNT_OWNER_GEO = 'AMER'
and CRM_ACCOUNT_OWNER_SALES_SEGMENT = 'SMB'
and PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM'
),

acct_base as 

(SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,      
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT,
 
2964 as carr_tier_1,
2400 as carr_tier_2,
1916 as carr_tier_3,
134985000 as funding_tier_1,
44909650 as funding_tier_2,
20624700 as funding_tier_3,
103 as dev_count_tier_1,
44 as dev_count_tier_2,
29 as dev_count_tier_3,
CASE
WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
CASE
WHEN
(((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
CASE
WHEN
(((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 , 
case when account_tier in ('Rank 1','Rank 1.5') and carr_this_account > 0 then 1
when account_tier_notes not like '%Ops%' and snapshot_date >= '2023-03-01' then 
        case when account_tier = 'Rank 2' then 2
         when account_tier = 'Rank 3' then 3
         else 1 end
else
 case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end 
 end as account_tier_calculated 
 
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
INNER JOIN acct_list on acct.dim_crm_account_id = acct_list.dim_crm_account_id
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
WHERE snapshot_date >= '2022-02-01'
              )
              
 SELECT acct_base.*,
 case when acct_base.snapshot_date >= '2023-08-01' then acct_flag.Pooled_SMB_Expand_Flag else null end as Pooled_SMB_Expand_Flag,
case when acct_base.snapshot_date >= '2023-08-01' then acct_flag.account_tier_fy23h1 else null end as account_tier_fy23h1
 FROM acct_base
 left join acct_flag on acct_base.dim_crm_account_id = acct_flag.dim_crm_account_id

 
