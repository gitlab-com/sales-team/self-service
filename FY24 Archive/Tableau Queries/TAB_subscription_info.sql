select 
sub.*,
first_value(bill.default_payment_method_type)
over(partition by bill.dim_crm_account_id order by term_start_date desc)
from common.dim_subscription sub
left join 
(SELECT distinct
dim_crm_account_id, 
default_payment_method_type
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_ARR"
where arr_month =date_trunc('month',current_date)
) bill
on bill.dim_crm_account_id = sub.dim_crm_account_id
where sub.subscription_status = 'Active'
and sub.term_end_date > current_date
