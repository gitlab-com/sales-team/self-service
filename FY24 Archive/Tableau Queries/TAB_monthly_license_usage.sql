select 
dim_crm_account_id,
arr_month,
subscription_name,
sum(overage_count) as net_license_usage,
sum(underutilization_amount) as net_license_amount,
sum(license_user_count) as license_user_count,
sum(billable_user_count) as billable_user_count,
min(price) as price
from

(SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month 
  ,  monthly_mart.term_end_date
          , ping_created_at 
          ,max(ping_created_at) over(partition by monthly_mart.SUBSCRIPTION_NAME, monthly_mart.snapshot_month) as latest_ping
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
        , MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
         , mart_arr.DIM_SUBSCRIPTION_ID
--         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
         , monthly_mart.subscription_start_date
         , monthly_mart.subscription_end_date

         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
        , (mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as underutilization_amount
        , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID) as latest_underutilization_month
    FROM 
 (
   select * from
 RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
   where   PRODUCT_TIER_NAME not like '%Storage%')
   mart_arr
  left join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID

             LEFT JOIN 
  (
    select
    DISTINCT
    snapshot_month,
    ping_created_at,
    DIM_SUBSCRIPTION_ID,
    dim_crm_account_id,
    INSTANCE_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    SUBSCRIPTION_NAME,
    subscription_start_date,
    subscription_end_date,
    term_end_date
    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
       and BILLABLE_USER_COUNT > 0
       and LICENSE_USER_COUNT > 0
 --   and subscription_status = 'Active'

  
  ) monthly_mart
  ON mart_arr.dim_crm_account_id = monthly_mart.dim_crm_account_id and mart_arr.subscription_name = monthly_mart.subscription_name
    WHERE ARR_MONTH >= DATE_TRUNC('month',date('2022-01-01'))
 and arr_month = snapshot_month

    -- and LICENSE_USER_COUNT > BILLABLE_USER_COUNT
  
--  and qsr_enabled_flag
order by dim_subscription_id, arr_month desc)
-- where arr_month = dateadd('month',-1,date_trunc('month',term_end_date))
where ping_created_at = latest_ping
group by 1,2,3
