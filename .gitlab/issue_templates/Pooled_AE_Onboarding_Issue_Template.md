*Please add a title using the format: Pooled AE Onboarding Checklist: Pooled AE Name*

# Pooled AE Onboarding Checklist

**Pooled AE Name**:

**Pooled AE Manager**:

**Start Date**: *MM/DD/YYYY*

# Checklist

**Live Enablement Sessions** (To be checked off by Self Service Ops)
- [ ] Pooled Model Overview.  *Completed on MM/DD/YYYY*
- [ ] Working Tier 1 Accounts. *Completed on MM/DD/YYYY*
- [ ] Working Tier 2/3 Accounts (via Cases). *Completed on MM/DD/YYYY*
- [ ] Working Tier 2/3 Accounts (via Cases) refresher.  To be completed 30 days after initial session. *Completed on MM/DD/YYYY*

**Enablement Content** (To be checked off by Pooled AE)
- [ ] Pooled Model [Handbook Page.](https://about.gitlab.com/handbook/sales/commercial/pooled_model/)  *Viewed on MM/DD/YYYY*
- [ ] Pooled Model [Enablement Deck.](https://docs.google.com/presentation/d/1ZKQcwKQ-CVYwzB3hTZxdGwZCmbG6pd7bxisV9W3PBw4/edit#slide=id.g12b319f6181_0_5) *Viewed on MM/DD/YYYY*
- [ ] Overview of [Renewals](https://internal-handbook.gitlab.io/handbook/sales/go-to-market/renewals/) at GitLab. *Viewed on MM/DD/YYYY*
- [ ] [How QSRs work.](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html) *Viewed on MM/DD/YYYY*
- [ ] End of Availability Sales [Overview](https://docs.google.com/presentation/d/16BaoPs1X8DsHCb-CGuQkkK9yIBTxXnrV_QiFHf_GQdM/edit#slide=id.g87a9962b0d_0_20) & [Approval Matrix.](https://docs.google.com/document/u/0/d/19T-ysFuEFWIlAv7Z9o1Q6-kVU9HKFb_nzGZ1uumpMLA/edit)
- [ ] Matterhorn (FY24 Price Increase) [FAQ.](https://docs.google.com/document/d/1foob9uq03qL_d4xrUQOI-WcI7PjfjzaFonqfWLy7VtI/edit)
- [ ] Chorus [customer calls](https://chorus.ai/playlists/753189). *Completed on MM/DD/YYYY*


**SFDC Reports/ Dashboards** (To be checked off by Pooled AE)
- [ ] Pooled Sales Team Case [Dash.](https://gitlab.my.salesforce.com/01Z8X000001Dk59) *Viewed on MM/DD/YYYY*
- [ ] Pooled Team [Dash.](https://gitlab.my.salesforce.com/01Z8X00000169Cq) *Viewed on MM/DD/YYYY*
- [ ] Pooled Clean Your Room [Dash.](https://gitlab.my.salesforce.com/01Z8X00000169Er) *Viewed on MM/DD/YYYY*
- [ ] SMB East FY24 [Dash.](https://gitlab.my.salesforce.com/01Z8X0000016AQy) *Viewed on MM/DD/YYYY*
- [ ] SMB AMER R7 [Dash.](https://gitlab.my.salesforce.com/01Z4M000000oXCS) *Viewed on MM/DD/YYYY*
- [ ] Pooled Team Churn [Dash.](https://gitlab.my.salesforce.com/01Z8X000000u6fO) *Viewed on MM/DD/YYYY*
- [ ] Tier 1 [Renewals - Next 60 Days.](https://gitlab.my.salesforce.com/00O8X000008glBb) *Viewed on MM/DD/YYYY*

**SFDC Object/ Report/ Dashboard Access**  (To be checked off by Self Service Ops)
- [ ] Pooled Sales Case Type access. *Completed on MM/DD/YYYY*
- [ ] Case queue access. *Completed on MM/DD/YYYY*
- [ ] Low Touch Sales Team folder access. *Completed on MM/DD/YYYY*
- [ ] Low Touch Sales Team Dashboards folder access. *Completed on MM/DD/YYYY*

**Outreach Sequences & Templates**  (To be checked off by Pooled AE)
- [ ] Renewals - [Low Touch Template](https://app1a.outreach.io/templates/239365)
- [ ] Renewals - [High Touch](https://app1a.outreach.io/templates/325978) (Requesting a meeting)
- [ ] Renewals - [EoA](https://app1a.outreach.io/templates/269196) (former Bronze/Starter customers)


**Slack channels to be added to**  (To be checked off by Self Service Ops)
- [ ] self-service_all_team.  *Added on MM/DD/YYYY*
- [ ] self-service_sales_team.  *Added on MM/DD/YYYY*
- [ ] self-service_pooled-team.  *Added on MM/DD/YYYY*
- [ ] self_service_ae_feedback_loop.  *Added on MM/DD/YYYY*


/assign @mikesmith1

/label ~"Self-Service Ops"

/confidential
