<!-- Please make sure all sections are filled out

-->
# Advocate PTO Coverage Template
This issue is created by Advocates whenever they have PTO plans for 5+ days. When someone takes PTO, they should be fully on PTO and enjoy their time :)


## :handshake: Coverage Tasks


### Advocate Tasks

* [ ] Assign this issue to yourself
* [ ] Title the issue `Advocate PTO Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
* [ ] Find and confirm a coverage buddy who will help while you’re on PTO and add `Buddy Name` here
* [ ] Make sure to clear the [Run Your Day Dash](https://gitlab.lightning.force.com/lightning/r/Dashboard/01ZPL000000wGpB2AU/view?queryScope=userFolders)
* [ ] [Next Steps](https://gitlab.lightning.force.com/lightning/r/Report/00OPL00000Ak9Yn2AJ/view): Review all of your cases with a `Next Step Date` during your PTO (enter your return date in the `6. Next Steps Date` filter) and take necessary action, by either pushing it to when you’re back if appropriate, or assigning it to your coverage buddy. Make sure that the `Next Step` and `Next Step Date` are clear for them to be aware of the needed action
* [ ] [In Progress Cases](https://gitlab.lightning.force.com/lightning/r/Report/00OPL000004FAEU2A4/view): Review all of your in progress cases. For any cases that will require action during your PTO, assign them to your coverage buddy. Make sure that the `Next Step` and `Next Step Date` are clear for them to be aware of the needed action
* [ ] [HVAs](https://gitlab.lightning.force.com/lightning/r/Report/00OPL00000Ak9qY2AR/view): Review your HVA opps closing in the next 60 days, and ensure you have a plan in place for all of them. If coverage is needed, inform your coverage buddy
* [ ] Review the [standard PTO guidelines](https://handbook.gitlab.com/handbook/people-group/paid-time-off/#communicating-your-time-off)
* [ ] Ensure your PTO is logged correctly in `WorkDay`
* [ ] Add on OOO message with your OOO dates in `SFDC`
* [ ] Update your OOO auto-reply message with the message below
* [ ] Assign this issue to yourself, your coverage buddy and your manager
* [ ] Set the due date of the case as your return date
* [ ] Share your coverage plan and communicate who will be your coverage buddy in your region’s team Slack channel


### Manager Tasks

* [ ] Review this coverage plan and sign-off once complete
* [ ] Add this PTO period to the [Advocate Planned PTO Gsheet](https://docs.google.com/spreadsheets/d/1iT9vZNwWLdZgP97ma9PA1ZKJ9yz-3NYhsbR8eqRxzTw/edit?gid=0#gid=0) and tag Max and Ed in a comment

## :e-mail: OOO Email Message

**Subject**
Out Of Office - please reach out to my team

**Body**

Dear sender,

I am currently out of office. I will be returning on the [202X-XX-XX], and will have limited access to my emails until then. 

In case your message relates to an ongoing case, please contact my colleague [buddy] on [buddy email].

For timely support on all other inquiries, please reach out to our team on smallbusiness@gitlab.com 

Wishing you a great day,

[Signature]

## 🔙 Returning Tasks

### Returning Advocate

* [ ] Connect with your buddy and ensure that any open or closed cases are reassigned to you

## :gear: Template Automation

/label ~"SMB advocate"

/confidential
