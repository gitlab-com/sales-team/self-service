**FY26**

[SMB Clean Up Dash](https://gitlab.lightning.force.com/lightning/r/Dashboard/01ZPL000000jrUn2AI/view?queryScope=userFolders)

| Use Case | SFDC Report | Issue (If applicable) | Status | Notes |
|----------|-------------|-----------------------|--------|-------|
| QSRs \>90 days old still open | [Link](https://gitlab.lightning.force.com/lightning/r/Report/00O8X0000096FIVUA2/view) |  |   | Close using this [template](https://docs.google.com/spreadsheets/d/1VnqEe_zk5Hm4jOlpE8i-T2wWzZhROqO7fFAkFXl5HYI/edit?gid=326851959#gid=326851959) |
| SMB Customers without FY26 Renewal Ops | [Link](https://gitlab.lightning.force.com/lightning/r/Report/00OPL00000Dcn0E2AR/view) |  |  | Open Sales Ops case, check legal riskrate status |
| Open cases, non Q or Advocate owned | [Link](https://gitlab.lightning.force.com/one/one.app#/sObject/00OPL000001Lo9J2AS/view) |  |  | Chatter the case creator/ last owner advising that the case should go to the queue |
| SMB Advocate Owned, Closed Opps | [Link](https://gitlab.lightning.force.com/one/one.app#/sObject/00OPL000001NjBx2AK/view) |  |  | Ask Lindsay to move the opps to EMEA/ AMER/ APAC SMB Sales, so that it mirrors the account owner |
| Open Cases under Non SMB Accounts | [Link](https://gitlab.lightning.force.com/one/one.app#/sObject/00OPL000002CZP32AO/view) |  |  | Close |
| Web Direct Renewals with 0 nARR | [Link](https://gitlab.lightning.force.com/lightning/r/Report/00OPL00000AUM3m2AH/view?queryScope=userFolders) |  |  | Open Sales Ops  for opportunities > 1k amount |
| Web Direct on HVA that need Case Credit | [Link](https://gitlab.lightning.force.com/lightning/r/Report/00OPL00000DSGOy2AP/view?queryScope=userFolders) |  |  | Create case and link opp |
