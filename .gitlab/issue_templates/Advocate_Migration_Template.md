<!-- Please make sure all sections are filled out

-->
# Advocate Migration Template
This issue needs to be created by the manager once a new rep has an end date in role.

**Advocate Name** - 

**Advocate ID (SFDC)** - 

**Departure Date** - 

**Team** -

**Action Items - ASM** 

- [ ] 15 days prior to departure date: Create this issue
- [ ] 15 days prior to departure date: Confirm who the HV Account cases should be transferred to, if there are not to be spread evenly amongst the exisiting team. **Advocate Name =** 

**Action Items - DNA** 

- [ ] 15 days prior to departure date: Remove advocate from [Tier 1 Ongoing Carve Document](https://docs.google.com/spreadsheets/d/1of7VI6jkpcF48tQLnBOkP-EyTuwysr4qTfnZAOoSp3Y/edit?gid=1887155802#gid=1887155802)
- [ ] 15 days prior to departure date: Move advocate's High Value Account cases to new owners (EMEA - split evenly among region, AMER - split evenly among other reps)
- [ ] 15 days prior to departure date: Ensure that all High Value Account Check In cases are updated to the new account owner
- [ ] 15 days prior to departure date: Move any open or in progress cases back to the queue (set status as Open), marked as High priority, and append "Transferred from X on mm/dd" to the case context

**List View Templates;**

- [Advocate Migration Template - HV](https://gitlab.lightning.force.com/lightning/o/Case/list?filterName=Advocate_Migration_Template)
- [Advocate Migration Template - Non HV](https://gitlab.lightning.force.com/lightning/o/Case/list?filterName=Advocate_Migration_Template_Non_HV)

**Action Items - Ops** 

SMB Advocate Tech [Stack](https://docs.google.com/spreadsheets/d/14YrGHDiMvLpeMAwi6p_G7uC7XpMsCqaJ1KhvUvUumbc/edit?gid=0#gid=0)

- [ ] On departure date: Update [roster](https://docs.google.com/spreadsheets/d/1aVr39PJlxK4YA5LzOB8HGC2ISwFM_IZd1Igqu-wgdbg/edit?gid=469415970#gid=469415970) 
- [ ] If promoted/transferred, exclude Advocate from the amer-smb-team@gitlab.com (google group: gds-a@gitlab.com) or emea-smb-team@gitlab.com (google group: gds-e@gitlab.com) alias
- [ ] On departure date: Update [OOO auto-responder](https://docs.google.com/document/d/15PMBolccCy33ARb-CsjbF5z2Z-okQA0TOLpocn5EFhk/edit) to direct to smallbusiness@gitlab.com (instead of the manager's email) by contacting IT directly via HelpLab to ask them to change it. (Not applicable if the Advocate is moving to a new role at GitLab)
- [ ] 15 days prior to departure date: Remove advocate from Calendly
- [ ] 15 days prior to departure date: Remove advocate from Stack Overflow

## Completed by
**DNA** - 

**Ops** -

**Link to resources / snippet** -

List View Templates;

- [Advocate Migration Template - HV](https://gitlab.lightning.force.com/lightning/o/Case/list?filterName=Advocate_Migration_Template)
- [Advocate Migration Template - Non HV](https://gitlab.lightning.force.com/lightning/o/Case/list?filterName=Advocate_Migration_Template_Non_HV)

/assign @sglad
/assign @eclu94
/assign @mhanks
/assign @mfleisher
/label ~"Self-Service Data"
/label ~"Self-Service Data Ad Hoc"
/label ~"Self-Service Data Intake"
/label ~"GDS Ops"
/confidential
