<!-- Please make sure all sections are filled out

-->
# Advocate Onboarding Template
This issue needs to be created by the hiring manager once a new rep has signed an offer and has a starting date.

**Advocate Name** - 

**Advocate ID (SFDC)** - 

**Starting Date** - 

**Team** -

**Action Items - DNA** 

- [ ] Add advocate to [Tier 1 Ongoing Carve Document](https://docs.google.com/spreadsheets/d/1of7VI6jkpcF48tQLnBOkP-EyTuwysr4qTfnZAOoSp3Y/edit?gid=1887155802#gid=1887155802) (confirm carving with team manager)
- [ ] Assign advocate Tier 1 accounts (TBC exactly when, first month or so should be onboarding)

**Action Items - Ops** 

SMB Advocate Tech [Stack](https://docs.google.com/spreadsheets/d/14YrGHDiMvLpeMAwi6p_G7uC7XpMsCqaJ1KhvUvUumbc/edit?gid=0#gid=0)

- [ ] Update [roster](https://docs.google.com/spreadsheets/d/1aVr39PJlxK4YA5LzOB8HGC2ISwFM_IZd1Igqu-wgdbg/edit?gid=469415970#gid=469415970) 
- [ ] Include Advocate in the amer-smb-team@gitlab.com or emea-smb-team@gitlab.com alias
- [ ] Add Advocate to [SMB Advocate Dash - Manager View](https://gitlab.lightning.force.com/lightning/r/Dashboard/01ZPL000000cNgN2AU/view?queryScope=userFolders)
- [ ] Create access issue and ensure all access, permissions, etc. are updated (sample [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/30837))
- [ ] Add advocate to Calendly [Invite to Calendly, Add to AMER/EMEA Team, Add to Shared Event]
- [ ] Add advocate to StackOverflow
- [ ] Add advocate to AirCover (ask Ali Shahrazad)
- [ ] Add advocate to these Slack Channels
    - #global_digital_smb-sdr 
    - #global-digital-smb-amer OR #global-digital-smb-emea
    - #global-digital-smb-feedback
    - #global-digital-smb-public
    - #global-digital-smb-team
- [ ] Two weeks after joining, add AMER/EMEA/APJ advocate to the Traction round robin (weighted to receive 1/2 of the case volume as the existing advocates)
    - [ ] Four weeks after joining, update the AMER/EMEA/APJ advocates Traction round robin weight to normal volume
    - [ ] Assign the AMER/EMEA/APJ advocate their 200 HVA Review Cases

**Action Items - Manager** 
- [ ] Add an annual recurring calendar invite with the advocate's GitLab start date to remember to give them a GitLab-iversary shoutout
- [ ] Add advocate to [shared team GDrive Folder](https://drive.google.com/drive/folders/1c6oncfcoelK77RS65YfF92iF-bdy-aRn)
- [ ] Add advocate to [team management sheet](https://docs.google.com/spreadsheets/d/1jENZhXCYujB6QlfVomXcU2pBO8FmrflUALu-7ppweHM/edit?gid=2121347534#gid=2121347534)
- [ ] On start date: post in #team-member-updates on the day of about the promotion/move
- [ ] Add to monthy all-hands

**Action Items - Dir** 
- [ ] On start date: post in #global-digital-smb_team and welcome the new rep(s)

## Completed by
**DNA** - 

**Ops** -

**Link to resources / snippet** -

/assign @sglad
/assign @eclu94
/assign @mhanks
/assign @mfleisher
/assign @taylorlund for AMER/APAC or @mnunes1 for EMEA
/assign @kkutob
/label ~"Self-Service Data"
/label ~"GDS Ops"
/label ~"Self-Service Data Ad Hoc"
/label ~"Self-Service Data Intake"
/confidential
