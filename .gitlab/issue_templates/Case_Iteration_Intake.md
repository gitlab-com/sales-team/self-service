<!-- Please make sure all sections are filled out

-->
# New Case / Case Iteration Intake Issue

**New Case Type or Iteration on Existing Case Type?**

**Source of Idea** - 

**DRI** - 

**NEW CASE TYPE** - 

**Target Release Date (should be a Tuesday)** - 

**Frequency of Case Creation:**
Ex: Is this a one-off case type or will this be included in the regular case creation data pull?

### Document any X-Functional/Legal/Marketing Dependencies:

### Relevant Links and Resources:


## CTA: 

The below steps apply new case type. Cases will result in the advocate sending outreach directly to the customer.

| Stage | Description |
|-------|-------------|
| GOAL |  |
| SIGNAL |  |
| TIMING |  |
| ACTION |  |
| OUTCOME |  |

### Data and Findings: 

### Trigger Criteria - DRI: @mfleisher OR other team member for campaigns

## Case Development Process and Approvals - DRI: @sglad

- [x] Create issue in [Case Iteration Epic](https://gitlab.com/groups/gitlab-com/sales-team/-/epics/120)
- [ ] Exploratory data analysis
- [ ] Feedback from GDS ASMs
- [ ] Input from GDS Leadership as needed (Enablement, Product/XFN, Discounting etc.)
- [ ] Include in Case Lab session (if applicable)
- [ ] 1-week advocate feedback cycle (proposal shared, feedback received, decision made)
- [ ] @kkutob signoff
- [ ] QA by other members of the DNA team
- [ ] Build case output in Combined Case Creation Tableau workbook/pump query, Traction, or one off data pull 

## Success metric and Monitoring plan - DRI: @mfleisher

- [ ] SFDC Report [[if linked to opportunity](https://gitlab.lightning.force.com/lightning/r/Report/00OPL000006hbCA2AY/view?queryScope=userFolders)] [[if based on adoption or utilization](https://gitlab.lightning.force.com/lightning/r/Report/00OPL000006hdtt2AA/view?queryScope=userFolders)]
- [ ] Tableau exploratory outcome dashboard view
- [ ] Scheduled 45-60 day reportback on case outcomes

## Enablement and Feedback plan - DRI: @sglad

- [ ] Messaging, sequences and revenue play development by select advocates
- [ ] [Handbook update](https://handbook.gitlab.com/handbook/sales/commercial/global_digital_smb/)
- [ ] [Case triggers sheet update](https://docs.google.com/spreadsheets/d/1ihpt5WDpoJmDWa5gA0eXvBJOgFOJgL1J_QxsBuDDowc/edit?gid=1223186811#gid=1223186811)
- [ ] [Case enablement slides](https://docs.google.com/presentation/d/15oJJ4DnNTxADddY3G1pc6VimgbSAoBZLLGSWbglp8VY/edit#slide=id.g2f4d9e3d8fa_0_37)
- [ ] Scheduled introductory Slack post (day before release)
- [ ] Scheduled feedback Slack post (14 days post-release)





/assign @mfleisher
/assign @slgad
/label ~"Self-Service Data"
/label ~"Self-Service Data Ad Hoc"
/label ~"Self-Service Data Intake"
/parent_epic &120
/confidential
